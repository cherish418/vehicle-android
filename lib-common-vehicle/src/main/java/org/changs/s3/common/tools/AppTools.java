package org.changs.s3.common.tools;

import android.content.Context;
import android.content.Intent;
import android.widget.Toast;

import org.changs.aplug.utils.AppUtils;
import org.changs.aplug.utils.IntentUtils;

/**
 * Created by yincs on 2017/10/12.
 */

public class AppTools {
    public static final String PKG_NAV = "org.changs.nav";


    public static void launcher(Context context, String pkg) {
        boolean installApp = AppUtils.isInstallApp(context, pkg);
        if (!installApp) {
            Toast.makeText(context, "未安装此应用,请先安装", Toast.LENGTH_SHORT).show();
            return;
        }
        Intent launchAppIntent = IntentUtils.getLaunchAppIntent(context, pkg);
        launchAppIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_RESET_TASK_IF_NEEDED);
        context.startActivity(launchAppIntent);
    }








}
