package org.changs.s3.common.upload;

import org.json.JSONObject;

/**
 * Created by yincs on 2017/9/20.
 */

public interface UploadManager {


    void upload(JSONObject msg, String filePath, OnUploadListener listener);


    interface OnUploadListener {

    }
}
