package org.changs.s3.common.aidl;

/**
 * Created by yincs on 2017/9/20.
 */

public interface IUpload {
    boolean upload(String msg, String filePath, UploadCallback callback) ;
}
