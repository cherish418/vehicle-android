package org.changs.s3.common.tools;

import android.content.Context;
import android.content.Intent;
import android.os.Environment;

import java.io.File;

/**
 * Created by yincs on 2017/9/14.
 */

public final class NavTools {
    public static final String ACTION_S3_NAV = "org.changs.nav.receive.nav";
    public static final String EXTRA_TYPE = "extra_type";
    public static final String EXTRA_NAME = "extra_name";
    public static final String EXTRA_LAT = "extra_lat";
    public static final String EXTRA_LNG = "extra_lng";

    public static final int TYPE_GO_POI = 0;
    public static final int TYPE_GO_HOME = 1;
    public static final int TYPE_GO_COMPANY = 2;
    public static final int TYPE_NAV_OPEN = 10;
    public static final int TYPE_NAV_CLOSE = 12;

    public interface Pref {
        public static final String URI_PATH = "content://org.changs.nav.provider.SpContentProvider";
        public static final String KEY_CONGESTION = "KEY_CONGESTION";//是否躲避拥堵(false)
        public static final String KEY_AVOID_SPEED = "KEY_AVOID_SPEED";//不走高速(false)
        public static final String KEY_AVOID_COST = "KEY_AVOID_COST";//避免收费(false)
        public static final String KEY_HIGHT_SPEED = "KEY_HIGHT_SPEED";//高速优先(false)
        public static final String KEY_CAR_NUMBER = "KEY_CAR_NUMBER";//车牌号(粤aaxxo2)
        public static final String KEY_RESTRICTIO = "KEY_RESTRICTIO";//避免限行(false)
        public static final String KEY_VEHICLE_HEIGHT = "KEY_VEHICLE_HEIGHT";//车辆高度（eg:1.2米）
        public static final String KEY_VEHICLE_LOAD = "KEY_VEHICLE_LOAD";//车辆载重（eg:1.5吨）
        public static final String KEY_VEHICLE_HEIGHT_SWITCH = "KEY_VEHICLE_HEIGHT_SWITCH";//设置车辆的高度是否参与算路
        public static final String KEY_VEHICLE_LOAD_SWITCH = "KEY_VEHICLE_LOAD_SWITCH";//设置车辆的载重是否参与算路
        public static final String KEY_CAR_SHAPE = "KEY_CAR_SHAPE";//车形状(正方形)
        public static final String KEY_CAR_LENGTH = "KEY_CAR_LENGTH";//车长度(1.5米)
    }

    //adb shell am broadcast -a "org.changs.nav.receive.nav" --ei extra_type 0 --es extra_name "北京" --ef extra_lat 113.924929 --ef extra_lon 22.553405
    public static void navToLoc(Context context, String name, double lat, double lng) {
        Intent intent = new Intent(ACTION_S3_NAV);
        intent.putExtra(EXTRA_TYPE, TYPE_GO_POI);
        intent.putExtra(EXTRA_NAME, name);
        intent.putExtra(EXTRA_LAT, lat);
        intent.putExtra(EXTRA_LNG, lng);
        intent.addFlags(Intent.FLAG_INCLUDE_STOPPED_PACKAGES);
        context.sendBroadcast(intent);
    }

    public static void navHome(Context context) {
        Intent intent = new Intent(ACTION_S3_NAV);
        intent.putExtra(EXTRA_TYPE, TYPE_GO_HOME);
        intent.addFlags(Intent.FLAG_INCLUDE_STOPPED_PACKAGES);
        context.sendBroadcast(intent);
    }

    public static void navCompany(Context context) {
        Intent intent = new Intent(ACTION_S3_NAV);
        intent.putExtra(EXTRA_TYPE, TYPE_GO_COMPANY);
        intent.addFlags(Intent.FLAG_INCLUDE_STOPPED_PACKAGES);
        context.sendBroadcast(intent);
    }

    public static void enterNav(Context context) {
        Intent intent = new Intent(ACTION_S3_NAV);
        intent.putExtra(EXTRA_TYPE, TYPE_NAV_OPEN);
        intent.addFlags(Intent.FLAG_INCLUDE_STOPPED_PACKAGES);
        context.sendBroadcast(intent);
    }

    public static void exitNav(Context context) {
        Intent intent = new Intent(ACTION_S3_NAV);
        intent.putExtra(EXTRA_TYPE, TYPE_NAV_CLOSE);
        intent.addFlags(Intent.FLAG_INCLUDE_STOPPED_PACKAGES);
        context.sendBroadcast(intent);
    }

    public static File getMapRootDir(Context context) {
        return new File(Environment.getExternalStorageDirectory(), "vehicle-map");
    }
}
