package org.changs.s3.common.aidl;

/**
 * Created by yincs on 2017/9/20.
 */

public interface IDeviceScan {
    String getRemoteIp(boolean show);

    boolean isScanning();

    boolean startScan();

    void registerCallback(DeviceScanCallback callback);

    void unregisterCallback(DeviceScanCallback callback);
}
