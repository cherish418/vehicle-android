package org.changs.s3.common;

import android.app.Activity;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;

import org.changs.aplug.ActivityManager;
import org.changs.aplug.utils.AppUtils;

import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by 惠安 on 2017/12/6.
 */


public class SoundCmdReceiver extends BroadcastReceiver {

    public static final String ACTION = "org.changs.s3.common.sound";

    private static final String EXTRA_CMD = "extra_cmd";
    private static final String EXTRA_DATA = "extra_data";
    private static final String EXTRA_APP = "extra_app";

    public static final String CMD_BACK_PRESS = "CMD_BACK_PRESS";
    public static final String CMD_BACK2HOME = "CMD_BACK2HOME";
    public static final String CMD_EXIT = "CMD_EXIT";


    public static final String CMD_MONITOR_MAGNIFY_ALL = "CMD_MONITOR_MAGNIFY_ALL";
    public static final String CMD_MONITOR_MAGNIFY_FRONT = "CMD_MONITOR_MAGNIFY_FRONT";
    public static final String CMD_MONITOR_MAGNIFY_BEHIND = "CMD_MONITOR_MAGNIFY_BEHIND";
    public static final String CMD_MONITOR_MAGNIFY_LEFT = "CMD_MONITOR_MAGNIFY_LEFT";
    public static final String CMD_MONITOR_MAGNIFY_RIGHT = "CMD_MONITOR_MAGNIFY_RIGHT";


    private static final Map<String, List<String>> sDefaultCommand = new HashMap<>();

    static {
        sDefaultCommand.put(CMD_BACK_PRESS, Arrays.asList("返回", "上一页"));
        sDefaultCommand.put(CMD_BACK2HOME, Arrays.asList("回到桌面", "退出"));
        sDefaultCommand.put(CMD_EXIT, Arrays.asList("关闭"));

        sDefaultCommand.put(CMD_MONITOR_MAGNIFY_ALL, Arrays.asList("打开全景监控", "全景"));
        sDefaultCommand.put(CMD_MONITOR_MAGNIFY_FRONT, Arrays.asList("放大前方画面", "前方"));
        sDefaultCommand.put(CMD_MONITOR_MAGNIFY_BEHIND, Arrays.asList("放大后方画面", "后方"));
        sDefaultCommand.put(CMD_MONITOR_MAGNIFY_LEFT, Arrays.asList("放大左方画面", "左方"));
        sDefaultCommand.put(CMD_MONITOR_MAGNIFY_RIGHT, Arrays.asList("放大右方画面", "右方"));
    }

    public static Map<String, List<String>> getDefaultCommand() {
        return sDefaultCommand;
    }


    //adb shell am broadcast -a "org.changs.s3.common.sound" --es extra_data "CMD_BACK_PRESS"
    //adb shell am broadcast -a "org.changs.s3.common.sound" --es extra_data "CMD_BACK2HOME"
    public static void sendBroadcast(Context context, String cmd, String data, String app) {
        Intent intent = new Intent(ACTION);
        intent.putExtra(EXTRA_CMD, cmd);
        intent.putExtra(EXTRA_DATA, data);
        intent.putExtra(EXTRA_APP, app);
        context.sendBroadcast(intent);
    }

    public static void sendBroadcast(Context context, String cmd, String data) {
        Intent intent = new Intent(ACTION);
        intent.putExtra(EXTRA_CMD, cmd);
        intent.putExtra(EXTRA_DATA, data);
        context.sendBroadcast(intent);
    }


    public static onReceiveCmdListener sOnReceiveCmdListener;

    @Override
    public void onReceive(Context context, Intent intent) {
        String action = intent.getAction();
        if (!ACTION.equals(action)) return;
        String app = intent.getStringExtra(EXTRA_APP);

        Activity currentActivity = ActivityManager.getInstance().getCurrentActivity();
        if (currentActivity != null) return;
        boolean needHandler = (app != null && context.getPackageName().equals(app))
                || (app == null && AppUtils.isAppForeground(context));
        if (!needHandler) {
            return;
        }
        String data = intent.getStringExtra(EXTRA_DATA);
        if (CMD_BACK_PRESS.equals(data)) {
            if (!currentActivity.isFinishing()) currentActivity.onBackPressed();
        } else if (CMD_BACK2HOME.equals(data)) {
            Intent homeIntent = new Intent();
            homeIntent.setAction(Intent.ACTION_MAIN);
            homeIntent.addCategory(Intent.CATEGORY_HOME);
            currentActivity.startActivity(homeIntent);
        } else if (CMD_EXIT.equals(data)) {
            ActivityManager.getInstance().finishAllActivity();
        } else if (sOnReceiveCmdListener != null) {
            sOnReceiveCmdListener.onReceiveCmd(data);
        }
    }

    public static void setOnReceiveCmdListener(onReceiveCmdListener onReceiveCmdListener) {
        SoundCmdReceiver.sOnReceiveCmdListener = onReceiveCmdListener;
    }

    public static interface onReceiveCmdListener {
        void onReceiveCmd(String data);
    }

}
