package org.changs.s3.common.aidl;

import org.changs.s3.common.aidl.DeviceScanCallback;

import org.changs.s3.common.aidl.UploadCallback;
/**
 * Created by yincs on 2017/9/20.
 * @link IDeviceScan
 * @link IUpload
 */
interface RemoteService {

    String getRemoteIp(boolean show);

    boolean isScanning();

    boolean startScan();

    void registerCallback(DeviceScanCallback callback);

    void unregisterCallback(DeviceScanCallback callback);

    boolean upload(String msg, String filePath, UploadCallback callback);
}
