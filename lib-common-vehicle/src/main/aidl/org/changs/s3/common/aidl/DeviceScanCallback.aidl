package org.changs.s3.common.aidl;

/**
 * Created by yincs on 2017/8/29.
 */

interface DeviceScanCallback {
    void startScan();

    void completeScan();

    void disconnect();
}
