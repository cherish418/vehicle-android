package org.changs.common.network.model;

public class CarVersion {
    /**  **/
    private String carVersionId;

    /**  **/
    private String carVersionCode;

    /**  **/
    private String carVersionName;

    /**  **/
    private String carVersionDes;


    private String url;


    public String getCarVersionId() {
        return carVersionId;
    }

    public void setCarVersionId(String carVersionId) {
        this.carVersionId = carVersionId;
    }

    public String getCarVersionCode() {
        return carVersionCode;
    }

    public void setCarVersionCode(String carVersionCode) {
        this.carVersionCode = carVersionCode;
    }

    public String getCarVersionName() {
        return carVersionName;
    }

    public void setCarVersionName(String carVersionName) {
        this.carVersionName = carVersionName;
    }

    public String getCarVersionDes() {
        return carVersionDes;
    }

    public void setCarVersionDes(String carVersionDes) {
        this.carVersionDes = carVersionDes;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

}