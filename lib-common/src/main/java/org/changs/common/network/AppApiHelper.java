package org.changs.common.network;

import com.google.gson.reflect.TypeToken;
import com.rx2androidnetworking.Rx2AndroidNetworking;

import org.changs.aplug.utils.JSONObject;
import org.changs.common.network.model.ApiRes;
import org.changs.common.network.model.CarVersion;

import java.io.File;

import javax.inject.Inject;
import javax.inject.Singleton;

import io.reactivex.Observable;

/**
 * Created by yincs on 2017/7/22.
 */

@Singleton
public class AppApiHelper implements ApiHelper {

    //    private final String url = "http://apigateway.xundaobao.com:80";


    @Inject
    public AppApiHelper() {
    }

    @Override
    public Observable<ApiRes> monitorRegister(String deviceId) {
        JSONObject params = new JSONObject();
        params.put("serviceName", "monitor.register");
        params.put("deviceId", deviceId);
        params.put("deviceType", "ANDROID");
        params.put("key", key);
        return postJson(params, new TypeToken<ApiRes>() {
        });
    }

    @Override
    public Observable<ApiRes> monitorConfigCarInfo(String deviceId, String carCode, int carHeight, int carWeight, int carLength, String brand, String carType) {
        JSONObject params = new JSONObject();
        params.put("serviceName", "monitor.configCarInfo");
        params.put("deviceId", deviceId);
        params.put("carCode", carCode);
        params.put("carHeight", carHeight);
        params.put("carWeight ", carWeight);
        params.put("carLength", carLength);
        params.put("brand", brand);
        params.put("carType", carType);
        params.put("key", key);
        return postJson(params, new TypeToken<ApiRes>() {
        });
    }

    @Override
    public Observable<ApiRes> monitorReportRoute(String deviceId, String srcSite, String targetSite, int distance, int speed, int duration) {
        JSONObject params = new JSONObject();
        params.put("serviceName", "monitor.reportRoute");
        params.put("deviceId", deviceId);
        params.put("srcSite", srcSite);
        params.put("targetSite", targetSite);
        params.put("distance ", distance);
        params.put("speed", speed);
        params.put("duration", duration);
        params.put("key", key);
        return postJson(params, new TypeToken<ApiRes>() {
        });
    }

    @Override
    public Observable<ApiRes> monitorReportSensorData(String deviceId, String sensorType, String sensorValue) {
        JSONObject params = new JSONObject();
        params.put("serviceName", "monitor.reportSensorData");
        params.put("deviceId", deviceId);
        params.put("sensorType", sensorType);
        params.put("sensorValue", sensorValue);
        params.put("key", key);
        return postJson(params, new TypeToken<ApiRes>() {
        });
    }

    @Override
    public Observable<ApiRes> monitorReportSensorData(String userId, File file) {
        return Rx2AndroidNetworking.post(null)
                .addUrlEncodeFormBodyParameter("serviceName", "monitor.uploadImage")
                .addUrlEncodeFormBodyParameter("userId", userId)
                .addFileBody(file)
                .build()
                .getParseObservable(new TypeToken<ApiRes>() {
                });


    }

    @Override
    public Observable<ApiRes<CarVersion>> checkCarUpgrade() {
        return postJson(URL_CHECK_CAR_UPGRADE, null,
                new TypeToken<ApiRes<CarVersion>>() {});
    }


    private <T> Observable<T> postJson(JSONObject params, TypeToken<T> resTypeToken) {
        return Rx2AndroidNetworking.post(null)
                .addJSONObjectBody(params)
                .build()
                .getParseObservable(resTypeToken);
    }

    private <D> Observable<ApiRes<D>> postJson(String url, JSONObject params,
                                               TypeToken<ApiRes<D>> resTypeToken) {
        return Rx2AndroidNetworking.post(url)
                .addJSONObjectBody(params)
                .build()
                .getParseObservable(resTypeToken);
    }
}
