package org.changs.common.network;

import org.changs.common.network.model.ApiRes;
import org.changs.common.network.model.CarVersion;

import java.io.File;

import io.reactivex.Observable;

/**
 * Created by yincs on 2017/7/22.
 */

public interface ApiHelper {
    public static final String BASE_URL = "http://120.77.155.12:18080/vehicle-api";
    public static final String key = "2fb067713ec96a01dc395afff5c95cc9dc7cc591fc02186fd3c106fa7459f606";
    /**
     * 检查车机升级
     */
    public static final String URL_CHECK_CAR_UPGRADE = BASE_URL + "/carVersion/checkUpgrade";

    /**
     * 设备向后台系统注册
     */
    Observable<ApiRes> monitorRegister(String deviceId);


    /**
     * 车辆基本配置
     */
    Observable<ApiRes> monitorConfigCarInfo(String deviceId,
                                            String carCode,
                                            int carHeight,
                                            int carWeight,
                                            int carLength,
                                            String brand,
                                            String carType);

    /**
     * 行车导航数据上报
     */
    Observable<ApiRes> monitorReportRoute(String deviceId,
                                          String srcSite,
                                          String targetSite,
                                          int distance,
                                          int speed,
                                          int duration);


    /**
     * 7.2.4车辆传感数据上报
     *
     * @param sensorType 传感类型：TEMPERATURE(温度)/POSITION（位置）/PRESSURE（气压）/DUST（灰尘）/SPEED（速度）/GRAVITY（重力）/刹车/熄火/加速度/倾斜
     */
    Observable<ApiRes> monitorReportSensorData(String deviceId,
                                               String sensorType,
                                               String sensorValue);

    /**
     * 7.2.5监控图片上报
     */
    Observable<ApiRes> monitorReportSensorData(String userId,
                                               File file);


    /**
     * 检查车机是否可版本
     */
    Observable<ApiRes<CarVersion>> checkCarUpgrade();
}
