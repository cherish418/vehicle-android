package org.changs.servlet;


public interface ReceiveFileCallback {

    void onStart();

    void onProgress(int currentPercent);

    void onSuccess();

    void onComplete();

    void onError(int i, String msg);
}