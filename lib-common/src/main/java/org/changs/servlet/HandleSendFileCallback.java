package org.changs.servlet;

/**
 * Created by yincs on 2017/8/28.
 */

public interface HandleSendFileCallback {
    void onStart();

    void onProgress(int progressPercent);

    void onSuccess();

    void onComplete();

    HandleSendFileCallback SIMPLE = new HandleSendFileCallback() {
        @Override
        public void onStart() {

        }

        @Override
        public void onProgress(int progressPercent) {

        }

        @Override
        public void onSuccess() {

        }

        @Override
        public void onComplete() {

        }
    };
}
