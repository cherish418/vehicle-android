package org.changs.servlet;

public interface HandleReceiveFileCallback {

    void onStart();

    void onProgress(int progressPercent);

    void onSuccess();

    void onComplete();

    HandleReceiveFileCallback SIMPLE = new HandleReceiveFileCallback() {
        @Override
        public void onStart() {

        }

        @Override
        public void onProgress(int progressPercent) {

        }

        @Override
        public void onSuccess() {

        }

        @Override
        public void onComplete() {

        }
    };
}