package org.changs.servlet;

import android.support.annotation.NonNull;

import java.io.File;

public class HandleReceiveFile {
    public final File file;
    public final boolean reject;
    public final String rejectMsg;

    public boolean fileAppend = false;
    public boolean isInterrupted;

    private HandleReceiveFileCallback callback = HandleReceiveFileCallback.SIMPLE;

    private HandleReceiveFile(File file, boolean reject, String rejectMsg) {
        this.file = file;
        this.reject = reject;
        this.rejectMsg = rejectMsg;
    }

    public static HandleReceiveFile accept(File file) {
        return new HandleReceiveFile(file, false, null);
    }

    public static HandleReceiveFile reject(String rejectMsg) {
        return new HandleReceiveFile(null, true, rejectMsg);
    }

    public HandleReceiveFile fileAppend(boolean fileAppend) {
        this.fileAppend = fileAppend;
        return this;
    }

    public HandleReceiveFile isInterrupted(boolean isInterrupted) {
        this.isInterrupted = isInterrupted;
        return this;
    }

    public HandleReceiveFile setCallback(HandleReceiveFileCallback callback) {
        this.callback = callback;
        return this;
    }

    @NonNull
    public HandleReceiveFileCallback getCallback() {
        return callback;
    }


}