package org.changs.servlet;

import com.google.gson.JsonObject;

public interface SenderMsgCallback {
        void onSuccess(JsonObject jsonObject);

        void onError(int code, String msg);
    }