package org.changs.servlet;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.PrintWriter;
import java.util.concurrent.TimeoutException;

/**
 * Created by yincs on 2017/8/28.
 */

public class IOUtils {
    public static final String END_SEQUENCE = "\n();;";
    public static final int NO_TIMEOUT = -1;
    public static String RECEIVE_FILE_TYPE = "------------------------RECEIVE_FILE_TYPE-------------------------------";
    public static String SEND_FILE_TYPE = "------------------------SEND_FILE_TYPE-------------------------------";
    public static String OK = "------------------------OK-------------------------------";
    public static final int TYPE_FILE_NAV = 1000;
    public static final String EXTRA_FILE_NAME = "extra_file_name";
    public static final String EXTRA_FILE_RELATIVE_PATH = "extra_file_relative_path";
    public static final String EXTRA_FILE_SIZE = "extra_file_size";
    public static final String EXTRA_FILE_TYPE = "extra_file_type";
    public static final String EXTRA_FILE_REMOTE_PATH = "extra_file_remote_path";
    public static final String EXTRA_DATA = "extra_data";
    public static final String EXTRA_REMOTE_IP = "extra_remote_ip";
    public static final String EXTRA_ACTION = "extra_action";
    public static final String EXTRA_IP_SENDER = "extra_ip_sender";


    public static class Action {
        public static final String SYM_MAP_DATA = "org.changs.servlet.action.SYM_MAP_DATA";
        public static final String CONNECT = "org.changs.servlet.action.CONNECT";
    }

    public static class Event{
        /**
         * 同步地图完成
         */
        public static final String SYNC_MAP_DONE = "event.SYNC_MAP_DONE";
    }

    public static PrintWriter getStreamWriter(OutputStream outputStream) {
        return new PrintWriter(new BufferedOutputStream(outputStream));
    }

    public static String readStreamMessage(InputStream inputStream, int timeout) throws IOException, TimeoutException {
        return readStreamMessage(readStream(inputStream, timeout));
    }

    public static String readStreamMessage(ByteArrayOutputStream outputStream) {
        String message = outputStream.toString();
        return message.substring(0, message.length() - END_SEQUENCE.length());
    }

    public static ByteArrayOutputStream readStream(InputStream inputStreamIns, int timeout) throws IOException, TimeoutException {
        BufferedInputStream inputStream = new BufferedInputStream(inputStreamIns);
        ByteArrayOutputStream inputStreamResult = new ByteArrayOutputStream();

        byte[] buffer = new byte[8096];
        int len;
        long calculatedTimeout = timeout != NO_TIMEOUT ? System.currentTimeMillis() + timeout : NO_TIMEOUT;

        do {
            if ((len = inputStream.read(buffer)) > 0)
                inputStreamResult.write(buffer, 0, len);

            if (calculatedTimeout != NO_TIMEOUT && System.currentTimeMillis() > calculatedTimeout)
                throw new TimeoutException("Read timed out!");
        }
        while (!inputStreamResult.toString().endsWith(END_SEQUENCE));

        return inputStreamResult;
    }

}
