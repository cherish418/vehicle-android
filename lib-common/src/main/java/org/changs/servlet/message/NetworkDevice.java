package org.changs.servlet.message;

import android.support.annotation.Keep;

/**
 * Created by yincs on 2017/9/6.
 */

@Keep
public class NetworkDevice {
    private String ip;

    public NetworkDevice(String ip) {
        this.ip = ip;
    }

    public NetworkDevice() {
    }

    public String getIp() {
        return ip;
    }

    public void setIp(String ip) {
        this.ip = ip;
    }

    @Override
    public String toString() {
        return "NetworkDevice{" +
                "ip='" + ip + '\'' +
                '}';
    }
}
