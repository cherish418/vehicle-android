package org.changs.servlet.message;

import android.support.annotation.Keep;

/**
 * Created by yincs on 2017/9/4.
 */

/**
 * 地图的配置文件
 */
@Keep
public class VMapMessage {

    /**
     * file : {"title":"北京市","code":"110000","url":"http://offlinedata.alicdn.com/mobilemap/mapdataae8v4/20170814/beijing.zip","fileName":"/storage/emulated/0/app-phone-map/data/VMAP2/beijing.zip.tmp","lLocalLength":0,"lRemoteLength":34920547,"mState":4,"version":"20170814","vMapFileNames":"beijing.dat;","isSheng":1,"mCompleteCode":100,"mCityCode":"010","pinyin":"beijing"}
     */

    private FileBean file;

    private long fileSize;

    public long getFileSize() {
        return fileSize;
    }

    public void setFileSize(long fileSize) {
        this.fileSize = fileSize;
    }

    public FileBean getFile() {
        return file;
    }

    public void setFile(FileBean file) {
        this.file = file;
    }

    public static class FileBean {
        /**
         * title : 北京市
         * code : 110000
         * url : http://offlinedata.alicdn.com/mobilemap/mapdataae8v4/20170814/beijing.zip
         * fileName : /storage/emulated/0/app-phone-map/data/VMAP2/beijing.zip.tmp
         * lLocalLength : 0
         * lRemoteLength : 34920547
         * mState : 4
         * version : 20170814
         * vMapFileNames : beijing.dat;
         * isSheng : 1
         * mCompleteCode : 100
         * mCityCode : 010
         * pinyin : beijing
         */

        private String title;
        private String code;
        private String url;
        private String fileName;
        private int lLocalLength;
        private int lRemoteLength;
        private int mState;
        private String version;
        private String vMapFileNames;
        private int isSheng;
        private int mCompleteCode;
        private String mCityCode;
        private String pinyin;

        public String getTitle() {
            return title;
        }

        public void setTitle(String title) {
            this.title = title;
        }

        public String getCode() {
            return code;
        }

        public void setCode(String code) {
            this.code = code;
        }

        public String getUrl() {
            return url;
        }

        public void setUrl(String url) {
            this.url = url;
        }

        public String getFileName() {
            return fileName;
        }

        public void setFileName(String fileName) {
            this.fileName = fileName;
        }

        public int getLLocalLength() {
            return lLocalLength;
        }

        public void setLLocalLength(int lLocalLength) {
            this.lLocalLength = lLocalLength;
        }

        public int getLRemoteLength() {
            return lRemoteLength;
        }

        public void setLRemoteLength(int lRemoteLength) {
            this.lRemoteLength = lRemoteLength;
        }

        public int getMState() {
            return mState;
        }

        public void setMState(int mState) {
            this.mState = mState;
        }

        public String getVersion() {
            return version;
        }

        public void setVersion(String version) {
            this.version = version;
        }

        public String getVMapFileNames() {
            return vMapFileNames;
        }

        public void setVMapFileNames(String vMapFileNames) {
            this.vMapFileNames = vMapFileNames;
        }

        public int getIsSheng() {
            return isSheng;
        }

        public void setIsSheng(int isSheng) {
            this.isSheng = isSheng;
        }

        public int getMCompleteCode() {
            return mCompleteCode;
        }

        public void setMCompleteCode(int mCompleteCode) {
            this.mCompleteCode = mCompleteCode;
        }

        public String getMCityCode() {
            return mCityCode;
        }

        public void setMCityCode(String mCityCode) {
            this.mCityCode = mCityCode;
        }

        public String getPinyin() {
            return pinyin;
        }

        public void setPinyin(String pinyin) {
            this.pinyin = pinyin;
        }
    }
}
