package org.changs.servlet;

import android.support.annotation.NonNull;

import java.io.File;

/**
 * Created by yincs on 2017/8/28.
 */

public class HandleSendFile {
    public final boolean send;
    public final String msg;
    public final File file;
    public boolean isInterrupted;
    private HandleSendFileCallback callback = HandleSendFileCallback.SIMPLE;

    private HandleSendFile(boolean send, String msg, File file) {
        this.send = send;
        this.msg = msg;
        this.file = file;
    }

    public static HandleSendFile accept(File file) {
        return new HandleSendFile(true, null, file);
    }

    public static HandleSendFile reject(String msg) {
        return new HandleSendFile(false, msg, null);
    }


    public HandleSendFile isInterrupted(boolean isInterrupted) {
        this.isInterrupted = isInterrupted;
        return this;
    }

    public HandleSendFile setCallback(HandleSendFileCallback callback) {
        this.callback = callback;
        return this;
    }

    @NonNull
    public HandleSendFileCallback getCallback() {
        return callback;
    }

}
