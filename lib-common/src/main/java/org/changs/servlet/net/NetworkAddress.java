package org.changs.servlet.net;

import java.util.concurrent.atomic.AtomicInteger;

/**
 * Created by yincs on 2017/9/16.
 */

public class NetworkAddress {
    private final String prefix;
    private final AtomicInteger site = new AtomicInteger(0);

    public NetworkAddress(String prefix) {
        this.prefix = prefix;
    }


    public String getPrefix() {
        return prefix;
    }

    public AtomicInteger getSite() {
        return site;
    }

}
