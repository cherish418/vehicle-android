package org.changs.servlet;

import java.io.File;

/**
 * Created by yincs on 2017/9/2.
 */

public class ReceiveFile {
    private final String remoteFilePath;
    private final long remoteFileSize;
    private final File localFile2Save;

    public ReceiveFile(String remoteFilePath, long remoteFileSize, File localFile2Save) {
        this.remoteFilePath = remoteFilePath;
        this.remoteFileSize = remoteFileSize;
        this.localFile2Save = localFile2Save;
    }

    public String getRemoteFilePath() {
        return remoteFilePath;
    }

    public long getRemoteFileSize() {
        return remoteFileSize;
    }

    public File getLocalFile2Save() {
        return localFile2Save;
    }
}
