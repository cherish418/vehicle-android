package org.changs.servlet;

/**
 * Created by yincs on 2017/8/9.
 */

public final class CommunicationConfig {

    //socket服务器监听的端口
    public static final int PHONE_SERVER_PORT = 11290;
    public static final int S3_SERVER_PORT = 11291;
    public static final int DEFAULT_SOCKET_TIMEOUT = 1129;
    public final static byte[] DEFAULT_BUFFER_SIZE = new byte[1024 * 512];
    public final static String[] DEFAULT_DISABLED_INTERFACES = new String[]{"rmnet"};



}
