package org.changs.servlet;

public interface SenderFileCallback {

    void onNext(int code, String msg);

    void onProgress(int progress);

    void onError(int code, String msg);

    void onSuccess();

    void onComplete();
}