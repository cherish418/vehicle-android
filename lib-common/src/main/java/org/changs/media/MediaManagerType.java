package org.changs.media;

import java.lang.annotation.Documented;

import javax.inject.Qualifier;

/**
 * Created by yincs on 2017/11/3.
 */

@Documented
@Qualifier
public @interface MediaManagerType {
}
