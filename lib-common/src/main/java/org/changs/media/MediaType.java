package org.changs.media;

/**
 * Created by yincs on 2017/9/5.
 */

public enum MediaType {
    VIDEO,
    PICTURE
}
