package org.changs.media;

/**
 * Created by yincs on 2017/9/5.
 */

public enum MediaSite {
    ALL,
    FRONT,
    BEHIND,
    LEFT,
    RIGHT;


    public static MediaSite parse(String fileName) {
        if (fileName.length() == 1) {
            switch (fileName) {
                case "f":
                    return FRONT;
                case "b":
                    return BEHIND;
                case "l":
                    return LEFT;
                case "r":
                    return RIGHT;
            }
        }
        for (MediaSite mediaSite : values()) {
            if (fileName.toLowerCase().contains(mediaSite.name().toLowerCase()))
                return mediaSite;
        }
        return null;
    }
}
