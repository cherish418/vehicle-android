package org.changs.media;

import java.util.List;

/**
 * Created by yincs on 2017/11/3.
 */

public interface IMediaManager {

    List<MediaFile> getMedia();

    List<MediaFile> getMedia(MediaType type, MediaSite site);

    void delete(MediaFile mediaFile);

    boolean upload(String remoteIp, MediaFile mediaFile);

    void reload();

    void lock(MediaFile mediaFile);

    void unLock(MediaFile mediaFile);

    void registerCallback(Callback callback);

    void unregisterCallback(Callback callback);

    interface Callback {

        void delete(MediaFile mediaFile);

        void reload();

        void upload(MediaFile mediaFile);
    }
}
