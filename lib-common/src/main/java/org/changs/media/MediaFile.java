package org.changs.media;

import android.graphics.Bitmap;
import android.media.MediaMetadataRetriever;
import android.os.Parcel;
import android.os.Parcelable;
import android.support.annotation.Nullable;

import org.changs.aplug.utils.ImageUtils;
import org.changs.aplug.utils.TimeUtils;

import java.io.File;
import java.util.Date;

import timber.log.Timber;

/**
 * Created by yincs on 2017/9/5.
 */

public class MediaFile implements Parcelable {
    private String name;
    private String time;
    private Bitmap thumb; //todo 可能会内存溢出
    private long length;
    private String absolutePath;
    private String relativePath;
    private MediaType type;
    private MediaSite site;
    private MediaState state;
    private boolean isLock;


    @Nullable
    static MediaFile parseFile(File file, MediaType type) {
        String name = file.getName();
        if (name.length() < 17) {
            Timber.e("parseVideoFile: 非法文件名 fileName=%s", name);
            return null;
        }
        String time = name.substring(0, 14);
        Date date = TimeUtils.string2Date(time, MediaManager.RECORD_SDF);
        time = MediaManager.USER_SDF.format(date);

        final MediaSite mediaSite = MediaSite.parse(name.substring(15, 16));
        if (mediaSite == null) {
            Timber.e("parseVideoFile: 非法文件名 fileName=%s", name);
            return null;
        }
        String absolutePath = file.getParentFile().getParentFile().getAbsolutePath();
        Timber.d("absolutePath = " + absolutePath);
        String relativePath = file.getAbsolutePath().replace(absolutePath, "");
        Timber.d("relativePath = " + relativePath);
        boolean isLock = name.substring(16, 17).equals("l");

        MediaFile mediaFile = new MediaFile();
        mediaFile.setName(name);
        mediaFile.setTime(time);
        mediaFile.setLength(file.length());
        mediaFile.setAbsolutePath(file.getAbsolutePath());
        mediaFile.setRelativePath(relativePath);
        mediaFile.setType(type);
        mediaFile.setSite(mediaSite);
        mediaFile.setLock(isLock);
        return mediaFile;
    }

    public static boolean isLock(String name) {
        if (name.length() < 17) return false;
        return name.substring(16, 17).equals("l");
    }

    public MediaFile() {
    }

    public boolean isLock() {
        return isLock;
    }

    public void setLock(boolean isLock) {
        this.isLock = isLock;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }

    public long getLength() {
        return length;
    }

    public void setLength(long length) {
        this.length = length;
    }

    public String getAbsolutePath() {
        return absolutePath;
    }

    public void setAbsolutePath(String absolutePath) {
        this.absolutePath = absolutePath;
    }

    public String getRelativePath() {
        return relativePath;
    }

    public void setRelativePath(String relativePath) {
        this.relativePath = relativePath;
    }

    public MediaType getType() {
        return type;
    }

    public void setType(MediaType type) {
        this.type = type;
    }

    public MediaSite getSite() {
        return site;
    }

    public void setSite(MediaSite site) {
        this.site = site;
    }

    public MediaState getState() {
        return state;
    }

    public void setState(MediaState state) {
        this.state = state;
    }

    public Bitmap getThumb() {
        if (thumb == null) {
            try {
                MediaMetadataRetriever retriever = new MediaMetadataRetriever();
                retriever.setDataSource(absolutePath);
                Bitmap bitmap = retriever.getFrameAtTime(2);
                retriever.release();
                thumb = ImageUtils.scale(bitmap, 60, 40, true);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        return thumb;
    }

    public void setThumb(Bitmap thumb) {
        this.thumb = thumb;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.name);
        dest.writeString(this.time);
        dest.writeLong(this.length);
        dest.writeString(this.absolutePath);
        dest.writeString(this.relativePath);
        dest.writeInt(this.type == null ? -1 : this.type.ordinal());
        dest.writeInt(this.site == null ? -1 : this.site.ordinal());
        dest.writeInt(this.state == null ? -1 : this.state.ordinal());
    }

    protected MediaFile(Parcel in) {
        this.name = in.readString();
        this.time = in.readString();
        this.length = in.readLong();
        this.absolutePath = in.readString();
        this.relativePath = in.readString();
        int tmpType = in.readInt();
        this.type = tmpType == -1 ? null : MediaType.values()[tmpType];
        int tmpSite = in.readInt();
        this.site = tmpSite == -1 ? null : MediaSite.values()[tmpSite];
        int tmpState = in.readInt();
        this.state = tmpState == -1 ? null : MediaState.values()[tmpState];
    }

    public static final Creator<MediaFile> CREATOR = new Creator<MediaFile>() {
        @Override
        public MediaFile createFromParcel(Parcel source) {
            return new MediaFile(source);
        }

        @Override
        public MediaFile[] newArray(int size) {
            return new MediaFile[size];
        }
    };

    @Override
    public boolean equals(Object obj) {
        return obj instanceof MediaFile
                && ((MediaFile) obj).getAbsolutePath().equals(getAbsolutePath());
    }
}
