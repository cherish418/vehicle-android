package org.changs.common;

import com.androidnetworking.error.ANError;

import org.changs.common.network.ApiHelper;
import org.changs.common.network.AppApiHelper;
import org.changs.common.network.model.ApiRes;
import org.junit.Test;

import io.reactivex.Observable;
import io.reactivex.annotations.NonNull;
import io.reactivex.functions.Consumer;

import static org.junit.Assert.assertEquals;

/**
 * Example local unit test, which will execute on the development machine (host).
 *
 * @see <a href="http://d.android.com/tools/testing">Testing documentation</a>
 */
public class ExampleUnitTest {

    @Test
    public void addition_isCorrect() throws Exception {
        assertEquals(4, 2 + 2);
    }

    @Test
    public void test1() throws Exception {
        ApiHelper apiHelper = new AppApiHelper();

        Observable<ApiRes> observable = apiHelper.monitorRegister("test-123456");
        observable.subscribe(new Consumer<ApiRes>() {
            @Override
            public void accept(@NonNull ApiRes apiRes) throws Exception {

            }
        }, new Consumer<Throwable>() {
            @Override
            public void accept(@NonNull Throwable throwable) throws Exception {
                if (throwable instanceof ANError) {
                    int errorCode = ((ANError) throwable).getErrorCode();
                    System.out.println("errorCode = " + errorCode);
                }
                System.out.println("throwable = " + throwable);
            }
        });
    }
}