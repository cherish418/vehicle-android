package aplug.di;

import android.content.Context;

import org.changs.media.MediaManager;
import org.changs.media.MediaManagerType;
import org.changs.s3.common.RemoteManager;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;

/**
 * Created by yincs on 2017/8/27.
 */

//存放全局对象
@Singleton
@Module
public abstract class AppModules {

    @Singleton
    @Provides
    static RemoteManager provideRemoteIPManager(Context context) {
        return new RemoteManager(context);
    }

    @MediaManagerType
    @Provides
    @MediaManager.Type
    static int provideMediaManagerType(Context context) {
        return MediaManager.TYPE_VEHICLE;
    }

}
