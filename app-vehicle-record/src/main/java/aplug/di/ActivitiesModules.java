package aplug.di;


import org.changs.aplug.annotation.ActivityScoped;
import org.changs.aplug.annotation.FragmentScoped;
import org.changs.record.home.MainActivity;
import org.changs.record.home.MainModule;
import org.changs.record.picture.PictureDetailAct;
import org.changs.record.picture.PictureFragment;
import org.changs.record.picture.PictureSiteFragment;
import org.changs.record.video.VideoFragment;
import org.changs.record.video.VideoPlayAct;
import org.changs.record.video.VideoSiteFragment;

import dagger.Module;
import dagger.android.ContributesAndroidInjector;

/**
 * Created by yincs on 2017/8/26.
 */

@Module
abstract public class ActivitiesModules {

    @ActivityScoped
    @ContributesAndroidInjector(modules = MainModule.class)
    abstract MainActivity mainActivity();
    @ActivityScoped
    @ContributesAndroidInjector
    abstract VideoPlayAct videoPlayAct();
    @ActivityScoped
    @ContributesAndroidInjector
    abstract PictureDetailAct pictureDetailAct();
    @FragmentScoped
    @ContributesAndroidInjector
    abstract PictureSiteFragment pictureAllFragment();
    @FragmentScoped
    @ContributesAndroidInjector
    abstract VideoSiteFragment videoAllFragment();
    @FragmentScoped
    @ContributesAndroidInjector
    abstract PictureFragment pictureFragment();
    @FragmentScoped
    @ContributesAndroidInjector
    abstract VideoFragment videoFragment();

}
