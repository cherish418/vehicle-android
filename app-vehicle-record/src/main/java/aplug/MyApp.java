package aplug;

import android.content.Context;
import android.support.multidex.MultiDex;

import org.changs.aplug.AplugManager;
import org.changs.record.R;
import org.changs.s3.common.analytics.AnalyticsTracker;

import aplug.di.DaggerAppComponent;
import dagger.android.AndroidInjector;
import dagger.android.support.DaggerApplication;


/**
 * Created by yincs on 2017/7/25.
 */

public class MyApp extends DaggerApplication {

    @Override
    protected void attachBaseContext(Context base) {
        MultiDex.install(this);
        super.attachBaseContext(base);
    }


    @Override
    protected AndroidInjector<? extends DaggerApplication> applicationInjector() {
        AnalyticsTracker analyticsTracker = new AnalyticsTracker(this, R.xml.global_tracker);
        AplugManager.get().onCreate(new AplugManager
                .Config(this)
                .addActivityLifecycleListener(analyticsTracker)
                .addFragmentLifecycleListener(analyticsTracker)
                .debug());

        return DaggerAppComponent.builder().application(this).build();
    }

    @Override
    public void onTerminate() {
        super.onTerminate();
        AplugManager.get().onTerminate();
    }

}