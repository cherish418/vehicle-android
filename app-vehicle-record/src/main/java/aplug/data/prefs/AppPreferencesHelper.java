package aplug.data.prefs;


import android.content.Context;
import android.content.SharedPreferences;

import javax.inject.Inject;
import javax.inject.Singleton;

import aplug.AppConfig;

/**
 * Created by yincs on 2017/7/22.
 */

@Singleton
public class AppPreferencesHelper implements PreferencesHelper {

    private final SharedPreferences prefs;

    @Inject
    public AppPreferencesHelper(Context context) {
        prefs = context.getSharedPreferences(AppConfig.PREFERENCE_FILE_NAME, Context.MODE_PRIVATE);
    }
}
