package aplug.data;


import org.changs.common.network.ApiHelper;

import aplug.data.db.DbHelper;
import aplug.data.prefs.PreferencesHelper;

/**
 * Created by yincs on 2017/7/24.
 */

public interface DataManager {
    DbHelper getDbHelper();

    ApiHelper getApiHelper();

    PreferencesHelper getPreferencesHelper();
}
