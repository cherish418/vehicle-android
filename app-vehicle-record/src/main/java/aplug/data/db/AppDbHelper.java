package aplug.data.db;

import java.util.List;
import java.util.concurrent.Callable;

import javax.inject.Inject;
import javax.inject.Singleton;

import aplug.data.db.model.DaoSession;
import aplug.data.db.model.ExampleRecord;
import aplug.data.db.model.User;
import io.reactivex.Observable;

/**
 * Created by yincs on 2017/7/22.
 */

@Singleton
public class AppDbHelper implements DbHelper {

    private final DaoSession daoSession;

    @Inject
    public AppDbHelper(DbOpenHelper dbOpenHelper) {
        this.daoSession = dbOpenHelper.getDaoSession();
    }

    @Override
    public Observable<Long> insertUser(User user) {
        return null;
    }

    @Override
    public Observable<List<User>> getAllUsers() {
        return null;
    }

    @Override
    public Observable<Long> insertExampleRecord(final ExampleRecord record) {
        return Observable.fromCallable(new Callable<Long>() {
            @Override
            public Long call() throws Exception {
                return daoSession.getExampleRecordDao().insertOrReplace(record);
            }
        });
    }

    @Override
    public Observable<List<ExampleRecord>> getAllExampleRecord() {
        return Observable.fromCallable(new Callable<List<ExampleRecord>>() {
            @Override
            public List<ExampleRecord> call() throws Exception {
                return daoSession.getExampleRecordDao().loadAll();
            }
        });
    }

    @Override
    public Observable<Boolean> clearExampleRecord() {
        return Observable.fromCallable(new Callable<Boolean>() {
            @Override
            public Boolean call() throws Exception {
                daoSession.getExampleRecordDao().deleteAll();
                return true;
            }
        });
    }

}
