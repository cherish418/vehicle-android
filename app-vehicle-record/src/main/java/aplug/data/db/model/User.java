package aplug.data.db.model;

import org.greenrobot.greendao.annotation.Entity;
import org.greenrobot.greendao.annotation.Id;
import org.greenrobot.greendao.annotation.Property;
import org.greenrobot.greendao.annotation.Generated;

/**
 * Created by yincs on 2017/7/22.
 */
@Entity(nameInDb = "user")
public class User {

    @Id
    private String userId;

    @Property(nameInDb = "user_name")
    private String userName;

    @Property(nameInDb = "last_login_time")
    private String lastLoginTime;

    @Property(nameInDb = "updated_at")
    private String updatedAt;

    @Generated(hash = 829579447)
    public User(String userId, String userName, String lastLoginTime,
            String updatedAt) {
        this.userId = userId;
        this.userName = userName;
        this.lastLoginTime = lastLoginTime;
        this.updatedAt = updatedAt;
    }

    @Generated(hash = 586692638)
    public User() {
    }

    public String getUserId() {
        return this.userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getUserName() {
        return this.userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getLastLoginTime() {
        return this.lastLoginTime;
    }

    public void setLastLoginTime(String lastLoginTime) {
        this.lastLoginTime = lastLoginTime;
    }

    public String getUpdatedAt() {
        return this.updatedAt;
    }

    public void setUpdatedAt(String updatedAt) {
        this.updatedAt = updatedAt;
    }
}
