package org.changs.record.picture;


import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.LinearLayoutManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import org.changs.aplug.base.BaseFragment;
import org.changs.media.MediaFile;
import org.changs.media.MediaManager;
import org.changs.media.MediaSite;
import org.changs.media.MediaType;
import org.changs.record.R;
import org.changs.record.databinding.LayoutRccViewBinding;
import org.changs.s3.common.RemoteManager;

import javax.inject.Inject;

import timber.log.Timber;

/**
 * 全部图片
 * Created by Administrator on 2017/8/24.
 */

public class PictureSiteFragment extends BaseFragment implements MediaManager.Callback {
    private static final String TAG_SITE = "tag_site";

    @Inject MediaManager mMediaManager;
    @Inject RemoteManager mRemoteManager;

    private LayoutRccViewBinding binding;
    private PictureAdapter mAdapter;
    private MediaSite mMediaSite;


    public static PictureSiteFragment newInstance(MediaSite mediaSite) {
        Bundle args = new Bundle();
        args.putString(TAG_SITE, mediaSite.toString());
        PictureSiteFragment fragment = new PictureSiteFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public View setup(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        binding = DataBindingUtil.inflate(inflater, R.layout.layout_rcc_view, container, false);
        mMediaManager.registerCallback(this);
        initData(savedInstanceState);
        return binding.getRoot();
    }

    private void initData(Bundle savedInstanceState) {
        if (savedInstanceState != null) {
            String site = savedInstanceState.getString(TAG_SITE);
            if (site != null) mMediaSite = MediaSite.valueOf(site);
        } else {
            final Bundle arguments = getArguments();
            if (arguments != null) {
                mMediaSite = MediaSite.valueOf(arguments.getString(TAG_SITE));
            }
        }
        Timber.d("mediaSite = " + mMediaSite);
        if (mMediaSite == null) return;
        binding.rccView.setLayoutManager(new LinearLayoutManager(getContext()));
        mAdapter = new PictureAdapter(getContext(), mMediaManager.getMedia(MediaType.PICTURE, mMediaSite));
        mAdapter.setOnItemClickListener((view, position) -> {
            PictureDetailAct.start(getContext(), mAdapter.getData().get(position));
        });
        mAdapter.setOnItemClickListener(R.id.tv_phone, (view, position) -> {
            MediaFile mediaFile = mAdapter.getData().get(position);
            String remoteIp = mRemoteManager.getRemoteIp();
            if (remoteIp == null) {
                onError("未检测到手机连接");
                return;
            }
            mMediaManager.upload(remoteIp, mediaFile);
            mAdapter.notifyDataSetChanged();
        });
        binding.rccView.setup()
                .adapter(mAdapter)
                .emptyText("当前没有图片~")
                .dynamic()
                .done();
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        mMediaManager.unregisterCallback(this);
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        if (mMediaSite != null)
            outState.putString(TAG_SITE, mMediaSite.toString());
    }

    @Override
    public void delete(MediaFile mediaFile) {
        if (isNotEffective(mediaFile)) return;
        boolean remove = mAdapter.getData().remove(mediaFile);
        if (remove) mAdapter.notifyDataSetChanged();
    }

    @Override
    public void reload() {
        if (mAdapter == null || mMediaSite == null) return;
        mAdapter.getData().clear();
        mAdapter.getData().addAll(mMediaManager.getMedia(MediaType.PICTURE, mMediaSite));
        mAdapter.notifyDataSetChanged();
    }

    @Override
    public void upload(MediaFile mediaFile) {
        if (isNotEffective(mediaFile)) return;
        mAdapter.notifyDataSetChanged();
    }

    private boolean isNotEffective(MediaFile mediaFile) {
        return mAdapter == null
                || mediaFile == null
                || mediaFile.getType() != MediaType.PICTURE
                || mMediaSite == null
                || (mMediaSite != MediaSite.ALL && mMediaSite != mediaFile.getSite());
    }
}
