package org.changs.record.picture;

import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentPagerAdapter;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import org.changs.aplug.base.BaseFragment;
import org.changs.media.MediaSite;
import org.changs.record.R;
import org.changs.record.databinding.FiveTabsFragmentBinding;

/**
 * 抓拍照片
 * Created by Administrator on 2017/8/24.
 */

public class PictureFragment extends BaseFragment {
    private FiveTabsFragmentBinding binding;

    private static final int[] titles = {R.string.all, R.string.front_up, R.string.back_surround, R.string.left_side, R.string.right_side};

    @Override
    public View setup(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        binding = DataBindingUtil.inflate(inflater, R.layout.five_tabs_fragment, container, false);
        initData();
        return binding.getRoot();
    }

    private void initData() {
        binding.fgVp.setAdapter(new FragmentPagerAdapter(getChildFragmentManager()) {
            @Override
            public Fragment getItem(int position) {
                switch (position) {
                    case 0:
                        return PictureSiteFragment.newInstance(MediaSite.ALL);
                    case 1:
                        return PictureSiteFragment.newInstance(MediaSite.FRONT);
                    case 2:
                        return PictureSiteFragment.newInstance(MediaSite.BEHIND);
                    case 3:
                        return PictureSiteFragment.newInstance(MediaSite.LEFT);
                    default:
                        return PictureSiteFragment.newInstance(MediaSite.RIGHT);
                }
            }

            @Override
            public int getCount() {
                return titles.length;
            }

            @Override
            public CharSequence getPageTitle(int position) {
                return getContext().getString(titles[position]);
            }
        });
        binding.tabLayout.setupWithViewPager(binding.fgVp);
    }
}
