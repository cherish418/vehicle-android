package org.changs.record.picture;


import android.content.Context;
import android.content.Intent;
import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.TextView;

import org.changs.aplug.base.BaseActivity;
import org.changs.media.MediaFile;
import org.changs.media.MediaManager;
import org.changs.record.R;
import org.changs.record.databinding.PictureDetailActBinding;

import javax.inject.Inject;

import aplug.utils.GlideUtils;

/**
 * 图片详情
 * Created by Administrator on 2017/8/24.
 */

public class PictureDetailAct extends BaseActivity implements View.OnClickListener {
    private static final String TAG_MEDIA_FILE = "tag_media_file";

    @Inject
    MediaManager mMediaManager;
    private PictureDetailActBinding binding;
    private boolean isLock, isShow = true;
    private MediaFile mMediaFile;

    public static void start(Context context, MediaFile mediaFile) {
        Intent starter = new Intent(context, PictureDetailAct.class);
        starter.putExtra(TAG_MEDIA_FILE, mediaFile);
        context.startActivity(starter);
    }

    @Override
    public void setup(Bundle savedInstanceState) {
        binding = DataBindingUtil.setContentView(this, R.layout.picture_detail_act);
        mMediaFile = getIntent().getParcelableExtra(TAG_MEDIA_FILE);
        initData(savedInstanceState);
    }

    private void initData(Bundle savedInstanceState) {
        if (savedInstanceState != null) {
            mMediaFile = savedInstanceState.getParcelable(TAG_MEDIA_FILE);
        } else {
            mMediaFile = getIntent().getParcelableExtra(TAG_MEDIA_FILE);
        }
        if (mMediaFile == null) {
            finish();
            return;
        }

        GlideUtils.loadBingImage(getContext(), mMediaFile.getAbsolutePath(), binding.photoView);
        binding.tvFileName.setText(mMediaFile.getName());
        binding.rlHead.setVisibility(View.GONE);
        binding.rlMed.setVisibility(View.GONE);
        binding.rlLock.setVisibility(View.GONE);

        binding.btnDelete.setOnClickListener(this);
        binding.btnLock.setOnClickListener(this);
        binding.btnUnlock.setOnClickListener(this);
        binding.tvBack.setOnClickListener(this);
        binding.photoView.setOnClickListener(v -> {
            if (isShow) {
                isShow = false;
                if (isLock) {
                    binding.rlLock.setVisibility(View.VISIBLE);
                } else {
                    binding.rlHead.setVisibility(View.VISIBLE);
                    binding.rlMed.setVisibility(View.VISIBLE);
                }
            } else {
                isShow = true;
                if (isLock) {
                    binding.rlLock.setVisibility(View.GONE);
                } else {
                    binding.rlHead.setVisibility(View.GONE);
                    binding.rlMed.setVisibility(View.GONE);
                }
            }
        });
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putParcelable(TAG_MEDIA_FILE, mMediaFile);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.tv_back:
                finish();
                break;
            case R.id.btn_delete:
                showDialog();
                break;
            case R.id.btn_unlock://解锁
                isLock = false;
                binding.rlHead.setVisibility(View.VISIBLE);
                binding.rlMed.setVisibility(View.VISIBLE);
                binding.rlLock.setVisibility(View.GONE);
                break;
            case R.id.btn_lock://上锁
                isLock = true;
                binding.rlHead.setVisibility(View.GONE);
                binding.rlMed.setVisibility(View.GONE);
                binding.rlLock.setVisibility(View.VISIBLE);
                break;
            case R.id.btn_cancel:
                dialog.dismiss();
                break;
            case R.id.btn_confirm:
                dialog.dismiss();
                finish();
                mMediaManager.delete(mMediaFile);
                break;
        }
    }

    private AlertDialog dialog;

    private void showDialog() {
        if (dialog == null) {
            View view = LayoutInflater.from(this).inflate(R.layout.dialog_delete, null);
            dialog = new AlertDialog.Builder(this).setView(view).create();
            TextView textView = (TextView) view.findViewById(R.id.tv_content);
            if (mMediaFile.isLock()) {//视频被锁定
                textView.setText("该图片已被您锁定，确定删除吗？");
            } else {
                textView.setText("确定删除该图片吗？");
            }
            view.findViewById(R.id.btn_cancel).setOnClickListener(this);
            view.findViewById(R.id.btn_confirm).setOnClickListener(this);
        }
        dialog.show();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        if (dialog != null)
            dialog = null;
    }
}
