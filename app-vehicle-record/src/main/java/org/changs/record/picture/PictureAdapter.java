package org.changs.record.picture;

import android.content.Context;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;

import org.changs.aplug.widget.irecyclerview.SimpleAdapter;
import org.changs.media.MediaFile;
import org.changs.media.MediaState;
import org.changs.record.R;
import org.changs.record.databinding.ItemListBinding;

import java.util.List;

import aplug.utils.GlideUtils;

/**
 * 图片的适配器
 * Created by Administrator on 2017/8/24.
 */

public class PictureAdapter extends SimpleAdapter<MediaFile, ItemListBinding> {
    private Context context;

    public PictureAdapter(Context context, List<MediaFile> pictures) {
        super(pictures, R.layout.item_list);
        this.context = context;
    }

    @Override
    public void onBindViewHolder(ItemListBinding binding, int position) {
        MediaFile picture = getData().get(position);

        binding.tvPhone.setVisibility(View.GONE);
        binding.vRefresh.setVisibility(View.GONE);
        binding.tvFinish.setVisibility(View.GONE);
        binding.ivVideo.setVisibility(View.GONE);
        binding.tvName.setText(picture.getName());
        binding.tvTime.setText(picture.getTime());

        GlideUtils.loadImage(context, picture.getAbsolutePath(), binding.image);

        binding.tvPhone.setVisibility(View.GONE);
        binding.vRefresh.setVisibility(View.GONE);
        binding.tvFinish.setVisibility(View.GONE);

        if (picture.getState() == MediaState.UPLOADED) {
            binding.tvFinish.setVisibility(View.VISIBLE);
            stopAnim(binding.vRefresh);
        } else if (picture.getState() == MediaState.UPLOADING) {
            binding.vRefresh.setVisibility(View.VISIBLE);
            startAnim(binding.vRefresh);
        } else {
            binding.tvPhone.setVisibility(View.VISIBLE);
            stopAnim(binding.vRefresh);
        }
    }

    private void startAnim(ImageView imageView) {
        if (imageView.getAnimation() != null) {
            Animation animation = imageView.getAnimation();
            if (animation.hasEnded()) animation.start();
        } else {
            Animation animation = AnimationUtils.loadAnimation(context, R.anim.uploading);
            imageView.startAnimation(animation);
        }
    }

    private void stopAnim(ImageView imageView) {
        if (imageView.getAnimation() != null) {
            Animation animation = imageView.getAnimation();
            if (!animation.hasEnded()) animation.cancel();
        }
    }
}
