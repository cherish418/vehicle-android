package org.changs.record.video;

import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentPagerAdapter;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import org.changs.aplug.base.BaseFragment;
import org.changs.media.MediaManager;
import org.changs.media.MediaSite;
import org.changs.record.R;
import org.changs.record.databinding.FiveTabsFragmentBinding;

import javax.inject.Inject;

/**
 * 视频录像
 * Created by Administrator on 2017/8/24.
 */

public class VideoFragment extends BaseFragment {
    private FiveTabsFragmentBinding binding;

    private static final int[] titles = {R.string.all, R.string.front_up,
            R.string.back_surround, R.string.left_side, R.string.right_side};
    @Inject MediaManager mMediaManager;

    @Override
    public View setup(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        binding = DataBindingUtil.inflate(inflater, R.layout.five_tabs_fragment, container, false);

        initData();

        return binding.getRoot();
    }

    @Override
    public void onResume() {
        super.onResume();
        mMediaManager.reload();
    }

    private void initData() {
        binding.fgVp.setAdapter(new FragmentPagerAdapter(getChildFragmentManager()) {
            @Override
            public Fragment getItem(int position) {
                switch (position) {
                    case 0:
                        return VideoSiteFragment.newInstance(MediaSite.ALL);
                    case 1:
                        return VideoSiteFragment.newInstance(MediaSite.FRONT);
                    case 2:
                        return VideoSiteFragment.newInstance(MediaSite.BEHIND);
                    case 3:
                        return VideoSiteFragment.newInstance(MediaSite.LEFT);
                    default:
                        return VideoSiteFragment.newInstance(MediaSite.RIGHT);
                }
            }

            @Override
            public int getCount() {
                return titles.length;
            }

            @Override
            public CharSequence getPageTitle(int position) {
                return getContext().getString(titles[position]);
            }
        });
        binding.tabLayout.setupWithViewPager(binding.fgVp);
    }
}
