package org.changs.record.video;

import android.content.Context;
import android.view.GestureDetector;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.WindowManager;
import android.widget.ImageButton;
import android.widget.TextView;

import org.changs.aplug.interf.Action1;
import org.changs.record.R;

import io.vov.vitamio.widget.MediaController;
import io.vov.vitamio.widget.VideoView;

/**
 * 播放控制器
 * Created by Administrator on 2017/8/25.
 */

public class MyMediaController extends MediaController {

    private GestureDetector mGestureDetector;
    private TextView img_back;
    private TextView textViewTime;
    private VideoView videoView;
    private VideoPlayAct activity;
    private ImageButton mLockView;
    private View mForwardView, mBackView, mDeleteView, mHead, mBottom, btnLock, rlLock;
    private Context context;
    private OnDeleteListener mDeleteListener;
    private boolean isLock;
    private int controllerWidth = 0;//设置mediaController高度为了使横屏时top显示在屏幕顶端
    /**
     * 文件锁定与未锁定监听
     * true 为锁定
     */
    private Action1<Boolean> mLockListener;


    //返回监听
    private View.OnClickListener backListener = new View.OnClickListener() {
        public void onClick(View v) {
            switch (v.getId()) {
                case R.id.mediacontroller_top_back://返回
                    if (activity != null) {
                        activity.finish();
                    }
                    break;
                case R.id.mediacontroller_play_speed://快进
                    if ((videoView.getCurrentPosition() + (10 * 1000) >= videoView.getDuration()))
                        videoView.seekTo(videoView.getDuration());
                    else
                        videoView.seekTo((videoView.getCurrentPosition() + (10 * 1000)));
                    break;
                case R.id.mediacontroller_play_backward://快退
                    if ((videoView.getCurrentPosition() - (10 * 1000)) <= 0)
                        videoView.seekTo(0);
                    else
                        videoView.seekTo((videoView.getCurrentPosition() - (10 * 1000)));
                    break;
                case R.id.mediacontroller_delete://删除
                    mDeleteListener.onDelete();
                    break;
                case R.id.mediacontroller_lock://上锁
                    setLock(true);
//                    mHead.setVisibility(GONE);
//                    mBottom.setVisibility(GONE);
//                    rlLock.setVisibility(VISIBLE);
                    if (mLockListener != null) mLockListener.call(true);
                    break;
                case R.id.btn_lock://解锁
                    setLock(false);
//                    mHead.setVisibility(VISIBLE);
//                    mBottom.setVisibility(VISIBLE);
//                    rlLock.setVisibility(GONE);
                    if (mLockListener != null) mLockListener.call(false);
                    break;

            }
            show(5000);
        }
    };

    //videoview 用于对视频进行控制的等，activity为了退出
    public MyMediaController(Context context, VideoView videoView, VideoPlayAct activity) {
        super(context);
        this.context = context;
        this.videoView = videoView;
        this.activity = activity;
        WindowManager wm = (WindowManager) context
                .getSystemService(Context.WINDOW_SERVICE);
        controllerWidth = wm.getDefaultDisplay().getWidth();
        mGestureDetector = new GestureDetector(context, new MyGestureListener());
    }

    @Override
    protected View makeControllerView() {
        //加入布局文件,mymediacontroller布局名称
        View v = ((LayoutInflater) getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE)).inflate(getResources().getIdentifier("mymediacontroller", "layout", getContext().getPackageName()), this);
        v.setMinimumHeight(controllerWidth);
        img_back = (TextView) v.findViewById(getResources().getIdentifier("mediacontroller_top_back", "id", context.getPackageName()));
        mForwardView = v.findViewById(getResources().getIdentifier("mediacontroller_play_speed", "id", context.getPackageName()));
        mBackView = v.findViewById(getResources().getIdentifier("mediacontroller_play_backward", "id", context.getPackageName()));
        mLockView = (ImageButton) v.findViewById(getResources().getIdentifier("mediacontroller_lock", "id", context.getPackageName()));
        mDeleteView = v.findViewById(getResources().getIdentifier("mediacontroller_delete", "id", context.getPackageName()));
        btnLock = v.findViewById(getResources().getIdentifier("btn_lock", "id", context.getPackageName()));
        mHead = v.findViewById(getResources().getIdentifier("rl_head", "id", context.getPackageName()));
        mBottom = v.findViewById(getResources().getIdentifier("rl_med", "id", context.getPackageName()));
        rlLock = v.findViewById(getResources().getIdentifier("rl_lock", "id", context.getPackageName()));
        textViewTime = (TextView) v.findViewById(getResources().getIdentifier("mediacontroller_time", "id", context.getPackageName()));

        mHead.setVisibility(VISIBLE);
        mBottom.setVisibility(VISIBLE);
        rlLock.setVisibility(GONE);

        img_back.setOnClickListener(backListener);
        mForwardView.setOnClickListener(backListener);
        mBackView.setOnClickListener(backListener);
        mLockView.setOnClickListener(backListener);
        mDeleteView.setOnClickListener(backListener);
        btnLock.setOnClickListener(backListener);

        setLock(isLock);

        return v;
    }

    public void setLock(boolean isLock) {
        this.isLock = isLock;
        if (mLockView != null) {
            mLockView.setImageResource(isLock ? R.mipmap.icon_video_lock : R.mipmap.icon_video_rewind);
        }
    }

    @Override
    public boolean dispatchKeyEvent(KeyEvent event) {
//        System.out.println("MYApp-MyMediaController-dispatchKeyEvent");
        return true;
    }

    @Override
    public boolean onTouchEvent(MotionEvent event) {
        if (mGestureDetector.onTouchEvent(event)) return true;
        // 处理手势结束
        switch (event.getAction() & MotionEvent.ACTION_MASK) {
            case MotionEvent.ACTION_UP:
                break;
        }
        return super.onTouchEvent(event);
    }

    private class MyGestureListener extends GestureDetector.SimpleOnGestureListener {
        @Override
        public boolean onSingleTapUp(MotionEvent e) {
            return false;
        }

        @Override
        public boolean onSingleTapConfirmed(MotionEvent e) {
            //当收拾结束，并且是单击结束时，控制器隐藏/显示
            toggleMediaControlsVisiblity();
            return super.onSingleTapConfirmed(e);
        }

        @Override
        public boolean onDown(MotionEvent e) {
            return true;
        }

        @Override
        public boolean onScroll(MotionEvent e1, MotionEvent e2, float distanceX, float distanceY) {
            return super.onScroll(e1, e2, distanceX, distanceY);
        }

        //双击暂停或开始
        @Override
        public boolean onDoubleTap(MotionEvent e) {
            if (!isLock)
                playOrPause();
            return true;
        }
    }

    //设置时间
    public void setTime(String time) {
        if (textViewTime != null)
            textViewTime.setText(time);
    }

    //隐藏/显示
    private void toggleMediaControlsVisiblity() {
        if (isShowing()) {
            hide();
        } else {
            show();
        }
    }

    //播放与暂停
    private void playOrPause() {
        if (videoView != null)
            if (videoView.isPlaying()) {
                videoView.pause();
            } else {
                videoView.start();
            }
    }

    public void setLockListener(Action1<Boolean> mLockListener) {
        this.mLockListener = mLockListener;
    }

    public interface OnDeleteListener {
        void onDelete();
    }

    public void setOnDeleteListener(OnDeleteListener l) {
        mDeleteListener = l;
    }
}