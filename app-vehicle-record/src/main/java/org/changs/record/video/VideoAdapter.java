package org.changs.record.video;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;

import org.changs.aplug.utils.EmptyUtils;
import org.changs.aplug.widget.irecyclerview.SimpleAdapter;
import org.changs.media.MediaFile;
import org.changs.media.MediaManager;
import org.changs.media.MediaState;
import org.changs.record.R;
import org.changs.record.databinding.ItemListBinding;

import java.util.List;

/**
 * 视频的适配器
 * Created by Administrator on 2017/8/24.
 */

public class VideoAdapter extends SimpleAdapter<MediaFile, ItemListBinding> {
    private Context context;
    private MediaManager mediaManager;
//    private SparseBooleanArray array = new SparseBooleanArray();

    public VideoAdapter(Context context, List<MediaFile> pictures, MediaManager mediaManager) {
        super(pictures, R.layout.item_list);
        this.context = context;
        this.mediaManager = mediaManager;
    }

    @Override
    public void onBindViewHolder(ItemListBinding binding, int position) {
        MediaFile video = getData().get(position);

        binding.tvPhone.setVisibility(View.GONE);
        binding.vRefresh.setVisibility(View.GONE);
        binding.tvFinish.setVisibility(View.GONE);
        binding.tvName.setText(video.getName());
        binding.tvTime.setText(video.getTime());

        if (!EmptyUtils.isEmpty(video.getThumb())) {
            binding.image.setImageBitmap(video.getThumb());
        } else {
            binding.image.setBackgroundColor(context.getResources().getColor(R.color.transparent_half));
        }

        binding.tvPhone.setVisibility(View.GONE);
        binding.vRefresh.setVisibility(View.GONE);
        binding.tvFinish.setVisibility(View.GONE);


        if (video.getState() == MediaState.UPLOADED) {
            binding.tvFinish.setVisibility(View.VISIBLE);
            stopAnim(binding.vRefresh);
        } else if (video.getState() == MediaState.UPLOADING) {
            binding.vRefresh.setVisibility(View.VISIBLE);
            startAnim(binding.vRefresh);
        } else {
            binding.tvPhone.setVisibility(View.VISIBLE);
            stopAnim(binding.vRefresh);
        }
    }

    private void startAnim(ImageView imageView) {
        if (imageView.getAnimation() != null) {
            Animation animation = imageView.getAnimation();
            if (animation.hasEnded()) animation.start();
        } else {
            Animation animation = AnimationUtils.loadAnimation(context, R.anim.uploading);
            imageView.startAnimation(animation);
        }
    }

    private void stopAnim(ImageView imageView) {
        if (imageView.getAnimation() != null) {
            Animation animation = imageView.getAnimation();
            if (!animation.hasEnded()) animation.cancel();
        }
    }

    @Override
    public void onDetachedFromRecyclerView(RecyclerView recyclerView) {
        super.onDetachedFromRecyclerView(recyclerView);

    }
}
