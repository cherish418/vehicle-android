package org.changs.record.home;

import org.changs.aplug.annotation.ActivityScoped;
import org.changs.record.R;
import org.changs.record.picture.PictureFragment;
import org.changs.record.video.VideoFragment;

import dagger.Module;
import dagger.Provides;

/**
 * Created by yincs on 2017/9/5.
 */

@Module
public abstract class MainModule {

    private static final String TAG_PICTURE_FRAGMENT = "TAG_PICTURE_FRAGMENT";
    private static final String TAG_VIDEO_FRAGMENT = "TAG_VIDEO_FRAGMENT";


    @ActivityScoped
    @Provides
    static PictureFragment providePictureFragment(MainActivity mainActivity) {
        PictureFragment fragment = (PictureFragment) mainActivity
                .getSupportFragmentManager().findFragmentByTag(TAG_PICTURE_FRAGMENT);
        if (fragment == null) {
            fragment = new PictureFragment();
            mainActivity.getSupportFragmentManager().beginTransaction()
                    .add(R.id.main_fl, fragment, TAG_PICTURE_FRAGMENT)
                    .hide(fragment)
                    .commit();
        }
        return fragment;
    }

    @ActivityScoped
    @Provides
    static VideoFragment provideVideoFragment(MainActivity mainActivity) {
        VideoFragment fragment = (VideoFragment) mainActivity
                .getSupportFragmentManager().findFragmentByTag(TAG_VIDEO_FRAGMENT);
        if (fragment == null) {
            fragment = new VideoFragment();
            mainActivity.getSupportFragmentManager().beginTransaction()
                    .add(R.id.main_fl, fragment, TAG_VIDEO_FRAGMENT)
                    .hide(fragment)
                    .commit();
        }
        return fragment;
    }
}
