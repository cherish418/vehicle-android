package org.changs.record.home;

import org.changs.aplug.base.IPresenter;
import org.changs.aplug.base.IView;

/**
 * Created by yincs on 2017/9/5.
 */

public interface MainContract {


    interface V extends IView{
        void afterLoadMedia();
    }

    interface P extends IPresenter<V> {
        void load();
    }

}
