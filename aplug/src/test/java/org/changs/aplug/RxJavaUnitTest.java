package org.changs.aplug;

import org.junit.Before;
import org.junit.Test;

import java.util.Arrays;
import java.util.List;
import java.util.concurrent.Callable;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.TimeUnit;

import io.reactivex.Observable;
import io.reactivex.annotations.NonNull;
import io.reactivex.disposables.Disposable;
import io.reactivex.functions.Action;
import io.reactivex.functions.BooleanSupplier;
import io.reactivex.functions.Consumer;
import io.reactivex.functions.Function;
import io.reactivex.plugins.RxJavaPlugins;
import io.reactivex.schedulers.Schedulers;
import timber.log.Timber;

/**
 * Created by yincs on 2017/8/2.
 */

public class RxJavaUnitTest {
    @Before
    public void setUp() throws Exception {
        Timber.plant(new Timber.Tree() {
            @Override
            protected void log(int priority, String tag, String message, Throwable t) {
                System.out.println(message);
            }
        });

    }

    @Test
    public void rxErrorHandler() throws Exception {
        final Disposable subscribe = Observable
                .fromCallable(new Callable<Object>() {
                    @Override
                    public Object call() throws Exception {
                        System.out.println("开始sleep");
                        Thread.currentThread().sleep(2000);
                        System.out.println("结束sleep");
                        return 1;
                    }
                })
                .subscribeOn(Schedulers.io())
                .subscribe(new Consumer<Object>() {
                               @Override
                               public void accept(@NonNull Object o) throws Exception {
                                   System.out.println("next RxJavaUnitTest.accept");
                               }
                           },
                        new Consumer<Throwable>() {
                            @Override
                            public void accept(@NonNull Throwable throwable) throws Exception {
                                System.out.println("error RxJavaUnitTest.accept");
//                                throwable.printStackTrace();
                            }
                        },
                        new Action() {
                            @Override
                            public void run() throws Exception {
                                System.out.println("completed RxJavaUnitTest.accept");
                            }
                        },
                        new Consumer<Disposable>() {
                            @Override
                            public void accept(@NonNull Disposable disposable) throws Exception {
                                System.out.println("disposable = " + disposable.isDisposed());
                            }
                        });


        System.out.println("执行完了");

        Thread.sleep(1000);
        subscribe.dispose();
        Thread.sleep(10000);
    }

    @Test
    public void test2() throws Exception {

        final Disposable subscribe = Observable
                .fromCallable(new Callable<Object>() {
                    @Override
                    public Object call() throws Exception {
                        Thread.sleep(1000);
                        return 1 / 0;
                    }
                })
                .subscribeOn(Schedulers.io())
                .subscribe(new Consumer<Object>() {
                               @Override
                               public void accept(@NonNull Object o) throws Exception {
                                   System.out.println("next RxJavaUnitTest.accept");
                               }
                           },
                        new Consumer<Throwable>() {
                            @Override
                            public void accept(@NonNull Throwable throwable) throws Exception {
                                System.out.println("error RxJavaUnitTest.accept");
//                                throwable.printStackTrace();
                            }
                        },
                        new Action() {
                            @Override
                            public void run() throws Exception {
                                System.out.println("completed RxJavaUnitTest.accept");
                            }
                        },
                        new Consumer<Disposable>() {
                            @Override
                            public void accept(@NonNull Disposable disposable) throws Exception {
                                System.out.println("disposable = " + disposable.isDisposed());
                            }
                        });

        subscribe.dispose();

        Thread.sleep(1000);
    }

    @Test
    public void test3() throws Exception {
        RxJavaPlugins.setErrorHandler(new Consumer<Throwable>() {
            @Override
            public void accept(@NonNull Throwable throwable) throws Exception {
                System.out.println("RxJavaUnitTest.accept");
            }
        });
        Disposable subscribe = Observable
                .fromCallable(new Callable<Object>() {
                    @Override
                    public Object call() throws Exception {
                        Thread.sleep(1000);
                        return 1 / 2;
                    }
                })
                .subscribeOn(Schedulers.io())
                .doOnDispose(new Action() {
                    @Override
                    public void run() throws Exception {
                        System.out.println("doOnDispose");
                    }
                })
                .doOnComplete(new Action() {
                    @Override
                    public void run() throws Exception {
                        System.out.println("doOnComplete");
                    }
                })
                .doOnError(new Consumer<Throwable>() {
                    @Override
                    public void accept(@NonNull Throwable throwable) throws Exception {
                        System.out.println("doOnError");
                    }
                })
                .subscribe();

//        subscribe.dispose();
        Thread.sleep(2000);
    }

    @Test
    public void test4() throws Exception {
        Observable
                .fromCallable(new Callable<Integer>() {
                    @Override
                    public Integer call() throws Exception {
                        System.out.println("call OverrideThread.currentThread().getName() = " + Thread.currentThread().getName());
                        throw new IllegalStateException("callback exception");
                    }
                })
                .subscribeOn(Schedulers.io())
                .observeOn(Schedulers.computation())
                .subscribe(new Consumer<Integer>() {
                    @Override
                    public void accept(@NonNull Integer integer) throws Exception {
                        System.out.println("accept Thread.currentThread().getName() = " + Thread.currentThread().getName());
                        System.out.println("integer = " + integer);
                    }
                }, new Consumer<Throwable>() {
                    @Override
                    public void accept(@NonNull Throwable throwable) throws Exception {
                        System.out.println("throwable = " + throwable);
                        System.out.println("throwable Thread.currentThread().getName() = " + Thread.currentThread().getName());
                    }
                });
        new CountDownLatch(1).await();


    }

    int response = 10;
    int result;

    @Test
    public void test5() throws Exception {
        result = 10;
        Observable
                .fromCallable(new Callable<Integer>() {
                    @Override
                    public Integer call() throws Exception {
                        Timber.d("response = " + response);
                        return response;
                    }
                })
                .delay(1, TimeUnit.SECONDS)
                .repeatUntil(new BooleanSupplier() {
                    @Override
                    public boolean getAsBoolean() throws Exception {
                        return result == -1;
                    }
                })
                .doOnComplete(new Action() {
                    @Override
                    public void run() throws Exception {
                        Timber.d("doOnComplete");
                    }
                })
                .subscribe(new Consumer<Integer>() {
                    @Override
                    public void accept(@NonNull Integer integer) throws Exception {
                        Timber.d("response = " + response);
                        if (response == 5) {
                            result = -1;
                        } else {
                            response--;
                        }
                    }
                });

        new CountDownLatch(1).await();
    }


    @Test
    public void test6() throws Exception {
        Observable
                .fromCallable(new Callable<Integer>() {
                    @Override
                    public Integer call() throws Exception {
                        Timber.d("response = " + response);
                        return response;
                    }
                })
                .delay(1, TimeUnit.SECONDS)
                .repeatUntil(new BooleanSupplier() {
                    @Override
                    public boolean getAsBoolean() throws Exception {
                        return result == -1;
                    }
                })
                .doOnComplete(new Action() {
                    @Override
                    public void run() throws Exception {
                        Timber.d("doOnComplete");
                    }
                })
                .subscribe(new Consumer<Integer>() {
                    @Override
                    public void accept(@NonNull Integer integer) throws Exception {
                        Timber.d("response = " + response);
                        if (response == 5) {
                            result = -1;
                        } else {
                            response--;
                        }
                    }
                });
    }

    @Test
    public void test7() throws Exception {
        Observable<Boolean> callable1 = Observable.fromCallable(new Callable<Boolean>() {
            @Override
            public Boolean call() throws Exception {
                Timber.d("call: 11");
                Thread.sleep(2000);
                Timber.d("call: 12");
                Thread.sleep(2000);
                Timber.d("call: 13");
                return true;
            }
        }).subscribeOn(Schedulers.io());
        Observable<Boolean> callable2 = Observable.fromCallable(new Callable<Boolean>() {
            @Override
            public Boolean call() throws Exception {
                Timber.d("call: 21");
                Thread.sleep(2000);
                Timber.d("call: 22");
                Thread.sleep(3000);
                Timber.d("call: 23");
                return true;
            }
        }).subscribeOn(Schedulers.io());

        Observable<Boolean> callable3 = Observable.fromCallable(new Callable<Boolean>() {
            @Override
            public Boolean call() throws Exception {
                Timber.d("call: 31");
                Thread.sleep(2000);
                Timber.d("call: 32");
                Thread.sleep(5000);
                Timber.d("call: 33");
                return true;
            }
        }).subscribeOn(Schedulers.io());
        List<Observable<Boolean>> list = Arrays.asList(callable1, callable2, callable3);
        Disposable subscribe = Observable
                .zip(list, new Function<Object[], Boolean>() {
                    @Override
                    public Boolean apply(@NonNull Object[] objects) throws Exception {
                        return true;
                    }
                })
                .doOnComplete(new Action() {
                    @Override
                    public void run() throws Exception {
                        Timber.d("run: doOnComplete");
                    }
                })
                .doOnDispose(new Action() {
                    @Override
                    public void run() throws Exception {
                        Timber.d("run: doOnDispose");
                    }
                })
                .subscribe(new Consumer<Object>() {
                    @Override
                    public void accept(@NonNull Object o) throws Exception {
                        Timber.d("accept() called with: o = [" + o + "]");
                    }
                });


        subscribe.dispose();
        new CountDownLatch(1).await();

    }
}
