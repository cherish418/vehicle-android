package org.changs.aplug;

import dagger.android.support.DaggerApplication;

/**
 * Created by yincs on 2017/8/29.
 */

public abstract class AplugApp extends DaggerApplication {
    private volatile boolean firstOnCreate = true;

    @Override
    public void onCreate() {
        super.onCreate();
        if (firstOnCreate) {
            synchronized (this) {
                if (firstOnCreate) {
                    firstCreate();
                    firstOnCreate = false;
                }
            }
        }
    }

    protected abstract void firstCreate();


    @Override
    public void onTerminate() {
        super.onTerminate();
        AplugManager.get().onTerminate();
    }

}
