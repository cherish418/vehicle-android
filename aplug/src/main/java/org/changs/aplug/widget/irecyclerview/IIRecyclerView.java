package org.changs.aplug.widget.irecyclerview;

import android.content.Context;
import android.support.annotation.LayoutRes;
import android.support.annotation.Nullable;
import android.support.v7.widget.LinearLayoutManager;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.TextView;

import org.changs.aplug.R;
import org.changs.aplug.utils.Preconditions;
import org.changs.aplug.widget.irecyclerview.footer.LoadMoreFooterView;

import java.util.List;

/**
 * Created by yincs on 2017/8/8.
 */

public class IIRecyclerView extends IRecyclerView {

    public IIRecyclerView(Context context) {
        super(context);
    }

    public IIRecyclerView(Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
    }

    public IIRecyclerView(Context context, @Nullable AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
    }

    public Setup setup() {
        return this.new Setup();
    }

    public void clear() {
        final Adapter iAdapter = getIAdapter();
        if (iAdapter != null && iAdapter.getItemCount() > 0 && iAdapter instanceof IDataAdapter) {
            ((IDataAdapter) iAdapter).getData().clear();
            getAdapter().notifyDataSetChanged();
        }
    }

    public void startLoad() {
        final Runnable runnable = new Runnable() {
            @Override
            public void run() {
                setRefreshing(true);
            }
        };
        if (isRefreshing()) {
            setRefreshing(false);
            postDelayed(runnable, 500);
        } else {
            post(runnable);
        }
    }

    @Override
    public LoadMoreFooterView getLoadMoreFooterView() {
        return (LoadMoreFooterView) super.getLoadMoreFooterView();
    }

    public class Setup implements OnRefreshListener, OnLoadMoreListener {
        private IDataSource iDataSource;
        private Adapter adapter;
        private List data;
        private String notMore;
        private View emptyView;
        private boolean dynamic;

        public Setup dataSource(IDataSource dataSource) {
            this.iDataSource = dataSource;
            return this;
        }

        public Setup data(List data) {
            this.data = data;
            return this;
        }

        public Setup adapter(Adapter adapter) {
            this.adapter = adapter;
            return this;
        }

        public Setup notMore(String notMoreTip) {
            this.notMore = notMoreTip;
            return this;
        }

        /**
         * 开启动态检测(每次数据集变化的时候)是否为空显示emptyView
         */
        public Setup dynamic() {
            dynamic = true;
            return this;
        }

        public Setup emptyView(@LayoutRes int res) {
            this.emptyView = LayoutInflater.from(getContext()).inflate(res, IIRecyclerView.this, false);
            return this;
        }


        public Setup emptyView(View emptyView) {
            this.emptyView = emptyView;
            return this;
        }

        public Setup emptyText(String emptyText) {
            if (emptyView == null) {
                this.emptyView = LayoutInflater.from(getContext()).inflate(R.layout.layout_irecyclerview_load_more_footer_empty_view, IIRecyclerView.this, false);
            }
            ((TextView) emptyView.findViewById(R.id.tv_message)).setText(emptyText);
            return this;
        }

        public IIRecyclerView done() {
            Preconditions.checkNotNull(adapter, "adapter 不能为null");
            if (this.data == null && adapter instanceof IDataAdapter)
                this.data = ((IDataAdapter) adapter).getData();
            Preconditions.checkNotNull(data, "data 不能为null");

            if (getLayoutManager() == null) {
                setLayoutManager(new LinearLayoutManager(getContext()));
            }
            setIAdapter(adapter);

            LoadMoreFooterView loadMoreFooterView = getLoadMoreFooterView();
            if (loadMoreFooterView != null) {
                loadMoreFooterView.setAdapter(adapter);
                if (notMore != null)
                    loadMoreFooterView.setNotMoreTip(notMore);
                if (emptyView != null) {
                    loadMoreFooterView.setEmptyView(emptyView);
                }
            }
            if (dynamic) {
                Preconditions.checkNotNull(loadMoreFooterView, "dynamic时 loadMoreFooterView不能为null");
                final MyAdapterDataObserver observer = new MyAdapterDataObserver(data);
                adapter.registerAdapterDataObserver(observer);
                observer.checkShowEmptyView();
            }
            if (iDataSource != null) {
                setOnRefreshListener(this);
                setOnLoadMoreListener(this);
            }
            return IIRecyclerView.this;
        }

        @Override
        public void onRefresh() {
            iDataSource.onRefresh(IIRecyclerView.this, data);
            if (!getLoadMoreFooterView().canLoadMore())
                getLoadMoreFooterView().setStatus(LoadMoreFooterView.Status.GONE);
        }

        @Override
        public void onLoadMore() {
            iDataSource.onLoadMore(IIRecyclerView.this, data);
        }
    }

    private class MyAdapterDataObserver extends AdapterDataObserver {

        private List data;

        public MyAdapterDataObserver(List data) {
            this.data = data;
        }

        @Override
        public void onChanged() {
            checkShowEmptyView();
        }

        @Override
        public void onItemRangeInserted(int positionStart, int itemCount) {
            checkShowEmptyView();
        }

        @Override
        public void onItemRangeRemoved(int positionStart, int itemCount) {
            checkShowEmptyView();
        }


        public void checkShowEmptyView() {
            if (data.size() == 0) {
                if (LoadMoreFooterView.Status.THE_END != getLoadMoreFooterView().getStatus())
                    getLoadMoreFooterView().setStatus(LoadMoreFooterView.Status.THE_END);
            } else {
                if (LoadMoreFooterView.Status.GONE != getLoadMoreFooterView().getStatus()) {
                    getLoadMoreFooterView().setStatus(LoadMoreFooterView.Status.GONE);
                }
            }
        }
    }

    public interface IDataSource {

        void onRefresh(IIRecyclerView rcc, List data);


        void onLoadMore(IIRecyclerView rcc, List data);
    }

    public interface IDataAdapter<T> {
        List<T> getData();
    }
}
