package org.changs.aplug.widget;

import android.app.Dialog;
import android.content.DialogInterface;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.FragmentManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.animation.LinearInterpolator;
import android.view.animation.RotateAnimation;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.TextView;

import org.changs.aplug.R;
import org.changs.aplug.base.IDialogOpt;
import org.changs.aplug.interf.Action;

import io.reactivex.disposables.Disposable;

public class LoadingDialog extends DialogFragment implements IDialogOpt {

    private RotateAnimation rotation;
    private ImageView vLoading;
    private TextView tvMessage;

    private Disposable[] mDisposable;
    private Action mCancelAction;
    private String mMessage;
    private boolean show;

    @NonNull
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        if (savedInstanceState != null)
            show = savedInstanceState.getBoolean("show");

        final FrameLayout root = new FrameLayout(getActivity());
        root.setLayoutParams(new ViewGroup.LayoutParams(
                ViewGroup.LayoutParams.MATCH_PARENT,
                ViewGroup.LayoutParams.MATCH_PARENT));

        final Dialog dialog = new Dialog(getContext());
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(root);
        if (dialog.getWindow() != null) {
            dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
            dialog.getWindow().setLayout(
                    ViewGroup.LayoutParams.MATCH_PARENT,
                    ViewGroup.LayoutParams.WRAP_CONTENT);
        }
        dialog.setCanceledOnTouchOutside(false);
        setCancelable(false);
        final View view = LayoutInflater.from(getContext()).inflate(R.layout.loading_dialog, root);

        vLoading = (ImageView) view.findViewById(R.id.v_loading);
        tvMessage = (TextView) view.findViewById(R.id.tv_message);
        view.findViewById(R.id.btn_close).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (mDisposable != null) {
                    for (Disposable disposable : mDisposable) {
                        if (!disposable.isDisposed()) disposable.dispose();
                    }
                }
                if (mCancelAction != null) {
                    mCancelAction.call();
                }
                dismiss();
            }
        });
        dialog.setOnDismissListener(new DialogInterface.OnDismissListener() {
            @Override
            public void onDismiss(DialogInterface dialog) {
                mDisposable = null;
                mCancelAction = null;
            }
        });
        return dialog;
    }


    @Override
    public void show(FragmentManager manager, String tag) {
        if (show) return;
        show = true;
        super.show(manager, tag);
    }

    @Override
    public void onResume() {
        super.onResume();
        if (this.rotation == null) {
            this.rotation = new RotateAnimation(0.0F, 360.0F, 1, 0.5F, 1, 0.5F);
            this.rotation.setRepeatCount(-1);
            this.rotation.setInterpolator(new LinearInterpolator());
            this.rotation.setDuration(2000L);
            this.rotation.setRepeatCount(-1);
        }
        this.vLoading.startAnimation(this.rotation);

        if (this.mMessage == null) this.mMessage = "正在加载中";
        tvMessage.setText(mMessage);
    }

    @Override
    public void onPause() {
        super.onPause();
        if (this.rotation != null) {
            this.rotation.cancel();
        }
    }

    public void dismiss() {
        if (!show) return;
        show = false;
        super.dismiss();
    }

    @Override
    public void dismissAllowingStateLoss() {
        if (!show) return;
        show = false;
        super.dismissAllowingStateLoss();
    }

    @Override
    public IDialogOpt setMessage(String message) {
        this.mMessage = message;
        return this;
    }

    @Override
    public IDialogOpt cancelAction(Action action) {
        this.mCancelAction = action;
        return this;
    }

    @Override
    public IDialogOpt bindDisposable(Disposable... disposable) {
        this.mDisposable = disposable;
        return this;
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putBoolean("show", show);
    }


}
