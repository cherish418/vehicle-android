package org.changs.aplug.interf;

import android.content.Intent;

/**
 * Created by yincs on 2017/9/15.
 */

public interface IService {
    void onCreate();

    void onStartCommand(Intent intent, int flags, int startId);

    void onDestroy();
}
