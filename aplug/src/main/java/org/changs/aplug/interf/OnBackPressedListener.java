package org.changs.aplug.interf;

/**
 * Created by yincs on 2017/10/14.
 */

public interface OnBackPressedListener {
    boolean onBackPressed();
}
