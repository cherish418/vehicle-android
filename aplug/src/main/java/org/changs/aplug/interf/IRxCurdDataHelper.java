package org.changs.aplug.interf;

import java.util.List;

import io.reactivex.Observable;

/**
 * Created by yincs on 2017/9/11.
 */

public interface IRxCurdDataHelper<T> {
    Observable<Boolean> insertOrUpdate(final T data);

    Observable<Boolean> delete(final T data);

    Observable<Boolean> clear();

    Observable<List<T>> loadAll();
}
