package org.changs.aplug.interf;

import android.app.Application;

/**
 * Created by yincs on 2017/7/25.
 */

public interface OnApplicationListener {

    void onCreate(Application application);

    void onTerminate();
}
