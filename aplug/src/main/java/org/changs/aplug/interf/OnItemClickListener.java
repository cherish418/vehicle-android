package org.changs.aplug.interf;

import android.view.View;

/**
 * Created by yincs on 2017/9/1.
 */

public interface OnItemClickListener {
    void onItemClick(View view, int position);
}
