package org.changs.aplug.interf;

/**
 * Created by yincs on 2016/12/2.
 */

public interface Action {
    void call();
}
