package org.changs.aplug.interf;

import android.view.View;

/**
 * Created by yincs on 2017/8/15.
 */

public abstract class OnIntervalClickListener implements View.OnClickListener {

    private static final int DEFAULT_INTERVAL = 800;
    private final int interval;
    private long lastOpt;

    public OnIntervalClickListener() {
        this.interval = DEFAULT_INTERVAL;
    }

    public OnIntervalClickListener(int interval) {
        this.interval = interval;
    }

    @Override
    public void onClick(View v) {
        final long currentTimeMillis = System.currentTimeMillis();
        if (currentTimeMillis - lastOpt < interval) return;
        lastOpt = currentTimeMillis;
        intervalOnClick(v);
    }

    public abstract void intervalOnClick(View v);
}
