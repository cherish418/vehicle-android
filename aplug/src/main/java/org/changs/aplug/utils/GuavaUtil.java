package org.changs.aplug.utils;

import android.support.annotation.Nullable;

/**
 * Created by Administrator on 2016/10/24.
 */

public class GuavaUtil {
    public static <T> T checkNotNull(T reference) {
        if(reference == null) {
            throw new NullPointerException();
        } else {
            return reference;
        }
    }
    public static <T> T checkNotNull(T reference, @Nullable Object errorMessage) {
        if(reference == null) {
            throw new NullPointerException(String.valueOf(errorMessage));
        } else {
            return reference;
        }
    }
}
