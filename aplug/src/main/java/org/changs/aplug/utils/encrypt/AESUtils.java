package org.changs.aplug.utils.encrypt;

import javax.crypto.Cipher;
import javax.crypto.spec.IvParameterSpec;
import javax.crypto.spec.SecretKeySpec;

/**
 * AES加解密工具
 * 
 * @author aceaddi_liang
 */
public class AESUtils {

	/**
	 * AES加密（加密结果外加多了一层Base64加密）
	 * 
	 * @param sSrc
	 *            待加密内容
	 * @param sKey
	 *            加密密钥(16位)
	 * @return 加密后的串
	 */
	public static String encryptToBytes(String sSrc, String sKey) {
		if (sKey == null) {
			return null;
		}

		// 判断Key是否为16位
		if (sKey.length() != 16) {
			return null;
		}

		try {
			byte[] raw = sKey.getBytes("ASCII");
			SecretKeySpec skeySpec = new SecretKeySpec(raw, "AES");
			Cipher cipher = Cipher.getInstance("AES/CBC/PKCS5Padding"); // 算法/模式/补码方式
			IvParameterSpec iv = new IvParameterSpec(sKey.getBytes()); // 使用CBC模式，需要一个向量iv，可增加加密算法的强度
			cipher.init(Cipher.ENCRYPT_MODE, skeySpec, iv);
			byte[] encrypted = cipher.doFinal(sSrc.getBytes());

			return Base64.encode(encrypted);
		} catch (Exception e) {
			e.printStackTrace();
		}

		return null;
	}

	/**
	 * AES解密
	 * 
	 * @param sSrc
	 *            待解密的内容 （该内容需已做了Base64加密）
	 * @param sKey
	 *            解密密钥 (16位)
	 * @return 解密后的String
	 */
	public static String decryptByBytes(String sSrc, String sKey) {
		if (sKey == null) {
			return null;
		}

		// 判断Key是否为16位
		if (sKey.length() != 16) {
			return null;
		}

		try {
			byte[] raw = sKey.getBytes("ASCII");
			SecretKeySpec skeySpec = new SecretKeySpec(raw, "AES");
			Cipher cipher = Cipher.getInstance("AES/CBC/PKCS5Padding"); // 算法/模式/补码方式
			IvParameterSpec iv = new IvParameterSpec(sKey.getBytes()); // 使用CBC模式，需要一个向量iv，可增加加密算法的强度
			cipher.init(Cipher.DECRYPT_MODE, skeySpec, iv);
			byte[] decryptBytes = cipher.doFinal(Base64.decode(sSrc));

			return new String(decryptBytes);
		} catch (Exception e) {
			e.printStackTrace();
		}

		return null;
	}
}
