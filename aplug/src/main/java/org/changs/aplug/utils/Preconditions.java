package org.changs.aplug.utils;

import android.support.annotation.Nullable;

/**
 * Created by yincs on 2017/9/8.
 */

public class Preconditions {

   public static <T> T checkNotNull(@Nullable T value, String message) {
        if (value == null) {
            throw new NullPointerException(message);
        }
        return value;
    }

    private Preconditions() {
        throw new AssertionError("No instances.");
    }
}
