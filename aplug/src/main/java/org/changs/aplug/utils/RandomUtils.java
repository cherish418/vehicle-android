package org.changs.aplug.utils;

import java.util.Random;

/**
 * Created by yincs on 2016/12/5.
 */

public class RandomUtils {

    private static Random random;


    private static void check() {
        if (random == null)
            random = new Random();
    }

    public static boolean nextBoolean() {
        check();
        return random.nextBoolean();
    }

    public static void nextBytes(byte[] bytes) {
        check();
        random.nextBytes(bytes);
    }

    public static double nextDouble() {
        check();
        return random.nextDouble();
    }

    public static float nextFloat() {
        check();
        return random.nextFloat();
    }

    public static int nextInt() {
        check();
        return random.nextInt();

    }

    public static int nextInt(int n) {
        check();
        return random.nextInt(n);
    }

    public static long nextLong() {
        check();
        return random.nextLong();
    }
}
