package org.changs.aplug.utils;

import android.app.Notification;
import android.app.NotificationManager;
import android.content.Context;
import android.support.v4.app.NotificationCompat;

import static android.content.Context.NOTIFICATION_SERVICE;

/**
 * Created by yincs on 2017/10/11.
 */

public class NotificationUtils {

    public static void notify(Context context, String title, String msg, int icon) {
        int id = (int) (Math.random() * 100 + 1);
        NotificationManager notificationManager = (NotificationManager) context.getSystemService(NOTIFICATION_SERVICE);
        NotificationCompat.Builder builder = new NotificationCompat.Builder(context);
        builder.setSmallIcon(icon);// 设置图标
        builder.setContentTitle(title);// 设置通知的标题
        builder.setContentText(msg);// 设置通知的内容
        builder.setWhen(System.currentTimeMillis());// 设置通知来到的时间
        builder.setAutoCancel(true); //自己维护通知的消失
        builder.setTicker(title);// 第一次提示消失的时候显示在通知栏上的
        builder.setOngoing(true);
        builder.setNumber(20);
        builder.setDefaults(Notification.FLAG_ONLY_ALERT_ONCE);
        Notification notification = builder.build();
        notification.flags = Notification.FLAG_AUTO_CANCEL;  //只有全部清除时，Notification才会清除
        notificationManager.notify(id, notification);
    }
}
