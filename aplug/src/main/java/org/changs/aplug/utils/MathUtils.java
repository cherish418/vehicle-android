package org.changs.aplug.utils;


/**
 * Created by Lenovo on 2016/5/14.
 */
public class MathUtils {


    public static int bezier(int str, int end, int... c) {
        return 0;
    }


    public static int bezierThree(float t, int str, int p1, int p2, int end) {
        float f = 1 - t;
        return (int) (str * Math.pow(f, 3)
                + 3 * p1 * t * Math.pow(f, 2)
                + 3 * p2 * Math.pow(t, 2) * f +
                end * Math.pow(t, 3));
    }

    public static int bezierTwo(float t, int str, int p1, int end) {
        return (int) (Math.pow(1 - t, 2) * str
                + 2 * t * (1 - t) * p1
                + Math.pow(t, 2) * end);
    }

    public static int bezierOne(float t, int str, int end) {
        return (int) ((1 - t) * str + t * end);
    }

    /**
     * 根据用户的起点和终点经纬度计算两点间距离，此距离为相对较短的距离，单位米。
     *
     * @param var0 起点的坐标
     * @param var1 终点的坐标
     * @return 返回两点间相对较短的距离，单位米。
     */
//    public static float calculateLineDistance(LatLng var0, LatLng var1) {
//        double var2 = 6378137.0D;
//        double var4 = Math.sin(var0.latitude * 3.141592653589793D / 180.0D) - Math.sin(var1.latitude * 3.141592653589793D / 180.0D);
//        double var6 = (var1.longitude - var0.longitude) / 360.0D;
//        if (var6 < 0.0D) {
//            ++var6;
//        }
//        return (float) (6.283185307179586D * var2 * var2 * var4 * var6);
//    }
}
