package org.changs.aplug.utils;

import android.support.annotation.NonNull;

import org.json.JSONException;

/**
 * Created by yincs on 2017/9/27.
 */

public class JSONObject extends org.json.JSONObject {

    @Override
    public JSONObject put(@NonNull  String name, boolean value) {
        try {
            super.put(name, value);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return this;
    }

    @Override
    public JSONObject put(@NonNull String name, double value) {
        try {
            super.put(name, value);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return this;
    }

    @Override
    public JSONObject put(@NonNull String name, int value) {
        try {
            super.put(name, value);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return this;
    }

    @Override
    public JSONObject put(@NonNull String name, long value) {
        try {
            super.put(name, value);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return this;
    }

    @Override
    public JSONObject put(@NonNull String name, Object value) {
        try {
            super.put(name, value);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return this;
    }
}
