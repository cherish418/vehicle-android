package org.changs.aplug.utils;

import android.widget.EditText;

/**
 * Created by yincs on 2017/8/2.
 */

public class TextViewUtils {

    public static void setTextSelectionLast(EditText editText,String text) {
        editText.setText(text);
        editText.setSelection(editText.length());
    }
}
