package org.changs.aplug.base;

import com.androidnetworking.error.ANError;

import io.reactivex.disposables.CompositeDisposable;

/**
 * Created by yincs on 2017/8/29.
 */

public class BasePresenter<V> implements IPresenter<V> {

    private V view;
    private CompositeDisposable compositeDisposable;

    @Override
    public void attachView(V v) {
        view = v;
    }

    @Override
    public void detachView() {
        if (compositeDisposable != null)
            compositeDisposable.dispose();
        view = null;
    }

    public V getMvpView() {
        return view;
    }

    public boolean isDetachView() {
        return view == null;
    }

    public boolean isAttachView() {
        return view != null;
    }

    protected CompositeDisposable getCompositeDisposable() {
        if (compositeDisposable == null)
            compositeDisposable = new CompositeDisposable();
        return compositeDisposable;
    }

    @Override
    public void handleApiError(ANError error) {

    }
}
