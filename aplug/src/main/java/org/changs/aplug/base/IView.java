package org.changs.aplug.base;

import android.app.Activity;

import com.trello.rxlifecycle2.LifecycleTransformer;

import io.reactivex.ObservableTransformer;

/**
 * Created by yincs on 2017/7/20.
 */

public interface IView extends IDelegateOpt {
    Activity getContext();


    <T> LifecycleTransformer<T> bindToLifecycle();

    /**
     * 绑定view的生命周期
     *
     * @param threadSwitch 是否线程切换,将自动切换到子线程中操作主线程中回调
     */
    <T> ObservableTransformer<T, T> bindUntilDestroy(boolean threadSwitch);
}
