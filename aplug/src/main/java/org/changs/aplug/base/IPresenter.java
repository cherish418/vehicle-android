package org.changs.aplug.base;

import com.androidnetworking.error.ANError;

/**
 * Created by yincs on 2017/7/20.
 */

public interface IPresenter<V> {

    void attachView(V v);

    void detachView();

    void handleApiError(ANError error);
}
