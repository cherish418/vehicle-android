package org.changs.aplug.base;

import android.app.Activity;
import android.support.annotation.StringRes;

import com.trello.rxlifecycle2.LifecycleTransformer;

import io.reactivex.ObservableTransformer;

/**
 * Created by yincs on 2017/8/2.
 */

/**
 * 依赖于host的生命周期
 */
public class BaseView implements IView {
    private IView host;

    public BaseView(IView host) {
        this.host = host;
    }

    @Override
    public IDialogOpt showLoading() {
        return this.host.showLoading();
    }

    @Override
    public void hideLoading() {
        this.host.hideLoading();
    }

    @Override
    public void onError(@StringRes int resId) {
        this.host.onError(resId);
    }

    @Override
    public void onError(String message) {
        this.host.onError(message);
    }

    @Override
    public void showMessage(String message) {
        this.host.showMessage(message);
    }

    @Override
    public void showMessage(@StringRes int resId) {
        this.host.showMessage(resId);
    }

    @Override
    public Activity getContext() {
        return host.getContext();
    }

    @Override
    public <T> LifecycleTransformer<T> bindToLifecycle() {
        return this.host.bindToLifecycle();
    }

    @Override
    public <T> ObservableTransformer<T, T> bindUntilDestroy(boolean threadSwitch) {
        return this.host.bindUntilDestroy(threadSwitch);
    }
}
