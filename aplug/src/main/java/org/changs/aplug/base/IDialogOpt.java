package org.changs.aplug.base;

import org.changs.aplug.interf.Action;

import io.reactivex.disposables.Disposable;

/**
 * Created by yincs on 2017/8/4.
 */

public interface IDialogOpt {
    IDialogOpt setMessage(String message);

    IDialogOpt cancelAction(Action action);
    //rx
    IDialogOpt bindDisposable(Disposable... disposable);
}
