package org.changs.aplug.base;

/**
 * Created by yincs on 2017/7/22.
 */

public interface DialogIView extends IView {

    void dismissDialog(String tag);
}
