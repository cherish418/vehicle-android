package org.changs.aplug.base;

import android.support.annotation.StringRes;
import android.support.design.widget.Snackbar;
import android.support.v4.content.ContextCompat;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import org.changs.aplug.R;
import org.changs.aplug.widget.LoadingDialog;

/**
 * Created by yincs on 2017/8/4.
 */

public class ActivityDelegate implements IDelegateOpt {

    private BaseActivity mActivity;
    private LoadingDialog loadingDialog;

    public ActivityDelegate(BaseActivity activity) {
        this.mActivity = activity;
    }

    @Override
    public IDialogOpt showLoading() {
        if (loadingDialog == null)
            loadingDialog = new LoadingDialog();
        loadingDialog.show(mActivity.getSupportFragmentManager(), LoadingDialog.class.getName());
        return loadingDialog;
    }

    @Override
    public void hideLoading() {
        if (loadingDialog != null)
            loadingDialog.dismissAllowingStateLoss();
    }

    @Override
    public void onError(@StringRes int resId) {
        onError(mActivity.getString(resId));
    }

    @Override
    public void onError(String message) {
        if (message != null) {
            showSnackBar(message);
        } else {
            showSnackBar(mActivity.getString(R.string.some_error));
        }
    }


    @Override
    public void showMessage(@StringRes int resId) {
        showMessage(mActivity.getString(resId));
    }

    @Override
    public void showMessage(String message) {
        if (message != null) {
            Toast.makeText(mActivity, message, Toast.LENGTH_SHORT).show();
        } else {
            Toast.makeText(mActivity, mActivity.getString(R.string.some_error), Toast.LENGTH_SHORT).show();
        }
    }

    private void showSnackBar(String message) {
        Snackbar snackbar = Snackbar.make(mActivity.findViewById(android.R.id.content),
                message, Snackbar.LENGTH_SHORT);
        View sbView = snackbar.getView();
        TextView textView = (TextView) sbView
                .findViewById(android.support.design.R.id.snackbar_text);
        textView.setTextColor(ContextCompat.getColor(mActivity, R.color.white));
        snackbar.show();
    }

}
