package org.changs.aplug.base;

/**
 * Created by yincs on 2017/8/4.
 */

import android.support.annotation.StringRes;

/**
 * 可定制的view的基本功能
 */
public interface IDelegateOpt {
    IDialogOpt showLoading();

    void hideLoading();

    void onError(@StringRes int resId);

    void onError(String message);

    void showMessage(@StringRes int resId);

    void showMessage(String message);

}
