package org.changs.aplug.simple;

import android.text.Editable;
import android.text.TextWatcher;

/**
 * Created by yincs on 2016/4/27.
 */
public class SimpleTextWatcher implements TextWatcher {
    @Override
    public void beforeTextChanged(CharSequence s, int start, int count, int after) {

    }

    @Override
    public void onTextChanged(CharSequence s, int start, int before, int count) {

    }

    @Override
    public void afterTextChanged(Editable s) {

    }
}
