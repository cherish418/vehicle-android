package org.changs.aplug.simple;

import com.androidnetworking.error.ANError;

import org.changs.aplug.base.IPresenter;

import io.reactivex.Observer;
import io.reactivex.annotations.NonNull;
import io.reactivex.disposables.Disposable;

/**
 * Created by yincs on 2017/8/2.
 */

public abstract class SimpleErrorSubscriber<T> implements Observer<T> {

   private IPresenter presenter;

    public SimpleErrorSubscriber(IPresenter iPresenter) {
        this.presenter = iPresenter;
    }

    @Override
    public void onSubscribe(@NonNull Disposable d) {

    }

    @Override
    public void onError(@NonNull Throwable e) {
        if (this.presenter != null)
            this.presenter.handleApiError(new ANError(e));
    }

    @Override
    public void onComplete() {

    }
}
