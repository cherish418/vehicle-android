package org.changs.aplug.simple;

import android.support.design.widget.TabLayout;

/**
 * Created by yincs on 2016/6/14.
 */
public class SimpeOnTabSelectedListener implements TabLayout.OnTabSelectedListener {
    @Override
    public void onTabSelected(TabLayout.Tab tab) {

    }

    @Override
    public void onTabUnselected(TabLayout.Tab tab) {

    }

    @Override
    public void onTabReselected(TabLayout.Tab tab) {

    }
}
