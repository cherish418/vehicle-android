package org.changs.aplug.simple.entry;

/**
 * Created by yincs on 2017/10/14.
 */

public class OnTrimMemoryEvent {
    public final int level;

    public OnTrimMemoryEvent(int level) {
        this.level = level;
    }
}
