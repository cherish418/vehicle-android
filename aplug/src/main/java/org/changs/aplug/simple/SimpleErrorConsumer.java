package org.changs.aplug.simple;

import com.androidnetworking.error.ANError;

import org.changs.aplug.base.IPresenter;

import io.reactivex.annotations.NonNull;
import io.reactivex.functions.Consumer;

/**
 * Created by yincs on 2017/8/2.
 */

public class SimpleErrorConsumer implements Consumer<Throwable> {
    private IPresenter presenter;

    public SimpleErrorConsumer() {
    }

    public SimpleErrorConsumer(IPresenter iPresenter) {
        this.presenter = iPresenter;
    }

    @Override
    public void accept(@NonNull Throwable throwable) throws Exception {
        if (this.presenter != null)
            this.presenter.handleApiError(new ANError(throwable));
    }
}
