package org.changs.aplug.exception;

/**
 * Created by yincs on 2017/9/9.
 */

public class AppException extends RuntimeException {


    public AppException() {
    }

    public AppException(String message) {
        super(message);
    }

    public AppException(String message, Throwable cause) {
        super(message, cause);
    }

    public AppException(Throwable cause) {
        super(cause);
    }
}
