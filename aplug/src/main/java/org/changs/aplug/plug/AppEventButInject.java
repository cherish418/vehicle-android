package org.changs.aplug.plug;

import android.app.Activity;
import android.app.Application;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;

import org.greenrobot.eventbus.EventBus;

import timber.log.Timber;

/**
 * Created by yincs on 2017/10/16.
 */

/**
 * 自动注册EventBus
 */
public class AppEventButInject extends FragmentManager.FragmentLifecycleCallbacks implements Application.ActivityLifecycleCallbacks {

    private static AppEventButInject sInstance;

    public static AppEventButInject getInstance() {
        if (sInstance == null) sInstance = new AppEventButInject();
        return sInstance;
    }

    private AppEventButInject() {
    }

    @Override
    public void onActivityCreated(Activity activity, Bundle savedInstanceState) {
        injectEventBusOnCreate(activity, true);
    }

    @Override
    public void onActivityStarted(Activity activity) {
    }

    @Override
    public void onActivityResumed(Activity activity) {
        injectEventBusOnResume(activity, true);
    }

    @Override
    public void onActivityPaused(Activity activity) {
    }

    @Override
    public void onActivityStopped(Activity activity) {
        injectEventBusOnResume(activity, false);
    }

    @Override
    public void onActivitySaveInstanceState(Activity activity, Bundle outState) {
    }

    @Override
    public void onActivityDestroyed(Activity activity) {
        injectEventBusOnCreate(activity, false);
    }


    @Override
    public void onFragmentCreated(FragmentManager fm, Fragment f, Bundle savedInstanceState) {
        super.onFragmentCreated(fm, f, savedInstanceState);
        injectEventBusOnCreate(f, true);
    }

    @Override
    public void onFragmentDestroyed(FragmentManager fm, Fragment f) {
        super.onFragmentDestroyed(fm, f);
        injectEventBusOnCreate(f, false);
    }

    @Override
    public void onFragmentResumed(FragmentManager fm, Fragment f) {
        super.onFragmentResumed(fm, f);
        injectEventBusOnResume(f, true);
    }

    @Override
    public void onFragmentPaused(FragmentManager fm, Fragment f) {
        super.onFragmentPaused(fm, f);
        injectEventBusOnResume(f, false);
    }

    public void injectEventBusOnCreate(Object object, boolean register) {
        if (object instanceof InjectEventBusOnCreate) {
            injectEventBus(object, register);
        }
    }

    public void injectEventBusOnResume(Object object, boolean register) {
        if (object instanceof InjectEventBusOnResume) {
            injectEventBus(object, register);
        }
    }

    public void injectEventBus(Object object, boolean register) {
        if (register) {
            EventBus.getDefault().register(object);
            Timber.d("注册 EventBus in " + object.getClass().getSimpleName());
        } else {
            EventBus.getDefault().unregister(object);
            Timber.d("反注册 EventBus in " + object.getClass().getSimpleName());
        }
    }

}
