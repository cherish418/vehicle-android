package aplug;

import android.os.Bundle;
import android.view.View;

import org.changs.aplug.base.BaseActivity;
import org.changs.aplug.base.BaseFragment;
import org.changs.aplug.base.IView;
import org.changs.aplug.lifecycle.LifecycleListener;
import org.changs.simple.R;

/**
 * Created by yincs on 2017/7/27.
 */

public class BackDisposeEvent implements LifecycleListener {
    @Override
    public void onCreated(IView iView, Bundle savedInstanceState) {
        if (iView instanceof BaseActivity) {
            final BaseActivity activity = (BaseActivity) iView;
            final View vBack = activity.findViewById(R.id.btn_back);
            if (vBack != null)
                vBack.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        activity.onBackPressed();
                    }
                });
        } else if (iView instanceof BaseFragment) {
            final BaseFragment fragment = (BaseFragment) iView;
            final View vBack = fragment.getRootView().findViewById(R.id.btn_back);
            if (vBack != null)
                vBack.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        fragment.getContext().onFragmentDetached(fragment.getClass().getName());
                    }
                });
        }
    }

    @Override
    public void onStarted(IView iView) {

    }

    @Override
    public void onResumed(IView iView) {

    }

    @Override
    public void onPaused(IView iView) {

    }

    @Override
    public void onStopped(IView iView) {

    }

    @Override
    public void onDestroying(IView iView) {

    }
}
