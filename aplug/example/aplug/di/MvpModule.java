package aplug.di1;

import android.content.Context;

import org.changs.aplug.base.IView;

import aplug.di1.annotation.ActivityContext;

import dagger.Module;
import dagger.Provides;
import io.reactivex.disposables.CompositeDisposable;

/**
 * Created by yincs on 2017/7/20.
 */

@Module
public class MvpModule {
    private static final String TAG = "MvpModule";

    private IView iView;

    public MvpModule(IView activity) {
        this.iView = activity;
    }

    @Provides
    @ActivityContext
    Context provideContext() {
        return iView.getContext();
    }

    @Provides
    IView provideMvpView() {
        return iView;
    }

    @Provides
    CompositeDisposable provideCompositeDisposable() {
        return new CompositeDisposable();
    }

}
