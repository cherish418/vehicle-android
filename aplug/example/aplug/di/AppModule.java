package aplug.di1;

import android.app.Application;
import android.content.Context;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;
import aplug.data.AppDataManager;
import aplug.data.DataManager;
import aplug.data.db.AppDbHelper;
import aplug.data.db.DbHelper;
import aplug.data.network.ApiHeader;
import aplug.data.network.ApiHelper;
import aplug.data.network.AppApiHelper;
import aplug.data.prefs.AppPreferencesHelper;
import aplug.data.prefs.PreferencesHelper;
import aplug.di1.annotation.ApiInfo;
import aplug.di1.annotation.ApplicationContext;
import aplug.di1.annotation.DatabaseInfo;
import aplug.di1.annotation.PreferenceInfo;

/**
 * Created by yincs on 2017/7/20.
 */


@Module
public class AppModule {
    private final Application mApplication;

    public AppModule(Application application) {
        mApplication = application;
    }

    @Provides
    Application provideApplication() {
        return mApplication;
    }

    @ApplicationContext
    @Provides
    Context provideContext() {
        return mApplication;
    }

    @Singleton
    @Provides
    DataManager provideDataManager(AppDataManager appDataManager) {
        return appDataManager;
    }

    @DatabaseInfo
    @Provides
    String provideDatabaseName() {
        return "test1";
    }


    @PreferenceInfo
    @Provides
    String providePreferenceName() {
        return "pretest1";
    }

    @Provides
    @ApiInfo
    String provideApiKey() {
        return "api-key";
    }


    @Singleton
    @Provides
    DbHelper provideDbHelper(AppDbHelper appDbHelper) {
        return appDbHelper;
    }

    @Singleton
    @Provides
    PreferencesHelper providePreferencesHelper(AppPreferencesHelper appPreferencesHelper) {
        return appPreferencesHelper;
    }

    @Singleton
    @Provides
    ApiHelper provideApiHelper(AppApiHelper appApiHelper) {
        return appApiHelper;
    }

    @Provides
    @Singleton
    ApiHeader.ProtectedApiHeader provideProtectedApiHeader(@ApiInfo String apiKey) {
        return new ApiHeader.ProtectedApiHeader(apiKey, 0L, null);
    }
}
