package aplug.di1;

import org.changs.aplug.base.IView;

import aplug.data.DataManager;
import io.reactivex.disposables.CompositeDisposable;

/**
 * Created by yincs on 2017/7/20.
 */

public class MvpPresenter<V extends IView> extends LifecyclePresenter {
    private final CompositeDisposable compositeDisposable;
    private final DataManager dataManager;
    private V mvpView;


    public MvpPresenter(IView mvpView, CompositeDisposable compositeDisposable, DataManager dataManager) {
        super(mvpView);
        this.compositeDisposable = compositeDisposable;
        this.dataManager = dataManager;
        this.mvpView = (V) mvpView;
    }

    @Override
    public final void onDetach() {
        super.onDetach();
        compositeDisposable.dispose();
        mvpView = null;
    }

    public CompositeDisposable getCompositeDisposable() {
        return compositeDisposable;
    }

    public DataManager getDataManager() {
        return dataManager;
    }

    public V getMvpView() {
        return mvpView;
    }

    public boolean isViewAttached() {
        return mvpView != null;
    }

    public boolean isViewDetached() {
        return mvpView == null;
    }
}
