package aplug.di1;

import aplug.di1.annotation.PerActivity;
import aplug.example.ExampleActivity;
import dagger.Component;

/**
 * Created by yincs on 2017/7/20.
 */

@PerActivity
@Component(dependencies = AppComponent.class, modules = MvpModule.class)
public interface MvpComponent {

    void inject(ExampleActivity activity);
}
