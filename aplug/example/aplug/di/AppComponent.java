package aplug.di1;


import android.app.Application;
import android.content.Context;

import javax.inject.Singleton;

import aplug.MyApp;
import aplug.data.DataManager;
import aplug.di1.annotation.ApplicationContext;
import dagger.Component;

/**
 * Created by yincs on 2017/7/20.
 */

@Singleton
@Component(modules = AppModule.class)
public interface AppComponent {

    void inject(MyApp app);

    @ApplicationContext
    Context context();

    Application application();

    DataManager getDataManager();

}

