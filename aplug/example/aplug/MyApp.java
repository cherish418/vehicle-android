package aplug;

import android.app.Application;

import org.changs.aplug.base.IView;
import org.changs.aplug.AplugInitializer;

import aplug.di1.AppComponent;
import aplug.di1.AppModule;
import aplug.di1.DaggerAppComponent;
import aplug.di1.DaggerMvpComponent;
import aplug.di1.MvpComponent;
import aplug.di1.MvpModule;


/**
 * Created by yincs on 2017/7/25.
 */

public class MyApp extends Application {

    private AplugInitializer aplugInitializer;
    private static AppComponent appComponent;

    @Override
    public void onCreate() {
        super.onCreate();
        aplugInitializer = new AplugInitializer.Builder()
                .addActivityLifecycleListener(new BackDisposeEvent())//添加返回事件监听
                .build();

        appComponent = DaggerAppComponent.builder()
                .appModule(new AppModule(this))
                .build();
    }

    @Override
    public void onTerminate() {
        super.onTerminate();
        aplugInitializer.onTerminate();
    }


    public static MvpComponent getMvpComponent(IView iView) {
        return DaggerMvpComponent.builder()
                .appComponent(appComponent)
                .mvpModule(new MvpModule(iView))
                .build();
    }
}
