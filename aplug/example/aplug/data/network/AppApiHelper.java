package aplug.data.network;

import javax.inject.Inject;
import javax.inject.Singleton;

/**
 * Created by yincs on 2017/7/22.
 */

@Singleton
public class AppApiHelper implements ApiHelper {

    private final ApiHeader mApiHeader;

    @Inject
    public AppApiHelper(ApiHeader mApiHeader) {
        this.mApiHeader = mApiHeader;
    }

    @Override
    public ApiHeader getApiHeader() {
        return null;
    }
}
