package aplug.data.network;

/**
 * Created by yincs on 2017/7/22.
 */

public interface ApiHelper {
    ApiHeader getApiHeader();
}
