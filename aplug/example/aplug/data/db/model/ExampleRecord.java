package aplug.data.db.model;

import org.greenrobot.greendao.annotation.Entity;
import org.greenrobot.greendao.annotation.Property;

import java.util.Date;
import org.greenrobot.greendao.annotation.Generated;

/**
 * Created by yincs on 2017/7/27.
 */

@Entity(nameInDb = "example_record")
public class ExampleRecord {


    @Property(nameInDb = "content")
    private String content;

    @Property(nameInDb = "add_time")
    private Date addTime;

    @Generated(hash = 1148767173)
    public ExampleRecord(String content, Date addTime) {
        this.content = content;
        this.addTime = addTime;
    }

    @Generated(hash = 191937533)
    public ExampleRecord() {
    }

    public String getContent() {
        return this.content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public Date getAddTime() {
        return this.addTime;
    }

    public void setAddTime(Date addTime) {
        this.addTime = addTime;
    }

}
