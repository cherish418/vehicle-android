package aplug.data.db;

import java.util.List;

import aplug.data.db.model.ExampleRecord;
import aplug.data.db.model.User;
import io.reactivex.Observable;

/**
 * Created by yincs on 2017/7/22.
 */

public interface DbHelper {
    /*example*/
    Observable<Long> insertExampleRecord(final ExampleRecord record);

    /*example*/
    Observable<List<ExampleRecord>> getAllExampleRecord();

    /*example*/
    Observable<Boolean> clearExampleRecord();


    Observable<Long> insertUser(final User user);

    Observable<List<User>> getAllUsers();

}
