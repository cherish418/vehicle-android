package aplug.data;

import android.content.Context;

import javax.inject.Inject;
import javax.inject.Singleton;

import aplug.data.db.DbHelper;
import aplug.data.network.ApiHelper;
import aplug.data.prefs.PreferencesHelper;
import aplug.di1.annotation.ApplicationContext;

/**
 * Created by yincs on 2017/7/24.
 */

@Singleton
public class AppDataManager implements DataManager {


    private final Context context;
    private final DbHelper dbHelper;
    private final PreferencesHelper preferencesHelper;
    private final ApiHelper apiHelper;

    @Inject
    public AppDataManager(@ApplicationContext Context context,
                          DbHelper dbHelper,
                          PreferencesHelper preferencesHelper,
                          ApiHelper apiHelper) {
        this.context = context;
        this.dbHelper = dbHelper;
        this.preferencesHelper = preferencesHelper;
        this.apiHelper = apiHelper;
    }

    @Override
    public DbHelper getDbHelper() {
        return dbHelper;
    }

    @Override
    public ApiHelper getApiHelper() {
        return apiHelper;
    }

    @Override
    public PreferencesHelper getPreferencesHelper() {
        return preferencesHelper;
    }
}
