package aplug;

import android.databinding.DataBindingUtil;
import android.databinding.ViewDataBinding;
import android.support.annotation.LayoutRes;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.ViewGroup;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by yincs on 2017/7/25.
 */

public abstract class SimpleAdapter<DATA, BINDING extends ViewDataBinding> extends RecyclerView.Adapter<SimpleAdapter.MyViewHolder<BINDING>> {

    private List<DATA> data = new ArrayList<>();

    private final int layoutRes;

    public SimpleAdapter(@LayoutRes int layoutRes) {
        this.layoutRes = layoutRes;
    }

    @Override
    public MyViewHolder<BINDING> onCreateViewHolder(ViewGroup parent, int viewType) {
        BINDING binding = DataBindingUtil.inflate(LayoutInflater.from(parent.getContext()), layoutRes, parent, false);
        return new MyViewHolder<>(binding);
    }


    @Override
    public int getItemCount() {
        return data.size();
    }

    public List<DATA> getData() {
        return data;
    }

    public static class MyViewHolder<BINDING extends ViewDataBinding> extends RecyclerView.ViewHolder {

        public BINDING binding;

        public MyViewHolder(BINDING binding) {
            super(binding.getRoot());
            this.binding = binding;
        }
    }
}
