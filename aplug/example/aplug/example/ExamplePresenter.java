package aplug.example;

import android.os.Bundle;

import org.changs.aplug.base.IView;
import org.changs.aplug.utils.EmptyUtils;

import java.util.Date;
import java.util.List;

import javax.inject.Inject;

import aplug.data.DataManager;
import aplug.data.db.model.ExampleRecord;
import aplug.di1.MvpPresenter;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.annotations.NonNull;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.functions.Consumer;
import io.reactivex.schedulers.Schedulers;

/**
 * Created by yincs on 2017/7/27.
 */

public class ExamplePresenter extends MvpPresenter<ExamplePresenter.V> {

    public static interface V extends IView {
        void loadRecord(List<ExampleRecord> exampleRecords);
    }

    @Inject
    public ExamplePresenter(IView mvpView, CompositeDisposable compositeDisposable, DataManager dataManager) {
        super(mvpView, compositeDisposable, dataManager);
    }

    @Override
    public void onCreated(IView iView, Bundle savedInstanceState) {
        super.onCreated(iView, savedInstanceState);
        loadAllRecord();
    }

    public void loadAllRecord() {
        getCompositeDisposable().add(getDataManager().getDbHelper()
                .getAllExampleRecord()
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Consumer<List<ExampleRecord>>() {
                    @Override
                    public void accept(@NonNull List<ExampleRecord> exampleRecords) throws Exception {
                        if (getMvpView() == null) return;
                        getMvpView().loadRecord(exampleRecords);
                    }
                }));

    }

    public void onNewRecordClick(String content) {
        if (EmptyUtils.isEmpty(content)) {
            getMvpView().onError("请输入内容");
        }
        getCompositeDisposable().add(getDataManager().getDbHelper()
                .insertExampleRecord(new ExampleRecord(content, new Date()))
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Consumer<Long>() {
                    @Override
                    public void accept(@NonNull Long aLong) throws Exception {
                        if (getMvpView() == null) return;
                        getMvpView().showMessage("添加成功！");
                        loadAllRecord();
                    }
                }));
    }


    public void onCleanRecordClick() {
        getCompositeDisposable().add(getDataManager().getDbHelper()
                .clearExampleRecord()
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Consumer<Boolean>() {
                    @Override
                    public void accept(@NonNull Boolean aBoolean) throws Exception {
                        if (getMvpView() == null) return;
                        loadAllRecord();
                    }
                }));
    }
}
