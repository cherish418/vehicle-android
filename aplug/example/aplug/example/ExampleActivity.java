package aplug.example;

import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.view.View;

import org.changs.aplug.base.BaseActivity;
import org.changs.simple.R;
import org.changs.simple.databinding.ExampleActivityBinding;
import org.changs.simple.databinding.ExampleItemBinding;

import java.text.SimpleDateFormat;
import java.util.List;
import java.util.Locale;

import javax.inject.Inject;

import aplug.MyApp;
import aplug.SimpleAdapter;
import aplug.data.db.model.ExampleRecord;

public class ExampleActivity extends BaseActivity implements ExamplePresenter.V {

    private ExampleActivityBinding binding;

    @Inject
    ExamplePresenter examplePresenter;

    @Override
    public void setup(Bundle savedInstanceState) {
        MyApp.getMvpComponent(this).inject(this);
        binding = DataBindingUtil.setContentView(this, R.layout.example_activity);
        initUI();
    }

    private void initUI() {
        binding.btnRecordAdd.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                examplePresenter.onNewRecordClick(binding.etContent.getText().toString());
            }
        });
        binding.btnRecordClear.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                examplePresenter.onCleanRecordClick();
            }
        });
    }

    @Override
    public void loadRecord(List<ExampleRecord> exampleRecords) {
        if (exampleRecords == null || exampleRecords.size() == 0) {
            binding.vNoRecord.setVisibility(View.VISIBLE);
            binding.vRecord.setVisibility(View.GONE);
            return;
        } else {
            binding.vNoRecord.setVisibility(View.GONE);
            binding.vRecord.setVisibility(View.VISIBLE);
        }
        SimpleAdapter<ExampleRecord, ExampleItemBinding> adapter = (SimpleAdapter) binding.rccView.getAdapter();
        if (adapter == null) {
            adapter = new SimpleAdapter<ExampleRecord, ExampleItemBinding>(R.layout.example_item) {
                @Override
                public void onBindViewHolder(MyViewHolder<ExampleItemBinding> holder, int position) {
                    final ExampleRecord record = getData().get(position);
                    holder.binding.tvTitle.setText(record.getContent());
                    holder.binding.tvContent.setText(String.format("添加时间:%s", new SimpleDateFormat("yyyyy.MMMMM.dd HH:mm:ss", Locale.getDefault()).format(record.getAddTime())));
                }
            };
            binding.rccView.setLayoutManager(new LinearLayoutManager(getContext()));
            binding.rccView.setAdapter(adapter);
        }
        adapter.getData().clear();
        adapter.getData().addAll(exampleRecords);
        adapter.notifyDataSetChanged();

        final LinearLayoutManager layoutManager = (LinearLayoutManager) binding.rccView.getLayoutManager();
        layoutManager.scrollToPosition(adapter.getData().size() - 1);
    }
}
