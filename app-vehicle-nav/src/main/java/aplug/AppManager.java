package aplug;

import android.content.Context;

import com.amap.api.maps.model.LatLng;
import com.amap.api.navi.AMapNavi;

import javax.inject.Inject;
import javax.inject.Singleton;

/**
 * Created by yincs on 2017/10/14.
 */

@Singleton
public class AppManager {

    private final Context mContext;

    private AMapNavi mAMapNavi;



    @Inject
    public AppManager(Context context) {
        this.mContext = context;
        this.mAMapNavi = AMapNavi.getInstance(context);
    }

    public Context getContext() {
        return mContext;
    }

    public AMapNavi getAMapNavi() {
        return mAMapNavi;
    }

    public LatLng getMyLatLng() {
        return new LatLng(22.559202, 114.047175);
    }


}
