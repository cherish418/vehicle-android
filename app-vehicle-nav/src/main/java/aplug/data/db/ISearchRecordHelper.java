package aplug.data.db;


import java.util.List;

import aplug.data.db.mode.SearchRecord;
import io.reactivex.Observable;

/**
 * Created by yincs on 2017/8/30.
 */

public interface ISearchRecordHelper {

    Observable<Long> insertOrUpdateSearchRecord(SearchRecord searchRecord);

    Observable<List<SearchRecord>> getAllPoiSearchRecord();

    Observable<List<SearchRecord>> getAllSearchRecord();

    Observable<Boolean> clearSearchRecord();
}
