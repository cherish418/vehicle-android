package aplug.data.db.mode;

import org.greenrobot.greendao.annotation.Entity;
import org.greenrobot.greendao.annotation.Id;
import org.greenrobot.greendao.annotation.Generated;

import java.io.Serializable;

/**
 * Created by 惠安 on 2017/11/23.
 */

/**
 * 收藏信息
 */
@Entity(nameInDb = "collect_info")
public class CollectInfo implements Serializable{

    private static final long serialVersionUID = 2964834797401496217L;
    @Id
    private Long id;
    /**
     * 收藏的名称
     */
    private String name;
    /**
     * 收藏的地址
     */
    private String address;
    private Double lat;
    private Double lon;


    @Generated(hash = 1011209531)
    public CollectInfo(Long id, String name, String address, Double lat,
            Double lon) {
        this.id = id;
        this.name = name;
        this.address = address;
        this.lat = lat;
        this.lon = lon;
    }

    @Generated(hash = 781720191)
    public CollectInfo() {
    }

    public Long getId() {
        return this.id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return this.name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Double getLat() {
        return this.lat;
    }

    public void setLat(Double lat) {
        this.lat = lat;
    }

    public Double getLon() {
        return this.lon;
    }

    public void setLon(Double lon) {
        this.lon = lon;
    }

    public String getAddress() {
        return this.address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public boolean isValid() {
        return name != null && address != null && lat != null && lon != null;
    }


}
