package aplug.data.db.mode;

import org.greenrobot.greendao.annotation.Entity;
import org.greenrobot.greendao.annotation.Generated;
import org.greenrobot.greendao.annotation.Id;
import org.greenrobot.greendao.annotation.Unique;

import java.util.Date;

/**
 * Created by yincs on 2017/7/24.
 */

@Entity(nameInDb = "search_record")
public class SearchRecord {

    public static final int TYPE_POI = 1;
    public static final int TYPE_NORMAL = 0;

    @Id
    private Long id;

    @Unique
    private String name;

    private String address;

    private long distance;

    private double latitude;

    private double longitude;

    private int type;

    private Date searchTime;

    @Generated(hash = 819393376)
    public SearchRecord(Long id, String name, String address, long distance, double latitude, double longitude, int type,
                        Date searchTime) {
        this.id = id;
        this.name = name;
        this.address = address;
        this.distance = distance;
        this.latitude = latitude;
        this.longitude = longitude;
        this.type = type;
        this.searchTime = searchTime;
    }

    @Generated(hash = 839789598)
    public SearchRecord() {
    }

    public Long getId() {
        return this.id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return this.name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getAddress() {
        return this.address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public long getDistance() {
        return this.distance;
    }

    public void setDistance(long distance) {
        this.distance = distance;
    }

    public double getLatitude() {
        return this.latitude;
    }

    public void setLatitude(double latitude) {
        this.latitude = latitude;
    }

    public double getLongitude() {
        return this.longitude;
    }

    public void setLongitude(double longitude) {
        this.longitude = longitude;
    }

    public int getType() {
        return this.type;
    }

    public void setType(int type) {
        this.type = type;
    }

    public Date getSearchTime() {
        return this.searchTime;
    }

    public void setSearchTime(Date searchTime) {
        this.searchTime = searchTime;
    }


    public static SearchRecord normal(String key) {
        return new SearchRecord(null, key, "", -1, -1, -1, TYPE_NORMAL, new Date());
    }

    public static SearchRecord poi(String key, String address, long distance, double latitude, double longitude) {
        return new SearchRecord(null, key, address, distance, latitude, longitude, TYPE_POI, new Date());
    }
}
