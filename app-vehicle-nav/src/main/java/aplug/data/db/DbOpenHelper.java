package aplug.data.db;

import android.content.Context;

import org.greenrobot.greendao.database.Database;

import javax.inject.Inject;
import javax.inject.Singleton;

import aplug.AppConfig;
import aplug.data.db.mode.DaoMaster;
import aplug.data.db.mode.DaoSession;


/**
 * Created by yincs on 2017/7/22.
 */
@Singleton
public class DbOpenHelper extends DaoMaster.OpenHelper {

    @Inject
    public DbOpenHelper(Context context) {
        super(context, AppConfig.DB_NAME);
    }

    @Override
    public void onUpgrade(Database db, int oldVersion, int newVersion) {
        super.onUpgrade(db, oldVersion, newVersion);
        System.out.println("db_old_version : " + oldVersion + ", db_new_version : " + newVersion);
        switch (oldVersion) {
            case 1:
                //db.execSQL("ALTER TABLE " + UserDao.TABLENAME + " ADD COLUMN "
                // + UserDao.Properties.Name.columnName + " TEXT DEFAULT 'DEFAULT_VAL'");
        }
    }

    private DaoSession daoSession;

    public DaoSession getDaoSession() {
        if (daoSession == null) {
            daoSession = new DaoMaster(getWritableDb()).newSession();
        }
        return daoSession;
    }
}
