package aplug.data.db;

import java.util.List;

import javax.inject.Inject;
import javax.inject.Singleton;

import aplug.data.db.mode.SearchRecord;
import aplug.data.db.mode.SearchRecordDao;
import io.reactivex.Observable;

/**
 * Created by yincs on 2017/10/17.
 */

@Singleton
public class SearchRecordHelper2 implements ISearchRecordHelper {

    private final SearchRecordDao recordDao;

    @Inject
    public SearchRecordHelper2(DbOpenHelper dbOpenHelper) {
        this.recordDao = dbOpenHelper.getDaoSession().getSearchRecordDao();
    }

    @Override
    public Observable<Long> insertOrUpdateSearchRecord(final SearchRecord searchRecord) {
        return Observable.fromCallable(() -> {
            try {
                return recordDao.insertOrReplace(searchRecord);
            } catch (Exception e) {
                e.printStackTrace();
            }
            return -1L;
        });
    }

    @Override
    public Observable<List<SearchRecord>> getAllPoiSearchRecord() {
        return Observable.fromCallable(recordDao::loadAll);
    }

    @Override
    public Observable<List<SearchRecord>> getAllSearchRecord() {
        return Observable.fromCallable(recordDao::loadAll);

//        return Observable.fromCallable(() ->
//                recordDao.queryBuilder()
//                        .where(SearchRecordDao.Properties.Type.eq(SearchRecord.TYPE_POI))
//                        .list()
//        );
    }

    @Override
    public Observable<Boolean> clearSearchRecord() {
        return Observable.fromCallable(() -> {
            recordDao.deleteAll();
            return true;
        });
    }

}
