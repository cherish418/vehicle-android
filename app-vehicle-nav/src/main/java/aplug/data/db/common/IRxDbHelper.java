package aplug.data.db.common;


import java.util.List;

import io.reactivex.Observable;

/**
 * Created by yincs on 2017/11/23.
 */

/**
 * 数据库的增删改查功能常用功能
 */
public interface IRxDbHelper<T, K> {

    Observable<T> findOne(K id);

    Observable<List<T>> findAll();

    Observable<Long> count();

    Observable<Long> insert(T data);

    Observable<Boolean> insert(List<T> data);

    Observable<Long> insertOrReplace(T data);

    Observable<Boolean> delete(T data);

    Observable<Boolean> deleteByKey(K id);

    Observable<Boolean> clear();

}
