package aplug.data.db.common;

import org.greenrobot.greendao.AbstractDao;

import java.util.Collections;
import java.util.List;

import io.reactivex.Observable;

/**
 * Created by yincs on 2017/11/23.
 */

public class RxDbHelper<T, K, DAO extends AbstractDao<T, K>> implements IRxDbHelper<T, K> {

    protected final DAO mDao;

    public RxDbHelper(DAO mDao) {
        this.mDao = mDao;
    }

    @Override
    public Observable<T> findOne(K id) {
        return Observable.fromCallable(() -> {
            try {
                return mDao.load(id);
            } catch (Exception e) {
                e.printStackTrace();
                return null;
            }
        });
    }

    @Override
    public Observable<List<T>> findAll() {
        return Observable.fromCallable(() -> {
            try {
                return mDao.loadAll();
            } catch (Exception e) {
                e.printStackTrace();
                return Collections.emptyList();
            }
        });
    }

    @Override
    public Observable<Long> count() {
        return Observable.fromCallable(() -> {
            try {
                return mDao.count();
            } catch (Exception e) {
                e.printStackTrace();
                return 0L;
            }
        });
    }

    @Override
    public Observable<Long> insert(T data) {
        return Observable.fromCallable(() -> {
            try {
                return mDao.insert(data);
            } catch (Exception e) {
//                e.printStackTrace();
                return -1L;
            }
        });
    }

    @Override
    public Observable<Long> insertOrReplace(T data) {
        return Observable.fromCallable(() -> {
            try {
                return mDao.insertOrReplace(data);
            } catch (Exception e) {
                e.printStackTrace();
                return -1L;
            }
        });
    }

    @Override
    public Observable<Boolean> insert(List<T> data) {
        return Observable.fromCallable(() -> {
            try {
                mDao.insertInTx(data);
                return true;
            } catch (Exception e) {
                e.printStackTrace();
                return false;
            }
        });
    }

    @Override
    public Observable<Boolean> delete(T data) {
        return Observable.fromCallable(() -> {
            try {
                mDao.delete(data);
                return true;
            } catch (Exception e) {
                e.printStackTrace();
                return false;
            }
        });
    }

    @Override
    public Observable<Boolean> deleteByKey(K id) {
        return Observable.fromCallable(() -> {
            try {
                mDao.deleteByKey(id);
                return true;
            } catch (Exception e) {
                e.printStackTrace();
                return false;
            }
        });
    }

    @Override
    public Observable<Boolean> clear() {
        return Observable.fromCallable(() -> {
            try {
                mDao.deleteAll();
                return true;
            } catch (Exception e) {
                e.printStackTrace();
                return false;
            }
        });
    }
}
