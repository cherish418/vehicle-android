package aplug.data.db;

import java.util.List;
import java.util.concurrent.Callable;

import javax.inject.Inject;
import javax.inject.Singleton;

import aplug.data.db.common.RxDbHelper;
import aplug.data.db.mode.SearchRecord;
import aplug.data.db.mode.SearchRecordDao;
import io.reactivex.Observable;

/**
 * Created by 惠安 on 2017/11/23.
 */

@Singleton
public class SearchRecordHelper extends RxDbHelper<SearchRecord, Long, SearchRecordDao> {
    @Inject
    public SearchRecordHelper(DbOpenHelper dbOpenHelper) {
        super(dbOpenHelper.getDaoSession().getSearchRecordDao());
    }


    public Observable<List<SearchRecord>> findAllPoiRecord() {
        return Observable.fromCallable(new Callable<List<SearchRecord>>() {
            @Override
            public List<SearchRecord> call() throws Exception {
                return mDao.queryBuilder()
                        .where(SearchRecordDao.Properties.Type.eq(SearchRecord.TYPE_POI))
                        .list();
            }
        });
    }
}
