package aplug.data.prefs;

/**
 * Created by yincs on 2017/7/22.
 */

public interface IPreferencesHelper {

    boolean isCongestion();//是否躲避拥堵
    void setCongestion(boolean congestion);

    boolean isAvoidSpeed();//不走高速
    void setAvoidSpeed(boolean avoidSpeed);

    boolean isAvoidCost();//避免收费
    void setAvoidCost(boolean avoidCost);

    boolean isHighSpeed();//高速优先
    void setHighSpeed(boolean hightSpeed);

    String getCarNumber();//车牌号
    void setCarNumber(String carNumber);

    boolean isRestriction();//避免限行
    void setRestriction(boolean restriction);

    String getVehicleHeight();//车辆高度（eg:1.2米）
    void setVehicleHeight(String vehicleHeight);

    boolean isVehicleHeightSwitch();//设置车辆的高度是否参与算路
    void setVehicleHeightSwitch(boolean vehicleHeightSwitch);

    String getVehicleLoad();//车辆载重（eg:1.5吨）
    void setVehicleLoad(String vehicleLoad);

    boolean isVehicleLoadSwitch();//设置车辆的载重是否参与算路
    void setVehicleLoadSwitch(boolean vehicleLoadSwitch);
}
