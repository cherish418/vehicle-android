package aplug.data.prefs;

import android.content.SharedPreferences;

import javax.inject.Inject;
import javax.inject.Singleton;

import timber.log.Timber;


/**
 * Created by yincs on 2017/7/22.
 */

@Singleton
public class PreferencesHelper implements IPreferencesHelper {

    private final SharedPreferences prefs;

    @Inject
    public PreferencesHelper(SharedPreferences prefs) {
        Timber.d("sharedPreferences = " + prefs);
        this.prefs = prefs;
    }

    @Override
    public boolean isCongestion() {
        return prefs.getBoolean(NavTools.Pref.KEY_CONGESTION, false);
    }

    @Override
    public void setCongestion(boolean congestion) {
        prefs.edit().putBoolean(NavTools.Pref.KEY_CONGESTION, congestion).apply();
    }

    @Override
    public boolean isAvoidSpeed() {
        return prefs.getBoolean(NavTools.Pref.KEY_AVOID_SPEED, false);
    }

    @Override
    public void setAvoidSpeed(boolean avoidSpeed) {
        prefs.edit().putBoolean(NavTools.Pref.KEY_AVOID_SPEED, avoidSpeed).apply();
    }

    @Override
    public boolean isAvoidCost() {
        return prefs.getBoolean(NavTools.Pref.KEY_AVOID_COST, false);
    }

    @Override
    public void setAvoidCost(boolean avoidCost) {
        prefs.edit().putBoolean(NavTools.Pref.KEY_AVOID_COST, avoidCost).apply();
    }

    @Override
    public boolean isHighSpeed() {
        return prefs.getBoolean(NavTools.Pref.KEY_HIGHT_SPEED, false);
    }

    @Override
    public void setHighSpeed(boolean hightSpeed) {
        prefs.edit().putBoolean(NavTools.Pref.KEY_HIGHT_SPEED, hightSpeed).apply();
    }

    @Override
    public String getCarNumber() {
        return prefs.getString(NavTools.Pref.KEY_CAR_NUMBER, "");
    }

    @Override
    public void setCarNumber(String carNumber) {
        prefs.edit().putString(NavTools.Pref.KEY_CAR_NUMBER, carNumber).apply();
    }

    @Override
    public boolean isRestriction() {
        return prefs.getBoolean(NavTools.Pref.KEY_RESTRICTIO, false);
    }

    @Override
    public void setRestriction(boolean restriction) {
        prefs.edit().putBoolean(NavTools.Pref.KEY_RESTRICTIO, restriction).apply();
    }

    @Override
    public String getVehicleHeight() {
        return prefs.getString(NavTools.Pref.KEY_VEHICLE_HEIGHT, "");
    }

    @Override
    public void setVehicleHeight(String vehicleHeight) {
        prefs.edit().putString(NavTools.Pref.KEY_VEHICLE_HEIGHT, vehicleHeight).apply();
    }

    @Override
    public boolean isVehicleHeightSwitch() {
        return prefs.getBoolean(NavTools.Pref.KEY_VEHICLE_HEIGHT_SWITCH, false);
    }

    @Override
    public void setVehicleHeightSwitch(boolean vehicleHeightSwitch) {
        prefs.edit().putBoolean(NavTools.Pref.KEY_VEHICLE_HEIGHT_SWITCH, vehicleHeightSwitch).apply();
    }

    @Override
    public String getVehicleLoad() {
        return prefs.getString(NavTools.Pref.KEY_VEHICLE_LOAD, "");
    }

    @Override
    public void setVehicleLoad(String vehicleLoad) {
        prefs.edit().putString(NavTools.Pref.KEY_VEHICLE_LOAD, vehicleLoad).apply();
    }

    @Override
    public boolean isVehicleLoadSwitch() {
        return prefs.getBoolean(NavTools.Pref.KEY_VEHICLE_LOAD_SWITCH, false);
    }

    @Override
    public void setVehicleLoadSwitch(boolean vehicleLoadSwitch) {
        prefs.edit().putBoolean(NavTools.Pref.KEY_VEHICLE_LOAD_SWITCH, vehicleLoadSwitch).apply();
    }
}
