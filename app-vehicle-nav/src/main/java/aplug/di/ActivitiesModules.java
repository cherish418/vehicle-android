package aplug.di;


import org.changs.aplug.annotation.ActivityScoped;
import org.changs.aplug.annotation.FragmentScoped;
import org.changs.nav.NavReceive;
import org.changs.nav.delegate.MapSwitchDelegate;
import org.changs.nav.ui.nav.CarNavFragment;
import org.changs.nav.ui.nav.CollectDetailFragment;
import org.changs.nav.ui.nav.MainActivity;
import org.changs.nav.ui.nav.MainMapFragment;
import org.changs.nav.ui.nav.MapBasicsFragment;
import org.changs.nav.ui.nav.MapPoiListFragment;
import org.changs.nav.ui.nav.MapRouteFragment;
import org.changs.nav.ui.nav.MarketDetailFragment;
import org.changs.nav.ui.nav.PoiListFragment;
import org.changs.nav.ui.nav.RouteListFragment;
import org.changs.nav.ui.nav.RoutePreferFragment;
import org.changs.nav.ui.nav.SearchMainFragment;
import org.changs.nav.ui.nav.SearchMoreFragment;
import org.changs.nav.ui.nav.SearchingFragment;
import org.changs.nav.ui.set.CollectAddFragment;
import org.changs.nav.ui.set.CollectFragment;
import org.changs.nav.ui.set.OfflineMapFragment;
import org.changs.nav.ui.set.PhoneFragment;
import org.changs.nav.ui.set.SetMainFragment;
import org.changs.nav.ui.set.SetPrefFragment;
import org.changs.s3.common.provider.SpContentProvider;

import dagger.Module;
import dagger.Provides;
import dagger.android.ContributesAndroidInjector;

@Module
public abstract class ActivitiesModules {

    @ActivityScoped
    @ContributesAndroidInjector(modules = MainModule.class)
    abstract SpContentProvider SpContentProvider();

    @ActivityScoped
    @ContributesAndroidInjector(modules = MainModule.class)
    abstract MainActivity mainActivity();

    @ActivityScoped
    @ContributesAndroidInjector
    abstract NavReceive NavReceive();

    @Module
    public static abstract class MainModule {
        @FragmentScoped
        @ContributesAndroidInjector
        abstract MainMapFragment MainMapFragment();

        @FragmentScoped
        @ContributesAndroidInjector
        abstract MarketDetailFragment MarketDetailFragment();

        @FragmentScoped
        @ContributesAndroidInjector
        abstract SearchMainFragment SearchMainFragment();

        @FragmentScoped
        @ContributesAndroidInjector
        abstract SearchingFragment SearchingFragment();

        @FragmentScoped
        @ContributesAndroidInjector
        abstract SearchMoreFragment SearchMoreFragment();

        @FragmentScoped
        @ContributesAndroidInjector
        abstract PoiListFragment PoiListFragment();

        @FragmentScoped
        @ContributesAndroidInjector
        abstract RouteListFragment RouteListFragment();

        @FragmentScoped
        @ContributesAndroidInjector
        abstract CarNavFragment CarNavFragment();

        @FragmentScoped
        @ContributesAndroidInjector
        abstract MapBasicsFragment MapBasicsFragment();

        @FragmentScoped
        @ContributesAndroidInjector
        abstract MapPoiListFragment MapPoiListFragment();

        @FragmentScoped
        @ContributesAndroidInjector
        abstract MapRouteFragment MapRouteFragment();

        @FragmentScoped
        @ContributesAndroidInjector
        abstract RoutePreferFragment RoutePreferFragment();

        @FragmentScoped
        @ContributesAndroidInjector
        abstract SetMainFragment SettingFragment();

        @FragmentScoped
        @ContributesAndroidInjector
        abstract PhoneFragment PhoneFragment();

        @FragmentScoped
        @ContributesAndroidInjector
        abstract OfflineMapFragment OfflineMapFragment();

        @FragmentScoped
        @ContributesAndroidInjector
        abstract SetPrefFragment SetupFragment();

        @FragmentScoped
        @ContributesAndroidInjector
        abstract CollectFragment CollectFragment();

        @FragmentScoped
        @ContributesAndroidInjector
        abstract CollectAddFragment CollectAddFragment();

        @FragmentScoped
        @ContributesAndroidInjector
        abstract CollectDetailFragment PoiDetailFragment();

        @ActivityScoped
        @Provides
        static MapSwitchDelegate providesMapSwitchDelegate(MainActivity activity) {
            return activity.getMainMapFragment();
        }


    }

}
