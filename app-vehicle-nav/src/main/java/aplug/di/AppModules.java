package aplug.di;

import android.content.Context;
import android.content.SharedPreferences;

import javax.inject.Singleton;

import aplug.AppConfig;
import dagger.Module;
import dagger.Provides;

/**
 * Created by yincs on 2017/8/27.
 */

//存放全局对象
@Module
public abstract class AppModules {

    @Singleton
    @Provides
    static SharedPreferences provideSharedPreferences(Context context) {
        return context.getSharedPreferences(AppConfig.PREFS_NAME, Context.MODE_PRIVATE);
    }
}
