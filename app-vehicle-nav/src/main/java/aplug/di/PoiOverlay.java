package aplug.di;

import android.location.Location;

import com.amap.api.maps.AMap;
import com.amap.api.maps.CameraUpdateFactory;
import com.amap.api.maps.model.BitmapDescriptor;
import com.amap.api.maps.model.BitmapDescriptorFactory;
import com.amap.api.maps.model.LatLng;
import com.amap.api.maps.model.LatLngBounds;
import com.amap.api.maps.model.Marker;
import com.amap.api.maps.model.MarkerOptions;
import com.amap.api.services.core.LatLonPoint;
import com.amap.api.services.core.PoiItem;

import org.changs.nav.R;

import java.util.ArrayList;

public class PoiOverlay {

    private static int[] markers = {R.mipmap.b_poi_1,
            R.mipmap.b_poi_2,
            R.mipmap.b_poi_3,
            R.mipmap.b_poi_4,
            R.mipmap.b_poi_5,
            R.mipmap.b_poi_6,
            R.mipmap.b_poi_7,
            R.mipmap.b_poi_8,
            R.mipmap.b_poi_9,
            R.mipmap.b_poi_10
    };

    private static int[] markers_hl = {R.mipmap.b_poi_1_hl,
            R.mipmap.b_poi_2_hl,
            R.mipmap.b_poi_3_hl,
            R.mipmap.b_poi_4_hl,
            R.mipmap.b_poi_5_hl,
            R.mipmap.b_poi_6_hl,
            R.mipmap.b_poi_7_hl,
            R.mipmap.b_poi_8_hl,
            R.mipmap.b_poi_9_hl,
            R.mipmap.b_poi_10_hl
    };
    private AMap mamap;
    private ArrayList<PoiItem> mPoiItems;
    private ArrayList<Marker> mPoiMarks = new ArrayList<>();
    private int selectIndex = -1;

    public PoiOverlay(AMap amap, ArrayList<PoiItem> pois) {
        mamap = amap;
        mPoiItems = pois;
    }

    public ArrayList<PoiItem> getPoiItems() {
        return mPoiItems;
    }

    /**
     * 添加Marker到地图中。
     *
     * @since V2.1.0
     */
    public void addToMap() {
        for (int i = 0; i < mPoiItems.size(); i++) {
            Marker marker = mamap.addMarker(getMarkerOptions(i));
            PoiItem item = mPoiItems.get(i);
            marker.setObject(item);
            mPoiMarks.add(marker);
        }
    }

    public void select(int index) {
        if (selectIndex != -1) {
            mPoiMarks.get(selectIndex).setIcon(getBitmapDescriptorHl(selectIndex));
        }
        mPoiMarks.get(index).setIcon(getBitmapDescriptor(index));
        selectIndex = index;

    }

    /**
     * 去掉PoiOverlay上所有的Marker。
     *
     * @since V2.1.0
     */
    public void removeFromMap() {
        for (Marker mark : mPoiMarks) {
            mark.remove();
        }
    }

    /**
     * 移动镜头到当前的视角。
     *
     * @since V2.1.0
     */
    public void zoomToSpan() {
        if (mPoiItems != null && mPoiItems.size() > 0) {
            if (mamap == null)
                return;
            LatLngBounds.Builder b = LatLngBounds.builder();
            for (int i = 0; i < mPoiItems.size(); i++) {
                final PoiItem poiItem = mPoiItems.get(i);
                b.include(new LatLng(poiItem.getLatLonPoint().getLatitude(),
                        poiItem.getLatLonPoint().getLongitude()));
            }
            final Location myLocation = mamap.getMyLocation();
            b.include(new LatLng(myLocation.getLatitude(), myLocation.getLongitude()));
            mamap.moveCamera(CameraUpdateFactory.newLatLngBounds(b.build(), 200));
        }
    }
//22.5298 \ 113.922
    public void zoomToSelect() {
        if (selectIndex == -1) return;
        final LatLonPoint latLonPoint = mPoiItems.get(selectIndex).getLatLonPoint();
        mamap.animateCamera(CameraUpdateFactory.newLatLng(
                new LatLng(latLonPoint.getLatitude(), latLonPoint.getLongitude())));


    }


    private MarkerOptions getMarkerOptions(int index) {
        return new MarkerOptions()
                .position(
                        new LatLng(mPoiItems.get(index).getLatLonPoint()
                                .getLatitude(), mPoiItems.get(index)
                                .getLatLonPoint().getLongitude()))
                .title(getTitle(index)).snippet(getSnippet(index))
                .infoWindowEnable(false)
                .icon(getBitmapDescriptor(index));
    }

    protected String getTitle(int index) {
        return mPoiItems.get(index).getTitle();
    }

    protected String getSnippet(int index) {
        return mPoiItems.get(index).getSnippet();
    }

    /**
     * 从marker中得到poi在list的位置。
     *
     * @param marker 一个标记的对象。
     * @return 返回该marker对应的poi在list的位置。
     * @since V2.1.0
     */
    public int getPoiIndex(Marker marker) {
        for (int i = 0; i < mPoiMarks.size(); i++) {
            if (mPoiMarks.get(i).equals(marker)) {
                return i;
            }
        }
        return -1;
    }

    public int getSelectIndex() {
        return selectIndex;
    }

    /**
     * 返回第index的poi的信息。
     *
     * @param index 第几个poi。
     * @return poi的信息。poi对象详见搜索服务模块的基础核心包（com.amap.api.services.core）中的类 <strong><a href="../../../../../../Search/com/amap/api/services/core/PoiItem.html" title_bar="com.amap.api.services.core中的类">PoiItem</a></strong>。
     * @since V2.1.0
     */
    public PoiItem getPoiItem(int index) {
        if (index < 0 || index >= mPoiItems.size()) {
            return null;
        }
        return mPoiItems.get(index);
    }

    private BitmapDescriptor getBitmapDescriptor(int arg0) {
        if (arg0 < 10) {
            return BitmapDescriptorFactory.fromResource(markers[arg0]);
        } else {
            return BitmapDescriptorFactory.fromResource(R.mipmap.marker_other_highlight);
        }
    }

    private BitmapDescriptor getBitmapDescriptorHl(int arg0) {
        if (arg0 < 10) {
            return BitmapDescriptorFactory.fromResource(markers_hl[arg0]);
        } else {
            return BitmapDescriptorFactory.fromResource(R.mipmap.marker_other_highlight);
        }
    }
}