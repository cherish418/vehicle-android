package org.changs.nav.mvp;

import org.changs.aplug.base.IPresenter;
import org.changs.aplug.base.IView;

/**
 * Created by yincs on 2017/10/14.
 */

public interface RouteListContract {
    interface V extends IView {
        void showRouteList(int[] ints, int select);
        void startNavView();
    }

    interface P extends IPresenter<V> {
        void calculate();

        void clickStartNav(int ints);
    }

}
