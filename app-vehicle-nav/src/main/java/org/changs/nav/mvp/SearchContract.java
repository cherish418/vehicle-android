package org.changs.nav.mvp;

import android.support.annotation.IntDef;

import com.amap.api.services.core.PoiItem;
import com.amap.api.services.help.Tip;

import org.changs.aplug.base.IPresenter;
import org.changs.aplug.base.IView;

import java.util.ArrayList;
import java.util.List;

import aplug.data.db.mode.SearchRecord;

/**
 * Created by yincs on 2017/10/14.
 */

public interface SearchContract {
    int STATE_MAIN = 0;
    int STATE_SEARCHING = 1;
    int STATE_MORE = 2;

    @IntDef({STATE_MAIN, STATE_SEARCHING, STATE_MORE})
    @interface State {
    }

    interface V extends IView {
        void showMainView();

        void showMainRecordList(List<SearchRecord> searchRecords);

        void showSearchView();

        void showSearchRecordList(List<SearchRecord> searchRecords);

        void showSearchTip(List<Tip> tips);

        void showMoreView();

        void startPoiListView(ArrayList<PoiItem> poiItems);

        void startRouteView(Tip tip, int[] ints);
    }

    interface P extends IPresenter<V> {
        void initState(@State int state);

        void loadMainRecord();

        void loadSearchRecord();

        void loadTip(String key);

        void clickTip(Tip tip);

        boolean onBackPressed(@State int state);

        void clickSearchPoi(String keyword);

        void clearRecord();

        void clickRecord(SearchRecord record);

        void clickSearchAround(String name);
    }

}
