package org.changs.nav.mvp;

import com.amap.api.navi.AMapNavi;
import com.amap.api.navi.model.NaviLatLng;

import org.changs.aplug.base.BasePresenter;
import org.changs.nav.delegate.global.AmapNavHelper;
import org.changs.nav.delegate.amap.AmapRouteHelper;
import org.changs.nav.delegate.amap.domain.PoiPoint;

import java.util.ArrayList;

import javax.inject.Inject;

/**
 * Created by yincs on 2017/10/14.
 */

public class RouteListPresenter extends BasePresenter<RouteListContract.V> implements RouteListContract.P {

    private final AmapRouteHelper mAmapRouteHelper;
    private final AmapNavHelper mAmapNavHelper;

    @Inject
    public RouteListPresenter(AmapRouteHelper amapRouteHelper, AmapNavHelper amapNavHelper) {
        this.mAmapRouteHelper = amapRouteHelper;
        this.mAmapNavHelper = amapNavHelper;
    }

    @Override
    public void calculate() {
        if (isDetachView()) return;
        getMvpView().showLoading().setMessage("路线规划中...")
                .cancelAction(mAmapRouteHelper::cancel);
        PoiPoint endPoint = mAmapNavHelper.getEndPoint();
        mAmapRouteHelper.calculate(new NaviLatLng(endPoint.getLatitude(), endPoint.getLongitude()),
                new ArrayList<>(), new AmapRouteHelper.AMapNaviListener() {
                    @Override
                    public void onError(int code, String msg) {
                        if (isDetachView()) return;
                        getMvpView().hideLoading();
                    }

                    @Override
                    public void onSuccess(int[] ints) {
                        getMvpView().hideLoading();
                        getMvpView().showRouteList(ints, 0);
                    }
                });
    }

    @Override
    public void clickStartNav(int ints) {
        AMapNavi aMapNavi = mAmapNavHelper.getAMapNavi();
        aMapNavi.selectRouteId(ints);
        mAmapNavHelper.startNav();
        getMvpView().startNavView();
    }
}
