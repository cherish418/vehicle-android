package org.changs.nav.mvp;

import com.amap.api.navi.model.NaviLatLng;
import com.amap.api.services.core.LatLonPoint;
import com.amap.api.services.core.PoiItem;

import org.changs.aplug.base.BasePresenter;
import org.changs.nav.delegate.amap.AmapRouteHelper;

import java.util.ArrayList;

import javax.inject.Inject;

/**
 * Created by yincs on 2017/10/14.
 */

public class PoiListPresenter extends BasePresenter<PoiListContract.V> implements PoiListContract.P {
    private final AmapRouteHelper mAmapRouteHelper;

    @Inject
    public PoiListPresenter(AmapRouteHelper amapRouteHelper) {
        this.mAmapRouteHelper = amapRouteHelper;
    }

    @Override
    public void clickGoHere(PoiItem poiItem) {
        if (isDetachView()) return;
        getMvpView().showLoading().setMessage("路线规划中...")
                .cancelAction(mAmapRouteHelper::cancel);
        LatLonPoint latLonPoint = poiItem.getLatLonPoint();
        mAmapRouteHelper.calculate(new NaviLatLng(latLonPoint.getLatitude(), latLonPoint.getLongitude()),
                new ArrayList<>(), new AmapRouteHelper.AMapNaviListener() {
                    @Override
                    public void onError(int code, String msg) {
                        if (isDetachView()) return;
                        getMvpView().hideLoading();
                    }

                    @Override
                    public void onSuccess(int[] ints) {
                        getMvpView().hideLoading();
                        getMvpView().startRouteView(ints);
                    }
                });
    }
}
