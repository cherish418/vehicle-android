package org.changs.nav.mvp;

import android.content.Context;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import org.changs.nav.R;

import java.util.ArrayList;
import java.util.List;

import timber.log.Timber;

/**
 * Created by yincs on 2017/9/1.
 */

public class SearchHelper {
    public final static QuickSearchEntry[] sQuickSearch = {
            new QuickSearchEntry("回家", R.mipmap.auto_dest_go_home), new QuickSearchEntry("去公司", R.mipmap.auto_dest_go_company), new QuickSearchEntry("收藏的点", R.mipmap.auto_ic_haved_collect_night),
            new QuickSearchEntry("卫生间", R.mipmap.around_icon_toilet), new QuickSearchEntry("加油站", R.mipmap.around_icon_gas_station), new QuickSearchEntry("停车厂", R.mipmap.around_icon_parking_lot),
            new QuickSearchEntry("洗车", R.mipmap.around_icon_car_wash), new QuickSearchEntry("汽车维修", R.mipmap.around_icon_car_repair), new QuickSearchEntry("更多", R.mipmap.around_icon_more)
    };

    public static final List<MoreDataEntry> sMoreDataEntryList = new ArrayList<>();


    public static class QuickSearchEntry {
        public String name;
        public int icon;

        public QuickSearchEntry(String name, int icon) {
            this.name = name;
            this.icon = icon;
        }
    }

    public static class MoreDataEntry {

        public static final int TYPE_TITLE = 1;
        public static final int TYPE_NORMAL = 0;

        int type;//0普通 //1标题
        String name;
        int icon;

        public MoreDataEntry(int type, String name, int icon) {
            this.type = type;
            this.name = name;
            this.icon = icon;
        }

    }

    public static class MoreDataOpt {
        public MoreDataOpt title(String name) {
            sMoreDataEntryList.add(new MoreDataEntry(MoreDataEntry.TYPE_TITLE, name, 0));
            return this;
        }

        public MoreDataOpt normal(String name) {
            sMoreDataEntryList.add(new MoreDataEntry(MoreDataEntry.TYPE_NORMAL, name, 0));
            return this;
        }

        public MoreDataOpt normal(String name, int icon) {
            sMoreDataEntryList.add(new MoreDataEntry(MoreDataEntry.TYPE_NORMAL, name, icon));
            return this;
        }
    }


    static {
        final MoreDataOpt moreDataOpt = new MoreDataOpt();
        moreDataOpt.title("常用")
                .normal("卫生间", R.mipmap.around_icon_car_wash)
                .normal("加油站", R.mipmap.around_icon_car_wash)
                .normal("停车场", R.mipmap.around_icon_car_wash)
                .normal("美食", R.mipmap.around_icon_car_wash)
                .normal("酒店", R.mipmap.around_icon_car_wash)
                .normal("洗车", R.mipmap.around_icon_car_wash)
                .normal("汽车维修", R.mipmap.around_icon_car_wash)
                .normal("汽车保养", R.mipmap.around_icon_car_wash)
                .normal("银行", R.mipmap.around_icon_car_wash)
                .normal("购物", R.mipmap.around_icon_car_wash);
        moreDataOpt.title("出行")
                .normal("加油站").normal("洗车").normal("汽车维修").normal("服务区").normal("4S店")
                .normal("加气站").normal("汽车店").normal("火车站").normal("飞机场").normal("中石化");
        moreDataOpt.title("餐饮")
                .normal("中餐").normal("快餐").normal("火锅").normal("西餐").normal("咖啡厅")
                .normal("美食").normal("川菜").normal("韩国料理").normal("日本料理").normal("小吃");
        moreDataOpt.title("住宿")
                .normal("宾馆").normal("星级酒店").normal("酒店").normal("快捷酒店").normal("三星级")
                .normal("四星级").normal("五星级").normal("汉庭酒店").normal("如家酒店").normal("七天酒店");
        moreDataOpt.title("出游")
                .normal("风景名胜").normal("公园广场").normal("游乐场").normal("动物园").normal("博物馆");
        moreDataOpt.title("购物")
                .normal("超市").normal("商场").normal("药店").normal("便利店").normal("步行街")
                .normal("花店").normal("五金店").normal("家具建材").normal("万达广场").normal("苏宁");
        moreDataOpt.title("娱乐")
                .normal("网吧").normal("KTV").normal("电影院").normal("洗浴").normal("足疗")
                .normal("台球").normal("酒吧").normal("夜店").normal("体育馆").normal("按摩");
        moreDataOpt.title("生活")
                .normal("银行").normal("ATM").normal("医院").normal("药店").normal("卫生间")
                .normal("邮局").normal("快递").normal("美容美发").normal("电讯营业厅").normal("宠物");
    }


    public static class SearchMoreAdapter extends RecyclerView.Adapter {

        private final LayoutInflater mInflater;
        private final OnItemClickListener mListener;

        public SearchMoreAdapter(Context context, OnItemClickListener listener) {
            this.mInflater = LayoutInflater.from(context);
            this.mListener = listener;

        }

        @Override
        public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            if (viewType == MoreDataEntry.TYPE_TITLE) {
                final View view = mInflater.inflate(R.layout.item_search_more_title, parent, false);
                return new ViewHolderTitle(view);
            }
            final View view = mInflater.inflate(R.layout.item_search_more_normal, parent, false);
            return new ViewHolderNormal(view);
        }

        @Override
        public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
            final MoreDataEntry moreDataEntry = sMoreDataEntryList.get(position);
            if (holder instanceof ViewHolderTitle) {
                ((ViewHolderTitle) holder).tvTitle.setText(moreDataEntry.name);
            } else if (holder instanceof ViewHolderNormal) {
                final ViewHolderNormal holderNormal = (ViewHolderNormal) holder;
                holderNormal.tvName.setText(moreDataEntry.name);
                holderNormal.setIcon(moreDataEntry.icon);

                holderNormal.itemView.setOnClickListener(v -> mListener.onItemClick(moreDataEntry.name));
            }
        }

        @Override
        public int getItemCount() {
            return sMoreDataEntryList.size();
        }

        @Override
        public int getItemViewType(int position) {
            return sMoreDataEntryList.get(position).type;
        }

        @Override
        public void onAttachedToRecyclerView(final RecyclerView recyclerView) {
            RecyclerView.LayoutManager layoutManager = recyclerView.getLayoutManager();
            if (layoutManager instanceof GridLayoutManager) {
                final GridLayoutManager gridLayoutManager = (GridLayoutManager) layoutManager;
                final GridLayoutManager.SpanSizeLookup spanSizeLookup = gridLayoutManager.getSpanSizeLookup();
                if (!(spanSizeLookup instanceof GridLayoutManager.DefaultSpanSizeLookup)) return;
                gridLayoutManager.setSpanSizeLookup(new GridLayoutManager.SpanSizeLookup() {
                    @Override
                    public int getSpanSize(int position) {
                        return sMoreDataEntryList.get(position).type == MoreDataEntry.TYPE_TITLE ?
                                gridLayoutManager.getSpanCount() : 1;
                    }
                });
            }
        }
    }

    public interface OnItemClickListener {
        void onItemClick(String name);
    }


    private static class ViewHolderTitle extends RecyclerView.ViewHolder {

        public TextView tvTitle;

        public ViewHolderTitle(View itemView) {
            super(itemView);
            tvTitle = (TextView) itemView.findViewById(R.id.tv_title);
        }
    }

    private static class ViewHolderNormal extends RecyclerView.ViewHolder {

        public TextView tvName;

        private ImageView ivIcon;

        public ViewHolderNormal(View itemView) {
            super(itemView);
            tvName = (TextView) itemView.findViewById(R.id.tv_name);
            ivIcon = (ImageView) itemView.findViewById(R.id.iv_icon);
        }

        public void setIcon(int res) {
            Timber.d("res = " + res);
            if (res == 0) {
                ivIcon.setVisibility(View.GONE);
            } else {
                ivIcon.setVisibility(View.VISIBLE);
                ivIcon.setImageResource(res);
            }
        }


    }
}
