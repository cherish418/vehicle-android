package org.changs.nav.mvp;

import com.amap.api.services.core.PoiItem;

import org.changs.aplug.base.IPresenter;
import org.changs.aplug.base.IView;

import java.util.ArrayList;

/**
 * Created by yincs on 2017/10/14.
 */

public interface PoiListContract {
    interface V extends IView {
        void showPostList(ArrayList<PoiItem> poiItems,int select);

        void startRouteView(int[] ints);
    }

    interface P extends IPresenter<V> {
        void clickGoHere(PoiItem poiItem);
    }

}
