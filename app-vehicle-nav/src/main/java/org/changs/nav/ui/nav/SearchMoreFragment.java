package org.changs.nav.ui.nav;

import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.GridLayoutManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import org.changs.nav.R;
import org.changs.nav.databinding.MainSearchPoiMoreLayoutBinding;
import org.changs.nav.delegate.amap.AmapSearchHelper;
import org.changs.nav.mvp.SearchHelper;

import javax.inject.Inject;

/**
 * Created by 惠安 on 2017/11/23.
 */

public class SearchMoreFragment extends BaseMapFragment {

    public static SearchMoreFragment newInstance() {
//        Bundle args = new Bundle();
        SearchMoreFragment fragment = new SearchMoreFragment();
//        fragment.setArguments(args);
        return fragment;
    }

    private MainSearchPoiMoreLayoutBinding mBinding;
    @Inject AmapSearchHelper mAmapSearchHelper;

    @Override
    public View setup(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        mBinding = DataBindingUtil.inflate(inflater, R.layout.main_search_poi_more_layout, container, false);
        return mBinding.getRoot();
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
//        mBinding.titleBar.baseGoBack.setOnClickListener(v -> getContext().onBackPressed());
        mBinding.rccView.setLayoutManager(new GridLayoutManager(getContext(), 5));
        mBinding.rccView.setAdapter(new SearchHelper.SearchMoreAdapter(getContext(), name -> {
            mAmapSearchHelper.search(this, name, true);
        }));

    }
}
