package org.changs.nav.ui.nav;

import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.text.Editable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import org.changs.aplug.simple.SimpleTextWatcher;
import org.changs.aplug.utils.EmptyUtils;
import org.changs.nav.R;
import org.changs.nav.databinding.RoutePlanSettingLayoutBinding;
import org.changs.nav.event.RouteRefreshEvent;
import org.greenrobot.eventbus.EventBus;

import javax.inject.Inject;

import aplug.data.prefs.PreferencesHelper;

/**
 * Created by yincs on 2017/9/1.
 */

public class RoutePreferFragment extends BaseMapFragment {

    private RoutePlanSettingLayoutBinding mBinding;
    @Inject PreferencesHelper mPreferencesHelper;

    @Override
    public View setup(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        mBinding = DataBindingUtil.inflate(inflater, R.layout.route_plan_setting_layout, container, false);
        mBinding.getRoot().setClickable(true);

        initUI();
        initData();
        return mBinding.getRoot();
    }

    private void initUI() {

        mBinding.btnDone.setOnClickListener(v -> {
            mPreferencesHelper.setCongestion(mBinding.cbCongestion.isChecked());
            mPreferencesHelper.setAvoidCost(mBinding.cbAvoidCost.isChecked());
            mPreferencesHelper.setAvoidSpeed(mBinding.cbAvoidSpeed.isChecked());
            mPreferencesHelper.setHighSpeed(mBinding.cbHightSpeed.isChecked());
            mPreferencesHelper.setVehicleHeightSwitch(mBinding.cbVehicleLoad.isSelected());
            mPreferencesHelper.setVehicleHeight(mBinding.tvVehicleLoad.getText().toString());
            mPreferencesHelper.setCarNumber(mBinding.tvCarNumber.getText().toString());
            mPreferencesHelper.setRestriction(mBinding.cbCarNumber.isSelected());
            EventBus.getDefault().post(new RouteRefreshEvent());
            getContext().onBackPressed();
        });
        mBinding.cbAvoidCost.setOnCheckedChangeListener((buttonView, isChecked) -> {
            if (isChecked) mBinding.cbHightSpeed.setChecked(false);
        });
        mBinding.cbAvoidSpeed.setOnCheckedChangeListener((buttonView, isChecked) -> {
            if (isChecked) mBinding.cbHightSpeed.setChecked(false);
        });
        mBinding.cbHightSpeed.setOnCheckedChangeListener((buttonView, isChecked) -> {
            if (isChecked) {
                mBinding.cbAvoidCost.setChecked(false);
                mBinding.cbAvoidSpeed.setChecked(false);
            }
        });
        mBinding.cbCarNumber.setOnClickListener(v -> {
            if (mBinding.tvCarNumber.getText().length() == 0) {
                editCarNumber();
                return;
            }
            mBinding.cbCarNumber.setSelected(!mBinding.cbCarNumber.isSelected());
        });
        mBinding.btnEditCarNumber.setOnClickListener(v -> editCarNumber());
        mBinding.tvCarNumber.addTextChangedListener(new SimpleTextWatcher() {
            @Override
            public void afterTextChanged(Editable s) {
                mBinding.tvCarNumber.setVisibility(s.length() > 0 ? View.VISIBLE : View.GONE);
            }
        });
        mBinding.cbVehicleLoad.setOnClickListener(v -> {
            if (mBinding.tvVehicleLoad.getText().length() == 0) {
                editVehicleLoad();
                return;
            }
            mBinding.cbVehicleLoad.setSelected(!mBinding.cbVehicleLoad.isSelected());
        });
        mBinding.btnEditCarVehicleLoad.setOnClickListener(v -> editVehicleLoad());
        mBinding.tvVehicleLoad.addTextChangedListener(new SimpleTextWatcher() {
            @Override
            public void afterTextChanged(Editable s) {
                mBinding.tvVehicleLoad.setVisibility(s.length() > 0 ? View.VISIBLE : View.GONE);
            }
        });
    }

    private void editVehicleLoad() {
        TextInputDialog.newInstance(mPreferencesHelper.getVehicleLoad(),
                "请输入车辆的重量,单位为吨(例：1.5)", result ->
                        mBinding.tvVehicleLoad.setText(result))
                .show(getFragmentManager());
    }

    private void editCarNumber() {
        CarNumberEditDialog.newInstance(mBinding.tvCarNumber.getText().toString(),
                result -> mBinding.tvCarNumber.setText(result))
                .show(getFragmentManager());
    }

    private void initData() {
        mBinding.cbCongestion.setChecked(mPreferencesHelper.isCongestion());
        mBinding.cbAvoidCost.setChecked(mPreferencesHelper.isAvoidCost());
        mBinding.cbAvoidSpeed.setChecked(mPreferencesHelper.isAvoidSpeed());
        mBinding.cbHightSpeed.setChecked(mPreferencesHelper.isHighSpeed());
        mBinding.tvCarNumber.setText(mPreferencesHelper.getCarNumber());
        mBinding.cbCarNumber.setSelected(mPreferencesHelper.isRestriction());
        mBinding.cbVehicleLoad.setSelected(mPreferencesHelper.isVehicleLoadSwitch());
        mBinding.tvVehicleLoad.setText(EmptyUtils.isEmpty(mPreferencesHelper.getVehicleLoad()) ?
                "" : mPreferencesHelper.getVehicleLoad() + "吨");
    }


    @Override
    protected boolean getOutsideTouchable() {
        return false;
    }
}
