package org.changs.nav.ui.nav.mapscene;

import android.os.Bundle;
import android.view.View;

import com.amap.api.services.core.PoiItem;

import org.changs.aplug.annotation.FragmentScoped;
import org.changs.nav.ui.nav.MainMapFragment;

import java.util.ArrayList;

import javax.inject.Inject;

import aplug.di.PoiOverlay;

/**
 * Created by yincs on 2017/11/26.
 */
@FragmentScoped
public class PoiListMapScene extends AbstractMapScene {
    private ArrayList<PoiItem> mPoiItems;
    private int mPosition;
    private PoiOverlay mPoiOverlay;

    @Inject
    public PoiListMapScene(MainMapFragment mapFragment) {
        super(mapFragment);
    }

    @Override
    protected void initMapEvent() {
        enableMyLocation(true);
        getAMap().setPointToCenter(mBinding.getRoot().getMeasuredWidth() / 4 * 3, mBinding.getRoot().getMeasuredHeight() / 2);
        getAMap().setOnMapTouchListener(null);
        getAMap().setOnMarkerClickListener(null);
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {

    }

    @Override
    public void showScene() {
        initMapEvent();
        mBinding.btn2d3d.setVisibility(View.VISIBLE);
        if (mPoiOverlay != null) {
            mPoiOverlay.removeFromMap();
            mPoiOverlay = null;
        }
        if (mPoiItems != null) {
            mPoiOverlay = new PoiOverlay(getAMap(), mPoiItems);
            mPoiOverlay.addToMap();
            mPoiOverlay.select(mPosition);
            mPoiOverlay.zoomToSelect();
        }
    }

    @Override
    public void hideScene() {
        if (mPoiOverlay != null) {
            mPoiOverlay.removeFromMap();
            mPoiOverlay = null;
        }
    }

    @Override
    public void removeScene() {
        if (mPoiOverlay != null) {
            mPoiOverlay.removeFromMap();
            mPoiOverlay = null;
        }
        mPoiItems = null;
        mPosition = 0;
    }

    public void setPoiItems(ArrayList<PoiItem> poiItems, int index) {
        this.mPoiItems = poiItems;
        this.mPosition = index;
    }

    public void selectPoiItem(int index) {
        if (mPoiOverlay == null) return;
        mPosition = index;
        mPoiOverlay.select(mPosition);
        mPoiOverlay.zoomToSelect();
    }
}
