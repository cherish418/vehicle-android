package org.changs.nav.ui.set;

import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import org.changs.aplug.interf.OnBackPressedListener;
import org.changs.nav.R;
import org.changs.nav.databinding.MainSettingFragmentBinding;
import org.changs.nav.delegate.MapSwitchDelegate;
import org.changs.nav.ui.nav.BaseMapFragment;
import org.changs.nav.ui.nav.CarNumberEditDialog;

import javax.inject.Inject;

import aplug.data.prefs.PreferencesHelper;

/**
 * Created by yincs on 2017/10/19.
 */

public class SetMainFragment extends BaseMapFragment implements OnBackPressedListener {
    private MainSettingFragmentBinding mBinding;
    @Inject PreferencesHelper mPreferencesHelper;
    @Inject MapSwitchDelegate mMapSwitchDelegate;

    @Override
    public View setup(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        mBinding = DataBindingUtil.inflate(inflater, R.layout.main_setting_fragment, container, false);
        initUI();
        initData();
        return mBinding.getRoot();
    }

    private void initData() {
        mBinding.tvCarNumber.setText(mPreferencesHelper.getCarNumber());
    }

    private void initUI() {
//        mBinding.btnBack.setOnClickListener(v -> {
//            getContext().onBackPressed();
//        });
        mBinding.btnCarInfo.setOnClickListener(v -> {
            CarNumberEditDialog.newInstance(mBinding.tvCarNumber.getText().toString(),
                    result -> {
                        mBinding.tvCarNumber.setText(result);
                        mPreferencesHelper.setCarNumber(result);
                    })
                    .show(getFragmentManager());
        });
        mBinding.btnFavorite.setOnClickListener(v -> {
            getFragmentManager().beginTransaction()
                    .replace(R.id.rl_container, new CollectFragment())
                    .addToBackStack(null)
                    .commitAllowingStateLoss();
        });
        mBinding.btnCarPhone2.setOnClickListener(v -> {
            getFragmentManager().beginTransaction()
                    .replace(R.id.rl_container, new PhoneFragment())
                    .addToBackStack(null)
                    .commitAllowingStateLoss();
        });
        mBinding.btnCarPhone.setOnClickListener(v -> {
            getFragmentManager().beginTransaction()
                    .replace(R.id.rl_container, new PhoneFragment())
                    .addToBackStack(null)
                    .commitAllowingStateLoss();
        });
        mBinding.btnOfflineMap.setOnClickListener(v -> {
            getFragmentManager().beginTransaction()
                    .replace(R.id.rl_container, new OfflineMapFragment())
                    .addToBackStack(null)
                    .commitAllowingStateLoss();
        });
        mBinding.btnSetting.setOnClickListener(v -> {
            getFragmentManager().beginTransaction()
                    .replace(R.id.rl_container, new SetPrefFragment())
                    .addToBackStack(null)
                    .commitAllowingStateLoss();
        });
    }

    @Override
    public boolean onBackPressed() {
        mMapSwitchDelegate.switch2Basics();
        return false;
    }
}
