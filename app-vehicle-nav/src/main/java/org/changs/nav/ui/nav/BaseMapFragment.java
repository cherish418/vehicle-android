package org.changs.nav.ui.nav;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;

import org.changs.aplug.base.BaseFragment;
import org.changs.nav.R;

/**
 * Created by yincs on 2017/11/26.
 */

public abstract class BaseMapFragment extends BaseFragment {


    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        if (getOutsideTouchable()) {
            FrameLayout rootView = (FrameLayout) inflater.inflate(R.layout.map_base_layout, container, false);
            View view = super.onCreateView(inflater, rootView, savedInstanceState);
            rootView.addView(view);
            rootView.setOnClickListener(v -> getContext().onBackPressed());
            return rootView;
        } else {
            return super.onCreateView(inflater, container, savedInstanceState);
        }

    }

    @Override
    public View setup(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return null;
    }

    public void startFragment(Fragment show, String backStackTag) {
        getFragmentManager().beginTransaction()
                .replace(R.id.rl_container, show)
                .addToBackStack(backStackTag)
                .commitAllowingStateLoss();
    }

    public void startFragment(Fragment hide, Fragment show) {
        getFragmentManager().beginTransaction()
                .add(R.id.rl_container, show)
                .hide(hide)
                .addToBackStack(null)
                .commitAllowingStateLoss();
    }

    public void startFragment(Fragment show, String tag, String backStackTag) {
        getFragmentManager().beginTransaction()
                .add(R.id.rl_container, show, tag)
                .addToBackStack(backStackTag)
                .commitAllowingStateLoss();
    }

    @Override
    public void onHiddenChanged(boolean hidden) {
        super.onHiddenChanged(hidden);
        if (hidden) onPause();
        else onResume();
    }

    /**
     * @return true 点击外部界面会自动调onBackPress();
     */
    protected boolean getOutsideTouchable() {
        return true;
    }
}
