package org.changs.nav.ui.nav.mapscene;

import android.location.Location;
import android.os.Bundle;
import android.view.View;

import com.amap.api.maps.model.LatLng;
import com.amap.api.navi.model.AMapNaviPath;
import com.amap.api.navi.model.NaviLatLng;

import org.changs.aplug.annotation.FragmentScoped;
import org.changs.nav.delegate.amap.AmapRouteHelper;
import org.changs.nav.delegate.amap.RouteOverlayOpt;
import org.changs.nav.ui.nav.MainMapFragment;

import java.util.ArrayList;

import javax.inject.Inject;

/**
 * Created by yincs on 2017/11/26.
 */
@FragmentScoped
public class RouteListMapScene extends AbstractMapScene {
    private RouteOverlayOpt mRouteOverlayOpt;
    private int[] mInts;
    private int mPosition;

    @Inject
    public RouteListMapScene(MainMapFragment mapFragment) {
        super(mapFragment);
    }

    @Override
    protected void initMapEvent() {
        enableMyLocation(false);
        getAMap().setPointToCenter(mBinding.getRoot().getMeasuredWidth() / 4 * 3, mBinding.getRoot().getMeasuredHeight() / 2);
        getAMap().setOnMapTouchListener(null);
        getAMap().setOnMarkerClickListener(null);
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {

    }

    @Override
    public void showScene() {
        initMapEvent();
        mBinding.btn2d3d.setVisibility(View.GONE);

        if (mRouteOverlayOpt != null) {
            mRouteOverlayOpt.clear();
            mRouteOverlayOpt = null;
        }
        if (mInts != null) {
            Location location = getAMap().getMyLocation();
            if (location == null) {
                return;
            }
            ArrayList<AMapNaviPath> list = AmapRouteHelper.getAMapNaviPaths(getContext(), mInts);
            if (list == null) {
                return;
            }
            mRouteOverlayOpt = new RouteOverlayOpt(getContext(), getAMap());
            mRouteOverlayOpt.addRoute(mInts, list);
            NaviLatLng endPoint = list.get(0).getEndPoint();

            mRouteOverlayOpt.zoomToSpan(new LatLng(location.getLatitude(), location.getLongitude()),
                    new LatLng(endPoint.getLatitude(), endPoint.getLongitude()), mBinding.getRoot());
            mRouteOverlayOpt.selectRoute(mPosition);
            getView().hideLoading();
        }
    }

    @Override
    public void hideScene() {
        if (mRouteOverlayOpt != null) {
            mRouteOverlayOpt.clear();
            mRouteOverlayOpt = null;
        }
    }

    @Override
    public void removeScene() {
        this.mInts = null;
        this.mPosition = 0;

        if (mRouteOverlayOpt != null) {
            mRouteOverlayOpt.clear();
            mRouteOverlayOpt = null;
        }
    }

    public void setRouteInts(int[] ints, int position) {
        this.mInts = ints;
        this.mPosition = position;
    }

    public void selectRoute(int index) {
        if (mRouteOverlayOpt == null) return;
        mPosition = index;
        mRouteOverlayOpt.selectRoute(index);
    }
}
