package org.changs.nav.ui;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;

import org.changs.aplug.ActivityManager;
import org.changs.nav.ui.nav.MainActivity;

/**
 * Created by yincs on 2017/12/5.
 */

public class SplashActivity extends Activity {
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Activity preActivity = ActivityManager.getInstance().getPreActivity();
        if (preActivity == null) {
            Intent intent = new Intent(this, MainActivity.class);
            intent.replaceExtras(getIntent());
            startActivity(intent);
        } else {
            startActivity(new Intent(this, preActivity.getClass()));
        }
        overridePendingTransition(0, 0);
        finish();

    }
}
