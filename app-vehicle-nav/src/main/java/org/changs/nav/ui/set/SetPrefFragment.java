package org.changs.nav.ui.set;

import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CompoundButton;

import org.changs.aplug.utils.EmptyUtils;
import org.changs.nav.R;
import org.changs.nav.databinding.DialogSettingBinding;
import org.changs.nav.ui.nav.BaseMapFragment;
import org.changs.nav.ui.nav.CarNumberEditDialog;

import javax.inject.Inject;

import aplug.data.prefs.PreferencesHelper;

/**
 * Created by yincs on 2017/9/7.
 */

public class SetPrefFragment extends BaseMapFragment {

    private DialogSettingBinding mBinding;
    @Inject PreferencesHelper mPreferencesHelper;

    @Override
    public View setup(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        mBinding = DataBindingUtil.inflate(inflater, R.layout.dialog_setting, container, false);
        initUI();
        return mBinding.getRoot();
    }

    private void initUI() {

        mBinding.btnEdit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startCarNumberView();

            }
        });
        mBinding.btnDelete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mPreferencesHelper.setCarNumber(null);
                setupCarNumberUI();
            }
        });
        mBinding.tbtnOn.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                mPreferencesHelper.setRestriction(isChecked);
            }
        });

        mBinding.cbElude.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                mPreferencesHelper.setCongestion(isChecked);
            }
        });
        mBinding.cbFree.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked && mBinding.cbGaosu.isChecked()) mBinding.cbGaosu.setChecked(false);
                mPreferencesHelper.setAvoidCost(isChecked);
            }
        });
        mBinding.cbNo.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked && mBinding.cbGaosu.isChecked()) mBinding.cbGaosu.setChecked(false);
                mPreferencesHelper.setAvoidSpeed(isChecked);
            }
        });
        mBinding.cbGaosu.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked && mBinding.cbFree.isChecked()) mBinding.cbFree.setChecked(false);
                if (isChecked && mBinding.cbNo.isChecked()) mBinding.cbNo.setChecked(false);
                mPreferencesHelper.setHighSpeed(isChecked);
            }
        });
    }

    private void startCarNumberView() {
        CarNumberEditDialog.newInstance(mBinding.tvCarNumber.getText().toString(),
                result -> {
                    mPreferencesHelper.setCarNumber(result);
                    setupCarNumberUI();
                })
                .show(getFragmentManager());
    }

    @Override
    public void onResume() {
        super.onResume();
        mBinding.cbElude.setChecked(mPreferencesHelper.isCongestion());
        mBinding.cbFree.setChecked(mPreferencesHelper.isAvoidCost());
        mBinding.cbNo.setChecked(mPreferencesHelper.isAvoidSpeed());
        mBinding.cbGaosu.setChecked(mPreferencesHelper.isHighSpeed());
        setupCarNumberUI();
    }

    private void setupCarNumberUI() {
        final String carNumber = mPreferencesHelper.getCarNumber();
        if (EmptyUtils.isEmpty(carNumber)) {
            mBinding.tvCarNumber.setText("");
            mBinding.tvCarNumber.setHint("点击输入车牌号");
            mBinding.tbtnOn.setChecked(false);
            mBinding.tbtnOn.setEnabled(false);
            mBinding.btnEdit.setVisibility(View.GONE);
            mBinding.btnDelete.setVisibility(View.GONE);
            mBinding.llNumber.setOnClickListener(v -> startCarNumberView());
        } else {
            mBinding.tbtnOn.setChecked(mPreferencesHelper.isRestriction());
            mBinding.tbtnOn.setEnabled(true);
            mBinding.llNumber.setOnClickListener(null);
            mBinding.tvCarNumber.setText(carNumber);
            mBinding.btnEdit.setVisibility(View.VISIBLE);
            mBinding.btnDelete.setVisibility(View.VISIBLE);
        }
    }

}
