package org.changs.nav.ui.nav;


import android.content.Context;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.os.Bundle;

import com.amap.api.col.fv;
import com.amap.api.navi.AmapNaviPage;
import com.amap.api.navi.AmapNaviParams;
import com.amap.api.navi.AmapRouteActivity;
import com.amap.api.navi.INaviInfoCallback;
import com.amap.api.navi.model.AMapNaviLocation;

import org.changs.nav.delegate.amap.AmapTTSController;

import timber.log.Timber;

/**
 * Created by yincs on 2017/7/28.
 */

public class MapNavActivity extends AmapRouteActivity implements INaviInfoCallback {

    public static void start(Context context) {
        Intent starter = new Intent(context, MapNavActivity.class);
        context.startActivity(starter);
    }

    AmapTTSController amapTTSController;

    @Override
    protected void onCreate(Bundle bundle) {
        setCallBack(this);
        amapTTSController = AmapTTSController.getInstance(getApplicationContext());
        amapTTSController.init();

        super.onCreate(bundle);
        this.newScr(new fv(2, bundle));
    }

    @Override
    public void setRequestedOrientation(int requestedOrientation) {
        super.setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE);
    }

    private void setCallBack(INaviInfoCallback callBack) {
        AmapNaviPage.getInstance().showRouteActivity(this, new AmapNaviParams(null), callBack);
    }

    @Override
    public void startActivity(Intent intent) {
        if (intent.getComponent().getClassName().equals(AmapRouteActivity.class.getName()))
            return;
        super.startActivity(intent);
    }

    public void newScr(fv var1) {
        if (var1.a == 2) {
            super.newScr(var1);
        }
    }

    @Override
    public void onInitNaviFailure() {

    }

    @Override
    public void onGetNavigationText(String s) {
        amapTTSController.onGetNavigationText(s);
    }

    @Override
    public void onLocationChange(AMapNaviLocation aMapNaviLocation) {

    }

    @Override
    public void onArriveDestination(boolean b) {

    }

    @Override
    public void onStartNavi(int i) {

    }

    @Override
    public void onCalculateRouteSuccess(int[] ints) {

    }

    @Override
    public void onCalculateRouteFailure(int i) {

    }

    @Override
    public void onStopSpeaking() {
        amapTTSController.stopSpeaking();
    }


    @Override
    protected void onResume() {
        super.onResume();
        Timber.d("onResume() called with: " + "");
    }



    @Override
    protected void onPause() {
        super.onPause();
        Timber.d("onPause() called with: " + "");
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        amapTTSController.destroy();
        setCallBack(null);
        Timber.d("onDestroy() called with: " + "");

    }

}
