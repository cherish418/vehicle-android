package org.changs.nav.ui.nav.mapscene;

/**
 * Created by yincs on 2017/11/26.
 */

import android.os.Bundle;
import android.support.annotation.CallSuper;
import android.support.v7.app.AppCompatActivity;

import com.amap.api.maps.AMap;
import com.amap.api.maps.model.MyLocationStyle;

import org.changs.aplug.base.IView;
import org.changs.nav.databinding.MainMapFragmentBinding;
import org.changs.nav.ui.nav.MainMapFragment;

/**
 * 地图的场景
 */
public abstract class AbstractMapScene {

    protected MainMapFragment mMapFragment;
    protected MainMapFragmentBinding mBinding;

    public AbstractMapScene(MainMapFragment mapFragment) {
        this.mMapFragment = mapFragment;
    }

    public IView getView() {
        return mMapFragment;
    }

    public AMap getAMap() {
        return mMapFragment.getAMap();
    }

    public AppCompatActivity getContext() {
        return mMapFragment.getContext();
    }

    /**
     * 初始化地图的事件，在不同的场景，地图的事件不一样
     */
    protected abstract void initMapEvent();

    @CallSuper
    public void onCreated(Bundle savedInstanceState) {
        mBinding = mMapFragment.getBinding();
    }

    public abstract void onSaveInstanceState(Bundle outState);


    /**
     * 显示场景
     */
    public abstract void showScene();

    /**
     * 隐藏场景
     */
    public abstract void hideScene();

    /**
     * 移除场景、移除变量
     */
    public abstract void removeScene();


    protected void enableMyLocation(boolean enable) {
        if (enable) {
            getAMap().setMyLocationEnabled(true);
            MyLocationStyle locationStyle = getAMap().getMyLocationStyle();
            if (locationStyle == null) locationStyle = new MyLocationStyle();
            locationStyle.myLocationType(MyLocationStyle.LOCATION_TYPE_FOLLOW_NO_CENTER);
            getAMap().setMyLocationStyle(locationStyle);
        } else {
            getAMap().setMyLocationEnabled(false);
        }

    }
}
