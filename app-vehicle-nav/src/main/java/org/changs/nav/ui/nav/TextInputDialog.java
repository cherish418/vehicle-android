package org.changs.nav.ui.nav;

import android.app.Dialog;
import android.databinding.DataBindingUtil;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.text.Editable;
import android.view.View;
import android.view.ViewGroup;

import org.changs.aplug.base.BaseDialog;
import org.changs.aplug.interf.Action1;
import org.changs.aplug.simple.SimpleTextWatcher;
import org.changs.nav.R;
import org.changs.nav.databinding.MainTextInputFragmentBinding;

/**
 * Created by yincs on 2017/10/17.
 */

public class TextInputDialog extends BaseDialog {

    private static final String TAG_TEXT = "TAG_TEXT";
    private static final String TAG_HINT = "TAG_HINT";
    private Action1<String> mDoneAction;

    public static TextInputDialog newInstance(String text, String hint, Action1<String> doneAction) {
        Bundle args = new Bundle();
        args.putString(TAG_TEXT, text);
        args.putString(TAG_HINT, hint);
        TextInputDialog fragment = new TextInputDialog();
        fragment.setArguments(args);
        fragment.mDoneAction = doneAction;
        return fragment;
    }


    private MainTextInputFragmentBinding mBinding;

    @NonNull
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        final Dialog dialog = new Dialog(getContext());
        mBinding = DataBindingUtil.inflate(getContext().getLayoutInflater(), R.layout.main_text_input_fragment, null, false);
        dialog.setContentView(mBinding.getRoot());
        if (dialog.getWindow() != null) {
            dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
            dialog.getWindow().setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT);
        }
        dialog.setCanceledOnTouchOutside(false);
        initUI();
        initData();
        return dialog;
    }

    @Override
    public void onResume() {
        super.onResume();
        if (mDoneAction == null && !getContext().isFinishing())
            dismiss();
    }

    private void initData() {
        if (getArguments() == null) return;
        String text = getArguments().getString(TAG_TEXT);
        String hint = getArguments().getString(TAG_HINT);
        if (hint != null) mBinding.etContent.setHint(hint);
        if (text != null) {
            mBinding.etContent.setText(text);
            mBinding.etContent.setSelection(text.length());
        }
    }

    private void initUI() {

        mBinding.btnDone.setOnClickListener(v -> {
            if (mDoneAction != null) mDoneAction.call(mBinding.etContent.getText().toString());
            getContext().onBackPressed();
        });
        mBinding.etContent.addTextChangedListener(new SimpleTextWatcher() {
            @Override
            public void afterTextChanged(Editable s) {
                mBinding.btnClear.setVisibility(s.length() > 0 ? View.VISIBLE : View.GONE);
            }
        });
        mBinding.btnClear.setOnClickListener(v -> mBinding.etContent.setText(""));
    }

}
