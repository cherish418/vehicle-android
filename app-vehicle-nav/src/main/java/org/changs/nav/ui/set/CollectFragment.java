package org.changs.nav.ui.set;

/**
 * Created by 惠安 on 2017/11/23.
 */

import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.LinearLayoutManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import org.changs.aplug.widget.irecyclerview.SimpleAdapter;
import org.changs.nav.R;
import org.changs.nav.databinding.CollectItemBinding;
import org.changs.nav.databinding.CollectListBinding;
import org.changs.nav.delegate.MapSwitchDelegate;
import org.changs.nav.delegate.amap.AmapRouteHelper;
import org.changs.nav.ui.nav.BaseMapFragment;
import org.changs.nav.ui.nav.CollectDetailFragment;

import javax.inject.Inject;

import aplug.data.db.CollectInfoHelper;
import aplug.data.db.mode.CollectInfo;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.schedulers.Schedulers;

/**
 * 收藏夹
 */
public class CollectFragment extends BaseMapFragment {

    private CollectListBinding mBinding;
    private CollectAdapter mCollectAdapter;
    @Inject CollectInfoHelper mCollectInfoHelper;
    @Inject MapSwitchDelegate mMapSwitchDelegate;
    @Inject AmapRouteHelper mAmapRouteHelper;

    @Override
    public String getTitle() {
        return "收藏列表";
    }

    @Override
    public View setup(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        mBinding = DataBindingUtil.inflate(inflater, R.layout.collect_list, container, false);
        return mBinding.getRoot();
    }

    @Override
    public void onResume() {
        super.onResume();
        setupCollectData();
    }

    private void setupCollectData() {
        if (mCollectAdapter == null) {
            mCollectAdapter = new CollectAdapter();
            mCollectAdapter.setOnItemClickListener((view, position) -> {
                CollectInfo collectInfo = mCollectAdapter.getData().get(position);
                if (!collectInfo.isValid()) {
                    startFragment(this, CollectAddFragment.newInstance(collectInfo.getId().intValue()));
                } else {
                    startFragment(this, CollectDetailFragment.newInstance(collectInfo));
                }
            });
            mBinding.rccView.setLayoutManager(new LinearLayoutManager(getContext()));
            mBinding.rccView.setAdapter(mCollectAdapter);
        } else {
            mCollectAdapter.getData().clear();
            mCollectAdapter.notifyDataSetChanged();
        }
        mCollectInfoHelper.findAll()
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(collects -> {
                    mCollectAdapter.getData().addAll(collects);
                    mCollectAdapter.notifyDataSetChanged();
                });
    }


    public static class CollectAdapter extends SimpleAdapter<CollectInfo, CollectItemBinding> {

        public CollectAdapter() {
            super(R.layout.collect_item);
        }

        @Override
        protected void onBindViewHolder(CollectItemBinding binding, CollectInfo collectInfo, int position) {
            super.onBindViewHolder(binding, collectInfo, position);
            binding.tvName.setText(collectInfo.getName());
            binding.tvAddress.setText(collectInfo.isValid() ? collectInfo.getAddress() : "点击设置");
        }
    }

}
