package org.changs.nav.ui.nav;

import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.amap.api.maps.model.LatLng;

import org.changs.nav.R;
import org.changs.nav.databinding.CollectDetailFragmentBinding;
import org.changs.nav.delegate.MapSwitchDelegate;
import org.changs.nav.delegate.amap.AmapRouteHelper;

import javax.inject.Inject;

import aplug.data.db.CollectInfoHelper;
import aplug.data.db.mode.CollectInfo;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.functions.Consumer;
import io.reactivex.schedulers.Schedulers;

/**
 * Created by 惠安 on 2017/11/24.
 */

public class CollectDetailFragment extends BaseMapFragment {

    private static final String TAG_POI_DETAIL = "TAG_POI_DETAIL";
    private static final String TAG_IS_COLLECTED = "TAG_IS_COLLECTED";

    public static CollectDetailFragment newInstance(CollectInfo detail) {
        Bundle args = new Bundle();
        args.putSerializable(TAG_POI_DETAIL, detail);
        CollectDetailFragment fragment = new CollectDetailFragment();
        fragment.setArguments(args);
        return fragment;
    }

    private CollectDetailFragmentBinding mBinding;
    private CollectInfo mCollectInfo;
    private boolean mIsCollected;
    @Inject MapSwitchDelegate mMapSwitchDelegate;
    @Inject CollectInfoHelper mCollectInfoHelper;
    @Inject AmapRouteHelper mAmapRouteHelper;

    @Override
    public View setup(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        mBinding = DataBindingUtil.inflate(inflater, R.layout.collect_detail_fragment, container, false);
        return mBinding.getRoot();
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        if (savedInstanceState != null) {
            mCollectInfo = (CollectInfo) savedInstanceState.getSerializable(TAG_POI_DETAIL);
            mIsCollected = savedInstanceState.getBoolean(TAG_IS_COLLECTED);
        } else {
            mCollectInfo = (CollectInfo) getArguments().getSerializable(TAG_POI_DETAIL);
            mIsCollected = true;
        }
        assert mCollectInfo != null;
        mBinding.tvName.setText(mCollectInfo.getName());
        mBinding.tvAddress.setText(mCollectInfo.getAddress());

        setCollectState(mIsCollected);
        mBinding.btnCollect.setOnClickListener(v -> {
            mBinding.btnCollect.setEnabled(false);
            if (!mIsCollected) {
                mCollectInfoHelper.insert(mCollectInfo)
                        .subscribeOn(Schedulers.io())
                        .observeOn(AndroidSchedulers.mainThread())
                        .subscribe(new Consumer<Long>() {
                            @Override
                            public void accept(Long aLong) throws Exception {
                                mMapSwitchDelegate.addCollectMarket(aLong, new LatLng(mCollectInfo.getLat(), mCollectInfo.getLon()));
                                mBinding.btnCollect.setEnabled(true);
                                setCollectState(true);
                                mIsCollected = true;
                            }
                        });
            } else {
                mCollectInfoHelper.delete(mCollectInfo)
                        .subscribeOn(Schedulers.io())
                        .observeOn(AndroidSchedulers.mainThread())
                        .subscribe(new Consumer<Boolean>() {
                            @Override
                            public void accept(Boolean aBoolean) throws Exception {
                                mMapSwitchDelegate.remoteCollectMarket(mCollectInfo.getId());
                                mBinding.btnCollect.setEnabled(true);
                                setCollectState(false);
                                mIsCollected = false;
                            }
                        });
            }
        });

        mBinding.btnGoHere.setOnClickListener(v -> {
            mAmapRouteHelper.calculate(this, mCollectInfo.getName(), mCollectInfo.getAddress(),
                    mCollectInfo.getLat(), mCollectInfo.getLon());
        });
    }

    private void setCollectState(boolean isCollect) {
        mBinding.ivCollect.setImageResource(isCollect ? R.mipmap.search_tip_collected : R.mipmap.search_tip_collect);
        mBinding.tvCollect.setText(isCollect ? "已收藏" : "收藏");
    }


    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putSerializable(TAG_POI_DETAIL, mCollectInfo);
    }

    @Override
    protected boolean getOutsideTouchable() {
        return false;
    }

    @Override
    public void onResume() {
        super.onResume();
        mMapSwitchDelegate.switch2CollectPoint(new LatLng(mCollectInfo.getLat(), mCollectInfo.getLon()));
    }
}
