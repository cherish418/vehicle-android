package org.changs.nav.ui.nav.mapscene;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.ViewGroup;

import com.amap.api.maps.CameraUpdateFactory;
import com.amap.api.maps.model.BitmapDescriptorFactory;
import com.amap.api.maps.model.LatLng;
import com.amap.api.maps.model.Marker;
import com.amap.api.maps.model.MarkerOptions;

import org.changs.aplug.annotation.FragmentScoped;
import org.changs.nav.R;
import org.changs.nav.ui.nav.MainMapFragment;

import javax.inject.Inject;

/**
 * Created by yincs on 2017/11/26.
 */

@FragmentScoped
public class CollectMapScene extends AbstractMapScene {

    private Marker mCollectMarket;
    private MarkerOptions mMarkerOptions;
    private LatLng mLatLng;

    @Inject
    public CollectMapScene(MainMapFragment mapFragment) {
        super(mapFragment);
    }

    @Override
    protected void initMapEvent() {
        enableMyLocation(true);
        getAMap().setPointToCenter(mBinding.getRoot().getMeasuredWidth() / 4 * 3, mBinding.getRoot().getMeasuredHeight() / 2);
        getAMap().setOnMapTouchListener(null);
        getAMap().setOnMarkerClickListener(null);
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {

    }

    @Override
    public void showScene() {
        initMapEvent();
        if (mCollectMarket != null) {
            mCollectMarket.remove();
            mCollectMarket = null;
        }
        if (mMarkerOptions == null) {
            mMarkerOptions = new MarkerOptions().icon(BitmapDescriptorFactory.fromView(LayoutInflater.from(getContext())
                    .inflate(R.layout.amap_marker_center_icon, (ViewGroup) mBinding.getRoot(), false)));
        }
        if (mLatLng != null) {
            mCollectMarket = getAMap().addMarker(mMarkerOptions.position(mLatLng));
            getAMap().animateCamera(CameraUpdateFactory.newLatLngZoom(mLatLng, 15));
        }
    }

    @Override
    public void hideScene() {
        if (mCollectMarket != null) {
            mCollectMarket.remove();
            mCollectMarket = null;
        }
    }

    @Override
    public void removeScene() {
        if (mCollectMarket != null) {
            mCollectMarket.remove();
            mCollectMarket = null;
        }
    }

    public void setLocation(LatLng latLng) {
        mLatLng = latLng;
    }
}
