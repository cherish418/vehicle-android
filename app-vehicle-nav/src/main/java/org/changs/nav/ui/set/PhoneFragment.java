package org.changs.nav.ui.set;

import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import org.changs.nav.R;
import org.changs.nav.databinding.MainCarPhoneFragmentBinding;
import org.changs.nav.ui.nav.BaseMapFragment;

/**
 * Created by yincs on 2017/9/7.
 */

public class PhoneFragment extends BaseMapFragment {


    private MainCarPhoneFragmentBinding mBinding;

    @Override
    public View setup(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        mBinding = DataBindingUtil.inflate(inflater, R.layout.main_car_phone_fragment, container, false);
        initUI();
        return mBinding.getRoot();
    }

    private void initUI() {

    }

    @Override
    public void onResume() {
        super.onResume();
//        if (remoteIp == null) {
        mBinding.vNoPhone.setVisibility(View.VISIBLE);
        mBinding.vHasPhone.setVisibility(View.GONE);
//        } else {
//            mBinding.vNoPhone.setVisibility(View.GONE);
//            mBinding.vHasPhone.setVisibility(View.VISIBLE);
//    }
    }
}
