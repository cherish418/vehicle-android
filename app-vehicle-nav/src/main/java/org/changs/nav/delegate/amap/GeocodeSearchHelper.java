package org.changs.nav.delegate.amap;

import android.content.Context;

import com.amap.api.services.core.LatLonPoint;
import com.amap.api.services.geocoder.GeocodeSearch;
import com.amap.api.services.geocoder.RegeocodeAddress;
import com.amap.api.services.geocoder.RegeocodeQuery;

import javax.inject.Inject;
import javax.inject.Singleton;

import io.reactivex.Observable;
import io.reactivex.ObservableEmitter;
import io.reactivex.ObservableOnSubscribe;

/**
 * Created by yincs on 2017/11/25.
 */

@Singleton
public class GeocodeSearchHelper {
    private GeocodeSearch mGeocodeSearch;

    @Inject
    public GeocodeSearchHelper(Context context) {
        mGeocodeSearch = new GeocodeSearch(context);
    }

    public Observable<RegeocodeAddress> getFromLocation(double lat, double lon) {
        return Observable.create(new ObservableOnSubscribe<RegeocodeAddress>() {
            @Override
            public void subscribe(ObservableEmitter<RegeocodeAddress> emitter) throws Exception {
                // 第一个参数表示一个Latlng，第二参数表示范围多少米，第三个参数表示是火系坐标系还是GPS原生坐标系
                try {
                    RegeocodeAddress regeocodeAddress = mGeocodeSearch.getFromLocation(new RegeocodeQuery(new LatLonPoint(lat, lon), 200, GeocodeSearch.AMAP));
                    emitter.onNext(regeocodeAddress);
                } catch (Exception e) {
                    emitter.onError(e);
                }
            }
        });
    }

}
