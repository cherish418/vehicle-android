package org.changs.nav.delegate.amap.domain;

import android.os.Parcel;
import android.os.Parcelable;

import aplug.data.db.mode.CollectInfo;

public class PoiDetail implements Parcelable {
    public String name;
    public String address;
    public double lat;
    public double lon;
    public boolean isCollect;

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.name);
        dest.writeString(this.address);
        dest.writeDouble(this.lat);
        dest.writeDouble(this.lon);
        dest.writeByte(this.isCollect ? (byte) 1 : (byte) 0);
    }

    public PoiDetail() {
    }

    protected PoiDetail(Parcel in) {
        this.name = in.readString();
        this.address = in.readString();
        this.lat = in.readDouble();
        this.lon = in.readDouble();
        this.isCollect = in.readByte() != 0;
    }

    public static final Parcelable.Creator<PoiDetail> CREATOR = new Parcelable.Creator<PoiDetail>() {
        @Override
        public PoiDetail createFromParcel(Parcel source) {
            return new PoiDetail(source);
        }

        @Override
        public PoiDetail[] newArray(int size) {
            return new PoiDetail[size];
        }
    };

    public static PoiDetail parse(CollectInfo collectInfo) {
        PoiDetail poiDetail = new PoiDetail();
        poiDetail.name = collectInfo.getName();
        poiDetail.address = collectInfo.getAddress();
        poiDetail.lat = collectInfo.getLat();
        poiDetail.lon = collectInfo.getLon();
        return poiDetail;
    }
}