package org.changs.nav.delegate.amap.domain;

import android.support.annotation.Keep;

import com.amap.api.location.AMapLocation;

/**
 * Created by yincs on 2017/10/16.
 */

@Keep
public class SimpleLocation {

    public static SimpleLocation fromAmapLocation(AMapLocation location) {
        SimpleLocation simpleLocation = new SimpleLocation();
        simpleLocation.setLatitude(location.getLatitude());
        simpleLocation.setLongitude(location.getLongitude());
        simpleLocation.setAccuracy(location.getAccuracy());
        simpleLocation.setCountry(location.getCountry());
        simpleLocation.setProvince(location.getProvince());
        simpleLocation.setCity(location.getCity());
        simpleLocation.setDistrict(location.getDistrict());
        simpleLocation.setAddress(location.getAddress());
        simpleLocation.setTime(location.getTime());
        return simpleLocation;
    }

    /**
     * 纬度
     */
    private Double latitude;
    /**
     * 经度
     */
    private Double longitude;
    /**
     * 精度
     */
    private float accuracy;
    /**
     * 国家
     */
    private String country;
    /**
     * 省
     */
    private String province;
    /**
     * 市
     */
    private String city;
    /**
     * 区县
     */
    private String district;

    /**
     * 地址
     */
    private String address;

    /**
     * 定位时间
     */
    private long time;

    public SimpleLocation() {
    }

    public Double getLatitude() {
        return latitude;
    }

    public void setLatitude(Double latitude) {
        this.latitude = latitude;
    }

    public Double getLongitude() {
        return longitude;
    }

    public void setLongitude(Double longitude) {
        this.longitude = longitude;
    }

    public float getAccuracy() {
        return accuracy;
    }

    public void setAccuracy(float accuracy) {
        this.accuracy = accuracy;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public String getProvince() {
        return province;
    }

    public void setProvince(String province) {
        this.province = province;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getDistrict() {
        return district;
    }

    public void setDistrict(String district) {
        this.district = district;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public long getTime() {
        return time;
    }

    public void setTime(long time) {
        this.time = time;
    }

    @Override
    public String toString() {
        return "SimpleLocation{" +
                "latitude=" + latitude +
                ", longitude=" + longitude +
                ", accuracy=" + accuracy +
                ", country='" + country + '\'' +
                ", province='" + province + '\'' +
                ", city='" + city + '\'' +
                ", district='" + district + '\'' +
                ", address='" + address + '\'' +
                ", time=" + time +
                '}';
    }
}
