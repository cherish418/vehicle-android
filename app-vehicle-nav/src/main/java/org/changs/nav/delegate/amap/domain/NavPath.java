package org.changs.nav.delegate.amap.domain;

import android.support.annotation.Keep;

import java.util.ArrayList;

/**
 * Created by yincs on 2017/10/14.
 */

@Keep
public class NavPath {
    private PoiPoint endPoi;
    private ArrayList<PoiPoint> ways;
    private int step;

    public NavPath() {

    }

    public PoiPoint getEndPoi() {
        return endPoi;
    }

    public void setEndPoi(PoiPoint endPoi) {
        this.endPoi = endPoi;
    }

    public ArrayList<PoiPoint> getWays() {
        return ways;
    }

    public void setWays(ArrayList<PoiPoint> ways) {
        this.ways = ways;
    }

    public int getStep() {
        return step;
    }

    public void setStep(int step) {
        this.step = step;
    }
}
