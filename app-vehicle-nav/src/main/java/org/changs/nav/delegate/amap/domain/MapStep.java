package org.changs.nav.delegate.amap.domain;

import android.os.Parcel;
import android.os.Parcelable;

import com.amap.api.maps.model.CameraPosition;

/**
 * Created by 惠安 on 2017/11/24.
 */

public class MapStep implements Parcelable {
    public CameraPosition mCameraPosition;

    public void updateCameraPosition(CameraPosition cameraPosition) {
        this.mCameraPosition = cameraPosition;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeParcelable(this.mCameraPosition, flags);
    }

    public MapStep() {
    }

    protected MapStep(Parcel in) {
        this.mCameraPosition = in.readParcelable(CameraPosition.class.getClassLoader());
    }

    public static final Parcelable.Creator<MapStep> CREATOR = new Parcelable.Creator<MapStep>() {
        @Override
        public MapStep createFromParcel(Parcel source) {
            return new MapStep(source);
        }

        @Override
        public MapStep[] newArray(int size) {
            return new MapStep[size];
        }
    };
}
