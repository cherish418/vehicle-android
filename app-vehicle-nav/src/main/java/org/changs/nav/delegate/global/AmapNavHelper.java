package org.changs.nav.delegate.global;

import android.content.Context;
import android.content.SharedPreferences;

import com.amap.api.navi.AMapNavi;

import org.changs.aplug.simple.entry.OnTrimMemoryEvent;
import org.changs.aplug.utils.JsonUtils;
import org.changs.nav.delegate.amap.domain.PoiPoint;
import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;

import java.util.List;

import javax.inject.Inject;
import javax.inject.Singleton;

import timber.log.Timber;

/**
 * Created by yincs on 2017/10/14.
 */

@Singleton
public class AmapNavHelper {
    private static final String TAG_END_POINT = "org.changs.nav.contract.global.AmapNavHelper.endPoint";
    private static final String TAG_WAYS_POINT = "org.changs.nav.contract.global.AmapNavHelper.ways";
    private static final String TAG_STEP = "org.changs.nav.contract.global.AmapNavHelper.step";

    private static final int STEP_DEFAULT = 0;
    private static final int STEP_PREPARED = 1;
    private static final int STEP_STARTED = 2;
    private int step = STEP_DEFAULT;
    private PoiPoint startPoint;
    private PoiPoint endPoint;
    private List<PoiPoint> ways;
    private final SharedPreferences sharedPreferences;
    private final Context mContext;

    @Inject
    public AmapNavHelper(SharedPreferences sharedPreferences, Context context) {
        this.sharedPreferences = sharedPreferences;
        this.  mContext = context;
        EventBus.getDefault().register(this);

        step = sharedPreferences.getInt(TAG_STEP, STEP_DEFAULT);
        if (step != STEP_DEFAULT) {
            endPoint = JsonUtils.parseObject(sharedPreferences.getString(TAG_END_POINT, null), PoiPoint.class);
            if (endPoint == null) {
                step = STEP_DEFAULT;
            } else {
                ways = JsonUtils.parseList(sharedPreferences.getString(TAG_WAYS_POINT, null));
            }
        }
        Timber.d("AmapNavHelper step = " + step);
    }

    @Subscribe
    public void onEvent(OnTrimMemoryEvent onTrimMemoryEvent) {
        sharedPreferences.edit()
                .putInt(TAG_STEP, step)
                .putString(TAG_END_POINT, JsonUtils.stringify(endPoint))
                .putString(TAG_WAYS_POINT, JsonUtils.stringify(ways))
                .apply();

        Timber.d("OnTrimMemoryEvent step = " + step);
    }

    /**
     * 准备导航
     */
    public void prepareNav(PoiPoint endPoint) {
        this.endPoint = endPoint;
        step = STEP_PREPARED;
    }

    /**
     * 开始导航
     */
    public void startNav() {
        step = STEP_STARTED;
    }

    /**
     * 结束导航
     */
    public void endNav() {
        endPoint = null;
        ways = null;
        step = STEP_DEFAULT;
    }

    public AMapNavi getAMapNavi() {
        return AMapNavi.getInstance(mContext);
    }

    public boolean isPrepared() {
        return endPoint != null && step == STEP_PREPARED;
    }

    public boolean isStarted() {
        return endPoint != null && step == STEP_STARTED;
    }

    public boolean isDefault() {
        return step == STEP_DEFAULT;
    }

    public PoiPoint getEndPoint() {
        return endPoint;
    }
}
