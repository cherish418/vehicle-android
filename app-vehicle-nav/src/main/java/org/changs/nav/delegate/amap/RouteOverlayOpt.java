package org.changs.nav.delegate.amap;

import android.content.Context;
import android.util.SparseArray;
import android.view.View;

import com.amap.api.maps.AMap;
import com.amap.api.maps.CameraUpdateFactory;
import com.amap.api.maps.model.LatLng;
import com.amap.api.maps.model.LatLngBounds;
import com.amap.api.navi.AMapNavi;
import com.amap.api.navi.model.AMapNaviPath;
import com.amap.api.navi.view.RouteOverLay;

import java.util.List;

import timber.log.Timber;

/**
 * Created by yincs on 2017/8/5.
 */

public class RouteOverlayOpt {
    private AMapNavi mAMapNavi;
    private Context mCtx;
    private AMap mAMap;
    private SparseArray<RouteOverLay> routeOverlays = new SparseArray<>();
    private int[] ints;
    private int select;

    public RouteOverlayOpt(Context ctx, AMap aMap) {
        this.mCtx = ctx;
        this.mAMap = aMap;
        mAMapNavi = AMapNavi.getInstance(mCtx.getApplicationContext());
    }

    public void clear() {
        for (int i = 0; i < routeOverlays.size(); i++) {
            RouteOverLay routeOverlay = routeOverlays.valueAt(i);
            routeOverlay.removeFromMap();
        }
        routeOverlays.clear();
    }

    public void addRoute(int[] ints, List<AMapNaviPath> list) {
        this.ints = ints;
        for (int i = 0; i < list.size(); i++) {
            AMapNaviPath path = list.get(i);
//            mAMap.moveCamera(CameraUpdateFactory.changeTilt(0));
            RouteOverLay routeOverLay = new RouteOverLay(mAMap, path, mCtx);
            routeOverLay.setTrafficLine(true);
            routeOverLay.addToMap();
            routeOverlays.put(ints[i], routeOverLay);
        }
    }

    public int[] getInts() {
        return ints;
    }

    public int getSelect() {
        return select;
    }

    public RouteOverLay selectRoute(int index) {
        select = index;
        RouteOverLay selectRoute = null;
        for (int i = 0; i < routeOverlays.size(); i++) {
            int routeID = routeOverlays.keyAt(i);
            final RouteOverLay routeOverLay = routeOverlays.get(routeID);
            if (i == index) {
                routeOverLay.setTransparency(1);
                routeOverLay.setZindex(1);
                mAMapNavi.selectRouteId(routeID);
                selectRoute = routeOverLay;
            } else {
                routeOverLay.setTransparency(0.4f);
                routeOverLay.setZindex(0);
            }
        }
        return selectRoute;
    }

    public SparseArray<RouteOverLay> getRouteOverlays() {
        return routeOverlays;
    }

    public void zoomToSpan(LatLng startLatLng, LatLng endLatlng, View mapView) {
        LatLngBounds.Builder b = LatLngBounds.builder();
        b.include(startLatLng);
        b.include(endLatlng);
        mAMap.animateCamera(CameraUpdateFactory.newLatLngBoundsRect(b.build(),
                mapView.getMeasuredWidth() / 2, mapView.getMeasuredWidth() / 5,
                mapView.getMeasuredHeight() / 5, mapView.getMeasuredHeight() / 5), new AMap.CancelableCallback() {
            @Override
            public void onFinish() {
                Timber.d("onFinish() called");
            }

            @Override
            public void onCancel() {
                Timber.d("onCancel() called");
            }
        });
    }
}
