package org.changs.nav.delegate;

import com.amap.api.maps.model.LatLng;
import com.amap.api.services.core.PoiItem;

import org.changs.nav.delegate.amap.domain.PoiPoint;

import java.util.ArrayList;

/**
 * Created by yincs on 2017/10/16.
 */

public interface MapSwitchDelegate {
    /**
     * 基础状态
     */
    void switch2Basics();

    /**
     * 显示poiList列表
     */
    void switch2PoiList(ArrayList<PoiItem> poiItems);

    /**
     * 显示路径规化视图
     */
    void switch2RouteList(int[] ints);

    void switch2RouteList(int[] ints, PoiPoint endPoint);

    void switch2CollectPoint(LatLng latLng);

    void addCollectMarket(Long id, LatLng latLng);

    void remoteCollectMarket(Long id);
}
