package org.changs.nav.event;

/**
 * Created by yincs on 2017/10/16.
 */

public class PoiItemClickEvent {
    public final int index;

    public PoiItemClickEvent(int index) {
        this.index = index;
    }
}
