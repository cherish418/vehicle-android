package org.changs.nav.event;

/**
 * Created by yincs on 2017/10/16.
 */

public class RouteItemClickEvent {
    public final int index;

    public RouteItemClickEvent(int index) {
        this.index = index;
    }
}
