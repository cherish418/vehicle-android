package org.changs.setting;

import android.content.Context;
import android.net.Uri;
import android.support.test.InstrumentationRegistry;
import android.support.test.runner.AndroidJUnit4;

import org.changs.s3.common.provider.SpProvider;
import org.junit.Test;
import org.junit.runner.RunWith;

import java.util.Map;

import static org.junit.Assert.*;

/**
 * Instrumentation test, which will execute on an Android device.
 *
 * @see <a href="http://d.android.com/tools/testing">Testing documentation</a>
 */
@RunWith(AndroidJUnit4.class)
public class ExampleInstrumentedTest {
    @Test
    public void useAppContext() throws Exception {
        // Context of the app under test.
        Context appContext = InstrumentationRegistry.getTargetContext();

        assertEquals("org.changs.setting", appContext.getPackageName());
    }

    @Test
    public void getSp() throws Exception {
        Context context = InstrumentationRegistry.getContext();
        SpProvider providerUtils = new SpProvider(context, Uri.parse("content://org.changs.nav.provider.SpContentProvider"), "KEY_CAR_NUMBER");
        Map<String, ?> all = providerUtils.getAll();
        for (Object o : all.values()) {
            System.out.println("sharedPreferences result = " + o);
        }
    }

    @Test
    public void modify() throws Exception {
        Context context = InstrumentationRegistry.getContext();
        new SpProvider(context, Uri.parse("content://org.changs.nav.provider.SpContentProvider"))
                .edit()
                .putString("KEY_CAR_NUMBER", "鲁123123")
                .apply();

    }
}
