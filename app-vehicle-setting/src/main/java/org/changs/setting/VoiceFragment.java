package org.changs.setting;

import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.annotation.IdRes;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RadioGroup;

import org.changs.aplug.base.BaseFragment;
import org.changs.aplug.utils.EmptyUtils;
import org.changs.setting.databinding.VoiceFragmentBinding;

import javax.inject.Inject;

import aplug.AppConfig;
import aplug.data.db.model.CarData;

/**
 * 声控与播报
 * Created by Administrator on 2017/8/28.
 */

public class VoiceFragment extends BaseFragment implements RadioGroup.OnCheckedChangeListener, CarPresenter.V {
    private VoiceFragmentBinding binding;

    private CarData carData;
    @Inject
    CarPresenter presenter;

    @Override
    public View setup(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        binding = DataBindingUtil.inflate(inflater, R.layout.voice_fragment, container, false);
        presenter.attachView(this);
        initData();
        return binding.getRoot();
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        presenter.detachView();
    }

    private void initData() {
        presenter.getCarData();
    }

    @Override
    public void onCheckedChanged(RadioGroup group, @IdRes int checkedId) {
        switch (checkedId) {
            case R.id.rbtn_detail://详细播报
                carData.setVoiceSend(AppConfig.VOICE_DETAIL);
                break;
            case R.id.rbtn_concise://简洁播报
                carData.setVoiceSend(AppConfig.VOICE_CONCISE);
                break;
            case R.id.rbtn_no://不播报
                carData.setVoiceSend(AppConfig.VOICE_NO);
                break;
            case R.id.rbtn_voice_man://男声
                carData.setVoiceType(AppConfig.VOICE_MAN);
                break;
            case R.id.rbtn_voice_woman://女声
                carData.setVoiceType(AppConfig.VOICE_WOMAN);
                break;
        }
        presenter.modifyCarData(carData);
    }

    private void setData() {
        if (!EmptyUtils.isEmpty(carData.getVoiceSend())) {//语音播报
            if (carData.getVoiceSend().equals(AppConfig.VOICE_DETAIL)) {
                binding.rbtnDetail.setChecked(true);
            } else if (carData.getVoiceSend().equals(AppConfig.VOICE_NO)) {
                binding.rbtnNo.setChecked(true);
            } else {
                binding.rbtnConcise.setChecked(true);
            }
        } else {
            binding.rbtnNo.setChecked(true);
            carData.setVoiceSend(AppConfig.VOICE_NO);
            presenter.modifyCarData(carData);
        }
        if (!EmptyUtils.isEmpty(carData.getVoiceType())) {//语音类型
            if (carData.getVoiceType().equals(AppConfig.VOICE_MAN)) {
                binding.rbtnVoiceMan.setChecked(true);
            } else {
                binding.rbtnVoiceWoman.setChecked(true);
            }
        } else {
            binding.rbtnVoiceMan.setChecked(true);
            carData.setVoiceType(AppConfig.VOICE_MAN);
            presenter.modifyCarData(carData);
        }

        binding.rgsStyle.setOnCheckedChangeListener(this);
        binding.rgsVoiceStyle.setOnCheckedChangeListener(this);
    }

    @Override
    public void getCar(CarData carData) {
        this.carData = carData;
        setData();
    }

    @Override
    public void showNoCar() {
        carData = new CarData((long) 1, "", "", "", "", "", AppConfig.AUTO_THEME, AppConfig.VOICE_NO,
                AppConfig.VOICE_MAN, false, false, false, false, "", AppConfig.MAP_ONLINE);
        presenter.addCarData(carData);
        setData();
    }

    @Override
    public void success() {
    }
}
