package org.changs.setting.car;

import android.support.annotation.NonNull;

import org.changs.aplug.base.IPresenter;
import org.changs.aplug.base.IView;

/**
 * Created by yincs on 2017/9/21.
 */

public interface CarContract {
    interface V extends IView {
        void showCarData(@NonNull CarData carData);
    }

    interface P extends IPresenter<V> {
        void loadData();
        void editCarNumber(String carNumber);
        void editVehicleHeight(String height);
        void editVehicleLoad(String load);
        void editCarShape(String shape);
        void editCarLength(String carLength);
    }
}
