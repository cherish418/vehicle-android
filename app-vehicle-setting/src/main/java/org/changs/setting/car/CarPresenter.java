package org.changs.setting.car;

import android.content.Context;
import android.net.Uri;

import org.changs.aplug.base.BasePresenter;
import org.changs.s3.common.provider.SpProvider;
import org.changs.s3.common.tools.NavTools;

import javax.inject.Inject;

/**
 * Created by yincs on 2017/9/21.
 */

public class CarPresenter extends BasePresenter<CarContract.V> implements CarContract.P {

    private final Context context;
    private CarData carData;

    @Inject
    public CarPresenter(Context context) {
        this.context = context;
    }

    @Override
    public void attachView(CarContract.V v) {
        super.attachView(v);
        loadData();
    }

    @Override
    public void loadData() {
        if (getMvpView() == null) return;
        SpProvider provider = new SpProvider(context, Uri.parse(NavTools.Pref.URI_PATH),
                NavTools.Pref.KEY_CAR_NUMBER, NavTools.Pref.KEY_VEHICLE_HEIGHT,
                NavTools.Pref.KEY_CAR_SHAPE, NavTools.Pref.KEY_CAR_LENGTH);
        String carNumber = provider.getString(NavTools.Pref.KEY_CAR_NUMBER, null);
        String vehicleHeight = provider.getString(NavTools.Pref.KEY_VEHICLE_HEIGHT, null);
        String vehicleLoad = provider.getString(NavTools.Pref.KEY_VEHICLE_LOAD, null);
        String carShape = provider.getString(NavTools.Pref.KEY_CAR_SHAPE, null);
        String carLength = provider.getString(NavTools.Pref.KEY_CAR_LENGTH, null);

        carData = new CarData(carNumber, vehicleHeight, vehicleLoad, carShape, carLength);
        getMvpView().showCarData(carData);
    }

    @Override
    public void editCarNumber(String carNumber) {
        if (carNumber.equals(carData.carNumber)) return;
        new SpProvider(context, Uri.parse(NavTools.Pref.URI_PATH))
                .edit().putString(NavTools.Pref.KEY_CAR_NUMBER, carNumber)
                .apply();
        carData.carNumber = carNumber;
    }

    @Override
    public void editVehicleHeight(String vehicleHeight) {
        if (vehicleHeight.equals(carData.vehicleHeight)) return;
        new SpProvider(context, Uri.parse(NavTools.Pref.URI_PATH))
                .edit().putString(NavTools.Pref.KEY_VEHICLE_HEIGHT, vehicleHeight)
                .apply();
        carData.vehicleHeight = vehicleHeight;
    }

    @Override
    public void editVehicleLoad(String vehicleLoad) {
        if (vehicleLoad.equals(carData.vehicleLoad)) return;
        new SpProvider(context, Uri.parse(NavTools.Pref.URI_PATH))
                .edit().putString(NavTools.Pref.KEY_VEHICLE_LOAD, vehicleLoad)
                .apply();
        carData.vehicleHeight = vehicleLoad;
    }

    @Override
    public void editCarShape(String carShape) {
        if (carShape.equals(carData.carShape)) return;
        new SpProvider(context, Uri.parse(NavTools.Pref.URI_PATH))
                .edit().putString(NavTools.Pref.KEY_CAR_SHAPE, carShape)
                .apply();
        carData.carShape = carShape;
    }

    @Override
    public void editCarLength(String carLength) {
        if (carLength.equals(carData.carLength)) return;
        new SpProvider(context, Uri.parse(NavTools.Pref.URI_PATH))
                .edit().putString(NavTools.Pref.KEY_CAR_LENGTH, carLength)
                .apply();
        carData.carLength = carLength;
    }
}
