package org.changs.setting.car;

/**
 * Created by yincs on 2017/9/21.
 */

public class CarData {
    public String carNumber;
    public String vehicleHeight;
    public String vehicleLoad;
    public String carShape;
    public String carLength;

    public CarData(String carNumber, String vehicleHeight, String vehicleLoad, String carShape, String carLength) {
        this.carNumber = carNumber;
        this.vehicleHeight = vehicleHeight;
        this.vehicleLoad = vehicleLoad;
        this.carShape = carShape;
        this.carLength = carLength;
    }
}
