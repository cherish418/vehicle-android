package org.changs.setting.car;

import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import org.changs.aplug.base.BaseFragment;
import org.changs.aplug.utils.EmptyUtils;
import org.changs.setting.R;
import org.changs.setting.databinding.CarMsgFragmentBinding;

import javax.inject.Inject;

import timber.log.Timber;

/**
 * 车辆信息
 * Created by Administrator on 2017/8/28.
 */

public class CarFragment extends BaseFragment implements CarContract.V {

    private CarMsgFragmentBinding binding;

    @Inject CarPresenter presenter;

    @Override
    public View setup(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        binding = DataBindingUtil.inflate(inflater, R.layout.car_msg_fragment, container, false);
        initEvent();
        return binding.getRoot();
    }

    private void initEvent() {
        binding.etCarNumber.setOnEditorActionListener((v, actionId, event) -> {
            String carNumber = binding.etCarNumber.getText().toString();
            Timber.d("car", "车牌：" + carNumber);
            presenter.editCarNumber(carNumber);
            return false;
        });
        binding.etMaxHeight.setOnEditorActionListener((v, actionId, event) -> {
            String vehicleHeight = binding.etMaxHeight.getText().toString();
            Timber.d("car", "装载高度：" + vehicleHeight);
            presenter.editVehicleHeight(vehicleHeight);
            return false;
        });
        binding.etCarWeight.setOnEditorActionListener((v, actionId, event) -> {
            String vehicleLoad = binding.etCarWeight.getText().toString();
            Timber.d("car", "车重：" + vehicleLoad);
            presenter.editVehicleLoad(vehicleLoad);
            return false;
        });
        binding.etCarType.setOnEditorActionListener((v, actionId, event) -> {
            String carShape = binding.etCarType.getText().toString();
            Timber.d("car", "车型：" + carShape);
            presenter.editCarShape(carShape);
            return false;
        });
        binding.etCarLength.setOnEditorActionListener((v, actionId, event) -> {
            String carLength = binding.etCarLength.getText().toString();
            Timber.d("car", "车长：" + carLength);
            presenter.editCarLength(carLength);
            return false;
        });
    }


    @Override
    public void onResume() {
        super.onResume();
        presenter.attachView(this);
    }


    @Override
    public void onDestroyView() {
        super.onDestroyView();
        presenter.detachView();
    }


    @Override
    public void showCarData(@NonNull org.changs.setting.car.CarData carData) {
        setText(binding.etCarNumber, carData.carNumber, "");
        setText(binding.etMaxHeight, carData.vehicleHeight, "");
        setText(binding.etCarWeight, carData.vehicleLoad, "");
        setText(binding.etCarType, carData.carShape, "");
        setText(binding.etCarLength, carData.carLength, "");
    }

    private void setText(TextView textView, String text, String def) {
        textView.setText(EmptyUtils.isEmpty(text) ? def : text);
    }
}
