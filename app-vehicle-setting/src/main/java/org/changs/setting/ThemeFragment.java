package org.changs.setting;

import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.annotation.IdRes;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RadioGroup;

import org.changs.aplug.base.BaseFragment;
import org.changs.aplug.utils.EmptyUtils;
import org.changs.setting.databinding.ThemeFragmentBinding;
import org.changs.setting.main.MainActivity;

import javax.inject.Inject;

import aplug.AppConfig;
import aplug.data.db.model.CarData;

/**
 * 主题
 * Created by Administrator on 2017/8/28.
 */

public class ThemeFragment extends BaseFragment implements RadioGroup.OnCheckedChangeListener, CarPresenter.V {
    private ThemeFragmentBinding binding;

    private CarData carData;
    @Inject
    CarPresenter presenter;

    private String isNight, theme;

    @Override
    public View setup(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        binding = DataBindingUtil.inflate(inflater, R.layout.theme_fragment, container, false);
        presenter.attachView(this);
        if (savedInstanceState != null) {
            theme = savedInstanceState.getString("theme");
        }
        initData();
        return binding.getRoot();
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        presenter.detachView();
    }

    private void initData() {
        presenter.getCarData();
    }

    private void setData() {
        if (!EmptyUtils.isEmpty(carData.getTheme())) {
            if (carData.getTheme().equals(AppConfig.AUTO_THEME)) {
                binding.rbtnAutoStyle.setChecked(true);
            } else if (carData.getTheme().equals(AppConfig.DAY_THEME)) {
                binding.rbtnDayStyle.setChecked(true);
            } else {
                binding.rbtnNightStyle.setChecked(true);
            }
        } else {
            binding.rbtnAutoStyle.setChecked(true);
            carData.setTheme(AppConfig.AUTO_THEME);
            presenter.modifyCarData(carData);
        }
        isNight = carData.getTheme();
        if (EmptyUtils.isEmpty(theme) || (!EmptyUtils.isEmpty(theme) && !theme.equals(isNight)))
            ((MainActivity) getActivity()).setDayNight(carData.getTheme());

        binding.rgsStyle.setOnCheckedChangeListener(this);
    }

    @Override
    public void onCheckedChanged(RadioGroup group, @IdRes int checkedId) {
        switch (checkedId) {
            case R.id.rbtn_auto_style://自动切换
                carData.setTheme(AppConfig.AUTO_THEME);
                isNight = AppConfig.AUTO_THEME;
                break;
            case R.id.rbtn_day_style://白天模式
                carData.setTheme(AppConfig.DAY_THEME);
                isNight = AppConfig.DAY_THEME;
                break;
            case R.id.rbtn_night_style://夜间模式
                carData.setTheme(AppConfig.NIGHT_THEME);
                isNight = AppConfig.NIGHT_THEME;
                break;
        }
        presenter.modifyCarData(carData);
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putString("theme", isNight);
    }

    @Override
    public void getCar(CarData carData) {
        this.carData = carData;
        setData();
    }

    @Override
    public void showNoCar() {
        carData = new CarData((long) 1, "", "", "", "", "", AppConfig.AUTO_THEME, AppConfig.VOICE_NO,
                AppConfig.VOICE_MAN, false, false, false, false, "", AppConfig.MAP_ONLINE);
        presenter.addCarData(carData);
        setData();
    }

    @Override
    public void success() {
        if (!EmptyUtils.isEmpty(isNight)) {
            if (isNight.equals(AppConfig.DAY_THEME)) {
                binding.rbtnDayStyle.setChecked(true);
            } else if (isNight.equals(AppConfig.NIGHT_THEME)) {
                binding.rbtnNightStyle.setChecked(true);
            }
            ((MainActivity) getActivity()).setDayNight(isNight);
        }
    }
}
