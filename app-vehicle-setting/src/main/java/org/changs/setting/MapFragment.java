package org.changs.setting;

import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.annotation.IdRes;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RadioGroup;

import org.changs.aplug.base.BaseFragment;
import org.changs.aplug.utils.EmptyUtils;
import org.changs.setting.databinding.MapNavFragmentBinding;

import javax.inject.Inject;

import aplug.AppConfig;
import aplug.data.db.model.CarData;

/**
 * 地图与导航
 * Created by Administrator on 2017/8/28.
 */

public class MapFragment extends BaseFragment implements RadioGroup.OnCheckedChangeListener, CarPresenter.V {
    private MapNavFragmentBinding binding;

    private CarData carData;
    @Inject
    CarPresenter presenter;

    @Override
    public View setup(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        binding = DataBindingUtil.inflate(inflater, R.layout.map_nav_fragment, container, false);
        presenter.attachView(this);
        initData();
        return binding.getRoot();
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        presenter.detachView();
    }

    private void initData() {
        presenter.getCarData();
    }

    private void setData() {
        if (!EmptyUtils.isEmpty(carData.getLine())) {//路线规划
            if (carData.getLine().equals(AppConfig.MAP_ONLINE)) {
                binding.rbtnOnline.setChecked(true);
            } else {
                binding.rbtnOffline.setChecked(true);
            }
        } else {
            binding.rbtnOnline.setChecked(true);
            carData.setLine(AppConfig.MAP_OFFLINE);
            presenter.modifyCarData(carData);
        }

        binding.rgsMapStyle.setOnCheckedChangeListener(this);
    }

    @Override
    public void onCheckedChanged(RadioGroup group, @IdRes int checkedId) {
        switch (checkedId) {
            case R.id.rbtn_offline://离线
                carData.setLine(AppConfig.MAP_OFFLINE);
                break;
            case R.id.rbtn_online://在线
                carData.setLine(AppConfig.MAP_ONLINE);
                break;
        }
        presenter.modifyCarData(carData);
    }

    @Override
    public void getCar(CarData carData) {
        this.carData = carData;
        setData();
    }

    @Override
    public void showNoCar() {
        carData = new CarData((long) 1, "", "", "", "", "", AppConfig.AUTO_THEME, AppConfig.VOICE_NO,
                AppConfig.VOICE_MAN, false, false, false, false, "", AppConfig.MAP_ONLINE);
        presenter.addCarData(carData);
        setData();
    }

    @Override
    public void success() {
    }
}
