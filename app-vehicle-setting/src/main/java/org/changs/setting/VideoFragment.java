package org.changs.setting;

import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CompoundButton;

import org.changs.aplug.base.BaseFragment;
import org.changs.aplug.utils.EmptyUtils;
import org.changs.setting.databinding.VideoFragmentBinding;

import javax.inject.Inject;

import aplug.AppConfig;
import aplug.data.db.model.CarData;

/**
 * 监控和视频
 * Created by Administrator on 2017/8/28.
 */

public class VideoFragment extends BaseFragment implements CompoundButton.OnCheckedChangeListener, CarPresenter.V {
    private VideoFragmentBinding binding;

    private boolean isVideo, isMonitor, isPicture, isSetPsw;
    private CarData carData;
    @Inject
    CarPresenter presenter;

    @Override
    public View setup(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        binding = DataBindingUtil.inflate(inflater, R.layout.video_fragment, container, false);
        presenter.attachView(this);
        initData();
        return binding.getRoot();
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        presenter.detachView();
    }

    private void initData() {
        presenter.getCarData();
    }

    private void initUI() {
        binding.tbtnVideo.setOnCheckedChangeListener(this);
        binding.tbtnMobile.setOnCheckedChangeListener(this);
        binding.tbtnTakePic.setOnCheckedChangeListener(this);
        binding.tbtnShowPsw.setOnCheckedChangeListener(this);

        binding.etPsw.setOnEditorActionListener((v, actionId, event) -> {
            carData.setPsw(binding.etPsw.getText().toString().trim());
            presenter.modifyCarData(carData);
            return false;
        });
    }

    private void setData() {
        isVideo = carData.getIsVideo();
        isMonitor = carData.getIsMonitor();
        isPicture = carData.getIsPicture();
        isSetPsw = carData.getIsSetPsw();
        if (!EmptyUtils.isEmpty(isVideo)) {
            binding.tbtnVideo.setChecked(isVideo);
        } else {
            binding.tbtnVideo.setChecked(false);
            carData.setIsVideo(false);
            presenter.modifyCarData(carData);
        }
        if (!EmptyUtils.isEmpty(isMonitor)) {
            binding.tbtnMobile.setChecked(isMonitor);
        } else {
            binding.tbtnMobile.setChecked(false);
            carData.setIsMonitor(false);
            presenter.modifyCarData(carData);
        }
        if (!EmptyUtils.isEmpty(isPicture)) {
            binding.tbtnTakePic.setChecked(isPicture);
        } else {
            binding.tbtnTakePic.setChecked(false);
            carData.setIsPicture(false);
            presenter.modifyCarData(carData);
        }
        if (!EmptyUtils.isEmpty(isSetPsw)) {
            binding.tbtnShowPsw.setChecked(isSetPsw);
            if (carData.getIsSetPsw()) {
                binding.rlSetPsw.setVisibility(View.VISIBLE);
                if (!EmptyUtils.isEmpty(carData.getPsw())) {
                    binding.etPsw.setText(carData.getPsw());
                }
            } else {
                binding.rlSetPsw.setVisibility(View.GONE);
            }
        } else {
            binding.tbtnShowPsw.setChecked(false);
            binding.rlSetPsw.setVisibility(View.GONE);
            carData.setIsSetPsw(false);
            presenter.modifyCarData(carData);
        }

        initUI();
    }

    @Override
    public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
        switch (buttonView.getId()) {
            case R.id.tbtn_video://监控时录像
                carData.setIsVideo(isChecked);
                break;
            case R.id.tbtn_mobile://移动监测
                carData.setIsMonitor(isChecked);
                break;
            case R.id.tbtn_take_pic://自动抓拍异常画面
                carData.setIsPicture(isChecked);
                break;
            case R.id.tbtn_show_psw://需要密码查看
                if (isChecked) {
                    binding.rlSetPsw.setVisibility(View.VISIBLE);
                } else {
                    binding.rlSetPsw.setVisibility(View.GONE);
                }
                carData.setIsSetPsw(isChecked);
                break;
        }
        presenter.modifyCarData(carData);
    }

    @Override
    public void getCar(CarData carData) {
        this.carData = carData;
        setData();
    }

    @Override
    public void showNoCar() {
        carData = new CarData((long) 1, "", "", "", "", "", AppConfig.AUTO_THEME, AppConfig.VOICE_NO,
                AppConfig.VOICE_MAN, false, false, false, false, "", AppConfig.MAP_ONLINE);
        presenter.addCarData(carData);
        setData();
    }

    @Override
    public void success() {
    }
}
