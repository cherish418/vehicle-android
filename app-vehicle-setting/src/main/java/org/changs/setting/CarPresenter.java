package org.changs.setting;

import org.changs.aplug.base.BasePresenter;
import org.changs.aplug.base.IView;

import javax.inject.Inject;

import aplug.data.DataManager;
import aplug.data.db.model.CarData;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.schedulers.Schedulers;

/**
 * 车辆信息
 * Created by kelly on 2017/9/5.
 */

public class CarPresenter extends BasePresenter<CarPresenter.V> {
    private final DataManager dataManager;

    interface V extends IView {
        void getCar(CarData carData);

        void showNoCar();

        void success();
    }

    @Inject
    public CarPresenter(DataManager dataManager) {
        this.dataManager = dataManager;
    }

    /**
     * 获取车辆信息
     */
    public void getCarData() {
        getCompositeDisposable().add(dataManager.getDbHelper().getCar(1)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(carData ->
                                getMvpView().getCar(carData)
                        , throwable -> getMvpView().showNoCar()));
    }

    /**
     * 修改车辆信息
     *
     * @param carData
     */
    public void modifyCarData(CarData carData) {
        getCompositeDisposable().add(dataManager.getDbHelper().modifyCar(carData)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(aBoolean -> {
                    if (aBoolean) {
                        getMvpView().success();
                    }
                }));
    }

    /**
     * 添加车辆信息
     *
     * @param
     */
    public void addCarData(CarData carData) {
        getCompositeDisposable().add(dataManager.getDbHelper().insertCar(carData)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe());
    }
}
