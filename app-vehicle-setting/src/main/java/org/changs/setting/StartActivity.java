package org.changs.setting;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.os.PersistableBundle;
import android.support.v7.app.AppCompatDelegate;

import org.changs.aplug.ActivityManager;
import org.changs.aplug.base.BaseActivity;
import org.changs.aplug.utils.EmptyUtils;
import org.changs.setting.databinding.StartActivityBinding;
import org.changs.setting.main.MainActivity;

import javax.inject.Inject;

import aplug.AppConfig;
import aplug.data.db.model.CarData;
import timber.log.Timber;

/**
 * 开机画面--》输入档案密码
 * Created by Administrator on 2017/9/7.
 */

public class StartActivity extends BaseActivity implements CarPresenter.V {
    private StartActivityBinding binding;

    private boolean isPsw;
    private String psw, isNight, theme;

    @Inject
    CarPresenter presenter;

    @Override
    public void setup(Bundle savedInstanceState) {
        Activity preActivity = ActivityManager.getInstance().getPreActivity();
        if (preActivity != null) {
            startActivity(new Intent(this, preActivity.getClass()));
            overridePendingTransition(0, 0);
            finish();
            return;
        }
        startActivity(new Intent(getContext(), MainActivity.class));
        overridePendingTransition(0, 0);
        finish();

//        binding = DataBindingUtil.setContentView(this, R.layout.start_activity);
//        if (savedInstanceState != null) {
//            theme = savedInstanceState.getString("theme");
//        }
//        startActivity(new Intent(getContext(), MainActivity.class));
//        presenter.attachView(this);
//        finish();
//        initData();
    }

    private void initData() {
        presenter.getCarData();
    }

    private void verifyPsw() {
        if (EmptyUtils.isEmpty(binding.etPsw.getText().toString().trim())) {
            showMessage(getResources().getString(R.string.input_psw));
        } else {
            if (psw.equals(binding.etPsw.getText().toString().trim())) {
                startActivity(new Intent(getContext(), MainActivity.class));
                finish();
            } else {
                showMessage(getResources().getString(R.string.error_psw));
            }
        }
    }

    @Override
    public void getCar(CarData carData) {
        if (!EmptyUtils.isEmpty(carData.getIsSetPsw())) {
            isPsw = carData.getIsSetPsw();
        } else {
            isPsw = false;
            carData.setIsSetPsw(false);
            presenter.modifyCarData(carData);
        }
        if (!EmptyUtils.isEmpty(carData.getPsw())) {
            psw = carData.getPsw();
        } else {
            psw = "";
        }

        if (!EmptyUtils.isEmpty(carData.getTheme())) {
            isNight = carData.getTheme();
            if (EmptyUtils.isEmpty(theme) || (!EmptyUtils.isEmpty(theme) && !theme.equals(isNight)))
                setDayNight(carData.getTheme());
        }

        inputPsw();
    }

    @Override
    public void showNoCar() {
        isPsw = false;
        CarData carData = new CarData((long) 1, "", "", "", "", "", AppConfig.AUTO_THEME, AppConfig.VOICE_NO,
                AppConfig.VOICE_MAN, false, false, false, false, "", AppConfig.MAP_ONLINE);
        presenter.addCarData(carData);
        isNight = AppConfig.AUTO_THEME;
        inputPsw();
    }

    private void inputPsw() {
        if (isPsw) {
            binding.btnEnter.setOnClickListener(v -> verifyPsw());
        } else {
            startActivity(new Intent(getContext(), MainActivity.class));
            finish();
        }
    }

    /**
     * 设置主题模式
     */
    private void setDayNight(String theme) {
        final boolean applyDayNight = getDelegate().applyDayNight();
        Timber.d("applyDayNight = " + applyDayNight);
        final int defaultNightMode = AppCompatDelegate.getDefaultNightMode();
        Timber.d("defaultNightMode = " + defaultNightMode);
        if (theme.equals(AppConfig.NIGHT_THEME)) {//夜间
            getDelegate().setLocalNightMode(AppCompatDelegate.MODE_NIGHT_YES);
        } else if (theme.equals(AppConfig.DAY_THEME)) {//日间
            getDelegate().setLocalNightMode(AppCompatDelegate.MODE_NIGHT_NO);
        } else {
            getDelegate().setLocalNightMode(AppCompatDelegate.MODE_NIGHT_AUTO);
        }
//        recreate();
    }

    @Override
    public void onSaveInstanceState(Bundle outState, PersistableBundle outPersistentState) {
        super.onSaveInstanceState(outState, outPersistentState);
        outState.putString("theme", isNight);
    }

    @Override
    public void success() {
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        presenter.detachView();
    }
}
