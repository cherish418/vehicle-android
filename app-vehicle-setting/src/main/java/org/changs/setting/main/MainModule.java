package org.changs.setting.main;

import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatActivity;

import org.changs.aplug.annotation.ActivityScoped;
import org.changs.aplug.annotation.FragmentScoped;
import org.changs.aplug.utils.FragmentUtils;
import org.changs.setting.car.CarFragment;
import org.changs.setting.MapFragment;
import org.changs.setting.R;
import org.changs.setting.ThemeFragment;
import org.changs.setting.VideoFragment;
import org.changs.setting.VoiceFragment;

import dagger.Module;
import dagger.Provides;
import dagger.android.ContributesAndroidInjector;

/**
 * Created by yincs on 2017/9/13.
 */

@Module
public abstract class MainModule {

    @FragmentScoped
    @ContributesAndroidInjector
    abstract CarFragment CarFragment();

    @FragmentScoped
    @ContributesAndroidInjector
    abstract MapFragment MapFragment();

    @FragmentScoped
    @ContributesAndroidInjector
    abstract ThemeFragment ThemeFragment();

    @FragmentScoped
    @ContributesAndroidInjector
    abstract VideoFragment VideoFragment();

    @FragmentScoped
    @ContributesAndroidInjector
    abstract VoiceFragment VoiceFragment();


    @ActivityScoped
    @Provides
    static CarFragment providerCarFragment(MainActivity mainActivity) {
        return addUniqueFragment(mainActivity, CarFragment.class);
    }

    @ActivityScoped
    @Provides
    static MapFragment providerMapFragment(MainActivity mainActivity) {
        return addUniqueFragment(mainActivity, MapFragment.class);
    }

    @ActivityScoped
    @Provides
    static ThemeFragment providerThemeFragment(MainActivity mainActivity) {
        return addUniqueFragment(mainActivity, ThemeFragment.class);
    }

    @ActivityScoped
    @Provides
    static VideoFragment providerVideoFragment(MainActivity mainActivity) {
        return addUniqueFragment(mainActivity, VideoFragment.class);
    }

    @ActivityScoped
    @Provides
    static VoiceFragment providerVoiceFragment(MainActivity mainActivity) {
        return addUniqueFragment(mainActivity, VoiceFragment.class);
    }

    @SuppressWarnings("unchecked")
    private static <T extends Fragment> T addUniqueFragment(AppCompatActivity activity, Class<T> clazz) {
        return FragmentUtils.addUniqueFragment(activity, R.id.main_fl, clazz);
    }
}
