package org.changs.setting.main;

import android.content.Intent;
import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.annotation.IdRes;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatDelegate;
import android.widget.RadioGroup;

import org.changs.aplug.base.BaseActivity;
import org.changs.setting.MapFragment;
import org.changs.setting.R;
import org.changs.setting.ThemeFragment;
import org.changs.setting.VideoFragment;
import org.changs.setting.VoiceFragment;
import org.changs.setting.car.CarFragment;
import org.changs.setting.databinding.ActivityMainBinding;

import javax.inject.Inject;

import aplug.AppConfig;
import dagger.Lazy;
import timber.log.Timber;

public class MainActivity extends BaseActivity {

    private static final String TAG_CURRENT_FRAGMENT = "tag_current_fragment";

    @Inject Lazy<CarFragment> carFragment;
    @Inject Lazy<MapFragment> mapFragment;
    @Inject Lazy<ThemeFragment> themeFragment;
    @Inject Lazy<VideoFragment> videoFragment;
    @Inject Lazy<VoiceFragment> voiceFragment;

    private ActivityMainBinding binding;
    private String currentFragmentTag;

    @Override
    public void setup(Bundle savedInstanceState) {
        binding = DataBindingUtil.setContentView(this, R.layout.activity_main);
        binding.rg.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup group, @IdRes int checkedId) {
                switch (checkedId) {
                    case R.id.btn_car://车辆信息
                        showFragment(carFragment.get());
                        break;
                    case R.id.btn_theme://主题
                        showFragment(themeFragment.get());
                        break;
                    case R.id.btn_voice://声控与播报
                        showFragment(voiceFragment.get());
                        break;
                    case R.id.btn_video://监控和视频
                        showFragment(videoFragment.get());
                        break;
                    case R.id.btn_map://地图与导航
                        showFragment(mapFragment.get());
                        break;
                    case R.id.btn_wifi://无线网络
                        startActivity(new Intent(android.provider.Settings.ACTION_WIFI_SETTINGS));
                        break;
                    case R.id.btn_car_code://车载二维码显示
                        try {
                            startActivity(Intent.parseUri("vehicle://remote", Intent.URI_INTENT_SCHEME));
                        } catch (Exception e) {
                            onError("您没有安装桌面应用，无法显示车载二维码");
                        }
                        break;
                }
            }
        });

        if (savedInstanceState != null) {
            currentFragmentTag = savedInstanceState.getString(TAG_CURRENT_FRAGMENT);
            Timber.d("savedInstanceState != null currentFragmentTag = " + currentFragmentTag);
        } else {
            showFragment(carFragment.get());
        }
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putString(TAG_CURRENT_FRAGMENT, currentFragmentTag);
    }

    public void setDayNight(String theme) {
        if (theme.equals(AppConfig.NIGHT_THEME)) {//夜间
            getDelegate().setLocalNightMode(AppCompatDelegate.MODE_NIGHT_YES);
        } else if (theme.equals(AppConfig.DAY_THEME)) {//日间
            getDelegate().setLocalNightMode(AppCompatDelegate.MODE_NIGHT_NO);
        } else {
            getDelegate().setLocalNightMode(AppCompatDelegate.MODE_NIGHT_AUTO);
        }
        recreate();
    }

    private void showFragment(Fragment fragment) {
        FragmentTransaction transaction;
        if (currentFragmentTag != null) {
            Fragment currentFragment = getSupportFragmentManager().findFragmentByTag(currentFragmentTag);
            transaction = getSupportFragmentManager().beginTransaction().hide(currentFragment);
        } else {
            transaction = getSupportFragmentManager().beginTransaction();
        }
        transaction.show(fragment).commit();
        currentFragmentTag = fragment.getClass().getName();
    }

}
