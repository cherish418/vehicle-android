package org.changs.setting.main;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Point;
import android.graphics.RectF;
import android.graphics.Shader;
import android.graphics.SweepGradient;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.util.AttributeSet;
import android.view.View;

import timber.log.Timber;

/**
 * Created by yincs on 2017/9/21.
 */

public class ZmtView extends View {

    private static final float ANGLE2RADIAN = (float) (2 * Math.PI / 360);
    private Paint paint = new Paint();
    private SweepGradient shader;
    private int primaryColor = Color.parseColor("#FA665B");
    private int[] primaryColors = new int[]{primaryColor, Color.WHITE, primaryColor};
    private int progress = 0;
    private int sweepAngle = 0;

    public ZmtView(Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
        paint.setAntiAlias(true);
        paint.setStrokeWidth(20);
        paint.setStyle(Paint.Style.STROKE);

        test();
    }

    private void test() {
        final Handler handler = new Handler();
        handler.postDelayed(new Runnable() {
            @Override
            public void run() {
                int progress = getProgress();
                Timber.d("progress = " + progress);
                setProgress(progress + 1);
                handler.postDelayed(this, 100);
            }
        }, 3000);
    }

    @Override
    protected void onDraw(Canvas canvas) {
        super.onDraw(canvas);
        canvas.rotate(-90, getMeasuredWidth() / 2, getMeasuredHeight() / 2);

        paint.setColor(Color.parseColor("#33404B"));
        canvas.drawCircle(getCenterX(), getCenterY(), getRadius(), paint);//画背景圆

        paint.setColor(Color.parseColor("#FA665B"));
        paint.setShader(getGradient());
        canvas.drawArc(getRectF(), 0, sweepAngle, false, paint);//画进度条
        paint.setShader(null);
        drawDot(getPoint(0), canvas, primaryColor);//画起点
        drawDot(getPoint(sweepAngle), canvas, getCurrentColor(progress % 100f / 100f, primaryColors));//画终点
    }

    private Point getPoint(float sweepAngle) {
        float angle = sweepAngle + 90;
        int radius = getRadius();
        double sin = Math.sin(ANGLE2RADIAN * angle);
        double cos = Math.cos(ANGLE2RADIAN * angle);
        int x = (int) (sin * radius + getCenterX());
        int y = (int) (-cos * radius + getCenterY());
        return new Point(x, y);
    }

    private void drawDot(Point point, Canvas canvas, int color) {
        paint.setColor(color);
        canvas.drawCircle(point.x, point.y, 15, paint);
        paint.setColor(Color.WHITE);
        canvas.drawCircle(point.x, point.y, 5, paint);
    }

    private Shader getGradient() {
        if (shader == null) {
            shader = new SweepGradient(getCenterX(), getCenterY(), primaryColors, null);
        }
        return shader;
    }

    private RectF getRectF() {
        int centerX = getCenterX();
        int centerY = getCenterY();
        int radius = getRadius();
        return new RectF(centerX - radius, centerY - radius, centerX + radius, centerY + radius);
    }


    private int getRadius() {
        return Math.min(getMeasuredWidth(), getMeasuredHeight()) / 10 * 3;
    }

    private int getCenterX() {
        return getMeasuredWidth() / 2;
    }

    private int getCenterY() {
        return getMeasuredHeight() / 2;
    }

    public static int getCurrentColor(float percent, int[] colors) {
        float[][] f = new float[colors.length][3];
        for (int i = 0; i < colors.length; i++) {
            f[i][0] = (colors[i] & 0xff0000) >> 16;
            f[i][1] = (colors[i] & 0x00ff00) >> 8;
            f[i][2] = (colors[i] & 0x0000ff);
        }
        float[] result = new float[3];
        for (int i = 0; i < 3; i++) {
            for (int j = 0; j < f.length; j++) {
                if (f.length == 1 || percent == j / (f.length - 1f)) {
                    result = f[j];
                } else {
                    if (percent > j / (f.length - 1f) && percent < (j + 1f) / (f.length - 1)) {
                        result[i] = f[j][i] - (f[j][i] - f[j + 1][i]) * (percent - j / (f.length - 1f)) * (f.length - 1f);
                    }
                }
            }
        }
        return Color.rgb((int) result[0], (int) result[1], (int) result[2]);
    }

    public void setProgress(int progress) {
        this.progress = progress;
        sweepAngle = 360 * (progress % 100) / 100;
        invalidate();
    }

    public int getProgress() {
        return progress;
    }


    protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
        super.onMeasure(widthMeasureSpec, heightMeasureSpec);
        setMeasuredDimension(measureHeight(widthMeasureSpec), measureHeight(heightMeasureSpec));
    }

    private int measureHeight(int measureSpec) {
        int result = 0; //结果
        int specMode = MeasureSpec.getMode(measureSpec);
        int specSize = MeasureSpec.getSize(measureSpec);
        switch (specMode) {
            case MeasureSpec.AT_MOST:  // 子容器可以是声明大小内的任意大小
                result = specSize;
                break;
            case MeasureSpec.EXACTLY: //父容器已经为子容器设置了尺寸,子容器应当服从这些边界,不论子容器想要多大的空间.  比如EditTextView中的DrawLeft
                result = specSize;
                break;
            case MeasureSpec.UNSPECIFIED:  //父容器对于子容器没有任何限制,子容器想要多大就多大. 所以完全取决于子view的大小
                result = getRadius() + 20;
                break;
            default:
                break;
        }
        return result;
    }

}
