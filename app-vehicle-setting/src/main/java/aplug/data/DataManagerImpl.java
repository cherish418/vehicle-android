package aplug.data;

import javax.inject.Inject;
import javax.inject.Singleton;

import aplug.data.db.DbHelper;
import aplug.data.prefs.PreferencesHelper;

/**
 * Created by yincs on 2017/8/30.
 */

@Singleton
public class DataManagerImpl implements DataManager {

    private final PreferencesHelper preferencesHelper;
    private final DbHelper dbHelper;

    @Inject
    public DataManagerImpl(PreferencesHelper preferencesHelper, DbHelper dbHelper) {
        this.preferencesHelper = preferencesHelper;
        this.dbHelper = dbHelper;
    }

    @Override
    public DbHelper getDbHelper() {
        return dbHelper;
    }

    @Override
    public PreferencesHelper getPreferencesHelper() {
        return preferencesHelper;
    }
}
