package aplug.data.db.model;

import org.greenrobot.greendao.annotation.Entity;
import org.greenrobot.greendao.annotation.Id;
import org.greenrobot.greendao.annotation.Generated;

/**
 * 车辆信息
 * Created by kelly on 2017/9/5.
 */
@Entity(nameInDb = "car")
public class CarData {
    @Id
    private Long id;//id
    private String carNumber;//车牌号
    private String height;//最大装载高度
    private String weight;//货车总重
    private String carType;//车型
    private String length;//车长
    private String theme;//主题 auto-自动切换  day-白天模式  night-夜间模式
    private String voiceSend;//语音播报 detail-详细播报  concise-简介播报 no-不播报
    private String voiceType;//语音包  man-男声  woman-女声
    private boolean isVideo;//监控时录像
    private boolean isMonitor;//监测
    private boolean isPicture;//抓拍异常照片
    private boolean isSetPsw;//需要密码查看
    private String psw;//密码
    private String line;//默认路线算法  offline-离线  online-在线

    @Generated(hash = 1215187179)
    public CarData(Long id, String carNumber, String height, String weight,
                   String carType, String length, String theme, String voiceSend,
                   String voiceType, boolean isVideo, boolean isMonitor, boolean isPicture,
                   boolean isSetPsw, String psw, String line) {
        this.id = id;
        this.carNumber = carNumber;
        this.height = height;
        this.weight = weight;
        this.carType = carType;
        this.length = length;
        this.theme = theme;
        this.voiceSend = voiceSend;
        this.voiceType = voiceType;
        this.isVideo = isVideo;
        this.isMonitor = isMonitor;
        this.isPicture = isPicture;
        this.isSetPsw = isSetPsw;
        this.psw = psw;
        this.line = line;
    }

    @Generated(hash = 678862163)
    public CarData() {
    }

    public Long getId() {
        return this.id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getCarNumber() {
        return this.carNumber;
    }

    public void setCarNumber(String carNumber) {
        this.carNumber = carNumber;
    }

    public String getHeight() {
        return this.height;
    }

    public void setHeight(String height) {
        this.height = height;
    }

    public String getWeight() {
        return this.weight;
    }

    public void setWeight(String weight) {
        this.weight = weight;
    }

    public String getCarType() {
        return this.carType;
    }

    public void setCarType(String carType) {
        this.carType = carType;
    }

    public String getLength() {
        return this.length;
    }

    public void setLength(String length) {
        this.length = length;
    }

    public String getTheme() {
        return this.theme;
    }

    public void setTheme(String theme) {
        this.theme = theme;
    }

    public String getVoiceSend() {
        return this.voiceSend;
    }

    public void setVoiceSend(String voiceSend) {
        this.voiceSend = voiceSend;
    }

    public String getVoiceType() {
        return this.voiceType;
    }

    public void setVoiceType(String voiceType) {
        this.voiceType = voiceType;
    }

    public boolean getIsVideo() {
        return this.isVideo;
    }

    public void setIsVideo(boolean isVideo) {
        this.isVideo = isVideo;
    }

    public boolean getIsMonitor() {
        return this.isMonitor;
    }

    public void setIsMonitor(boolean isMonitor) {
        this.isMonitor = isMonitor;
    }

    public boolean getIsPicture() {
        return this.isPicture;
    }

    public void setIsPicture(boolean isPicture) {
        this.isPicture = isPicture;
    }

    public boolean getIsSetPsw() {
        return this.isSetPsw;
    }

    public void setIsSetPsw(boolean isSetPsw) {
        this.isSetPsw = isSetPsw;
    }

    public String getPsw() {
        return this.psw;
    }

    public void setPsw(String psw) {
        this.psw = psw;
    }

    public String getLine() {
        return this.line;
    }

    public void setLine(String line) {
        this.line = line;
    }
}
