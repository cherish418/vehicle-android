package aplug.data.db;

import aplug.data.db.model.CarData;
import io.reactivex.Observable;

/**
 * 车辆信息
 * Created by yincs on 2017/7/22.
 */

public interface DbHelper {

    Observable<Boolean> insertCar(CarData carData);

    Observable<Boolean> modifyCar(CarData carData);

    Observable<CarData> getCar(long id);
}
