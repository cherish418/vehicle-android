package aplug;


import android.os.Process;

import org.changs.aplug.AplugManager;

import aplug.di.DaggerAppComponent;
import dagger.android.AndroidInjector;
import dagger.android.support.DaggerApplication;
import timber.log.Timber;

/**
 * Created by yincs on 2017/7/25.
 */

public class MyApp extends DaggerApplication {

    @Override
    public void onCreate() {
        super.onCreate();
        Timber.d("onCreate() Process.myPid() " + Process.myPid());
        Timber.d("onCreate() Process.myUid() " + Process.myUid());
    }

    @Override
    protected AndroidInjector<? extends DaggerApplication> applicationInjector() {
        AplugManager.get().onCreate(new AplugManager
                .Config(this)
                .debug());
        Timber.d("application onCreate");

        return DaggerAppComponent.builder().application(this).build();
    }

    @Override
    public void onTerminate() {
        super.onTerminate();
        AplugManager.get().onTerminate();
    }

}