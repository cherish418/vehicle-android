package org.changs.launcher;

import org.junit.Test;

import java.util.HashSet;
import java.util.Set;

import static org.junit.Assert.assertEquals;

/**
 * Example local unit test, which will execute on the development machine (host).
 *
 * @see <a href="http://d.android.com/tools/testing">Testing documentation</a>
 */
public class ExampleUnitTest {
    @Test
    public void addition_isCorrect() throws Exception {
        assertEquals(4, 2 + 2);
    }


    @Test
    public void test1() throws Exception {
//        realCenter(new Point(0, 4));
//        realCenter(new Point(7, 13));
//        realCenter(new Point(10, 14));
//        realCenter(new Point(7, 7));
//        realCenter(new Point(7, 6));
//        realCenter(new Point(9, 3));
//        realCenter(new Point(2, 2));
        realCenter(new Point(599, 599)); //0<=x <= 599
        realCenter(new Point(595, 598)); //0<=x <= 599
        realCenter(new Point(2, 597)); //0<=x <= 599
        realCenter(new Point(1, 598)); //0<=x <= 599

        real2Virtual(new Point(598, 598));
        real2Virtual(new Point(594, 597));
        real2Virtual(new Point(2, 596));
        real2Virtual(new Point(2, 596));


    }

    @Test
    public void test2() throws Exception {
        virtual2Real(new Point(0, 0));
        virtual2Real(new Point(1, 1));
        virtual2Real(new Point(2, 2));
        virtual2Real(new Point(1, 2));
        virtual2Real(new Point(0, 3));
        virtual2Real(new Point(3, 0));
        virtual2Real(new Point(149, 149));
        virtual2Real(new Point(2, 199));
        virtual2Real(new Point(1, 198));
    }

    @Test
    public void test3() throws Exception {
        real2Virtual(new Point(598, 598));
        real2Virtual(new Point(547, 118));
        real2Virtual(new Point(0, 602));
    }


    //从指定的中心点开始环形向外搜索
    @Test
    public void test4() throws Exception {
        Point point = real2Virtual(realCenter(new Point(300, 300)));
        int rang = 1;
        for (int i = 0; i <= rang; i++) {
            if (i == 0) {
                searchCheckLegal(point.x, point.y);
            } else {
                searchH(point.y - i, point.x - i, point.x + i);
                searchH(point.y + i, point.x - i, point.x + i);
                searchV(point.x - i, point.y - i, point.y + i);
                searchV(point.x + i, point.y - i, point.y + i);
            }
        }

        System.out.println("legalCount = " + legalCount);
        System.out.println("illegalCount = " + illegalCount);
    }

    private int legalCount;
    private int illegalCount;
    private Set<Point> set = new HashSet<>();

    private boolean searchCheckLegal(int x, int y) {
        final Point point = virtual2Real(new Point(x, y));
        final boolean add = set.add(point);
        if (!add) {
            System.out.println("有重复的坐标");
        }
        if (point.x > 599 || point.y > 599) {
            illegalCount++;
            System.out.println("x=" + x + ",y=" + y + "," + point);
            return false;
        } else {
            legalCount++;
            search(point.x, point.y);
        }
        return true;
    }

    private void search(int x, int y) {
        System.out.println("ExampleUnitTest.search{" + x + "," + y + "}");
    }

    //29750
    private void searchH(int y, int minX, int maxX) {
        if (y < 0 || y > 199) return;
        if (minX < 0) minX = 0;
        if (maxX > 199) maxX = 199;
        for (int x = minX; x <= maxX; x++) {
            if (!searchCheckLegal(x, y)) {
                break;
            }
        }
    }

    private void searchV(int x, int minY, int maxY) {
        if (x < 0 || x > 199) return;
        if (minY < 0) minY = 0;
        if (maxY > 199) maxY = 199;
        for (int y = minY + 1; y < maxY; y++) {
            if (!searchCheckLegal(x, y)) {
                break;
            }
        }
    }

    private Point virtual2Real(Point point) {
        Point result = new Point();
        if (point.x < point.y) {
            result.x = 4 * point.x + 2;
            result.y = 3 * point.y + point.x + 2;
        } else if (point.x > point.y) {
            result.y = 4 * point.y + 2;
            result.x = 3 * point.x + point.y + 2;
        } else {
            result.x = result.y = 4 * point.x + 2;
        }
//        System.out.println("result = " + result);
        return result;
    }

    private Point real2Virtual(Point point) {
        Point result = new Point();
        if (point.x < point.y) {
            result.x = (point.x - 2) / 4;
            result.y = (point.y - 2 - result.x) / 3;
        } else if (point.x > point.y) {
            result.y = (point.y - 2) / 4;
            result.x = (point.x - 2 - result.y) / 3;
        } else {
            result.x = result.y = (point.x - 2) / 4;
        }
        System.out.println("result = " + result);
        return result;
    }


    public Point realCenter(Point point) {
        Point result = new Point();
        int differ = point.y - point.x;
        int offset = Math.abs(differ) % 3;
        if (offset == 1) {
            offset = 1;
        } else if (offset == 0) {
            offset = 2;
        } else if (offset == 2) {
            offset = 3;
        }
        if (differ >= 0) {
            result.y = point.x / 4 * 4 + differ + offset;
            result.x = point.x / 4 * 4 + 2;
        } else {
            result.x = point.y / 4 * 4 - differ + offset;
            result.y = point.y / 4 * 4 + 2;
        }
        System.out.println("result = " + result);
        return result;
    }


    public static class Point {
        public int x;
        public int y;

        public Point() {
        }

        public Point(int x, int y) {
            this.x = x;
            this.y = y;
        }

        @Override
        public String toString() {
            return "Point{" +
                    x +
                    "," + y +
                    '}';
        }

        @Override
        public int hashCode() {
            return (x + ":" + y).hashCode();
        }

        @Override
        public boolean equals(Object obj) {
            return obj instanceof Point
                    && ((Point) obj).x == x
                    && ((Point) obj).y == y;
        }
    }
}