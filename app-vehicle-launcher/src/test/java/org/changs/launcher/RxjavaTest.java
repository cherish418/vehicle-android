package org.changs.launcher;

import org.junit.Test;

import java.util.Arrays;
import java.util.List;
import java.util.concurrent.CountDownLatch;

import io.reactivex.Observable;
import io.reactivex.functions.Action;
import io.reactivex.functions.Consumer;
import io.reactivex.schedulers.Schedulers;

/**
 * Created by yincs on 2017/10/27.
 */

public class RxjavaTest {

    @Test
    public void test1() throws Exception {
        List<String> arr = Arrays.asList("1", "2", "3", "4", "5");
        Observable.fromIterable(arr)
                .doOnNext(s -> {
                    System.out.println(Thread.currentThread().getName() + "s1 = " + s);
                    Thread.sleep(1000);
                    System.out.println(Thread.currentThread().getName() + "s2 = " + s);
                })

                .doOnComplete(new Action() {
                    @Override
                    public void run() throws Exception {
                        System.out.println(Thread.currentThread().getName() + "doOnComplete");
                    }
                })
                .subscribeOn(Schedulers.io())
                .observeOn(Schedulers.single())
                .doOnComplete(new Action() {
                    @Override
                    public void run() throws Exception {
                        System.out.println(Thread.currentThread().getName() + "doOnComplete2");
                    }
                })
                .subscribe(new Consumer<String>() {
                    @Override
                    public void accept(String s) throws Exception {
                        System.out.println(Thread.currentThread().getName());
                    }
                });

        new CountDownLatch(1).await();
    }
}
