package org.changs.launcher;

import android.content.Context;
import android.support.test.InstrumentationRegistry;
import android.support.test.runner.AndroidJUnit4;

import org.changs.s3.common.tools.NavTools;
import org.junit.Test;
import org.junit.runner.RunWith;

/**
 * Created by yincs on 2017/10/12.
 */
@RunWith(AndroidJUnit4.class)
public class BroadcastTest {
    @Test
    public void navOpen() throws Exception {
        Context context = InstrumentationRegistry.getContext();
        NavTools.enterNav(context);
    }

    @Test
    public void navToLoc() throws Exception {
        Context context = InstrumentationRegistry.getContext();
        NavTools.navToLoc(context, "天天",  22.537234,114.052301);
    }
}
