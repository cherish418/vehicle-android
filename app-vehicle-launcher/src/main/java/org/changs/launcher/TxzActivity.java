package org.changs.launcher;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.widget.Toast;

import org.changs.aplug.utils.JsonUtils;
import org.changs.launcher.databinding.LayoutSoundControllerBinding;
import org.changs.launcher.databinding.LayoutTxzBinding;
import org.changs.launcher.sound.PoIResult;
import org.changs.launcher.sound.SoundController;
import org.changs.launcher.sound.SoundControllerImpl;

/**
 * Created by yincs on 2017/9/12.
 */

public class TxzActivity extends Activity {

    public static void start(Context context) {
        Intent starter = new Intent(context, TxzActivity.class);
        context.startActivity(starter);
    }

    private SoundController ISoundControllerImpl;
    private LayoutSoundControllerBinding controllerBinding;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        ISoundControllerImpl = new SoundControllerImpl(getApplicationContext());
        ISoundControllerImpl.onCreate();
//
//
        LayoutTxzBinding binding = DataBindingUtil.setContentView(this, R.layout.layout_txz);
        binding.btnShow.setOnClickListener(v -> ISoundControllerImpl.open());
        binding.btnHide.setOnClickListener(v -> ISoundControllerImpl.close());
//        binding.btnClickable.setOnClickListener(v -> ISoundControllerImpl.enableClickable(true));
//        binding.btnNotClickable.setOnClickListener(v -> ISoundControllerImpl.enableClickable(false));
        binding.btnAnimStart.setOnClickListener(v -> ISoundControllerImpl.startRecording());
        binding.btnAnimStop.setOnClickListener(v -> ISoundControllerImpl.idle());
        binding.btnShowCity.setOnClickListener(v -> ISoundControllerImpl.showAddressChoice(JsonUtils.parseObject(address, PoIResult.class)));
        binding.btnBottom.setOnClickListener(v -> Toast.makeText(this, "可以点击", Toast.LENGTH_SHORT).show());


//        binding.btnShow.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                if (controllerBinding == null) {
//                    controllerBinding = DataBindingUtil
//                            .inflate(LayoutInflater.from(getApplicationContext()),
//                                    R.layout.layout_sound_controller, null, false);
//                    WindowManager.LayoutParams floatBallParams = new WindowManager.LayoutParams();
//                    floatBallParams.width =  WindowManager.LayoutParams.MATCH_PARENT;
//                    floatBallParams.height = WindowManager.LayoutParams.WRAP_CONTENT;
//                    floatBallParams.gravity = Gravity.BOTTOM;
//                    floatBallParams.type = WindowManager.LayoutParams.TYPE_TOAST;
//                    floatBallParams.flags = WindowManager.LayoutParams.FLAG_NOT_FOCUSABLE |
//                            WindowManager.LayoutParams.FLAG_NOT_TOUCH_MODAL;
//                    floatBallParams.format = PixelFormat.RGBA_8888;
//                    WindowManager windowManager = (WindowManager) getApplicationContext().getSystemService(Context.WINDOW_SERVICE);
//                    windowManager.addView(controllerBinding.getRoot(), floatBallParams);
//                    controllerBinding.vControl.setOnClickListener(new View.OnClickListener() {
//                        @Override
//                        public void onClick(View v) {
//                            Timber.d("onClick() called with: v = [" + v + "]");
////                            controllerBinding.getRoot().setVisibility(View.GONE);
//                            floatBallParams.height = WindowManager.LayoutParams.MATCH_PARENT;
//                            windowManager.updateViewLayout(controllerBinding.getRoot(),floatBallParams);
//                            controllerBinding.vCityList.setVisibility(View.VISIBLE);
//
//                        }
//                    });
//                }
//                controllerBinding.getRoot().setVisibility(View.VISIBLE);
//            }
//        });
//
//        binding.btnHide.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                if (controllerBinding != null)
//                    controllerBinding.getRoot().setVisibility(View.GONE);
//            }
//        });

    }

    String address = "{\"type\":2,\"keywords\":\"商场\",\"poitype\":\"business\",\"action\":\"nav\",\"city\":\"深圳市\",\"count\":8,\"pois\":[{\"lat\":22.67678,\"lng\":114.00468,\"city\":\"深圳\",\"name\":\"福轩购物广场\",\"geo\":\"龙华新区华盛路30号\",\"distance\":320,\"coordtype\":\"GCJ02\",\"source\":2,\"poitype\":2,\"telephone\":\"\",\"website\":\"http:\\/\\/m.dianping.com\\/shop\\/10817966?utm_source=open&appKey=1094069028\",\"avgPrice\":0,\"branchName\":\"\",\"categories\":[\"综合商场\"],\"dealCount\":0,\"hasCoupon\":false,\"hasDeal\":false,\"hasPark\":false,\"hasWifi\":false,\"photoUrl\":\"http:\\/\\/qcloud.dpfile.com\\/pc\\/toBRzd1GaP-yxk3-FUM3k3L33BVd5eKqxRgTxT_6VcHlcUH7rzv8DwqPYkisxejSTYGVDmosZWTLal1WbWRW3A.jpg\",\"regions\":[\"龙华新区\",\"大浪\"],\"reviewCount\":0,\"score\":6,\"scoreDecoration\":0,\"scoreProduct\":0,\"scoreService\":0},{\"lat\":22.674326,\"lng\":113.99438,\"city\":\"深圳\",\"name\":\"大浪商业中心\",\"geo\":\"龙华新区大浪街道华旺路大浪商业中心\",\"distance\":836,\"coordtype\":\"GCJ02\",\"source\":2,\"poitype\":2,\"telephone\":\"0755-28106888\",\"website\":\"http:\\/\\/m.dianping.com\\/shop\\/10811083?utm_source=open&appKey=1094069028\",\"avgPrice\":0,\"branchName\":\"\",\"categories\":[\"综合商场\"],\"dealCount\":0,\"hasCoupon\":false,\"hasDeal\":false,\"hasPark\":false,\"hasWifi\":false,\"photoUrl\":\"http:\\/\\/qcloud.dpfile.com\\/pc\\/BlmJhPK4hESkgFG86WF_yPD8eEC_4CvgPGVRzTqOJZQmdYVslRrwrN8bQtG7blG3TYGVDmosZWTLal1WbWRW3A.jpg\",\"regions\":[\"龙华新区\",\"大浪\"],\"reviewCount\":26,\"score\":7,\"scoreDecoration\":2,\"scoreProduct\":7.5,\"scoreService\":2},{\"lat\":22.67586,\"lng\":114.00098,\"city\":\"深圳\",\"name\":\"金百纳商业广场\",\"geo\":\"龙华新区华昌路华富工业园A-501\",\"distance\":204,\"coordtype\":\"GCJ02\",\"source\":2,\"poitype\":2,\"telephone\":\"\",\"website\":\"http:\\/\\/m.dianping.com\\/shop\\/38075194?utm_source=open&appKey=1094069028\",\"avgPrice\":0,\"branchName\":\"\",\"categories\":[\"综合商场\"],\"dealCount\":0,\"hasCoupon\":false,\"hasDeal\":false,\"hasPark\":false,\"hasWifi\":false,\"photoUrl\":\"http:\\/\\/qcloud.dpfile.com\\/pc\\/4cgoywFURyy5Mmi1m7shK5Xj-ePQ6BnM7afpwkBMZkseMqLHaI9dxFdEUG3hE-B_TYGVDmosZWTLal1WbWRW3A.jpg\",\"regions\":[\"宝安区\"],\"reviewCount\":2,\"score\":6,\"scoreDecoration\":2,\"scoreProduct\":6.900000095367432,\"scoreService\":2},{\"lat\":22.672625,\"lng\":113.99857,\"city\":\"深圳\",\"name\":\"华信购物广场\",\"geo\":\"龙华新区大浪街道华荣路19号\",\"distance\":466,\"coordtype\":\"GCJ02\",\"source\":2,\"poitype\":2,\"telephone\":\"0755-27048216\",\"website\":\"http:\\/\\/m.dianping.com\\/shop\\/3164095?utm_source=open&appKey=1094069028\",\"avgPrice\":0,\"branchName\":\"\",\"categories\":[\"综合商场\"],\"dealCount\":0,\"hasCoupon\":false,\"hasDeal\":false,\"hasPark\":false,\"hasWifi\":false,\"photoUrl\":\"http:\\/\\/qcloud.dpfile.com\\/pc\\/HsE2gt82oF_oGpvxJ0VaBaZWssaf7anBMVGicN9Q0HM1m2288y4rrC36ptR-LwCUTYGVDmosZWTLal1WbWRW3A.jpg\",\"regions\":[\"龙华新区\",\"大浪\"],\"reviewCount\":7,\"score\":6,\"scoreDecoration\":2,\"scoreProduct\":7,\"scoreService\":1},{\"lat\":22.66238,\"lng\":114.01961,\"city\":\"深圳\",\"name\":\"天虹商场\",\"geo\":\"龙华新区龙华街道龙观东路花园新村门楼侧1-5楼\",\"distance\":2224,\"coordtype\":\"GCJ02\",\"source\":2,\"poitype\":2,\"telephone\":\"0755-28149680\",\"website\":\"http:\\/\\/m.dianping.com\\/shop\\/1698067?utm_source=open&appKey=1094069028\",\"avgPrice\":215,\"branchName\":\"龙华店\",\"categories\":[\"综合商场\"],\"dealCount\":0,\"hasCoupon\":false,\"hasDeal\":false,\"hasPark\":false,\"hasWifi\":false,\"photoUrl\":\"http:\\/\\/p0.meituan.net\\/apiback\\/badb6beb820f7557c68dbaa3a7811644103585.jpg%40278w_200h_0e_1l%7Cwatermark%3D1%26%26r%3D1%26p%3D9%26x%3D2%26y%3D2%26relative%3D1%26o%3D20\",\"regions\":[\"龙华新区\",\"龙华\"],\"reviewCount\":107,\"score\":7,\"scoreDecoration\":2,\"scoreProduct\":7.599999904632568,\"scoreService\":2}]}";
}
