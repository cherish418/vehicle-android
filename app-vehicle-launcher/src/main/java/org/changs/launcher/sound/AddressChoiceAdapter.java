package org.changs.launcher.sound;

import org.changs.aplug.widget.irecyclerview.SimpleAdapter;
import org.changs.launcher.R;
import org.changs.launcher.databinding.SoundAddressChoiceItemBinding;

/**
 * Created by yincs on 2017/9/14.
 */

public class AddressChoiceAdapter extends SimpleAdapter<PoIResult.PoiItem, SoundAddressChoiceItemBinding> {
    public AddressChoiceAdapter() {
        super(R.layout.sound_address_choice_item);
    }

    @Override
    public void onBindViewHolder(SoundAddressChoiceItemBinding binding, int position) {
        final PoIResult.PoiItem poiItem = getData().get(position);
        binding.tvPosition.setText(String.valueOf(position + 1));
        binding.tvName.setText(poiItem.getName());
        binding.tvAddress.setText(poiItem.getGeo());
        binding.tvDistance.setText(poiItem.getDistance() + "km");
    }
}
