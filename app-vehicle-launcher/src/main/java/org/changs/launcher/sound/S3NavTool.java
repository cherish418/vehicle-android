package org.changs.launcher.sound;

import android.content.Context;

import com.txznet.sdk.TXZNavManager;
import com.txznet.sdk.bean.Poi;

import org.changs.s3.common.tools.AppTools;
import org.changs.s3.common.tools.NavTools;

import timber.log.Timber;

/**
 * Created by yincs on 2017/9/14.
 */

public class S3NavTool implements TXZNavManager.NavTool {

    private final Context context;

    public S3NavTool(Context context) {
        this.context = context;
    }

    @Override
    public void navToLoc(Poi poi) {
        Timber.d("navToLoc() called with: poi = [" + poi + "]");
        NavTools.navToLoc(context, poi.getName(), poi.getLat(), poi.getLng());
    }

    @Override
    public void speakLimitSpeed() {

    }

    @Override
    public void navHome() {
        Timber.d("navHome() called");
        NavTools.navHome(context);
    }

    @Override
    public void setHomeLoc(Poi poi) {

    }

    @Override
    public void navCompany() {
        Timber.d("navCompany() called");
        NavTools.navCompany(context);
    }

    @Override
    public void setCompanyLoc(Poi poi) {

    }

    @Override
    public boolean isInNav() {
        Timber.d("isInNav() called");
        return false;
    }

    @Override
    public void enterNav() {
        Timber.d("enterNav() called");
//        NavTools.enterNav(context);
        AppTools.launcher(context, AppTools.PKG_NAV);
    }

    @Override
    public void exitNav() {
        Timber.d("exitNav() called");
        NavTools.exitNav(context);
    }

    @Override
    public void setStatusListener(TXZNavManager.NavToolStatusListener navToolStatusListener) {

    }

    @Override
    public void setStatusListener(TXZNavManager.NavToolStatusHighListener navToolStatusHighListener) {

    }
}
