package org.changs.launcher.sound;

import android.support.annotation.Keep;

import java.util.List;

/**
 * Created by yincs on 2017/9/12.
 */

@Keep
public class PoIResult {

    /**
     * type : 2
     * keywords : 白石洲
     * poitype :
     * action : nav
     * city : 深圳市
     * count : 8
     * pois : [{"lat":22.539604,"lng":113.967048,"city":"深圳市","name":"白石洲(地铁站)","geo":"1号线/罗宝线","distance":15416,"coordtype":"GCJ02","source":3,"poitype":1,"telephone":"","website":""},{"lat":22.532715,"lng":113.9684,"city":"深圳市","name":"白石洲","geo":"南山区","distance":16129,"coordtype":"GCJ02","source":3,"poitype":1,"telephone":"","website":""},{"lat":22.53941,"lng":113.968765,"city":"深圳市","name":"白石洲(公交站)","geo":"M487路","distance":15395,"coordtype":"GCJ02","source":3,"poitype":1,"telephone":"","website":""},{"lat":22.543486,"lng":113.965034,"city":"深圳市","name":"白石洲西(公交站)","geo":"26路;43路;58路;58路区间快线车/58路大站快车;B683路;E3路;M486路;M487路;M488路","distance":15050,"coordtype":"GCJ02","source":3,"poitype":1,"telephone":"","website":""},{"lat":22.533175,"lng":113.968238,"city":"深圳市","name":"白石洲3","geo":"沙河街1-6号","distance":16083,"coordtype":"GCJ02","source":3,"poitype":1,"telephone":"","website":""},{"lat":22.532627,"lng":113.968312,"city":"深圳市","name":"白石洲大楼","geo":"白石洲东三坊90-1","distance":16141,"coordtype":"GCJ02","source":3,"poitype":1,"telephone":"","website":""},{"lat":22.539188,"lng":113.968803,"city":"深圳市","name":"白石洲2(公交站)","geo":"113路;320路;323路;324路;327路;328路;338路;369路;383路;K578路(右环)/K578路快线;M222路;M372路;M413路;M435路;M448路;M486路;海滨观光线(观光4线);联通区间线;观光线1路;高峰专线12路;高峰专线74路","distance":15418,"coordtype":"GCJ02","source":3,"poitype":1,"telephone":"","website":""},{"lat":22.539202,"lng":113.968765,"city":"深圳市","name":"白石洲1(公交站)","geo":"21路;70路;79路;90路;101路;123路;204路;209路;223路;234路;245路区间车;M398路;N4路;N4路区间;N24路;高峰专线12路","distance":15418,"coordtype":"GCJ02","source":3,"poitype":1,"telephone":"","website":""}]
     * curPage : 0
     * maxPage : 2
     * titlefix : 白石洲
     * prefix : 找到
     * aftfix : 的结果
     */

    private int type;
    private String keywords;
    private String poitype;
    private String action;
    private String city;
    private int count;
    private int curPage;
    private int maxPage;
    private String titlefix;
    private String prefix;
    private String aftfix;
    private List<PoiItem> pois;

    public int getType() {
        return type;
    }

    public void setType(int type) {
        this.type = type;
    }

    public String getKeywords() {
        return keywords;
    }

    public void setKeywords(String keywords) {
        this.keywords = keywords;
    }

    public String getPoitype() {
        return poitype;
    }

    public void setPoitype(String poitype) {
        this.poitype = poitype;
    }

    public String getAction() {
        return action;
    }

    public void setAction(String action) {
        this.action = action;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public int getCount() {
        return count;
    }

    public void setCount(int count) {
        this.count = count;
    }

    public int getCurPage() {
        return curPage;
    }

    public void setCurPage(int curPage) {
        this.curPage = curPage;
    }

    public int getMaxPage() {
        return maxPage;
    }

    public void setMaxPage(int maxPage) {
        this.maxPage = maxPage;
    }

    public String getTitlefix() {
        return titlefix;
    }

    public void setTitlefix(String titlefix) {
        this.titlefix = titlefix;
    }

    public String getPrefix() {
        return prefix;
    }

    public void setPrefix(String prefix) {
        this.prefix = prefix;
    }

    public String getAftfix() {
        return aftfix;
    }

    public void setAftfix(String aftfix) {
        this.aftfix = aftfix;
    }

    public List<PoiItem> getPois() {
        return pois;
    }

    public void setPois(List<PoiItem> pois) {
        this.pois = pois;
    }

    public static class PoiItem {
        /**
         * lat : 22.539604
         * lng : 113.967048
         * city : 深圳市
         * name : 白石洲(地铁站)
         * geo : 1号线/罗宝线
         * distance : 15416
         * coordtype : GCJ02
         * source : 3
         * poitype : 1
         * telephone :
         * website :
         */

        private double lat;
        private double lng;
        private String city;
        private String name;
        private String geo;
        private int distance;
        private String coordtype;
        private int source;
        private int poitype;
        private String telephone;
        private String website;

        public double getLat() {
            return lat;
        }

        public void setLat(double lat) {
            this.lat = lat;
        }

        public double getLng() {
            return lng;
        }

        public void setLng(double lng) {
            this.lng = lng;
        }

        public String getCity() {
            return city;
        }

        public void setCity(String city) {
            this.city = city;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public String getGeo() {
            return geo;
        }

        public void setGeo(String geo) {
            this.geo = geo;
        }

        public int getDistance() {
            return distance;
        }

        public void setDistance(int distance) {
            this.distance = distance;
        }

        public String getCoordtype() {
            return coordtype;
        }

        public void setCoordtype(String coordtype) {
            this.coordtype = coordtype;
        }

        public int getSource() {
            return source;
        }

        public void setSource(int source) {
            this.source = source;
        }

        public int getPoitype() {
            return poitype;
        }

        public void setPoitype(int poitype) {
            this.poitype = poitype;
        }

        public String getTelephone() {
            return telephone;
        }

        public void setTelephone(String telephone) {
            this.telephone = telephone;
        }

        public String getWebsite() {
            return website;
        }

        public void setWebsite(String website) {
            this.website = website;
        }
    }
}
