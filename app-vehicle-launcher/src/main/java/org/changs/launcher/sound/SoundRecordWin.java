package org.changs.launcher.sound;

import android.os.Handler;

import com.txznet.sdk.TXZResourceManager;

import org.changs.aplug.utils.JsonUtils;

import timber.log.Timber;

/**
 * Created by yincs on 2017/9/14.
 */

public class SoundRecordWin implements TXZResourceManager.RecordWin {

    private SoundController SoundController;
    private Handler handler;

    public SoundRecordWin(SoundController SoundController, Handler handler) {
        this.SoundController = SoundController;
        this.handler = handler;
    }

    @Override
    public void showWheatherInfo(String data) {
//                DebugUtil.showTips("显示天气数据：" + data);
        log("显示天气数据：" + data);
    }

    @Override
    public void showUsrText(String data) {
//                DebugUtil.showTips("显示用户文本：" + data);
        log("显示用户文本：" + data);
        handler.post(() -> SoundController.showUsrText(data));

    }

    @Override
    public void showSysText(String data) {
//                DebugUtil.showTips("显示系统文本：" + data);
        log("显示系统文本：" + data);
        handler.post(() -> SoundController.showSysText(data));
    }

    @Override
    public void showStockInfo(String data) {
//                DebugUtil.showTips("显示股票数据：" + data);
        log("显示股票数据：" + data);
    }

    @Override
    public void showContactChoice(String data) {
//                DebugUtil.showTips("显示联系人选择数据：" + data);
        log("显示联系人选择数据：" + data);
    }

    @Override
    public void showAddressChoice(String data) {
//                DebugUtil.showTips("显示地址选择数据：" + data);
        log("显示地址选择数据：" + data);
        final PoIResult poIResult;
        try {
            poIResult = JsonUtils.parseObject(data, PoIResult.class);
        } catch (Exception e) {
            e.printStackTrace();
            return;
        }
        handler.post(() -> SoundController.showAddressChoice(poIResult));
    }

    @Override
    public void showWxContactChoice(String s) {

    }

    @Override
    public void showAudioChoice(String s) {

    }

    @Override
    public void showListChoice(int i, String s) {

    }

    @Override
    public void snapPager(boolean b) {

    }

    @Override
    public void setOperateListener(RecordWinOperateListener listener) {
        // TODO 记录下监听器，通知窗口操作给大宝
        log("通知窗口操作给大宝：" + listener);
//        this.operate = listener;
    }

    @Override
    public void open() {
//                DebugUtil.showTips("打开录音窗口");
        log("打开录音窗口");
        handler.post(() -> SoundController.open());
    }

    @Override
    public void onVolumeChange(int volume) {
        // DebugUtil.showTips("音量变化：" + volume);
        // TODO 处理音量变化
    }

    @Override
    public void onStatusChange(RecordStatus status) {
//                DebugUtil.showTips("录音状态变化：" + status);
        log("录音状态变化 status = " + status);
//        lastRecordTime = System.currentTimeMillis();
        handler.post(() -> {
            if (status == RecordStatus.STATUS_RECORDING) {
                SoundController.startRecording();
            } else if (status == RecordStatus.STATUS_RECOGONIZING) {
                SoundController.startRecognizing();
            } else {
                SoundController.idle();
            }
        });

//        operate.onTouch();
    }

    @Override
    public void onProgressChanged(int progress) {
//                DebugUtil.showTips("自动操作进度条变化：" + progress);
        log("onProgressChanged progress = " + progress);
        if (progress == -1) {
            handler.post(() -> {
                SoundController.hideAddressChoice();
            });
        }
    }

    @Override
    public void close() {
//                DebugUtil.showTips("关闭录音窗口");
        log("关闭录音窗口");
        handler.post(() -> SoundController.close());

    }

    private void log(String msg) {
        Timber.d(msg);
    }

}
