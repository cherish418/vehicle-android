package org.changs.launcher.sound;

import org.changs.aplug.interf.IService;

/**
 * Created by yincs on 2017/9/14.
 */

public interface SoundController extends IService {


    /**
     * 唤醒声控
     */
    void open();

    /**
     * 关闭声控
     */
    void close();

    /**
     * 开始录音
     */
    void startRecording();

    /**
     * 开始识别
     */
    void startRecognizing();

    /**
     * 停止识别
     */
    void idle();


    /**
     * 显示用户文本
     */
    void showUsrText(String msg);

    /**
     * 显示系统文本
     */
    void showSysText(String msg);

    void showAddressChoice(PoIResult poIResult);

    void hideAddressChoice();
}
