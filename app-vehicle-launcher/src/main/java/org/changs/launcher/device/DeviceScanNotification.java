package org.changs.launcher.device;

import android.app.Notification;
import android.content.Context;
import android.support.v4.app.NotificationManagerCompat;
import android.support.v7.app.NotificationCompat;

import org.changs.launcher.R;

import javax.inject.Inject;

/**
 * Created by yincs on 2017/8/29.
 */

public class DeviceScanNotification {
    public static final String TAG = "DeviceScanNotification";
    public static final String EXTRA_NOTIFICATION_ID = "notificationId";

    private Context mContext;
    private NotificationManagerCompat mNotificationManager;

    @Inject
    public DeviceScanNotification(Context context) {
        this.mContext = context;
        mNotificationManager = NotificationManagerCompat.from(context);
    }

    public void showScanning() {
        final Notification notification = new NotificationCompat.Builder(mContext)
                .setSmallIcon(R.mipmap.ic_launcher)
                .setContentTitle("正在扫描手机...")
                .setContentText("setContentText")
                .setContentInfo("setContentInfo")
                .setOngoing(true)
                .build();
        mNotificationManager.notify(123, notification);
    }

    public void showConnect() {
        final Notification notification = new NotificationCompat.Builder(mContext)
                .setSmallIcon(R.mipmap.ic_launcher)
                .setContentTitle("已连接至手机")
                .setContentText("setContentText")
                .setContentInfo("setContentInfo")
                .setOngoing(true)
                .build();
        mNotificationManager.notify(123, notification);
    }

    public void showConnectFail() {
        final Notification notification = new NotificationCompat.Builder(mContext)
                .setSmallIcon(R.mipmap.ic_launcher)
                .setContentTitle("连接手机失败")
                .setContentText("点击重新连接")
                .setContentInfo("setContentInfo")
                .setOngoing(true)
                .build();
        mNotificationManager.notify(123, notification);
    }

}
