package org.changs.launcher.device;

import android.content.Context;

import com.google.gson.JsonObject;

import org.changs.aplug.utils.DeviceUtils;

/**
 * Created by 惠安 on 2017/11/29.
 */

public class DeviceConnectUtils {

    private static String sAndroidID;

    private static String substringAndroidID(Context context, int length) {
        if (sAndroidID == null) {
            sAndroidID = DeviceUtils.getAndroidID(context);
        }
        if (length == -1) return sAndroidID;
        int real = Math.min(sAndroidID.length(), length);
        String result = sAndroidID.substring(0, real);
        while (result.length() < length) {
            result += result.length();
        }
        return result;
    }

    public static String getDeviceName(Context context) {
        return substringAndroidID(context, -1);
    }

    public static String getApWifiSSID(Context context) {
        return "s3" + substringAndroidID(context, 4);
    }

    public static String getApWifiPwd(Context context) {
        return "s3" + substringAndroidID(context, 8);
    }

    public static JsonObject genDeviceApJsonObject(Context context) {
        JsonObject jsonObject = new JsonObject();
        jsonObject.addProperty("name", getDeviceName(context));
        jsonObject.addProperty("ssid", getApWifiSSID(context));
        jsonObject.addProperty("pwd", getApWifiPwd(context));
        return jsonObject;
    }


}
