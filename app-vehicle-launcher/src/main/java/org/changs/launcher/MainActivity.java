package org.changs.launcher;

import android.content.BroadcastReceiver;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.v4.view.PagerAdapter;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import org.changs.aplug.base.BaseActivity;
import org.changs.aplug.utils.ShellUtils;
import org.changs.launcher.databinding.ActivityMainBinding;
import org.changs.launcher.databinding.LayoutMain1Binding;
import org.changs.launcher.databinding.LayoutMain2Binding;
import org.changs.launcher.ota.OTAPresenter;
import org.changs.launcher.service.RemoteService;
import org.changs.s3.common.RemoteManager;
import org.changs.s3.common.tools.AppTools;

import javax.inject.Inject;

import timber.log.Timber;

public class MainActivity extends BaseActivity implements MainPresenter.V {

    private ActivityMainBinding mBinding;
    private TextView fileView;
    private TextView msgView;

    @Inject
    MainPresenter mainPresenter;
    @Inject
    OTAPresenter oTAPresenter;

    @Override
    public void setup(Bundle savedInstanceState) {
        ShellUtils.execCmd("", true);
        mainPresenter.attachView(this);
        oTAPresenter.attachView(this);
        oTAPresenter.installPackage();

        mBinding = DataBindingUtil.setContentView(this, R.layout.activity_main);
        mBinding.btnTxz.setOnClickListener(v -> {
            TxzActivity.start(this);
        });

        mBinding.viewPager.setAdapter(new PagerAdapter() {
            @Override
            public int getCount() {
                return 2;
            }

            @Override
            public boolean isViewFromObject(View view, Object object) {
                return view == object;
            }

            @Override
            public Object instantiateItem(ViewGroup container, int position) {
                Timber.d("container = " + container);
                View view = container.getChildAt(position);
                if (view == null) {
                    if (position == 0) {
                        LayoutMain1Binding binding = DataBindingUtil.inflate(getLayoutInflater(), R.layout.layout_main1, mBinding.viewPager, false);
                        view = binding.getRoot();
                        fileView = binding.tvRecordCount;
                        msgView = binding.tvMessageUnread;
                        binding.btnMonitor.setOnClickListener(v -> startComponent("org.changs.monitor"));
                        binding.btnMapNav.setOnClickListener(v -> startComponent("org.changs.nav"));
                        binding.btnMessage.setOnClickListener(v -> startComponent("org.changs.message"));
                        binding.btnRecord.setOnClickListener(v -> startComponent("org.changs.record"));
                        binding.btnSetting.setOnClickListener(v -> startComponent("org.changs.setting"));
                    } else {
                        LayoutMain2Binding binding = DataBindingUtil.inflate(getLayoutInflater(), R.layout.layout_main2, mBinding.viewPager, false);
                        view = binding.getRoot();
                        binding.btnMusic.setOnClickListener(v -> startComponent("com.txznet.music"));
                    }
                    container.addView(view, position);
                }
                return view;
            }

            @Override
            public void destroyItem(ViewGroup container, int position, Object object) {
//                super.destroyItem(container, position, object);
            }
        });

        startService(new Intent(this, RemoteService.class));
    }

    private void initData() {
        mainPresenter.getFileNumber();
    }

    @Override
    protected void onResume() {
        super.onResume();
        initData();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
//        unregisterReceiver(homeReceiver);
//        unregisterReceiver(receiver);
    }

    private void startComponent(String pkg) {
        AppTools.launcher(this, pkg);
//        overridePendingTransition(R.anim.fade_in, R.anim.fade_out);
    }


    @Override
    public void onBackPressed() {
//        super.onBackPressed();
    }

    private final BroadcastReceiver homeReceiver = new BroadcastReceiver() {
        final String SYS_KEY = "reason"; // 标注下这里必须是这么一个字符串值

        final String SYS_HOME_KEY = "homekey";// 标注下这里必须是这么一个字符串值

        @Override
        public void onReceive(Context context, Intent intent) {
            String action = intent.getAction();
            if (action.equals(Intent.ACTION_CLOSE_SYSTEM_DIALOGS)) {
                String reason = intent.getStringExtra(SYS_KEY);
                if (reason != null && reason.equals(SYS_HOME_KEY)) {
                    if (showLauncherDialog1() || showLauncherDialog2() || showLauncherDialog3()) {
                    }
                }
            }
        }
    };

    private boolean showLauncherDialog1() {
        try {
            Intent intentw = new Intent(Intent.ACTION_MAIN);
            intentw.addCategory(Intent.CATEGORY_HOME);
            intentw.setClassName("android",
                    "com.android.internal.app.ResolverActivity");
            startActivity(intentw);
            return true;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return false;
    }


    private boolean showLauncherDialog2() {
        try {
            Intent paramIntent = new Intent("android.intent.action.MAIN");
            paramIntent.setComponent(new ComponentName("android", "com.android.internal.app.ResolverActivity"));
            paramIntent.addCategory("android.intent.category.DEFAULT");
            paramIntent.addCategory("android.intent.category.HOME");
            startActivity(paramIntent);
            return true;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return false;
    }

    private boolean showLauncherDialog3() {
        try {
            Intent paramIntent = new Intent("android.intent.action.MAIN");
            paramIntent.setComponent(new ComponentName("com.huawei.android.internal.app",
                    "com.huawei.android.internal.app.HwResolverActivity"));
            paramIntent.addCategory("android.intent.category.DEFAULT");
            paramIntent.addCategory("android.intent.category.HOME");
            startActivity(paramIntent);
            return true;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return false;
    }

    private void register() {
        final IntentFilter filter = new IntentFilter();
        filter.addAction(RemoteManager.ACTION_DEVICE_CONNECTED);
        filter.addAction(RemoteManager.ACTION_DEVICE_DISCONNECT);
        registerReceiver(receiver, filter);
    }

    public BroadcastReceiver receiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            if (intent.getAction().equals(RemoteManager.ACTION_DEVICE_CONNECTED)) {
                mBinding.btnTxz.setText("设备已连接");
            } else if (intent.getAction().equals(RemoteManager.ACTION_DEVICE_DISCONNECT)) {
                mBinding.btnTxz.setText("设备未连接");
            }
        }
    };

    @Override
    public void getFileNumber(int count) {
        fileView.setText("共有" + count + "个文件");
    }
}
