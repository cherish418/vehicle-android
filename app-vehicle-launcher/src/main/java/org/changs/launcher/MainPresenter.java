package org.changs.launcher;

import android.content.Context;

import org.changs.aplug.base.BasePresenter;
import org.changs.aplug.base.IView;

import java.io.File;

import javax.inject.Inject;

import io.reactivex.Observable;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.schedulers.Schedulers;


/**
 * Created by qwe on 2017/12/7.
 */

public class MainPresenter extends BasePresenter<MainPresenter.V> {

    public interface V extends IView {
        void getFileNumber(int count);
    }

    //    private final DataManager dataManager;
    private Context context;
    private String path = "/sdcard/app-s3-record/";

    @Inject
    public MainPresenter(Context context) {
//        this.dataManager = dataManager;
        this.context = context;
    }

    public void getFileNumber() {
        Observable
                .fromCallable(() -> {
                    File image = new File(path + "picture");
                    File[] images = image.listFiles();
                    File video = new File(path + "video");
                    File[] videos = video.listFiles();
                    int imageSize = images == null ? 0 : images.length;
                    int videoSize = videos == null ? 0 : videos.length;
                    return imageSize + videoSize;
                })
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(result -> {
                    getMvpView().getFileNumber(result);
                });
    }
}
