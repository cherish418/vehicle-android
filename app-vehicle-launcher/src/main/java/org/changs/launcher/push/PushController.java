package org.changs.launcher.push;

import android.content.Context;
import android.content.Intent;

import com.iot.push.sdk.IMessageRecevier;
import com.iot.push.sdk.IoTCommunicationClient;
import com.iot.push.sdk.IotSdkData;
import com.iot.push.sdk.IotSdkEventNotify;
import com.iot.push.sdk.message.Message;

import org.changs.aplug.annotation.ServiceScoped;
import org.changs.aplug.interf.IService;
import org.changs.aplug.utils.DeviceUtils;
import org.changs.aplug.utils.JsonUtils;
import org.changs.s3.common.aidl.IUpload;
import org.changs.s3.common.aidl.UploadCallback;

import javax.inject.Inject;

import timber.log.Timber;

/**
 * Created by yincs on 2017/9/21.
 */

@ServiceScoped
public class PushController implements IService, IUpload {
    private final String appId = "9092c4ee91ee49d6870cc7c1900d907e";
    private final String appSecret = "3eae59ff3d8142bdb8b4e2f06f1fcd19";
    private final String clientId;
    private final IMessageRecevier messageRecevier = new MessageRecevier();
    private final IotSdkEventNotify iotSdkEventNotify = new MyEventNotify();

    private Context context;

    @Inject
    public PushController(Context context) {
        this.context = context;

        this.clientId = DeviceUtils.getAndroidID(context);
        Timber.d("clientId = " + clientId);
    }

    public void start() {
        try {
            IoTCommunicationClient.connect(appId, appSecret, clientId, messageRecevier, iotSdkEventNotify);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onCreate() {
        start();
    }

    @Override
    public void onStartCommand(Intent intent, int flags, int startId) {

    }

    @Override
    public void onDestroy() {

    }

    @Override
    public boolean upload(String msg, String filePath, UploadCallback callback) {
        return false;
    }


    private class MessageRecevier implements IMessageRecevier {
        @Override
        public void recevier(Message message) {
            Timber.d("IMessageRecevier message = " + message.getMetaInfo().getContent());
        }
    }

    private class MyEventNotify extends IotSdkEventNotify {

        @Override
        public void recevier(String s, IotSdkData iotSdkData) {
            Timber.d("IotSdkEventNotify iotSdkData = " + JsonUtils.stringify(iotSdkData));
        }
    }
}
