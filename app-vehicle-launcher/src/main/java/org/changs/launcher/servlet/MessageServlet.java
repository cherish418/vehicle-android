package org.changs.launcher.servlet;

import android.content.Context;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.annotation.WorkerThread;

import com.google.gson.JsonObject;

import org.changs.aplug.annotation.ServiceScoped;
import org.changs.aplug.interf.IService;
import org.changs.launcher.device.ConnectServletOpt;
import org.changs.servlet.AplugServlet;
import org.changs.servlet.CommunicationConfig;
import org.changs.servlet.HandleReceiveFile;
import org.changs.servlet.HandleReceiveFileCallback;
import org.changs.servlet.HandleSendFile;
import org.changs.servlet.IOUtils;

import java.io.File;

import javax.inject.Inject;

import timber.log.Timber;

/**
 * Created by yincs on 2017/9/15.
 */

//车机的消息接收中心、再广播出去
@ServiceScoped
public class MessageServlet implements IService {

    private final Context mContext;
    private final ConnectServletOpt mConnectServletOpt;

    @Inject
    public MessageServlet(Context context, ConnectServletOpt connectServletOpt) {
        this.mContext = context;
        this.mConnectServletOpt = connectServletOpt;
    }

    @Override
    public void onCreate() {
        if (!mAplugServlet.start()) {
            Timber.e("mCommunicationServer 通信组件启动失败");
        }
        Timber.d("onCreate: 通信组件启动成功");
    }

    @Override
    public void onStartCommand(Intent intent, int flags, int startId) {
    }

    @Override
    public void onDestroy() {
        mAplugServlet.stop();
        Timber.d("onCreate: 通信组件关闭成功");
    }

    private AplugServlet mAplugServlet = new AplugServlet(CommunicationConfig.S3_SERVER_PORT) {
        @WorkerThread
        @NonNull
        @Override
        protected JsonObject handleRequest(JsonObject request) {
            Timber.d("handleRequest() called with: " + "request = [" + request + "]");
            final String action = request.get(IOUtils.EXTRA_ACTION).getAsString();
            String data = null;
            if (request.has(IOUtils.EXTRA_DATA)) {
                data = request.get(IOUtils.EXTRA_DATA).getAsString();
            }
            Intent intent = new Intent(action);
            if (mConnectServletOpt.getNetworkDevice() != null) {
                intent.putExtra(IOUtils.EXTRA_REMOTE_IP, mConnectServletOpt.getNetworkDevice().getIp());
            }
            if (data != null) intent.putExtra(IOUtils.EXTRA_DATA, data);
            intent.addFlags(Intent.FLAG_INCLUDE_STOPPED_PACKAGES);
            mContext.sendBroadcast(intent);

            final JsonObject jsonObject = new JsonObject();
            jsonObject.addProperty(IOUtils.EXTRA_DATA, IOUtils.OK);
            return jsonObject;
        }

        @WorkerThread
        @NonNull
        @Override
        protected HandleReceiveFile handleReceiveFile(JsonObject request) {
            Timber.d("handleReceiveFile() called with: " + "request = [" + request + "]");
            if ("cmd".equals(request.get(IOUtils.EXTRA_FILE_TYPE).getAsString())) {
                HandleReceiveFile accept = HandleReceiveFile.accept(new File("/sdcard", request.get(IOUtils.EXTRA_FILE_NAME).getAsString()));
                accept.setCallback(new HandleReceiveFileCallback() {
                    @Override
                    public void onStart() {
                    }

                    @Override
                    public void onProgress(int progressPercent) {
                    }

                    @Override
                    public void onSuccess() {
                    }

                    @Override
                    public void onComplete() {
                        Timber.d(" ota文件传输完成 ");
                    }
                });
                return accept;
            }
            return HandleReceiveFile.reject("不收");
        }

        @WorkerThread
        @NonNull
        @Override
        protected HandleSendFile handleSendFile(JsonObject request) {
            Timber.d("handleSendFile() called with: " + "request = [" + request + "]");
            return HandleSendFile.reject("不发");
        }
    };
}
