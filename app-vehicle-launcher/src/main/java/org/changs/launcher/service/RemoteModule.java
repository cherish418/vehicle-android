package org.changs.launcher.service;

import android.content.Context;
import android.net.wifi.WifiManager;
import android.os.Handler;

import dagger.Module;
import dagger.Provides;

/**
 * Created by yincs on 2017/9/15.
 */

@Module
public abstract class RemoteModule {


    @Provides
    static Handler provideHandler() {
        return new Handler();
    }

    @Provides
    static WifiManager provideWifiManager(Context context) {
        return (WifiManager) context.getSystemService(Context.WIFI_SERVICE);
    }

}
