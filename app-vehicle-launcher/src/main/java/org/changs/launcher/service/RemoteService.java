package org.changs.launcher.service;

import android.content.Intent;
import android.os.IBinder;
import android.os.RemoteException;
import android.support.annotation.Nullable;

import org.changs.aplug.interf.IService;
import org.changs.launcher.device.DeviceController;
import org.changs.launcher.push.PushController;
import org.changs.launcher.servlet.MessageServlet;
import org.changs.launcher.sound.SoundControllerImpl;
import org.changs.s3.common.aidl.DeviceScanCallback;
import org.changs.s3.common.aidl.UploadCallback;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

import dagger.android.DaggerService;
import timber.log.Timber;

/**
 * Created by yincs on 2017/9/15.
 */

public class RemoteService extends DaggerService {

    @Inject DeviceController mDeviceController;

    @Inject MessageServlet mMessageServlet;

    @Inject SoundControllerImpl mSoundControllerImpl;

    @Inject PushController mPushController;

    private final List<IService> mService = new ArrayList<>();

    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return new MyBinder();
    }

    private void initServices() {
        mService.clear();
        mService.add(mDeviceController);
        mService.add(mMessageServlet);
        mService.add(mSoundControllerImpl);
        mService.add(mPushController);
    }

    @Override
    public void onCreate() {
        super.onCreate();
        Timber.d("onCreate() called with: " + "");
        initServices();

        for (IService service : mService) {
            try {
                service.onCreate();
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        Timber.d("onStartCommand() called with: " + "intent = [" + intent + "], flags = [" + flags + "], startId = [" + startId + "]");
        for (IService service : mService) {
            try {
                service.onStartCommand(intent, flags, startId);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        return START_STICKY;
    }

    @Override
    public void onDestroy() {
        Timber.d("onDestroy() called with: " + "");
        super.onDestroy();
        for (IService service : mService) {
            try {
                service.onDestroy();
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }


    public class MyBinder extends org.changs.s3.common.aidl.RemoteService.Stub {

        @Override
        public String getRemoteIp(boolean show) throws RemoteException {
            return mDeviceController.getRemoteIp(show);
        }

        @Override
        public boolean isScanning() throws RemoteException {
            return mDeviceController.isScanning();
        }

        @Override
        public boolean startScan() throws RemoteException {
            return mDeviceController.startScan();
        }

        @Override
        public void registerCallback(DeviceScanCallback callback) throws RemoteException {
            mDeviceController.registerCallback(callback);
        }

        @Override
        public void unregisterCallback(DeviceScanCallback callback) throws RemoteException {
            mDeviceController.unregisterCallback(callback);
        }

        @Override
        public boolean upload(String msg, String filePath, UploadCallback callback) throws RemoteException {
            return mPushController.upload(msg, filePath, callback);
        }
    }


}
