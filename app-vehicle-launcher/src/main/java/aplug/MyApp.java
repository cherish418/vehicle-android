package aplug;

import android.content.Context;
import android.support.multidex.MultiDex;

import org.changs.aplug.AplugManager;
import org.changs.aplug.utils.ProcessUtils;
import org.changs.launcher.R;
import org.changs.s3.common.analytics.AnalyticsTracker;

import aplug.di.DaggerAppComponent;
import dagger.android.AndroidInjector;
import dagger.android.support.DaggerApplication;
import io.reactivex.annotations.NonNull;
import io.reactivex.functions.Consumer;
import io.reactivex.plugins.RxJavaPlugins;
import timber.log.Timber;


/**
 * Created by yincs on 2017/7/25.
 */

public class MyApp extends DaggerApplication {

    @Override
    protected void attachBaseContext(Context base) {
        MultiDex.install(this);
        super.attachBaseContext(base);
    }

    @Override
    public void onCreate() {
        super.onCreate();
        boolean defaultProcesses = ProcessUtils.isDefaultProcess(this);
        Timber.d("defaultProcesses = " + defaultProcesses);
    }

    @Override
    protected AndroidInjector<? extends DaggerApplication> applicationInjector() {
        AplugManager.get().onCreate(new AplugManager
                .Config(this)
                .addActivityLifecycleListener(new AnalyticsTracker(this, R.xml.global_tracker))
                .debug());
        RxJavaPlugins.setErrorHandler(new Consumer<Throwable>() {
            @Override
            public void accept(@NonNull Throwable throwable) throws Exception {
                Timber.d("RxJavaPlugins ErrorHandle throwable = [" + throwable + "]");
                throwable.printStackTrace();
            }
        });

        Timber.d("application onCreate");
        return DaggerAppComponent.builder().application(this).build();
    }

    @Override
    public void onTerminate() {
        super.onTerminate();
        AplugManager.get().onTerminate();
    }
}