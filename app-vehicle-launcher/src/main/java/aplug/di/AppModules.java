package aplug.di;

import android.app.Application;
import android.content.Context;
import android.content.SharedPreferences;

import javax.inject.Singleton;

import dagger.Binds;
import dagger.Module;
import dagger.Provides;

/**
 * Created by yincs on 2017/8/27.
 */

//存放全局对象

@Module
public abstract class AppModules {


    @Singleton
    @Binds
    abstract Context provideContext(Application application);

    @Singleton
    @Provides
    static SharedPreferences provideSharedPreferences(Context context) {
        return context.getSharedPreferences("prefs", Context.MODE_PRIVATE);
    }


//    @Singleton
//    @Binds
//    abstract ApiHelper provideApiHelper(AppApiHelper impl);

//    @Singleton
//    @Binds
//    abstract PreferencesHelper providePreferencesHelper(PreferencesHelperImpl impl);
//
//
//    @Singleton
//    @Binds
//    abstract OfflineMapHelper provideSearchRecordHelper(OfflineMapHelper.Impl impl);
//
//    @Singleton
//    @Binds
//    abstract DataManager provideDataManager(DataManagerImpl impl);
//
//    @Singleton
//    @Binds
//    abstract MediaManager provideMediaManager(MediaManagerImpl impl);
}
