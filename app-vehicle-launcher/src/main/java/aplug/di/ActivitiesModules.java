package aplug.di;


import org.changs.aplug.annotation.ActivityScoped;
import org.changs.aplug.annotation.ServiceScoped;
import org.changs.launcher.MainActivity;
import org.changs.launcher.service.RemoteActivity;
import org.changs.launcher.service.RemoteModule;
import org.changs.launcher.service.RemoteService;

import dagger.Module;
import dagger.android.ContributesAndroidInjector;

/**
 * Created by yincs on 2017/8/26.
 */

@Module
abstract public class ActivitiesModules {

    @ActivityScoped
    @ContributesAndroidInjector
    abstract MainActivity MainActivity();

    @ServiceScoped
    @ContributesAndroidInjector(modules = RemoteModule.class)
    abstract RemoteService RemoteService();

    @ActivityScoped
    @ContributesAndroidInjector
    abstract RemoteActivity RemoteActivity();
}
