package org.changs.monitor;

import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.constraint.ConstraintLayout;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import org.changs.aplug.base.BaseFragment;
import org.changs.monitor.camera.CameraTaskManager;
import org.changs.monitor.databinding.Test1Binding;
import org.changs.s3.common.SoundCmdReceiver;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

/**
 * Created by yincs on 2017/11/8.
 */

public class Test1Fragment extends BaseFragment implements SoundCmdReceiver.onReceiveCmdListener {
    @Inject CameraTaskManager mCameraTaskManager;

    private Test1Binding mBinding;
    private List<CameraTaskManager.CameraTask.TextureSurfaceFactory> mTextureSurfaces = new ArrayList<>();

    @Override
    public View setup(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        mBinding = DataBindingUtil.inflate(inflater, R.layout.test1, container, false);
        return mBinding.getRoot();
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        CameraTaskManager.CameraTask cameraTask0 = mCameraTaskManager.getCameraTask("0");
//        CameraTaskManager.CameraTask cameraTask1 = mCameraTaskManager.getCameraTask("1");
//        CameraTaskManager.CameraTask cameraTask2 = mCameraTaskManager.getCameraTask("2");
//        CameraTaskManager.CameraTask cameraTask3 = mCameraTaskManager.getCameraTask("3");
        mTextureSurfaces.add(cameraTask0.newTextureSurface(mBinding.texture0));
//        mTextureSurfaces.add(cameraTask1.newTextureSurface(mBinding.texture1));
//        mTextureSurfaces.add(cameraTask2.newTextureSurface(mBinding.texture2));
//        mTextureSurfaces.add(cameraTask3.newTextureSurface(mBinding.texture3));

        mBinding.texture0.setOnClickListener(v -> trans(1, 1));
        mBinding.texture1.setOnClickListener(v -> trans(1, 0));
        mBinding.texture2.setOnClickListener(v -> trans(0, 1));
        mBinding.texture3.setOnClickListener(v -> trans(0, 0));
    }


    /**
     * 转成hp\vp大小
     */
    private void trans2(float hp, float vp) {
        ConstraintLayout.LayoutParams lhp = (ConstraintLayout.LayoutParams) mBinding.guidelineHorizontal.getLayoutParams();
        ConstraintLayout.LayoutParams lpv = (ConstraintLayout.LayoutParams) mBinding.guidelineVertical.getLayoutParams();
        if (lhp.guidePercent != hp || lpv.guidePercent != vp) {
            lhp.guidePercent = hp;
            lpv.guidePercent = vp;
            mBinding.guidelineHorizontal.setLayoutParams(lhp);
            mBinding.guidelineVertical.setLayoutParams(lpv);
        }
    }

    /**
     * 大小来回转化
     */
    private void trans(float hp, float vp) {
        ConstraintLayout.LayoutParams lhp = (ConstraintLayout.LayoutParams) mBinding.guidelineHorizontal.getLayoutParams();
        ConstraintLayout.LayoutParams lpv = (ConstraintLayout.LayoutParams) mBinding.guidelineVertical.getLayoutParams();
        if (lhp.guidePercent == hp && lpv.guidePercent == vp) {//放大状态
            lhp.guidePercent = 0.5f;
            lpv.guidePercent = 0.5f;
        } else {
            lhp.guidePercent = hp;
            lpv.guidePercent = vp;
        }
        mBinding.guidelineHorizontal.setLayoutParams(lhp);
        mBinding.guidelineVertical.setLayoutParams(lpv);
    }

    @Override
    public void onResume() {
        super.onResume();
        for (CameraTaskManager.CameraTask.TextureSurfaceFactory textureSurface : mTextureSurfaces) {
            textureSurface.onResume();
        }
        SoundCmdReceiver.setOnReceiveCmdListener(this);
    }

    @Override
    public void onPause() {
        super.onPause();
        for (CameraTaskManager.CameraTask.TextureSurfaceFactory textureSurface : mTextureSurfaces) {
            textureSurface.onPause();
        }
        SoundCmdReceiver.setOnReceiveCmdListener(null);
    }

    @Override
    public void onReceiveCmd(String data) {
        switch (data) {
            case SoundCmdReceiver.CMD_MONITOR_MAGNIFY_ALL:
                trans2(0.5f, 0.5f);break;
            case SoundCmdReceiver.CMD_MONITOR_MAGNIFY_FRONT:
                trans2(1, 1);break;
            case SoundCmdReceiver.CMD_MONITOR_MAGNIFY_BEHIND:
                trans2(0, 1);break;
            case SoundCmdReceiver.CMD_MONITOR_MAGNIFY_LEFT:
                trans2(1, 0);break;
            case SoundCmdReceiver.CMD_MONITOR_MAGNIFY_RIGHT:
                trans2(0, 0);break;
        }
    }
}
