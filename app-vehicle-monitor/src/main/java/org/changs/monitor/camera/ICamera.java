package org.changs.monitor.camera;

import java.util.List;

/**
 * Created by 惠安 on 2017/12/4.
 */

/**
 * <p>
 * 存放文件时[f|b|l|r]分别表示前|后|左|后摄向头
 * </p>
 * <p>
 * 视频文件存放位置及格式         {mediaDir}/video/yyyyMMddHHmmss_[f|b|l|r].mp4
 * **视频文件名称示例:201710101201_f.mp4 表示在2017年10月10日12时01分前置摄向头录制的视频
 * 录制中的临时文件存放位置及格式 {mediaDir}/video/yyyyMMddHHmmss_[f|b|l|r].mp4.temp
 * 图片文件存放位置及格式         {mediaDir}/picture/yyyyMMddHHmmssSSS_[f|b|l|r].mp4
 * **图片文件名称示例:201710101201001_f.mp4 表示在2017年10月10日12时01分001毫秒前置摄向头拍照的照片
 * </p>
 */
public interface ICamera {

    int SITE_FRONT  = 1;        //前摄向头
    int SITE_BEHIND = 1 << 1;   //后摄向头
    int SITE_LEFT   = 1 << 2;   //左摄向头
    int SITE_RIGHT  = 1 << 3;   //右摄向头

    /**
     * 初始化
     * <p>
     * 设置媒体目录剩余空间最低告警值
     * 当目录剩余空间低于该值时为触发回调{@link Callback#onMediaDirLowMemory()}
     * 应用端会在该回调中根据业务场景删除一些旧文件
     * </p>
     *
     * @param mediaDir       媒体存储目录
     * @param lowMemoryLimit 媒体目录剩余空间最低告警值
     * @param callback       事件回调
     */
    void init(String mediaDir, long lowMemoryLimit, Callback callback);


    /**
     * 开启|关闭视频录制
     *
     * @param enable true表示开启
     * @param site   摄向头方位 支持同时开启多个,例如 SITE_FRONT|SITE_BEHIND|SITE_LEFT|SITE_RIGHT表示开启全部
     *               即二进制1表示只选择了前置摄向头、二进制1001表示选择了右、前摄向头、二进制1101表示选择了右、左、前摄向头
     */
    void enableVideoRecord(boolean enable, int site);

    /**
     * 抓拍
     *
     * @param site 摄向头方位 支持同时开启多个,例如 SITE_FRONT|SITE_BEHIND|SITE_LEFT|SITE_RIGHT表示开启全部
     */
    void takePicture(int site);


    /**
     * 回调函数
     */
    interface Callback {

        /**
         * 开启录制成功
         *
         * @param site 摄向头方位 支持同时开启多个,例如 SITE_FRONT|SITE_BEHIND|SITE_LEFT|SITE_RIGHT表示开启全部
         */
        void onVideoRecordSuccess(int site);

        /**
         * 抓拍成功，
         *
         * @param pictureNames 返回该次抓拍的图片名称集合
         */
        void onTakePictureSuccess(List<String> pictureNames);

        /**
         * 媒体目录剩余空间最低告警值回调
         */
        void onMediaDirLowMemory();

        /**
         * 错误回调
         *
         * @param site 摄向头方位 支持同时开启多个,例如 SITE_FRONT|SITE_BEHIND|SITE_LEFT|SITE_RIGHT表示开启全部
         * @param code 1xx、录制失败 2xx、拍照失败
         * @param msg  错误描述
         */
        void onError(int site, int code, String msg);

    }
}
