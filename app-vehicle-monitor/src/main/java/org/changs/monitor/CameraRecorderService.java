package org.changs.monitor;

import android.content.Intent;
import android.os.IBinder;
import android.support.annotation.Nullable;

import dagger.android.DaggerService;

/**
 * Created by yincs on 2017/10/18.
 */

public class CameraRecorderService extends DaggerService {

    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    @Override
    public void onCreate() {
        super.onCreate();
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        return super.onStartCommand(intent, flags, startId);
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
    }


}
