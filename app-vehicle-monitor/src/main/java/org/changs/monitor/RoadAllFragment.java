package org.changs.monitor;

import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import org.changs.aplug.base.BaseFragment;
import org.changs.monitor.camera.CameraTaskManager;
import org.changs.monitor.databinding.RoadAllFragmentBinding;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

/**
 * Created by yincs on 2017/10/18.
 */

public class RoadAllFragment extends BaseFragment {

    private static final int SIZE = 4;

    @Inject CameraTaskManager mCameraTaskManager;
    private RoadAllFragmentBinding mBinding;
    private List<CameraTaskManager.CameraTask.TextureSurfaceFactory> mTextureSurfaces = new ArrayList<>();

    @Override
    public View setup(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        mBinding = DataBindingUtil.inflate(inflater, R.layout.road_all_fragment, container, false);
        CameraTaskManager.CameraTask cameraTask0 = mCameraTaskManager.getCameraTask("0");
        CameraTaskManager.CameraTask cameraTask1 = mCameraTaskManager.getCameraTask("1");
        CameraTaskManager.CameraTask cameraTask2 = mCameraTaskManager.getCameraTask("2");
        CameraTaskManager.CameraTask cameraTask3 = mCameraTaskManager.getCameraTask("3");
        mTextureSurfaces.add(cameraTask0.newTextureSurface(mBinding.texture0));
        mTextureSurfaces.add(cameraTask1.newTextureSurface(mBinding.texture1));
        mTextureSurfaces.add(cameraTask2.newTextureSurface(mBinding.texture2));
        mTextureSurfaces.add(cameraTask3.newTextureSurface(mBinding.texture3));
        mBinding.texture0.setOnClickListener(new MyCameraClickListener("0"));
        mBinding.texture1.setOnClickListener(new MyCameraClickListener("1"));
        mBinding.texture2.setOnClickListener(new MyCameraClickListener("2"));
        mBinding.texture3.setOnClickListener(new MyCameraClickListener("3"));
        return mBinding.getRoot();
    }

    @Override
    public void onResume() {
        super.onResume();
        for (CameraTaskManager.CameraTask.TextureSurfaceFactory textureSurface : mTextureSurfaces) {
            textureSurface.onResume();
        }
    }

    @Override
    public void onPause() {
        super.onPause();
        for (CameraTaskManager.CameraTask.TextureSurfaceFactory textureSurface : mTextureSurfaces) {
            textureSurface.onPause();
        }
    }

    private class MyCameraClickListener implements View.OnClickListener {
        private final String cameraId;

        private MyCameraClickListener(String cameraId) {
            this.cameraId = cameraId;
        }

        @Override
        public void onClick(View v) {
            getFragmentManager()
                    .beginTransaction()
                    .add(R.id.fl_container, RoadFocusFragment.newInstance(cameraId))
                    .addToBackStack(null)
                    .commitAllowingStateLoss();
        }
    }
}
