package org.changs.monitor;

import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import org.changs.aplug.base.BaseFragment;
import org.changs.monitor.camera.CameraTaskManager;
import org.changs.monitor.databinding.FocusFragmentBinding;

import javax.inject.Inject;

/**
 * Created by yincs on 2017/10/18.
 */

public class RoadFocusFragment extends BaseFragment {
    private static final String TAG_CAMERA_ID = "tag_camera_id";
    private CameraTaskManager.CameraTask.TextureSurfaceFactory mSurfaceFactory;

    public static RoadFocusFragment newInstance(String id) {
        Bundle args = new Bundle();
        args.putString(TAG_CAMERA_ID, id);
        RoadFocusFragment fragment = new RoadFocusFragment();
        fragment.setArguments(args);
        return fragment;
    }

    private FocusFragmentBinding mBinding;
    @Inject CameraTaskManager mCameraTaskManager;

    @Override
    public View setup(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        mBinding = DataBindingUtil.inflate(inflater, R.layout.focus_fragment, container, false);
        String cameraId = getArguments().getString(TAG_CAMERA_ID);
        mSurfaceFactory = mCameraTaskManager.getCameraTask(cameraId).newTextureSurface(mBinding.texture);
        mBinding.vLeft.setOnClickListener(v -> getFragmentManager().popBackStack());
        return mBinding.getRoot();
    }

    @Override
    public void onResume() {
        super.onResume();
        mSurfaceFactory.onResume();
    }

    @Override
    public void onPause() {
        super.onPause();
        mSurfaceFactory.onPause();
    }
}
