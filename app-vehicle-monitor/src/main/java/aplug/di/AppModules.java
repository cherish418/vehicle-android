package aplug.di;

import javax.inject.Singleton;

import aplug.data.DataManager;
import aplug.data.DataManagerImpl;
import aplug.data.db.AppDbHelper;
import aplug.data.db.DbHelper;
import aplug.data.prefs.PreferencesHelper;
import aplug.data.prefs.PreferencesHelperImpl;
import dagger.Binds;
import dagger.Module;

/**
 * Created by yincs on 2017/8/27.
 */

//存放全局对象
@Singleton
@Module
public abstract class AppModules {

    @Singleton
    @Binds
    abstract PreferencesHelper providePreferencesHelper(PreferencesHelperImpl impl);

    @Singleton
    @Binds
    abstract DbHelper provideSearchRecordHelper(AppDbHelper impl);

    @Singleton
    @Binds
    abstract DataManager provideDataManager(DataManagerImpl impl);


}
