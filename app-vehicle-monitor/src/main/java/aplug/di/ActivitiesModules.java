package aplug.di;


import org.changs.aplug.annotation.ActivityScoped;
import org.changs.aplug.annotation.FragmentScoped;
import org.changs.monitor.CameraRecorderService;
import org.changs.monitor.MainActivity;
import org.changs.monitor.RoadAllFragment;
import org.changs.monitor.RoadFocusFragment;
import org.changs.monitor.Test1Fragment;
import org.changs.monitor.TestFragment;

import dagger.Module;
import dagger.android.ContributesAndroidInjector;

/**
 * Created by yincs on 2017/8/26.
 */

@Module
public abstract class ActivitiesModules {
    @ActivityScoped
    @ContributesAndroidInjector(modules = MainModule.class)
    abstract MainActivity MainActivity();

    @Module
    public abstract static class MainModule {

        @FragmentScoped
        @ContributesAndroidInjector
        abstract RoadAllFragment RoadAllFragment();

        @FragmentScoped
        @ContributesAndroidInjector
        abstract RoadFocusFragment RoadFocusFragment();

        @FragmentScoped
        @ContributesAndroidInjector
        abstract TestFragment TestFragment();

        @FragmentScoped
        @ContributesAndroidInjector
        abstract Test1Fragment Test1Fragment();

    }

    @ActivityScoped
    @ContributesAndroidInjector
    abstract CameraRecorderService CameraRecorderService();


}
