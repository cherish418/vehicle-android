package aplug;

/**
 * Created by yincs on 2017/8/24.
 */

public final class AppConfig {

    //aplug
    public static final String PREFERENCE_FILE_NAME = "preference";
    public static final String DATABASE_FILE_NAME = "data";

    //主题
    public static final String DAY_THEME = "day";
    public static final String NIGHT_THEME = "night";
    public static final String AUTO_THEME = "auto";

    //播报声音
    public static final String VOICE_MAN = "man";
    public static final String VOICE_WOMAN = "woman";

    //播报形式
    public static final String VOICE_DETAIL = "detail";
    public static final String VOICE_CONCISE = "concise";
    public static final String VOICE_NO = "no";

    //地图线路
    public static final String MAP_ONLINE = "online";
    public static final String MAP_OFFLINE = "offline";
}
