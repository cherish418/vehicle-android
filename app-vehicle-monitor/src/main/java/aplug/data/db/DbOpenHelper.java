package aplug.data.db;

import android.content.Context;
import android.content.ContextWrapper;
import android.database.DatabaseErrorHandler;
import android.database.sqlite.SQLiteDatabase;
import android.os.Environment;
import android.util.Log;

import org.greenrobot.greendao.database.Database;

import java.io.File;
import java.io.IOException;

import javax.inject.Inject;
import javax.inject.Singleton;

import aplug.AppConfig;
import aplug.data.db.model.DaoMaster;
import timber.log.Timber;

/**
 * Created by yincs on 2017/7/22.
 */
@Singleton
public class DbOpenHelper extends DaoMaster.OpenHelper {

    @Inject
    public DbOpenHelper(Context context) {
        super(new ContextWrapper(context) {
            @Override
            public File getDatabasePath(String name) {
                // 判断是否存在sd卡
                boolean sdExist = Environment.MEDIA_MOUNTED.equals(Environment.getExternalStorageState());
                if (!sdExist) {// 如果不存在,
                    Log.e("SD卡管理：", "SD卡不存在，请加载SD卡");
                    return null;
                } else {// 如果存在
                    // 获取sd卡路径
                    String dbDir = Environment.getExternalStorageDirectory().getAbsolutePath();
                    dbDir += "/net.comikon.reader";// 数据库所在目录
                    String dbPath = dbDir + "/" + name;// 数据库路径
                    // 判断目录是否存在，不存在则创建该目录
                    File dirFile = new File(dbDir);
                    if (!dirFile.exists())
                        dirFile.mkdirs();

                    // 数据库文件是否创建成功
                    boolean isFileCreateSuccess = false;
                    // 判断文件是否存在，不存在则创建该文件
                    File dbFile = new File(dbPath);
                    if (!dbFile.exists()) {
                        try {
                            isFileCreateSuccess = dbFile.createNewFile();// 创建文件
                        } catch (IOException e) {
                            e.printStackTrace();
                        }
                    } else
                        isFileCreateSuccess = true;
                    // 返回数据库文件对象
                    if (isFileCreateSuccess)
                        return dbFile;
                    else
                        return super.getDatabasePath(name);
                }
            }

            @Override
            public SQLiteDatabase openOrCreateDatabase(String name, int mode, SQLiteDatabase.CursorFactory factory) {
                return super.openOrCreateDatabase(name, mode, factory);
            }

            @Override
            public SQLiteDatabase openOrCreateDatabase(String name, int mode, SQLiteDatabase.CursorFactory factory, DatabaseErrorHandler errorHandler) {
                return super.openOrCreateDatabase(name, mode, factory, errorHandler);
            }
        }, AppConfig.DATABASE_FILE_NAME);
    }

    @Override
    public void onUpgrade(Database db, int oldVersion, int newVersion) {
        super.onUpgrade(db, oldVersion, newVersion);
        Timber.d("DEBUG", "DB_OLD_VERSION : " + oldVersion + ", DB_NEW_VERSION : " + newVersion);
        switch (oldVersion) {
            case 1:
            case 2:
                //db.execSQL("ALTER TABLE " + UserDao.TABLENAME + " ADD COLUMN "
                // + UserDao.Properties.Name.columnName + " TEXT DEFAULT 'DEFAULT_VAL'");
        }
    }
}
