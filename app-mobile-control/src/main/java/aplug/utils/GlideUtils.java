package aplug.utils;

import android.content.Context;
import android.graphics.Bitmap;
import android.support.v4.graphics.drawable.RoundedBitmapDrawable;
import android.support.v4.graphics.drawable.RoundedBitmapDrawableFactory;
import android.widget.ImageView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.request.target.BitmapImageViewTarget;

import org.changs.control.R;


/**
 * 图片加载框架
 * Created by Administrator on 2017/8/11.
 */

public class GlideUtils {
    //加载图片
    public static void loadImage(Context context, String url, ImageView imageView) {
        Glide.with(context)
                .load(url)
                .asBitmap()
                .placeholder(R.mipmap.icon_document_pic_normal)
                .error(R.mipmap.icon_document_pic_normal)
                .diskCacheStrategy(DiskCacheStrategy.ALL) //设置缓存
                .into(imageView);
    }

    //加载图片
    public static void loadBigImage(Context context, String url, ImageView imageView) {
        Glide.with(context)
                .load(url)
                .asBitmap()
                .placeholder(R.mipmap.icon_img)
                .error(R.mipmap.icon_img)
                .diskCacheStrategy(DiskCacheStrategy.ALL) //设置缓存
                .into(imageView);
    }

    //加载圆角图片
    public static void loadRoundImage(final Context context, String url, final ImageView imageView) {
        Glide.with(context)
                .load(url)
                .asBitmap()
                .placeholder(R.mipmap.icon_document_pic_normal)
                .error(R.mipmap.icon_document_pic_normal)
                .diskCacheStrategy(DiskCacheStrategy.ALL) //设置缓存
                .into(new BitmapImageViewTarget(imageView) {
                    @Override
                    protected void setResource(Bitmap resource) {
                        super.setResource(resource);
                        RoundedBitmapDrawable circularBitmapDrawable =
                                RoundedBitmapDrawableFactory.create(context.getResources(), resource);
                        circularBitmapDrawable.setCornerRadius(10); //设置圆角弧度
                        imageView.setImageDrawable(circularBitmapDrawable);
                    }
                });
    }

    //加载圆形图片
    public static void loadCirclePic(final Context context, String url, final ImageView imageView) {
        Glide.with(context)
                .load(url)
                .asBitmap()
                .placeholder(R.mipmap.icon_document_pic_normal)
                .error(R.mipmap.icon_document_pic_normal)
                .diskCacheStrategy(DiskCacheStrategy.ALL) //设置缓存
                .into(new BitmapImageViewTarget(imageView) {
                    @Override
                    protected void setResource(Bitmap resource) {
                        RoundedBitmapDrawable circularBitmapDrawable =
                                RoundedBitmapDrawableFactory.create(context.getResources(), resource);
                        circularBitmapDrawable.setCircular(true);
                        imageView.setImageDrawable(circularBitmapDrawable);
                    }
                });
    }
}
