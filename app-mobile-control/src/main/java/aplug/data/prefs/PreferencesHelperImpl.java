package aplug.data.prefs;


import android.content.Context;
import android.content.SharedPreferences;

import javax.inject.Inject;
import javax.inject.Singleton;

import aplug.AppConfig;

/**
 * Created by yincs on 2017/7/22.
 */

@Singleton
public class PreferencesHelperImpl implements PreferencesHelper {

    private final SharedPreferences prefs;

    @Inject
    public PreferencesHelperImpl(Context context) {
        prefs = context.getSharedPreferences(AppConfig.PREFERENCE_FILE_NAME, Context.MODE_PRIVATE);
    }

    @Override
    public void setIsOpen(boolean isOpen) {
        prefs.edit().putBoolean("isOpen", isOpen).apply();
    }

    @Override
    public boolean getIsOpen() {
        return prefs.getBoolean("isOpen", false);
    }

    @Override
    public void setPsw(String psw) {
        prefs.edit().putString("psw", psw).apply();
    }

    @Override
    public String getPsw() {
        return prefs.getString("psw", "");
    }

    @Override
    public void setWifiAutoUpdate(boolean enable) {
        prefs.edit().putBoolean("wifiAutoUpdate", enable).apply();
    }

    @Override
    public boolean isWifiAutoUpdate() {
        return prefs.getBoolean("wifiAutoUpdate", true);
    }

    @Override
    public boolean isAlreadySyncMap() {
        return prefs.getBoolean("isAlreadySyncMap", false);
    }

    @Override
    public void setAlreadySyncMap(boolean isAlreadySyncMap) {
        prefs.edit().putBoolean("isAlreadySyncMap", isAlreadySyncMap).apply();
    }

    @Override
    public String getCarVersion() {
        return prefs.getString("CarVersion", "");
    }

    @Override
    public void setCarVersion(String carVersion) {
        prefs.edit().putString("CarVersion", carVersion).apply();
    }

    @Override
    public String getCarVersionWait2Vehicle() {
        return prefs.getString("CarVersionWait2Vehicle", null);
    }


    @Override
    public void setCarVersionWait2Vehicle(String path) {
        prefs.edit().putString("CarVersionWait2Vehicle", path).apply();
    }
}
