package aplug.data.prefs;

/**
 * Created by yincs on 2017/7/22.
 */

public interface PreferencesHelper {

    void setIsOpen(boolean isOpen);

    boolean getIsOpen();

    void setPsw(String psw);

    String getPsw();

    void setWifiAutoUpdate(boolean enable);

    boolean isWifiAutoUpdate();


    /**
     * 是否已同步地图
     */
    boolean isAlreadySyncMap();
    void setAlreadySyncMap(boolean isAlreadySyncMap);


    /**
     * 车机版本
     */
    String  getCarVersion();
    void setCarVersion(String carVersion);

    /**
     * 是否有已经下载好的文件
     */
    String getCarVersionWait2Vehicle();
    void setCarVersionWait2Vehicle(String path);
}
