package aplug.data.db;

import org.changs.aplug.interf.IRxCurdDataHelper;

import java.util.List;

import javax.inject.Inject;
import javax.inject.Singleton;

import aplug.data.db.model.Machine;
import aplug.data.db.model.MachineDao;
import io.reactivex.Observable;

/**
 * Created by yincs on 2017/9/11.
 */

@Singleton
public class MachineHelper implements IRxCurdDataHelper<Machine> {
    private final MachineDao mDao;

    @Inject
    public MachineHelper(DbOpenHelper helper) {
        mDao = helper.getDaoSession().getMachineDao();
    }

    @Override
    public Observable<Boolean> insertOrUpdate(Machine data) {
        return Observable.fromCallable(() -> {
            try {
                mDao.insertOrReplace(data);
                return true;
            } catch (Exception e) {
                e.printStackTrace();
                return false;
            }
        });
    }

    @Override
    public Observable<Boolean> delete(Machine data) {
        return Observable.fromCallable(() -> {
            try {
                mDao.delete(data);
                return true;
            } catch (Exception e) {
                e.printStackTrace();
                return false;
            }
        });
    }

    @Override
    public Observable<Boolean> clear() {
        return Observable.fromCallable(() -> {
            try {
                mDao.deleteAll();
                return true;
            } catch (Exception e) {
                e.printStackTrace();
                return false;
            }
        });
    }

    @Override
    public Observable<List<Machine>> loadAll() {
        return Observable.fromCallable(() -> mDao.loadAll());
    }
}
