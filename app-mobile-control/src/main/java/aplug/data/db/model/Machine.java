package aplug.data.db.model;

import org.greenrobot.greendao.annotation.Entity;
import org.greenrobot.greendao.annotation.Generated;

/**
 * Created by yincs on 2017/9/11.
 */

@Entity(nameInDb = "machine")
public class Machine {
    private String name;
    private String ssid;
    private String pwd;
    @Generated(hash = 1360938136)
    public Machine(String name, String ssid, String pwd) {
        this.name = name;
        this.ssid = ssid;
        this.pwd = pwd;
    }
    @Generated(hash = 1796908865)
    public Machine() {
    }
    public String getName() {
        return this.name;
    }
    public void setName(String name) {
        this.name = name;
    }
    public String getSsid() {
        return this.ssid;
    }
    public void setSsid(String ssid) {
        this.ssid = ssid;
    }
    public String getPwd() {
        return this.pwd;
    }
    public void setPwd(String pwd) {
        this.pwd = pwd;
    }


    @Override
    public String toString() {
        return "Machine{" +
                "name='" + name + '\'' +
                ", ssid='" + ssid + '\'' +
                ", pwd='" + pwd + '\'' +
                '}';
    }
}
