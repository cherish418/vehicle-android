package aplug.data.db.model;

/**
 * Created by yincs on 2017/9/3.
 */

import org.greenrobot.greendao.annotation.Entity;
import org.greenrobot.greendao.annotation.Generated;
import org.greenrobot.greendao.annotation.Transient;
import org.greenrobot.greendao.annotation.Unique;

/**
 * 存新下载的地图数据。边上车机时同步过去。
 */

@Entity(nameInDb = "offline_map")
public class OfflineMap {

    @Unique
    private String cityName;

    @Transient
    private String absolute;

    @Generated(hash = 1834477436)
    public OfflineMap(String cityName) {
        this.cityName = cityName;
    }

    @Generated(hash = 76039601)
    public OfflineMap() {
    }

    public String getCityName() {
        return this.cityName;
    }

    public void setCityName(String cityName) {
        this.cityName = cityName;
    }


    @Override
    public String toString() {
        return "OfflineMapSymMessage{" +
                "cityName='" + cityName + '\'' +
                '}';
    }
}
