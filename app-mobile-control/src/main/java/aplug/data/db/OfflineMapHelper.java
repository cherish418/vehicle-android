package aplug.data.db;

import java.util.List;

import javax.inject.Inject;
import javax.inject.Singleton;

import aplug.data.db.model.OfflineMap;
import aplug.data.db.model.OfflineMapDao;
import io.reactivex.Observable;

/**
 * Created by yincs on 2017/9/3.
 */

public interface OfflineMapHelper {

    Observable<Boolean> insertOrUpdate(final OfflineMap OfflineMap);

    Observable<Boolean> deleteByName(final String name);

    Observable<Boolean> delete(final OfflineMap offlineMap);

    Observable<Boolean> clear();

    Observable<List<OfflineMap>> loadAll();

    @Singleton
    class Impl implements OfflineMapHelper {

        private final OfflineMapDao offlineMapDao;

        @Inject
        public Impl(DbOpenHelper helper) {
            offlineMapDao = helper.getDaoSession().getOfflineMapDao();
        }

        @Override
        public Observable<Boolean> insertOrUpdate(final OfflineMap OfflineMap) {
            return Observable.fromCallable(() -> {
                try {
                    offlineMapDao.insertInTx(OfflineMap);
                    return true;
                } catch (Exception e) {
                    e.printStackTrace();
                }
                return false;
            });
        }

        @Override
        public Observable<Boolean> deleteByName(final String name) {
            return null;
        }

        @Override
        public Observable<Boolean> delete(final OfflineMap offlineMap) {
            return null;
        }

        @Override
        public Observable<Boolean> clear() {
            return Observable.fromCallable(() -> {
                offlineMapDao.deleteAll();
                return true;
            });
        }

        @Override
        public Observable<List<OfflineMap>> loadAll() {
            return Observable.fromCallable(() -> offlineMapDao.loadAll());
        }
    }
}
