package aplug;

import android.content.Context;
import android.os.Handler;

import aplug.data.db.PushMsgHelper;
import aplug.data.prefs.PreferencesHelper;

/**
 * Created by yincs on 2017/7/24.
 */

public interface AppManager {

    Context getContext();

    PreferencesHelper getPreferencesHelper();

    PushMsgHelper getPushMsgHelper();

    String getRemoteIp();

    void disRemoteIp();

    Handler getHandler();

    void onTerminate();
}
