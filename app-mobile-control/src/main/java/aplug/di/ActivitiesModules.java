package aplug.di;


import org.changs.aplug.annotation.ActivityScoped;
import org.changs.control.ui.carfile.CarFilesActivity;
import org.changs.control.ui.carfile.picture.PictureDetailAct;
import org.changs.control.ui.carfile.picture.PictureFragment;
import org.changs.control.ui.carfile.video.VideoFragment;
import org.changs.control.ui.carfile.video.VideoPlayAct;
import org.changs.control.ui.carupdate.CarUpdateActivity;
import org.changs.control.ui.home.HomeActivity;
import org.changs.control.ui.msg.MsgActivity;
import org.changs.control.ui.offlinemap.OfflineMapActivity;
import org.changs.control.ui.setting.AboutActivity;
import org.changs.control.ui.setting.FeedBackActivity;
import org.changs.control.ui.wifihost.WifiHostActivity;

import dagger.Module;
import dagger.android.ContributesAndroidInjector;

/**
 * Created by yincs on 2017/8/26.
 */

@Module
abstract public class ActivitiesModules {

    @ActivityScoped
    @ContributesAndroidInjector
    abstract AboutActivity AboutActivity();

    @ActivityScoped
    @ContributesAndroidInjector
    abstract CarFilesActivity CarFilesActivity();

    @ActivityScoped
    @ContributesAndroidInjector
    abstract CarUpdateActivity CarUpdateActivity();

    @ActivityScoped
    @ContributesAndroidInjector
    abstract FeedBackActivity FeedBackActivity();

    @ActivityScoped
    @ContributesAndroidInjector
    abstract HomeActivity HomeActivity();

    @ActivityScoped
    @ContributesAndroidInjector
    abstract OfflineMapActivity OfflineMapActivity();

    @ActivityScoped
    @ContributesAndroidInjector
    abstract MsgActivity MsgActivity();

    @ActivityScoped
    @ContributesAndroidInjector
    abstract PictureFragment PictureAllFragment();

    @ActivityScoped
    @ContributesAndroidInjector
    abstract PictureDetailAct PictureDetailAct();

    @ActivityScoped
    @ContributesAndroidInjector
    abstract VideoFragment VideoAllFragment();

    @ActivityScoped
    @ContributesAndroidInjector
    abstract VideoPlayAct VideoPlayAct();

    @ActivityScoped
    @ContributesAndroidInjector
    abstract WifiHostActivity WifiHostActivity();
}
