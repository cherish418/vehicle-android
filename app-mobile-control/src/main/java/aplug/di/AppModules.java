package aplug.di;

import android.content.Context;

import org.changs.common.network.ApiHelper;
import org.changs.common.network.AppApiHelper;
import org.changs.media.MediaManager;
import org.changs.media.MediaManagerType;

import javax.inject.Singleton;

import aplug.AppManager;
import aplug.AppManagerImpl;
import aplug.data.db.OfflineMapHelper;
import aplug.data.prefs.PreferencesHelper;
import aplug.data.prefs.PreferencesHelperImpl;
import dagger.Binds;
import dagger.Module;
import dagger.Provides;

/**
 * Created by yincs on 2017/8/27.
 */

//存放全局对象

@Module
public abstract class AppModules {

    @Singleton
    @Binds
    abstract Context provideContext(AppManagerImpl appManager);

    @Singleton
    @Binds
    abstract AppManager provideDataManager(AppManagerImpl appManager);

    @Singleton
    @Binds
    abstract PreferencesHelper providePreferencesHelper(PreferencesHelperImpl impl);

    @Singleton
    @Binds
    abstract OfflineMapHelper provideSearchRecordHelper(OfflineMapHelper.Impl impl);

    @MediaManagerType
    @Provides
    @org.changs.media.MediaManager.Type
    static int provideMediaManagerType(Context context) {
        return MediaManager.TYPE_PHONE;
    }

    @Singleton
    @Binds
    abstract ApiHelper provideApiHelper(AppApiHelper appApiHelper);
}
