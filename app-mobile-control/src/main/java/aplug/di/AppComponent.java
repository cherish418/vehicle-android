package aplug.di;

import javax.inject.Singleton;

import aplug.AppManagerImpl;
import dagger.BindsInstance;
import dagger.Component;
import dagger.android.AndroidInjector;
import dagger.android.support.AndroidSupportInjectionModule;

/**
 * Created by yincs on 2017/8/26.
 */

@Singleton
@Component(modules = {
        AndroidSupportInjectionModule.class,
        ActivitiesModules.class,
        AppModules.class})
public interface AppComponent extends AndroidInjector<AppManagerImpl> {

    @Component.Builder
    interface Builder {

        @BindsInstance
        Builder application(AppManagerImpl appManager);

        AppComponent build();
    }

}
