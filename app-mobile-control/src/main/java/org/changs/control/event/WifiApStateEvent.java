package org.changs.control.event;

/**
 * Created by yincs on 2017/10/11.
 */

public class WifiApStateEvent {
    /**
     * 10---正在关闭；11---已关闭；12---正在开启；13---已开启
     */
    public final int state;

    public WifiApStateEvent(int state) {
        this.state = state;
    }
}
