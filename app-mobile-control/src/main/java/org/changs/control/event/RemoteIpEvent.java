package org.changs.control.event;

/**
 * Created by yincs on 2017/10/10.
 */

public class RemoteIpEvent {

    public final String remoteIP;

    public RemoteIpEvent(String remoteIP) {
        this.remoteIP = remoteIP;
    }
}
