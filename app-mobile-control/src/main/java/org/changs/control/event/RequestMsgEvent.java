package org.changs.control.event;

/**
 * Created by yincs on 2017/10/26.
 */

public class RequestMsgEvent {

    public final String action;
    public final String data;

    public RequestMsgEvent(String action, String data) {
        this.action = action;
        this.data = data;
    }
}
