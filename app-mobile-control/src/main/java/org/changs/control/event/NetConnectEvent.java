package org.changs.control.event;

/**
 * Created by yincs on 2017/10/26.
 */

public class NetConnectEvent {

    public final boolean isConnect;
    public final boolean isWifi;


    public NetConnectEvent(boolean isConnect, boolean isWifi) {
        this.isConnect = isConnect;
        this.isWifi = isWifi;
    }
}
