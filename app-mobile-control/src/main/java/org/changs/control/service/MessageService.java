package org.changs.control.service;

import android.app.Notification;
import android.app.NotificationManager;
import android.app.Service;
import android.content.Intent;
import android.os.IBinder;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.annotation.WorkerThread;
import android.support.v7.app.NotificationCompat;

import com.google.gson.JsonObject;

import org.changs.control.R;
import org.changs.control.event.RequestMsgEvent;
import org.changs.servlet.AplugServlet;
import org.changs.servlet.CommunicationConfig;
import org.changs.servlet.HandleReceiveFile;
import org.changs.servlet.HandleSendFile;
import org.changs.servlet.HandleSendFileCallback;
import org.changs.servlet.IOUtils;
import org.greenrobot.eventbus.EventBus;

import aplug.AppConfig;
import timber.log.Timber;

//import static org.changs.communicate.CommunicationService.TYPE_VERIFY_DEVICE;

/**
 * Created by yincs on 2017/8/28.
 */

//手机的消息接收中心

//车机的消息接收中心、再广播出去
public class MessageService extends Service {

    NotificationManager mNotificationManager;

    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    @Override
    public void onCreate() {
        super.onCreate();
        mNotificationManager = (NotificationManager) getSystemService(NOTIFICATION_SERVICE);
        if (!mAplugServlet.start()) {
            Timber.e("mCommunicationServer 通信组件启动失败");
            stopSelf();
        }
        Timber.d("onCreate: 通信组件启动成功");
    }

    @Override
    public void onDestroy() {
        super.onDestroy();

        mAplugServlet.stop();
        stopForeground(true);
        Timber.d("onCreate: 通信组件关闭成功");
    }

    private AplugServlet mAplugServlet = new AplugServlet(CommunicationConfig.PHONE_SERVER_PORT) {
        @WorkerThread
        @NonNull
        @Override
        protected JsonObject handleRequest(JsonObject request) {
            Timber.d("handleRequest() called with: " + "request = [" + request + "]");
//            AplugServlet.defaultHandleRequest(MessageService.this, request);
            final String action = request.get(IOUtils.EXTRA_ACTION).getAsString();
            String data = null;
            if (request.has(IOUtils.EXTRA_DATA)) {
                data = request.get(IOUtils.EXTRA_DATA).getAsString();
            }

            EventBus.getDefault().post(new RequestMsgEvent(action, data));
//            Intent intent = new Intent(action);
//            if (data != null) intent.putExtra(IOUtils.EXTRA_DATA, data);
//            intent.addFlags(Intent.FLAG_INCLUDE_STOPPED_PACKAGES);
//            context.sendBroadcast(intent);

            final JsonObject jsonObject = new JsonObject();
            jsonObject.addProperty(IOUtils.EXTRA_DATA, IOUtils.OK);
            return request;
        }

        @WorkerThread
        @NonNull
        @Override
        protected HandleReceiveFile handleReceiveFile(JsonObject request) {
            Timber.d("handleReceiveFile() called with: " + "request = [" + request + "]");
            return AplugServlet.defaultHandleReceiveFile(AppConfig.getRootFile(), request);
        }

        @WorkerThread
        @NonNull
        @Override
        protected HandleSendFile handleSendFile(JsonObject request) {
            Timber.d("handleSendFile() called with: " + "request = [" + request + "]");
            final HandleSendFile sendFile = AplugServlet.defaultHandleSendFile(request);
            if (sendFile.send) {
                sendFile.setCallback(new MyHandleSendFileCallback(request));
            }
            return sendFile;
        }
    };


    public int getNotificationSettings() {
        return NotificationCompat.DEFAULT_SOUND | NotificationCompat.DEFAULT_VIBRATE | NotificationCompat.DEFAULT_LIGHTS;
    }

    private class MyHandleSendFileCallback implements HandleSendFileCallback {
        private static final String DEFAULT_FILE_NAME = "文件";
        final JsonObject request;

        private MyHandleSendFileCallback(JsonObject request) {
            this.request = request;
        }

        @Override
        public void onStart() {
            String fileName = DEFAULT_FILE_NAME;
            if (request.has(IOUtils.EXTRA_FILE_NAME)) {
                fileName = request.get(IOUtils.EXTRA_FILE_NAME).getAsString();
            }
            final Notification notification = new NotificationCompat.Builder(getApplicationContext())
                    .setSmallIcon(R.mipmap.ic_launcher)
                    .setContentTitle("离线地图同步中...")
                    .setContentText("正在同步" + fileName)
                    .setContentInfo("phone")
                    .setDefaults(getNotificationSettings())
                    .setPriority(NotificationCompat.PRIORITY_HIGH)
                    .build();

            mNotificationManager.notify(1, notification);
        }

        @Override
        public void onProgress(int progressPercent) {

        }

        @Override
        public void onSuccess() {

        }

        @Override
        public void onComplete() {
            mNotificationManager.cancel(1);
        }
    }
}

