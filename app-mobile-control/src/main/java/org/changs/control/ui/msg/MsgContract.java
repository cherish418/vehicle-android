package org.changs.control.ui.msg;

import org.changs.aplug.base.IPresenter;
import org.changs.aplug.base.IView;
import org.changs.control.push.PushMsg;

import java.util.List;

/**
 * Created by yincs on 2017/9/6.
 */

public interface MsgContract {

    interface V extends IView {

        void showMsgList(List<PushMsg> pushMsgs);
    }

    interface P extends IPresenter<V> {
        void clickClear();
    }
}
