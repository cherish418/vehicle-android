package org.changs.control.ui.carfile.video;

import android.content.Context;
import android.content.Intent;
import android.databinding.DataBindingUtil;
import android.os.Bundle;

import org.changs.aplug.base.BaseActivity;
import org.changs.control.R;
import org.changs.control.databinding.VideoPlayActBinding;
import org.changs.media.MediaFile;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.concurrent.TimeUnit;

import io.reactivex.Observable;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;
import io.vov.vitamio.MediaPlayer;

/**
 * 视频播放
 * Created by Administrator on 2017/8/24.
 */

public class VideoPlayAct extends BaseActivity {
    public static final String TAG_MEDIA_FILE = "tag_media_file";

    private VideoPlayActBinding binding;
    private Disposable disposable;
    private MyMediaController myMediaController;
    private SimpleDateFormat sdf;

    private MediaFile mMediaFile;

    public static Intent getIntent(Context context, MediaFile mediaFile) {
        Intent intent = new Intent(context, VideoPlayAct.class);
        intent.putExtra(TAG_MEDIA_FILE, mediaFile);
        return intent;
    }

    @Override
    public void setup(Bundle savedInstanceState) {
        binding = DataBindingUtil.setContentView(this, R.layout.video_play_act);

        initData(savedInstanceState);
    }

    private void initData(Bundle savedInstanceState) {
        sdf = new SimpleDateFormat("HH:mm:ss");

        if (savedInstanceState != null) {
            mMediaFile = savedInstanceState.getParcelable(TAG_MEDIA_FILE);
        } else {
            mMediaFile = getIntent().getParcelableExtra(TAG_MEDIA_FILE);
        }
        if (mMediaFile == null) {
            finish();
            return;
        }
        //设置播放地址
//        binding.videoView.setVideoURI(Uri.parse("http://112.253.22.157/17/z/z/y/u/zzyuasjwufnqerzvyxgkuigrkcatxr/hc.yinyuetai.com/D046015255134077DDB3ACA0D7E68D45.flv"));
        binding.videoView.setVideoPath(mMediaFile.getAbsolutePath());
        //创建控制器
        myMediaController = new MyMediaController(this, binding.videoView, this);
        //设置显示时长5s后隐藏
        myMediaController.show(5000);
        //设置控制器
        binding.videoView.setMediaController(myMediaController);
        //设置高画质
        binding.videoView.setVideoQuality(MediaPlayer.VIDEOQUALITY_HIGH);
        binding.videoView.requestFocus();
    }

    //系统当前时间
    private void countTime() {
        disposable = Observable.interval(1, TimeUnit.SECONDS)
                .compose(this.bindUntilDestroy(true))
                .subscribeOn(AndroidSchedulers.mainThread())
                .subscribe(aLong -> {
                    String str = sdf.format(new Date());
                    myMediaController.setTime(str);
                });
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        if (disposable != null)
            disposable.dispose();
        android.os.Process.killProcess(android.os.Process.myPid());
    }
}
