package org.changs.control.ui.home;

import android.content.Intent;
import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.view.View;

import com.google.zxing.integration.android.IntentIntegrator;
import com.google.zxing.integration.android.IntentResult;

import org.changs.aplug.base.BaseActivity;
import org.changs.control.R;
import org.changs.control.databinding.HomeActivityBinding;
import org.changs.control.ui.capture.CaptureActivity;
import org.changs.control.ui.carfile.CarFilesActivity;
import org.changs.control.ui.carupdate.CarUpdateActivity;
import org.changs.control.ui.msg.MsgActivity;
import org.changs.control.ui.offlinemap.OfflineMapActivity;
import org.changs.control.ui.setting.AboutActivity;

import javax.inject.Inject;

import aplug.AppManager;

/**
 * 首页
 * Created by Administrator on 2017/8/29.
 */

public class HomeActivity extends BaseActivity implements View.OnClickListener, HomeContract.V {
    private HomeActivityBinding mBinding;

    @Inject
    HomePresenter mPresenter;
    @Inject
    AppManager appManager;

    @Override
    public void setup(Bundle savedInstanceState) {
        mBinding = DataBindingUtil.setContentView(this, R.layout.home_activity);
        mBinding.tvMore.setOnClickListener(this);
        mBinding.tvCarVideo.setOnClickListener(this);
        mBinding.tvMsg.setOnClickListener(this);
        mBinding.tvMap.setOnClickListener(this);
        mBinding.tvUpdate.setOnClickListener(this);
    }

    @Override
    protected void onResume() {
        super.onResume();
        mPresenter.attachView(this);
    }

    @Override
    protected void onPause() {
        super.onPause();
        mPresenter.detachView();
    }

    @Override
    public void finish() {
        super.finish();
        mPresenter.finish();
    }


    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.tv_more://
                startActivity(new Intent(getContext(), AboutActivity.class));
                break;
            case R.id.btn_connect_machine://连接车机
                mPresenter.clickConnect();
                break;
            case R.id.tv_car_video://车机档案
                startActivity(new Intent(getContext(), CarFilesActivity.class));
                break;
            case R.id.tv_msg://消息通知
                startActivity(new Intent(getContext(), MsgActivity.class));
                break;
            case R.id.tv_map://地图更新
                startActivity(new Intent(getContext(), OfflineMapActivity.class));
                break;
            case R.id.tv_update://车机升级
                startActivity(new Intent(getContext(), CarUpdateActivity.class));
                break;
        }
    }


    @Override
    public void startCaptureActivity() {
        CaptureActivity.start(this);
    }

    @Override
    public void showDisConnected() {
        mBinding.btnConnectMachine.setText("连接");
        mBinding.btnConnectMachine.setEnabled(true);
        mBinding.btnConnectMachine.setOnClickListener(v -> {
            mPresenter.clickConnect();
            mBinding.btnConnectMachine.setText("正在连接");
            mBinding.btnConnectMachine.setEnabled(false);
        });
        mBinding.ivStatus.setImageResource(R.mipmap.stat_un);
        mBinding.tvCarInfo.setVisibility(View.GONE);
    }

    @Override
    public void showConnected(String msg) {
        mBinding.btnConnectMachine.setText("断开");
        mBinding.btnConnectMachine.setEnabled(true);
        mBinding.btnConnectMachine.setOnClickListener(v -> {
            mPresenter.clickDisConnect();
            mBinding.btnConnectMachine.setText("正在断开");
            mBinding.btnConnectMachine.setEnabled(false);
        });
        mBinding.ivStatus.setImageResource(R.mipmap.stat_uun);
        mBinding.tvCarInfo.setVisibility(View.VISIBLE);
        mBinding.tvCarInfo.setText(msg);
    }

    @Override
    public void showConnecting() {
        mBinding.btnConnectMachine.setText("断开");
        mBinding.btnConnectMachine.setEnabled(true);
        mBinding.btnConnectMachine.setOnClickListener(v -> {
            mPresenter.clickDisConnect();
            mBinding.btnConnectMachine.setText("正在断开");
            mBinding.btnConnectMachine.setEnabled(false);
        });
        mBinding.tvCarInfo.setVisibility(View.GONE);
        mBinding.ivStatus.setImageResource(R.mipmap.stat_wait);
    }

    @Override
    public void showMesg(boolean isShow) {
        if (isShow) {
            mBinding.viewNewMsg.setVisibility(View.VISIBLE);
        } else {
            mBinding.viewNewMsg.setVisibility(View.GONE);
        }
    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        final IntentResult result = IntentIntegrator.parseActivityResult(requestCode, resultCode, data);
        if (result != null) {
            if (result.getContents() != null) {
                mPresenter.parseCaptureResult(result.getContents());
            }
        } else {
            super.onActivityResult(requestCode, resultCode, data);
        }
    }


}
