package org.changs.control.ui.offlinemap;

import android.support.annotation.IntDef;

import com.amap.api.maps.offlinemap.OfflineMapCity;
import com.amap.api.maps.offlinemap.OfflineMapProvince;

import org.changs.aplug.base.IPresenter;
import org.changs.aplug.base.IView;

import java.util.List;

/**
 * Created by yincs on 2017/10/9.
 */

public interface OfflineMapContract {
    public static final int STATE_CAN_SYNC = 0;
    public static final int STATE_SYNCING = 1;
    public static final int STATE_SYNC_DONE = 2;


    @IntDef({STATE_CAN_SYNC, STATE_SYNCING, STATE_SYNC_DONE})
    public @interface State {
    }

    interface V extends IView {
        void notifyDownload();

        void showCity(List<Object> provinces);

        void notifySyncMapStateChange(@OfflineMapContract.State int state);
    }

    interface P extends IPresenter<V> {
        /**
         * 开启wifi自动更新
         *
         * @param enable true 开启
         */
        void enableWifiAutoUpdate(boolean enable);

        /**
         * 下载省
         */
        void download(OfflineMapProvince province);

        /**
         * 下载城市
         */
        void download(OfflineMapCity city);

        void delete(OfflineMapProvince province);

        void delete(OfflineMapCity city);

        void pause();

        /**
         * 更新
         */
        void update();

        /**
         * 同步到车载地图
         */
        void syncVehicleMap();
    }
}
