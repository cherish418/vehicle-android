package org.changs.control.ui.carupdate;

import android.databinding.DataBindingUtil;
import android.os.Bundle;

import org.changs.aplug.base.BaseActivity;
import org.changs.common.network.model.CarVersion;
import org.changs.control.R;
import org.changs.control.databinding.CarUpdateActivityBinding;

import javax.inject.Inject;

import aplug.data.prefs.PreferencesHelper;

/**
 * 车机升级
 * Created by Administrator on 2017/8/29.
 */

public class CarUpdateActivity extends BaseActivity implements CarUpdateContract.V {
    private CarUpdateActivityBinding mBinding;

    @Inject CarUpdatePresenter mCarUpdatePresenter;
    @Inject PreferencesHelper mPreferencesHelper;

    @Override
    public void setup(Bundle savedInstanceState) {
        mBinding = DataBindingUtil.setContentView(this, R.layout.car_update_activity);
    }

    @Override
    protected void onResume() {
        mCarUpdatePresenter.attachView(this);
        super.onResume();
        mCarUpdatePresenter.updateState();

        if (mPreferencesHelper.getCarVersion() == null) {
            mBinding.tvCarVersion.setText("未知车机版本，请先连接车机");
        } else {
            mBinding.tvCarVersion.setText("当前车机版本:" + mPreferencesHelper.getCarVersion());
        }
    }

    @Override
    public void carVersionDefault() {
        mBinding.btnUpdate.setText(R.string.check_upgradle);
        mBinding.btnUpdate.setOnClickListener(v -> mCarUpdatePresenter.checkCarUpgrade());
    }

    @Override
    public void carVersionNewest() {
        mBinding.btnUpdate.setText(R.string.already_latest_version);
        mBinding.btnUpdate.setEnabled(false);
    }

    @Override
    public void carVersionChecking() {
        mBinding.btnUpdate.setText("正在检测中");
        mBinding.btnUpdate.setEnabled(false);
    }

    @Override
    public void carVersionCanUpdate(CarVersion carVersion) {
        mBinding.tvDes.setText(String.format("更新说明：\n%s", carVersion.getCarVersionDes()));
        mBinding.btnUpdate.setText("可更新，点击开始下载");
        mBinding.btnUpdate.setOnClickListener(v -> mCarUpdatePresenter.download());
    }

    @Override
    public void carVersionDownloading(float progress) {
        mBinding.btnUpdate.setText(String.format("正在下载中%d%", (int) (progress * 100)));
    }

    @Override
    public void carOtaCan2Vehicle() {
        mBinding.btnUpdate.setText("等待同步到车机");
        mBinding.btnUpdate.setEnabled(false);
    }
}
