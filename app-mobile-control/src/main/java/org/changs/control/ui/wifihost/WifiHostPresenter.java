package org.changs.control.ui.wifihost;

import android.content.Context;
import android.net.wifi.WifiConfiguration;
import android.net.wifi.WifiManager;

import org.changs.aplug.base.BasePresenter;
import org.changs.aplug.utils.DeviceUtils;
import org.changs.aplug.utils.EmptyUtils;

import java.lang.reflect.Method;

import javax.inject.Inject;

import aplug.AppManager;

/**
 * Created by Administrator on 2017/9/8.
 */

public class WifiHostPresenter extends BasePresenter<WifiContract.V> implements WifiContract.P {

    private final AppManager appManager;
    private final WifiManager wifiManager;
    private final Context context;

    @Inject
    public WifiHostPresenter(Context context, AppManager appManager) {
        this.appManager = appManager;
        this.context = context;
        wifiManager = (WifiManager) context.getSystemService(Context.WIFI_SERVICE);
    }

    public void setIsOpen(boolean isOpen) {
        appManager.getPreferencesHelper().setIsOpen(isOpen);
    }

    public void setPsw(String psw) {
        appManager.getPreferencesHelper().setPsw(psw);
    }

    public void getIsOpen() {
        boolean isOpen = appManager.getPreferencesHelper().getIsOpen();
        String psw = appManager.getPreferencesHelper().getPsw();
        getMvpView().getOpen(isOpen, psw);
    }

    // wifi热点开关
    public boolean setWifiApEnabled(boolean enabled) {
        if (enabled) { // disable WiFi in any case
            //wifi和热点不能同时打开，所以打开热点的时候需要关闭wifi
            wifiManager.setWifiEnabled(false);
        }
        if (EmptyUtils.isEmpty(appManager.getPreferencesHelper().getPsw())) {
            getMvpView().showMessage("请设置热点密码");
            return false;
        }
        try {
            //热点的配置类
            WifiConfiguration apConfig = new WifiConfiguration();
            //配置热点的名称(可以在名字后面加点随机数什么的)
            apConfig.SSID = DeviceUtils.getAndroidID(context);
            //配置热点的密码
            apConfig.preSharedKey = appManager.getPreferencesHelper().getPsw();

            apConfig.hiddenSSID = true;
            apConfig.status = WifiConfiguration.Status.ENABLED;
            apConfig.allowedAuthAlgorithms.set(WifiConfiguration.AuthAlgorithm.OPEN);
            apConfig.allowedProtocols.set(WifiConfiguration.Protocol.RSN);
            apConfig.allowedProtocols.set(WifiConfiguration.Protocol.WPA);
            apConfig.allowedKeyManagement.set(WifiConfiguration.KeyMgmt.WPA_PSK);
            apConfig.allowedPairwiseCiphers.set(WifiConfiguration.PairwiseCipher.CCMP);
            apConfig.allowedPairwiseCiphers.set(WifiConfiguration.PairwiseCipher.TKIP);
            apConfig.allowedGroupCiphers.set(WifiConfiguration.GroupCipher.CCMP);
            apConfig.allowedGroupCiphers.set(WifiConfiguration.GroupCipher.TKIP);

            //通过反射调用设置热点
            Method method = wifiManager.getClass().getMethod(
                    "setWifiApEnabled", WifiConfiguration.class, Boolean.TYPE);
            //返回热点打开状态
            return (Boolean) method.invoke(wifiManager, apConfig, enabled);
        } catch (Exception e) {
            return false;
        }
    }
}
