package org.changs.control.ui.carupdate;

import org.changs.aplug.base.IPresenter;
import org.changs.aplug.base.IView;
import org.changs.common.network.model.CarVersion;

/**
 * Created by 惠安 on 2017/12/20.
 */

public interface CarUpdateContract {

    interface V extends IView {
        void carVersionDefault();

        /**
         * 已是最新版本
         */
        void carVersionNewest();

        void carVersionChecking();

        /**
         * 可更新
         */
        void carVersionCanUpdate(CarVersion carVersion);

        /**
         * 下载中
         *
         * @param progress 进度
         */
        void carVersionDownloading(float progress);

        /**
         * 可传输至车机
         */
        void carOtaCan2Vehicle();

    }

    interface P extends IPresenter<V> {

        /**
         * 检查状态
         */
        void updateState();

        /**
         * 检查状态
         */
        void updateState(String des);

        /**
         * 检查状态
         */
        void checkCarUpgrade();

        /**
         * 下载
         */
        void download();


        void ota2Vehicle();
    }
}
