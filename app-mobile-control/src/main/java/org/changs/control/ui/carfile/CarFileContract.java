package org.changs.control.ui.carfile;

import org.changs.aplug.base.IPresenter;
import org.changs.aplug.base.IView;

/**
 * Created by Administrator on 2017/9/6.
 */

public class CarFileContract {
    interface V extends IView {
        void afterLoadMedia();
    }

    interface P extends IPresenter<V> {
        void load();
    }
}
