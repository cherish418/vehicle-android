package org.changs.control.ui.setting;

import android.databinding.DataBindingUtil;
import android.os.Bundle;

import org.changs.aplug.base.BaseActivity;
import org.changs.control.R;
import org.changs.control.databinding.FeedBackActBinding;

/**
 * 反馈
 * Created by Administrator on 2017/8/30.
 */

public class FeedBackActivity extends BaseActivity {
    private FeedBackActBinding binding;

    @Override
    public void setup(Bundle savedInstanceState) {
        binding = DataBindingUtil.setContentView(this, R.layout.feed_back_act);

    }
}
