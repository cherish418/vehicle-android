package org.changs.control.ui.msg;

import android.app.AlertDialog;
import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.TextView;

import org.changs.aplug.base.BaseActivity;
import org.changs.control.R;
import org.changs.control.databinding.MsgActivityBinding;
import org.changs.control.push.PushMsg;

import java.util.List;

import javax.inject.Inject;

/**
 * 消息通知
 * Created by Administrator on 2017/8/29.
 */

public class MsgActivity extends BaseActivity implements MsgContract.V {

    @Inject MsgPresenter presenter;

    private MsgActivityBinding binding;
    private MsgAdapter mAdapter;
    private AlertDialog dialog;

    @Override
    public void setup(Bundle savedInstanceState) {
        binding = DataBindingUtil.setContentView(this, R.layout.msg_activity);

        binding.tvClear.setOnClickListener(v -> showDialog());
        binding.rccView.setLayoutManager(new LinearLayoutManager(getContext()));

        mAdapter = new MsgAdapter(getContext(), binding.rccView, presenter);
        binding.rccView.setup()
                .adapter(mAdapter)
                .emptyText("没有消息~")
                .dynamic()
                .done();
    }

    @Override
    protected void onResume() {
        super.onResume();
        presenter.attachView(this);
    }

    @Override
    protected void onPause() {
        super.onPause();
        presenter.detachView();
    }

    @Override
    public void showMsgList(List<PushMsg> pushMsgs) {
        mAdapter.getData().clear();
        mAdapter.getData().addAll(pushMsgs);
        mAdapter.notifyDataSetChanged();
        binding.tvClear.setVisibility(pushMsgs.size() > 0 ? View.VISIBLE : View.GONE);
    }


    private void showDialog() {
        if (dialog == null) {
            View view = LayoutInflater.from(getContext()).inflate(R.layout.dialog_delete, null);
            dialog = new AlertDialog.Builder(getContext()).setView(view).create();
            TextView textView = (TextView) view.findViewById(R.id.tv_content);
            textView.setText("确定清空消息列表吗？");
            view.findViewById(R.id.btn_cancel).setOnClickListener(v -> dialog.dismiss());
            view.findViewById(R.id.btn_confirm).setOnClickListener(v -> {
                presenter.clickClear();
                dialog.dismiss();
            });
        }
        dialog.show();
    }

}
