package org.changs.control.ui.setting;

import android.content.Intent;
import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.text.method.LinkMovementMethod;

import org.changs.aplug.base.BaseActivity;
import org.changs.aplug.utils.AppUtils;
import org.changs.control.R;
import org.changs.control.databinding.AboutActBinding;

/**
 * 关于
 * Created by Administrator on 2017/8/30.
 */

public class AboutActivity extends BaseActivity {
    private AboutActBinding binding;

    @Override
    public void setup(Bundle savedInstanceState) {
        binding = DataBindingUtil.setContentView(this, R.layout.about_act);

        initData();
    }

    private void initData() {
        binding.tvVersion.setText("V " + AppUtils.getAppVersionName(this));
        binding.tvWeb.setMovementMethod(LinkMovementMethod.getInstance());
        binding.tvPhone.setMovementMethod(LinkMovementMethod.getInstance());
        binding.feedback.setOnClickListener(v -> startActivity(new Intent(getContext(), FeedBackActivity.class)));
    }
}
