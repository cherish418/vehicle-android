package org.changs.control.ui.carfile.picture;

import android.app.AlertDialog;
import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.TextView;

import org.changs.aplug.widget.irecyclerview.SimpleAdapter;
import org.changs.control.R;
import org.changs.control.databinding.ItemListBinding;
import org.changs.media.MediaFile;
import org.changs.media.MediaManager;

import java.util.List;

import aplug.utils.GlideUtils;
import aplug.utils.SwipeItemDelegate;

/**
 * 图片的适配器
 * Created by Administrator on 2017/8/24.
 */

public class PictureAdapter extends SimpleAdapter<MediaFile, ItemListBinding> {
    private Context context;
    private MediaManager mediaManager;
    private SwipeItemDelegate delegate;

    public PictureAdapter(Context context, List<MediaFile> pictures, RecyclerView rcyView, MediaManager mediaManager) {
        super(pictures, R.layout.item_list);
        this.context = context;
        this.mediaManager = mediaManager;
        delegate = new SwipeItemDelegate(this, rcyView);
    }

    @Override
    public void onBindViewHolder(ItemListBinding binding, int position) {
        delegate.setDelegate(binding.swipeLayout);
        MediaFile picture = getData().get(position);

        binding.ivVideo.setVisibility(View.GONE);
        binding.tvName.setText(picture.getName());
        binding.tvTime.setText(picture.getTime());
        GlideUtils.loadImage(context, picture.getAbsolutePath(), binding.image);

        binding.btnDelete.setOnClickListener(v -> showDialog(picture));
        binding.getRoot().setOnClickListener(v -> context.startActivity(PictureDetailAct.getIntent(context, picture)));
    }

    private AlertDialog dialog;

    private void showDialog(MediaFile picture) {
        if (dialog == null) {
            View view = LayoutInflater.from(context).inflate(R.layout.dialog_delete, null);
            dialog = new AlertDialog.Builder(context).setView(view).create();
            TextView textView = (TextView) view.findViewById(R.id.tv_content);
            textView.setText("确定删除该图片吗？");
            view.findViewById(R.id.btn_cancel).setOnClickListener(v -> dialog.dismiss());
        }
        dialog.show();
        dialog.findViewById(R.id.btn_confirm).setOnClickListener(v -> {
            mediaManager.delete(picture);
            dialog.dismiss();
            delegate.closeOpenedSwipeItemLayoutWithAnim();
        });
    }
}
