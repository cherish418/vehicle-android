package org.changs.control.ui.offlinemap;

import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.amap.api.maps.offlinemap.OfflineMapCity;
import com.amap.api.maps.offlinemap.OfflineMapProvince;
import com.amap.api.maps.offlinemap.OfflineMapStatus;
import com.daimajia.numberprogressbar.NumberProgressBar;

import org.changs.control.databinding.ItemMapUpdateCity1Binding;
import org.changs.control.databinding.ItemMapUpdateProvince1Binding;

/**
 * Created by yincs on 2017/10/26.
 */

class AdapterHelper {

    private TextView name;
    private TextView size;
    private TextView des;
    private ImageView paused;
    private ImageView btn;
    private ImageView del;
    private NumberProgressBar progress;

    public AdapterHelper(ItemMapUpdateCity1Binding binding) {
        name = binding.tvName;
        size = binding.tvSize;
        des = binding.btnDownloadDes;
        btn = binding.btnDownload;
        progress = binding.progress;
        del = binding.btnDelete;
        paused = binding.btnPause;
    }

    public AdapterHelper(ItemMapUpdateProvince1Binding binding) {
        name = binding.tvName;
        size = binding.tvSize;
        des = binding.btnDownloadDes;
        btn = binding.btnDownload;
        progress = binding.progress;
        del = binding.btnDelete;
        paused = binding.btnPause;
    }

    public void setupState(OfflineMapProvince province) {
        if (province.getCityList().size() == 1) {
            OfflineMapCity city = province.getCityList().get(0);
            setupState(city);
            return;
        }
        name.setText(province.getProvinceName());
        size.setText(getSize(province.getSize()));
        setupState(province.getCityList().size() > 1, province.getState(), province.getcompleteCode());
    }

    public void setupState(OfflineMapCity city) {
        name.setText(city.getCity());
        size.setText(getSize(city.getSize()));
        setupState(false, city.getState(), city.getcompleteCode());
    }


    public void setupState(boolean isProvince, int state, int completeCode) {
        del.setVisibility(View.GONE);
        btn.setVisibility(View.GONE);
        paused.setVisibility(View.GONE);
        progress.setVisibility(View.GONE);
        switch (state) {
            case OfflineMapStatus.EXCEPTION_NETWORK_LOADING:
            case OfflineMapStatus.EXCEPTION_AMAP:
            case OfflineMapStatus.EXCEPTION_SDCARD:
            case OfflineMapStatus.CHECKUPDATES://默认状态
            case OfflineMapStatus.ERROR://默认状态
                btn.setVisibility(View.VISIBLE);
                if (isProvince && completeCode > 0) {
                    des.setText("已下载" + completeCode + "%");
                } else {
                    des.setText("");
                }
                break;
            case OfflineMapStatus.WAITING://等待下载
                des.setText("等待下载");
                break;
            case OfflineMapStatus.LOADING://正在下载
                if (isProvince) {
                    btn.setVisibility(View.VISIBLE);
                } else {
                    paused.setVisibility(View.VISIBLE);
                }
                des.setText("正在下载" + completeCode + "%");
                progress.setVisibility(View.VISIBLE);
                progress.setProgress(completeCode);
                break;
            case OfflineMapStatus.PAUSE:
                btn.setVisibility(View.VISIBLE);
                des.setText("继续下载" + completeCode + "%");
                progress.setVisibility(View.VISIBLE);
                progress.setProgress(completeCode);
                break;
            case OfflineMapStatus.UNZIP://正在解压
                des.setText("正在解压" + completeCode + "%");
                progress.setVisibility(View.VISIBLE);
                progress.setProgress(completeCode);
                break;
            case OfflineMapStatus.SUCCESS://下载成功
                des.setText("下载完成");
                del.setVisibility(View.VISIBLE);
                break;
            case OfflineMapStatus.NEW_VERSION:
                btn.setVisibility(View.VISIBLE);
                des.setText("有更新");
        }
    }


    public static String getSize(long size) {
        return String.format("%.2f M", (size / 1024.0f / 1024.0f));
    }

}
