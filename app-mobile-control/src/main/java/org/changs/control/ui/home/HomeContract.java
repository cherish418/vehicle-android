package org.changs.control.ui.home;

import org.changs.aplug.base.IPresenter;
import org.changs.aplug.base.IView;

/**
 * Created by yincs on 2017/9/6.
 */

public interface HomeContract {

    interface V extends IView {
        void startCaptureActivity();

        /**
         * 显示已断开连接
         */
        void showDisConnected();

        /**
         * 显示已连接
         */
        void showConnected(String msg);

        /**
         * 显示正在连接
         */
        void showConnecting();

        /**
         * 显示是否有消息
         */
        void showMesg(boolean isShow);
    }

    interface P extends IPresenter<V> {
        void clickConnect();

        void clickDisConnect();

        void parseCaptureResult(String content);

        void finish();
    }


}
