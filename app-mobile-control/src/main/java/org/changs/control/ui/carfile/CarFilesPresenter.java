package org.changs.control.ui.carfile;

import org.changs.aplug.base.BasePresenter;
import org.changs.media.MediaFile;
import org.changs.media.MediaManager;

import javax.inject.Inject;

/**
 * Created by Administrator on 2017/9/6.
 */

public class CarFilesPresenter extends BasePresenter<CarFileContract.V> implements CarFileContract.P, MediaManager.Callback {
    private final MediaManager mMediaManager;

    @Inject
    public CarFilesPresenter(MediaManager mMediaManager) {
        this.mMediaManager = mMediaManager;
    }

    @Override
    public void attachView(CarFileContract.V v) {
        super.attachView(v);
        mMediaManager.registerCallback(this);
        load();
    }

    @Override
    public void detachView() {
        super.detachView();
        mMediaManager.unregisterCallback(this);
    }

    @Override
    public void load() {
//        if (mMediaManager.getMedia().size() == 0) {
        if (isAttachView())
            getMvpView().showLoading().setMessage("正在加载本地数据");
        mMediaManager.reload();
//        } else {
//            if (isAttachView()) {
//                getMvpView().afterLoadMedia();
//            }
//        }
    }

    @Override
    public void delete(MediaFile mediaFile) {

    }

    @Override
    public void reload() {
        if (isAttachView()) {
            getMvpView().afterLoadMedia();
            getMvpView().hideLoading();
        }
    }

    @Override
    public void upload(MediaFile mediaFile) {

    }
}
