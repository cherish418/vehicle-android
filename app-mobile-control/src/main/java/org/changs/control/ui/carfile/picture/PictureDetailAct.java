package org.changs.control.ui.carfile.picture;

import android.content.Context;
import android.content.Intent;
import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.view.View;

import org.changs.aplug.base.BaseActivity;
import org.changs.control.R;
import org.changs.control.databinding.PictureDetailActBinding;
import org.changs.media.MediaFile;

import aplug.utils.GlideUtils;

/**
 * 图片详情
 * Created by Administrator on 2017/8/24.
 */

public class PictureDetailAct extends BaseActivity implements View.OnClickListener {
    private static final String TAG_MEDIA_FILE = "tag_media_file";

    private PictureDetailActBinding binding;
    private boolean isShow = true;

    private MediaFile mMediaFile;

    public static Intent getIntent(Context context, MediaFile mediaFile) {
        Intent intent = new Intent(context, PictureDetailAct.class);
        intent.putExtra(TAG_MEDIA_FILE, mediaFile);
        return intent;
    }

    @Override
    public void setup(Bundle savedInstanceState) {
        binding = DataBindingUtil.setContentView(this, R.layout.picture_detail_act);

        mMediaFile = getIntent().getParcelableExtra(TAG_MEDIA_FILE);
        initData(savedInstanceState);
    }

    private void initData(Bundle savedInstanceState) {
        if (savedInstanceState != null) {
            mMediaFile = savedInstanceState.getParcelable(TAG_MEDIA_FILE);
        } else {
            mMediaFile = getIntent().getParcelableExtra(TAG_MEDIA_FILE);
        }
        if (mMediaFile == null) {
            finish();
            return;
        }

        GlideUtils.loadBigImage(getContext(), mMediaFile.getAbsolutePath(), binding.photoView);
        binding.rlHead.setVisibility(View.GONE);
        binding.tvFileName.setText(mMediaFile.getName());

        binding.tvBack.setOnClickListener(this);
        binding.rlBg.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.tv_back:
                finish();
                break;
            case R.id.rl_bg:
                if (isShow) {
                    binding.rlHead.setVisibility(View.VISIBLE);
                    isShow = false;
                } else {
                    binding.rlHead.setVisibility(View.GONE);
                    isShow = true;
                }
                break;
        }
    }
}
