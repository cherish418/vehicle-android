package org.changs.control.ui.wifihost;

import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.CompoundButton;

import org.changs.aplug.base.BaseActivity;
import org.changs.aplug.utils.EmptyUtils;
import org.changs.control.R;
import org.changs.control.databinding.WifiHostActBinding;

import javax.inject.Inject;

/**
 * 手机热点设置
 * Created by Administrator on 2017/9/8.
 */

public class WifiHostActivity extends BaseActivity implements CompoundButton.OnCheckedChangeListener, WifiContract.V {
    private WifiHostActBinding binding;

    @Inject
    WifiHostPresenter presenter;

    private boolean isOpen;
    private String psw;

    @Override
    public void setup(Bundle savedInstanceState) {
        binding = DataBindingUtil.setContentView(this, R.layout.wifi_host_act);

        presenter.attachView(this);
        presenter.getIsOpen();
    }

    private void initData() {
        if (isOpen) {
            binding.rlPsw.setVisibility(View.VISIBLE);
        } else {
            binding.rlPsw.setVisibility(View.GONE);
        }

        binding.tbtnWifi.setChecked(isOpen);
        binding.etPsw.setText(psw);

        binding.tbtnWifi.setOnCheckedChangeListener(this);
        binding.etPsw.setOnEditorActionListener((v, actionId, event) -> {
            Log.d("car", binding.etPsw.getText().toString().trim());
            if (EmptyUtils.isEmpty(binding.etPsw.getText().toString().trim())) {
                showMessage("请输入密码");
            } else {
                presenter.setPsw(binding.etPsw.getText().toString().trim());
                presenter.setWifiApEnabled(isOpen);
            }
            return false;
        });
    }

    @Override
    public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
        if (isChecked)
            binding.rlPsw.setVisibility(View.VISIBLE);
        else
            binding.rlPsw.setVisibility(View.GONE);

        isOpen = isChecked;
        presenter.setIsOpen(isChecked);
        presenter.setWifiApEnabled(isChecked);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        presenter.detachView();
    }

    @Override
    public void onBackPressed() {
        if (isOpen && EmptyUtils.isEmpty(binding.etPsw.getText().toString().trim())) {
            showMessage("请输入密码并点击完成");
            return;
        }
        super.onBackPressed();
    }

    @Override
    public void getOpen(boolean isOpen, String psw) {
        this.isOpen = isOpen;
        this.psw = psw;

        initData();
    }
}
