package org.changs.control.ui.wifihost;

import org.changs.aplug.base.IPresenter;
import org.changs.aplug.base.IView;

/**
 * Created by Administrator on 2017/9/8.
 */

public class WifiContract {
    interface V extends IView {
        void getOpen(boolean isOpen, String psw);
    }

    interface P extends IPresenter<V> {
    }
}
