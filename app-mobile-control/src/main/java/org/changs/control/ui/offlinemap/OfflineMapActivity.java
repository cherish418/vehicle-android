package org.changs.control.ui.offlinemap;

import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.view.ViewGroup;

import org.changs.aplug.base.BaseActivity;
import org.changs.control.R;
import org.changs.control.databinding.MapActivityBinding;
import org.changs.control.databinding.MapCityHeaderBinding;

import java.util.List;

import javax.inject.Inject;

import aplug.AppManager;

/**
 * Created by yincs on 2017/10/9.
 */

public class OfflineMapActivity extends BaseActivity implements OfflineMapContract.V {
    @Inject OfflineMapPresenter mOfflineMapPresenter;
    @Inject AppManager mAppManager;

    private MapCityHeaderBinding mHeaderBinding;
    private MapActivityBinding mBinding;

    @Override
    public void setup(Bundle savedInstanceState) {
        mBinding = DataBindingUtil.setContentView(this, R.layout.map_activity);
        mHeaderBinding = DataBindingUtil.inflate(getLayoutInflater(), R.layout.map_city_header,
                (ViewGroup) mBinding.getRoot(), false);
        mHeaderBinding.tbtnWifi.setChecked(mAppManager.getPreferencesHelper().isWifiAutoUpdate());
        mHeaderBinding.tbtnWifi.setOnCheckedChangeListener((buttonView, isChecked) -> mOfflineMapPresenter.enableWifiAutoUpdate(isChecked));


        mBinding.rccView.addHeaderView(mHeaderBinding.getRoot());
        mBinding.rccView.setLayoutManager(new LinearLayoutManager(getContext()));

        mBinding.btnSyncMap.setOnClickListener(v -> mOfflineMapPresenter.syncVehicleMap());

    }

    @Override
    protected void onResume() {
        super.onResume();
        mOfflineMapPresenter.attachView(this);
    }

    @Override
    protected void onPause() {
        super.onPause();
        mOfflineMapPresenter.detachView();
    }

    @Override
    public void notifyDownload() {
        if (mBinding.rccView.getAdapter() != null)
            mBinding.rccView.getAdapter().notifyDataSetChanged();
    }

    @Override
    public void showCity(List<Object> provinces) {
        mBinding.rccView.setIAdapter(new MapUpdateAdapter(provinces, mOfflineMapPresenter));
    }

    @Override
    public void notifySyncMapStateChange(@OfflineMapContract.State int state) {
        String des = null;
        switch (state) {
            case OfflineMapContract.STATE_CAN_SYNC:
                des = "可同步！连接到车机将自动同步";
                break;
            case OfflineMapContract.STATE_SYNCING:
                des = "同步中";
                break;
            case OfflineMapContract.STATE_SYNC_DONE:
                des = "已同步";
                break;
        }
        if (des == null) return;
        mHeaderBinding.tvDes.setText(des);
    }


}
