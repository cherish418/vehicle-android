package org.changs.control.ui.offlinemap;

import android.databinding.DataBindingUtil;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.amap.api.maps.offlinemap.OfflineMapCity;
import com.amap.api.maps.offlinemap.OfflineMapProvince;
import com.amap.api.maps.offlinemap.OfflineMapStatus;
import com.daimajia.numberprogressbar.NumberProgressBar;

import org.changs.aplug.widget.irecyclerview.IIRecyclerView;
import org.changs.control.R;
import org.changs.control.databinding.ItemMapUpdateCity1Binding;
import org.changs.control.databinding.ItemMapUpdateProvince1Binding;

import java.util.List;

/**
 * Created by yincs on 2017/9/3.
 */

public class MapUpdateAdapter extends RecyclerView.Adapter implements IIRecyclerView.IDataAdapter {

    private final static int TYPE_CITY = 0;
    private final static int TYPE_PROVINCE = 1;
    private final static int TYPE_TIP = 2;

    private final List<Object> mData;
    private final OfflineMapPresenter mOfflineMapPresenter;
    private RecyclerView mRccView;

    public MapUpdateAdapter(List<Object> data, OfflineMapPresenter offlineMapPresenter) {
        this.mData = data;
        this.mOfflineMapPresenter = offlineMapPresenter;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(parent.getContext());
        if (viewType == TYPE_PROVINCE) {
            ItemMapUpdateProvince1Binding binding = DataBindingUtil.inflate(inflater, R.layout.item_map_update_province1, parent, false);
            return new MyProvinceHolder(binding);
        } else if (viewType == TYPE_CITY) {
            ItemMapUpdateCity1Binding binding = DataBindingUtil.inflate(inflater, R.layout.item_map_update_city1, parent, false);
            return new MyCityHolder(binding);
        } else if (viewType == TYPE_TIP) {
            return new RecyclerView.ViewHolder(inflater.inflate(R.layout.item_map_header_tip, parent, false)) {
            };
        } else {
            throw new IllegalArgumentException("非法viewType = " + viewType);
        }
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, final int position) {
        Object o = mData.get(position);
        if (o instanceof String) {
            TextView tip = (TextView) holder.itemView;
            tip.setText((String) o);
        } else if (o instanceof OfflineMapProvince) {
            final OfflineMapProvince province = (OfflineMapProvince) o;
            final MyProvinceHolder provinceHolder = (MyProvinceHolder) holder;
            new AdapterHelper(provinceHolder.binding).setupState(province);
            provinceHolder.itemView.setOnClickListener(v -> {
                if (province.getCityList().size() == 1) return;
                if (isProvinceExpand(position)) {
                    mData.removeAll(province.getCityList());
                } else {
                    mData.addAll(position + 1, province.getCityList());
                }
                if (mRccView != null) {
                    int adapterPosition = provinceHolder.getAdapterPosition();
                    mRccView.getLayoutManager().scrollToPosition(adapterPosition + 3);
                }
                notifyDataSetChanged();
            });
            provinceHolder.binding.btnDownload.setOnClickListener(v -> mOfflineMapPresenter.download(province));
            provinceHolder.binding.btnDelete.setOnClickListener(v -> mOfflineMapPresenter.delete(province));
            provinceHolder.binding.btnPause.setOnClickListener(v -> mOfflineMapPresenter.pause());
        } else {
            final OfflineMapCity city = (OfflineMapCity) o;
            MyCityHolder cityHolder = (MyCityHolder) holder;
            new AdapterHelper(cityHolder.binding).setupState(city);
            cityHolder.binding.btnDownload.setOnClickListener(v -> mOfflineMapPresenter.download(city));
            cityHolder.binding.btnDelete.setOnClickListener(v -> mOfflineMapPresenter.delete(city));
            cityHolder.binding.btnPause.setOnClickListener(v -> mOfflineMapPresenter.pause());
        }
    }

    @Override
    public void onAttachedToRecyclerView(RecyclerView recyclerView) {
        super.onAttachedToRecyclerView(recyclerView);
        mRccView = recyclerView;
    }


    @Override
    public void onDetachedFromRecyclerView(RecyclerView recyclerView) {
        super.onDetachedFromRecyclerView(recyclerView);
        mRccView = null;
    }

    private void setupState(int state, int completeCode,
                            View btnDownload, View vDownloaded, View vDownloading,
                            View waiting, View zipping, View btnUpdate,
                            NumberProgressBar progress) {
        btnDownload.setVisibility(View.GONE);
        vDownloaded.setVisibility(View.GONE);
        vDownloading.setVisibility(View.GONE);
        waiting.setVisibility(View.GONE);
        zipping.setVisibility(View.GONE);
        btnUpdate.setVisibility(View.GONE);
        progress.setVisibility(View.GONE);

        switch (state) {
            case OfflineMapStatus.EXCEPTION_NETWORK_LOADING:
            case OfflineMapStatus.EXCEPTION_AMAP:
            case OfflineMapStatus.EXCEPTION_SDCARD:
            case OfflineMapStatus.CHECKUPDATES://默认状态
            case OfflineMapStatus.ERROR://默认状态
            case OfflineMapStatus.PAUSE:
                btnDownload.setVisibility(View.VISIBLE);
                break;
            case OfflineMapStatus.WAITING://等待下载
                waiting.setVisibility(View.VISIBLE);
                break;
            case OfflineMapStatus.LOADING://正在下载
                vDownloading.setVisibility(View.VISIBLE);
                progress.setVisibility(View.VISIBLE);
                progress.setProgress(completeCode);
                break;
            case OfflineMapStatus.UNZIP://正在解压
                zipping.setVisibility(View.VISIBLE);
                progress.setVisibility(View.VISIBLE);
                progress.setProgress(completeCode);
                break;
            case OfflineMapStatus.SUCCESS://下载成功
                vDownloaded.setVisibility(View.VISIBLE);
                break;
            case OfflineMapStatus.NEW_VERSION:
                btnUpdate.setVisibility(View.VISIBLE);
        }
    }

    private boolean isProvinceExpand(int position) {
        int nextPosition = position + 1;
        if (mData.size() <= nextPosition) return false;
        return mData.get(nextPosition) instanceof OfflineMapCity;
    }

    @Override
    public int getItemCount() {
        return mData.size();
    }

    @Override
    public int getItemViewType(int position) {
        Object item = mData.get(position);
        if (item instanceof String) return TYPE_TIP;
        return item instanceof OfflineMapProvince ? TYPE_PROVINCE : TYPE_CITY;
    }

    @Override
    public List getData() {
        return mData;
    }

    private class MyProvinceHolder extends RecyclerView.ViewHolder {

        final ItemMapUpdateProvince1Binding binding;

        public MyProvinceHolder(ItemMapUpdateProvince1Binding binding) {
            super(binding.getRoot());
            this.binding = binding;
        }
    }

    private class MyCityHolder extends RecyclerView.ViewHolder {

        final ItemMapUpdateCity1Binding binding;

        public MyCityHolder(ItemMapUpdateCity1Binding binding) {
            super(binding.getRoot());
            this.binding = binding;
        }
    }
}
