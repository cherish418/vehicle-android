package org.changs.control.ui.carfile;

import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.annotation.IdRes;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.widget.RadioButton;
import android.widget.RadioGroup;

import org.changs.aplug.base.BaseActivity;
import org.changs.control.R;
import org.changs.control.databinding.CarFilesActBinding;
import org.changs.control.ui.carfile.picture.PictureFragment;
import org.changs.control.ui.carfile.video.VideoFragment;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

/**
 * 车机档案
 * Created by Administrator on 2017/8/29.
 */

public class CarFilesActivity extends BaseActivity implements RadioGroup.OnCheckedChangeListener, CarFileContract.V {

    @Inject CarFilesPresenter presenter;

    private CarFilesActBinding binding;
    private List<Fragment> fragments;

    @Override
    public void setup(Bundle savedInstanceState) {
        binding = DataBindingUtil.setContentView(this, R.layout.car_files_act);

        presenter.attachView(this);

        initData();
    }

    private void initData() {
        fragments = new ArrayList<>();
        binding.rg.setOnCheckedChangeListener(this);

        fragments.add(new VideoFragment());
        fragments.add(new PictureFragment());

        ((RadioButton) binding.rg.getChildAt(0)).setChecked(true);

        FragmentPagerAdapter adapter = new FragmentPagerAdapter(getSupportFragmentManager()) {
            @Override
            public Fragment getItem(int position) {
                return fragments.get(position);
            }

            @Override
            public int getCount() {
                return fragments.size();
            }
        };
        binding.vp.setCurrentItem(0);
        binding.vp.setAdapter(adapter);
        binding.vp.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {
            }

            @Override
            public void onPageSelected(int position) {
                ((RadioButton) binding.rg.getChildAt(position)).setChecked(true);
            }

            @Override
            public void onPageScrollStateChanged(int state) {
            }
        });
    }

    @Override
    public void onCheckedChanged(RadioGroup group, @IdRes int checkedId) {
        switch (checkedId) {
            case R.id.rbtn_video:
                binding.vp.setCurrentItem(0);
                break;
            case R.id.rbtn_picture:
                binding.vp.setCurrentItem(1);
                break;
        }
    }

    @Override
    public void afterLoadMedia() {
    }

    @Override
    public void finish() {
        super.finish();
        presenter.detachView();
    }
}
