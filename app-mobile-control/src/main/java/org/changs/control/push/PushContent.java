package org.changs.control.push;

import java.util.List;

/**
 * Created by Administrator on 2017/4/25.
 */

public class PushContent {
    private String action;
    private BodyBean body;

    public PushContent() {
        body = new BodyBean();
    }

    public String getAction() {
        return action;
    }

    public PushContent setAction(String action) {
        this.action = action;
        return this;
    }

    public BodyBean getBody() {
        return body;
    }

    public BodyBean setBody(BodyBean body) {
        this.body = body;
        return this.body;
    }

    public static class BodyBean {
        private String alias;
        private AppVOBean appVO;
        private String clientId;
        private String deviceModel;
        private String id;
        private LocationBean location;
        private String manufacturers;
        private String network;
        private boolean online;
        private String opType;
        private String opVersion;
        private int status;
        private List<String> groups;
        private List<String> tags;

        public BodyBean() {
            location = new LocationBean();
            appVO = new AppVOBean();
        }

        public String getAlias() {
            return alias;
        }

        public BodyBean setAlias(String alias) {
            this.alias = alias;
            return this;
        }

        public AppVOBean getAppVO() {
            return appVO;
        }

        public BodyBean setAppVO(AppVOBean appVO) {
            this.appVO = appVO;
            return this;
        }

        public String getClientId() {
            return clientId;
        }

        public BodyBean setClientId(String clientId) {
            this.clientId = clientId;
            return this;
        }

        public String getDeviceModel() {
            return deviceModel;
        }

        public BodyBean setDeviceModel(String deviceModel) {
            this.deviceModel = deviceModel;
            return this;
        }

        public String getId() {
            return id;
        }

        public BodyBean setId(String id) {
            this.id = id;
            return this;
        }

        public LocationBean getLocation() {
            return location;
        }

        public BodyBean setLocation(LocationBean location) {
            this.location = location;
            return this;
        }

        public String getManufacturers() {
            return manufacturers;
        }

        public BodyBean setManufacturers(String manufacturers) {
            this.manufacturers = manufacturers;
            return this;
        }

        public String getNetwork() {
            return network;
        }

        public BodyBean setNetwork(String network) {
            this.network = network;
            return this;
        }

        public boolean isOnline() {
            return online;
        }

        public BodyBean setOnline(boolean online) {
            this.online = online;
            return this;
        }

        public String getOpType() {
            return opType;
        }

        public BodyBean setOpType(String opType) {
            this.opType = opType;
            return this;
        }

        public String getOpVersion() {
            return opVersion;
        }

        public BodyBean setOpVersion(String opVersion) {
            this.opVersion = opVersion;
            return this;
        }

        public int getStatus() {
            return status;
        }

        public BodyBean setStatus(int status) {
            this.status = status;
            return this;
        }

        public List<String> getGroups() {
            return groups;
        }

        public BodyBean setGroups(List<String> groups) {
            this.groups = groups;
            return this;
        }

        public List<String> getTags() {
            return tags;
        }

        public BodyBean setTags(List<String> tags) {
            this.tags = tags;
            return this;
        }

        public static class AppVOBean {
            /**
             * appId : ad0b5d197e514ef19d5efb710032a5a5
             * appName : 测试Demo
             */

            private String appId;
            private String appName;

            public String getAppId() {
                return appId;
            }

            public AppVOBean setAppId(String appId) {
                this.appId = appId;
                return this;
            }

            public String getAppName() {
                return appName;
            }

            public AppVOBean setAppName(String appName) {
                this.appName = appName;
                return this;
            }
        }

        public static class LocationBean {
            /**
             * address : 登良路888号
             * areaName : 南山区
             * cityName : 深圳市
             * provinceName : 广东省
             */


            private String address;
            private String areaName;
            private String cityName;
            private String provinceName;
            private double lon;
            private double lat;

            public String getAddress() {
                return address;
            }

            public LocationBean setAddress(String address) {
                this.address = address;
                return this;
            }

            public String getAreaName() {
                return areaName;
            }

            public LocationBean setAreaName(String areaName) {
                this.areaName = areaName;
                return this;
            }

            public String getCityName() {
                return cityName;
            }

            public LocationBean setCityName(String cityName) {
                this.cityName = cityName;
                return this;
            }

            public String getProvinceName() {
                return provinceName;
            }

            public LocationBean setProvinceName(String provinceName) {
                this.provinceName = provinceName;
                return this;
            }

            public double getLon() {
                return lon;
            }

            public LocationBean setLon(double lon) {
                this.lon = lon;
                return this;
            }

            public double getLat() {
                return lat;
            }

            public LocationBean setLat(double lat) {
                this.lat = lat;
                return this;
            }
        }
    }
}
