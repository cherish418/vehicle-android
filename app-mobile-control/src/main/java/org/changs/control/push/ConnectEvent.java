package org.changs.control.push;

import android.util.Log;

import com.iot.push.sdk.Configure;
import com.iot.push.sdk.IotSdkData;
import com.iot.push.sdk.IotSdkEventNotify;


/**
 * Created by kelly on 2017/5/31.
 */

public class ConnectEvent extends IotSdkEventNotify {

    @Override
    public void recevier(String eType, IotSdkData sdkData) {
        Log.i(Configure.SYS_LOG, "eType:" + eType);
        Log.i(Configure.SYS_LOG, "retCode" + sdkData.getCode());
        Log.e("msg111=", "code = " + sdkData.getCode());
        Log.e("msg111=", "msg = " + sdkData.getMessage());
        Log.e("msg111=", "data = " + sdkData.getData());
//        EventBus.getDefault().post(new MessageEvent(sdkData.getMessage()));
    }
}