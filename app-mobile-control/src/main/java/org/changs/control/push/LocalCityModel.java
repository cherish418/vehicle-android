package org.changs.control.push;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.google.gson.annotations.SerializedName;

import org.changs.aplug.AplugManager;
import org.changs.aplug.utils.JsonUtils;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by yincs on 2016/12/12.
 */

public class LocalCityModel {

    private static final LocalCityModel LOCAL_CITY_MODEL = new LocalCityModel();

    public static LocalCityModel get() {
        return LOCAL_CITY_MODEL;
    }

    private List<LocalCity> cityList;


    public List<LocalCity> getCityList() {
        if (cityList == null)
            setup();
        return cityList;
    }

    public void setup() {
        cityList = new ArrayList<>();
        InputStream open = null;
        BufferedReader reader = null;
        try {
            StringBuilder sb = new StringBuilder();
            open = AplugManager.get().getContext().getAssets().open("city.json");
            //            open = new FileInputStream("F:\\p2n\\android-svn\\XunBao\\app\\src\\main\\assets\\city.json");
            reader = new BufferedReader(new InputStreamReader(open));
            String line;
            while ((line = reader.readLine()) != null) {
                sb.append(line);
            }

            JSONArray ja = JSON.parseArray(sb.toString());
            for (int i = 0; i < ja.size(); i++) {
                final JSONObject jsonObject = ja.getJSONObject(i);
                LocalCity city = JsonUtils.parseObject(jsonObject.toString(), LocalCity.class);
                cityList.add(city);
            }
        } catch (Exception ignore) {
        } finally {
            if (open != null)
                try {
                    open.close();
                } catch (IOException ignore) {
                }
            if (reader != null)
                try {
                    reader.close();
                } catch (IOException ignore) {
                }
        }
    }

    public static class LocalCity {

        @SerializedName("id")
        private String id;

        @SerializedName("n")
        private String areaName;

        @SerializedName("s")
        private List<LocalCity> subAreaList;

        public String getId() {
            return id;
        }

        public void setId(String id) {
            this.id = id;
        }

        public String getAreaName() {
            return areaName;
        }

        public void setAreaName(String areaName) {
            this.areaName = areaName;
        }

        public List<LocalCity> getSubAreaList() {
            return subAreaList;
        }

        public void setSubAreaList(List<LocalCity> subAreaList) {
            this.subAreaList = subAreaList;
        }
    }

}
