package org.changs.control.push;

import android.content.Context;

import com.iot.push.sdk.IMessageRecevier;
import com.iot.push.sdk.IoTCommunicationClient;
import com.iot.push.sdk.IotSdkData;
import com.iot.push.sdk.IotSdkEventNotify;
import com.iot.push.sdk.message.Message;

import org.changs.aplug.utils.DeviceUtils;
import org.changs.aplug.utils.JsonUtils;
import org.changs.aplug.utils.NotificationUtils;
import org.changs.aplug.utils.ProcessUtils;
import org.changs.control.R;
import org.greenrobot.eventbus.EventBus;

import javax.inject.Inject;
import javax.inject.Singleton;

import aplug.data.db.PushMsgHelper;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.schedulers.Schedulers;
import timber.log.Timber;

/**
 * Created by yincs on 2017/10/11.
 */


@Singleton
public class PushClient {

    private final Context mContext;
    private final PushMsgHelper mPushMsgHelper;
    private boolean registered;

    @Inject
    public PushClient(Context context, PushMsgHelper pushMsgHelper) {
        this.mContext = context;
        mPushMsgHelper = pushMsgHelper;
    }

    public void register() {
//        if (registered) {
//            Timber.d("register: 推送已经注册过了");
//            return;
//        }
        String appId = "9092c4ee91ee49d6870cc7c1900d907e";
        String appSecret = "3eae59ff3d8142bdb8b4e2f06f1fcd19";
        String clientId = DeviceUtils.getAndroidID(mContext);
        Timber.d("clientId = " + clientId);
        Timber.d("register: 正在注册推送," + ProcessUtils.getCurrentProcessName(mContext));
        try {
            IoTCommunicationClient.connect(appId, appSecret, clientId, new MessageReceiver(), new SdkEvent());
            Timber.d("register: 正在注册完成");
//            registered = true;
        } catch (Exception e) {
            e.printStackTrace();
            Timber.d("register: 注册推送失败");
//            registered = false;
        }
    }

    private class MessageReceiver implements IMessageRecevier {
        @Override
        public void recevier(Message message) {
            if (message == null || message.getMetaInfo() == null
                    || message.getMetaInfo().getContent() == null) {
                return;
            }
            PushMsg pushMsg;
            try {
                pushMsg = JsonUtils.parseObject(message.getMetaInfo().getContent(), PushMsg.class);
            } catch (Exception e) {
                e.printStackTrace();
                return;
            }

            mPushMsgHelper.insertOrUpdate(pushMsg)
                    .subscribeOn(Schedulers.io())
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribe(result ->
                            EventBus.getDefault().post(new PushMessageEvent(pushMsg)));

            NotificationUtils.notify(mContext,
                    pushMsg.getMsgTitle(), pushMsg.getMsgContent(),
                    R.mipmap.icon_msg_maps);
        }
    }

    private class SdkEvent extends IotSdkEventNotify {

        @Override
        public void recevier(String eType, IotSdkData sdkData) {
            Timber.d("eType = " + eType);
            Timber.d("sdkData.getCode() = " + sdkData.getCode());
            Timber.d("sdkData.getMessage() = " + sdkData.getMessage());
//            EventBus.getDefault().post(new MessageEvent(sdkData.getMessage()));
        }
    }
}
