package org.changs.control.push;

import android.text.TextUtils;
import android.util.Log;

import java.io.Serializable;
import java.util.List;

/**
 * Created by yincs on 2016/4/21.
 */
public class LocationInfo implements Serializable {

    private static final String TAG = "LocationInfo";

    private static final long serialVersionUID = -2436203568260126205L;

    private double longitude;//经度
    private double latitude; //纬度
    private String province;//省
    private String city;//市
    private String district;//区、县
//    private String street;//街道、乡镇
    private String address;//地    址
//    private String poi;// 兴趣点

    private String cityCode;//城市编码
//    private String adCode;//区域 码

    public LocationInfo() {
    }

    public double getLongitude() {
        return longitude;
    }

    public void setLongitude(double longitude) {
        this.longitude = longitude;
    }

    public double getLatitude() {
        return latitude;
    }

    public void setLatitude(double latitude) {
        this.latitude = latitude;
    }

    public String getProvince() {
        return province;
    }

    public void setProvince(String province) {
        this.province = province;
    }

    public String getCity() {
        return TextUtils.isEmpty(city) ? "定位失败,重新定位" : city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getDistrict() {
        return district;
    }

    public void setDistrict(String district) {
        this.district = district;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }
    //
//    public String getStreet() {
//        return street;
//    }
//
//    public void setStreet(String street) {
//        this.street = street;
//    }

//    public String getCityName() {
//        return address;
//    }
//
//    public void setCityName(String address) {
//        this.address = address;
//    }

//    public String getPoi() {
//        return poi;
//    }
//
//    public void setPoi(String poi) {
//        this.poi = poi;
//    }

    public void setCityCode(String cityCode) {
        this.cityCode = cityCode;
    }

    public String getCityCode() {
        return cityCode;
    }


    public void mathCityCode() {
        if (!TextUtils.isEmpty(this.province)) {
            final String provinceName = this.province.replace("省", "");
            final List<LocalCityModel.LocalCity> cityList = LocalCityModel.get().getCityList();
            for (int i = 0; i < cityList.size(); i++) {
                final LocalCityModel.LocalCity province = cityList.get(i);
                if (province.getAreaName().equals(provinceName)) {
                    final List<LocalCityModel.LocalCity> subAreaList = province.getSubAreaList();
                    if (!TextUtils.isEmpty(this.city) && subAreaList != null) {
                        for (int j = 0; j < subAreaList.size(); j++) {
                            final LocalCityModel.LocalCity city = subAreaList.get(j);
                            if (this.city.equals(city.getAreaName())) {
                                cityCode = city.getId();
                                return;
                            }
                        }
                    }
                    if (cityCode == null) {
                        cityCode = province.getId();
                        return;
                    }
                }
            }

            String msg = "onReceiveLocation mathCityCode fail : province = " + province + ",city = " + city;
            Log.e(TAG, msg);
//            AnalyzeUtils.reportError(msg);
        }
    }
}
