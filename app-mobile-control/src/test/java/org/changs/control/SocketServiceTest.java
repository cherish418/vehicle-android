package org.changs.control;

import org.changs.servlet.socket.CoolTransfer;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import java.io.File;
import java.net.ServerSocket;
import java.util.concurrent.CountDownLatch;

import timber.log.Timber;

/**
 * Created by yincs on 2017/8/9.
 */

public class SocketServiceTest {
    @Before
    public void setUp() throws Exception {
        System.out.println("SocketServiceTest.setUp");
        Timber.plant(new Timber.Tree() {
            @Override
            protected void log(int priority, String tag, String message, Throwable t) {
                System.out.println("tag:" + message);
            }
        });
    }

    @Test
    public void send() throws Exception {
        MyCoolTransferSend send = new MyCoolTransferSend();
        File file = new File("sokectTest/手机/地图文件.txt");
        send.send("127.0.0.1", 57807, file, new byte[256], "extra");
        new CountDownLatch(1).await();
    }


    @Test
    public void receive() throws Exception {
        MyCoolTransferReceive receive = new MyCoolTransferReceive();
        File file = new File("sokectTest/车机/地图文件.txt");
        receive.receive(0, file, 60, new byte[256], 1000, "extra");
        new CountDownLatch(1).await();
    }


    @After
    public void tearDown() throws Exception {
        System.out.println("SocketServiceTest.tearDown");
    }

    private class MyCoolTransferSend extends CoolTransfer.Send {
        @Override
        public Flag onError(TransferHandler handler, Exception error) {
            Timber.d("onError() called with: handler = [" + handler + "], error = [" + error + "]");
            return null;
        }

        @Override
        public void onNotify(TransferHandler handler, int percent) {
            Timber.d("onNotify() called with: handler = [" + handler + "], percent = [" + percent + "]");
        }

        @Override
        public void onTransferCompleted(TransferHandler handler) {
            Timber.d("onTransferCompleted() called with: handler = [" + handler + "]");
        }

        @Override
        public void onInterrupted(TransferHandler handler) {
            Timber.d("onInterrupted() called with: handler = [" + handler + "]");
        }

        @Override
        public Flag onSocketReady(TransferHandler handler) {
            Timber.d("onSocketReady() called with: handler = [" + handler + "]");
            return Flag.CONTINUE;
        }

        @Override
        public Flag onStart(TransferHandler handler) {
            Timber.d("onStart() called with: handler = [" + handler + "]");
            return Flag.CONTINUE;
        }

        @Override
        public void onStop(TransferHandler handler) {
            Timber.d("onStop() called with: handler = [" + handler + "]");
        }
    }

    private class MyCoolTransferReceive extends CoolTransfer.Receive {
        @Override
        public Flag onSocketReady(Handler handler, ServerSocket serverSocket) {
            Timber.d("onSocketReady() called with: handler = [" + handler + "], serverSocket = [" + serverSocket + "]");
            return Flag.CONTINUE;
        }

        @Override
        public Flag onError(TransferHandler handler, Exception error) {
            Timber.d("onError() called with: handler = [" + handler + "], error = [" + error + "]");
            return Flag.CANCEL_ALL;
        }

        @Override
        public void onNotify(TransferHandler handler, int percent) {
            Timber.d("onNotify() called with: handler = [" + handler + "], percent = [" + percent + "]");
        }

        @Override
        public void onTransferCompleted(TransferHandler handler) {
            Timber.d("onTransferCompleted() called with: handler = [" + handler + "]");
        }

        @Override
        public void onInterrupted(TransferHandler handler) {
            Timber.d("onInterrupted() called with: handler = [" + handler + "]");
        }

        @Override
        public Flag onSocketReady(TransferHandler handler) {
            Timber.d("onSocketReady() called with: handler = [" + handler + "]");
            return Flag.CONTINUE;
        }

        @Override
        public Flag onStart(TransferHandler handler) {
            Timber.d("onStart() called with: handler = [" + handler + "]");
            return Flag.CONTINUE;
        }
    }


}
