package org.changs.message;

import org.greenrobot.greendao.annotation.Entity;
import org.greenrobot.greendao.annotation.Generated;
import org.greenrobot.greendao.annotation.Id;

/**
 * Created by Administrator on 2017/9/12.
 */

@Entity(nameInDb = "PushMsg")
public class PushMsg {
    @Id
    private Long id;
    private String msgTitle;//消息标题
    private String msgContent;//消息内容
    private String msgLink;//消息链接
    private String msgType;//消息类型 1：系统升级
    private String msgTerminate;//消息终端类型 1:APP2:VEHICLE多个终端时,号分隔
    private String msgTarget;//1.设备型号：deviceModel/vm100，deviceModel/vm200，多个设备型号时，号分隔2.地区：area/5180003.全部终端时不填
    private String tags;//接收终端的标签

    @Generated(hash = 2100560944)
    public PushMsg(Long id, String msgTitle, String msgContent, String msgLink, String msgType,
            String msgTerminate, String msgTarget, String tags) {
        this.id = id;
        this.msgTitle = msgTitle;
        this.msgContent = msgContent;
        this.msgLink = msgLink;
        this.msgType = msgType;
        this.msgTerminate = msgTerminate;
        this.msgTarget = msgTarget;
        this.tags = tags;
    }

    @Generated(hash = 868472971)
    public PushMsg() {
    }

    public String getMsgTitle() {
        return msgTitle;
    }

    public void setMsgTitle(String msgTitle) {
        this.msgTitle = msgTitle;
    }

    public String getMsgContent() {
        return msgContent;
    }

    public void setMsgContent(String msgContent) {
        this.msgContent = msgContent;
    }

    public String getMsgLink() {
        return msgLink;
    }

    public void setMsgLink(String msgLink) {
        this.msgLink = msgLink;
    }

    public String getMsgType() {
        return msgType;
    }

    public void setMsgType(String msgType) {
        this.msgType = msgType;
    }

    public String getMsgTerminate() {
        return msgTerminate;
    }

    public void setMsgTerminate(String msgTerminate) {
        this.msgTerminate = msgTerminate;
    }

    public String getMsgTarget() {
        return msgTarget;
    }

    public void setMsgTarget(String msgTarget) {
        this.msgTarget = msgTarget;
    }

    public String getTags() {
        return tags;
    }

    public void setTags(String tags) {
        this.tags = tags;
    }

    public Long getId() {
        return this.id;
    }

    public void setId(Long id) {
        this.id = id;
    }
}
