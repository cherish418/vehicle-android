package org.changs.message.push;

import com.iot.push.sdk.message.Message;

/**
 * Created by Administrator on 2017/5/2.
 */

public class ReceiveMessageEvent {
    private Message message;

    public ReceiveMessageEvent(Message message) {
        this.message = message;
    }

    public Message getMessage() {
        return message;
    }

    public void setMessage(Message message) {
        this.message = message;
    }
}
