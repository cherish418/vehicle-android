package org.changs.message;

import android.app.Notification;
import android.app.NotificationManager;
import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.v4.app.NotificationCompat;
import android.support.v7.widget.LinearLayoutManager;

import org.changs.aplug.base.BaseActivity;
import org.changs.aplug.utils.JsonUtils;
import org.changs.message.databinding.ActivityMainBinding;
import org.changs.message.push.IotPushPresenter;
import org.changs.message.push.LocationInfo;
import org.changs.message.push.ReceiveMessageEvent;
import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;

import java.util.List;

import javax.inject.Inject;

public class MainActivity extends BaseActivity implements IotPushPresenter.V {
    private ActivityMainBinding binding;
    private MsgAdapter mAdapter;

    @Inject
    IotPushPresenter presenter;

    @Override
    public void setup(Bundle savedInstanceState) {
        binding = DataBindingUtil.setContentView(this, R.layout.activity_main);

        presenter.attachView(this);
        EventBus.getDefault().register(this);

        initPush();
        initData();
    }

    /**
     * 消息 推送
     */
    private void initPush() {
        LocationInfo locationInfo = new LocationInfo();
        locationInfo.setLongitude(113.94087);
        locationInfo.setLatitude(22.50898);
        locationInfo.setProvince("广东省");
        locationInfo.setCity("深圳市");
        locationInfo.setDistrict("南山区");
        locationInfo.setAddress("12345");
        locationInfo.setCityCode("101280601");
        presenter.initPush(locationInfo);
    }

    /**
     * 数据初始化
     */
    private void initData() {
        presenter.getMsgList();
        mAdapter = new MsgAdapter(getContext(), binding.rccView, presenter);
        binding.rccView.setLayoutManager(new LinearLayoutManager(getContext()));

        binding.rccView.setup()
                .adapter(mAdapter)
                .emptyText("当前没有消息哦~")
                .dynamic()
                .done();
    }

    public void showMesg(String msg, String title) {
        int id = (int) (Math.random() * 100 + 1);

        NotificationManager notificationManager = (NotificationManager) getSystemService(NOTIFICATION_SERVICE);
        NotificationCompat.Builder builder = new NotificationCompat.Builder(this);
        builder.setSmallIcon(R.mipmap.icon_msg_map);// 设置图标
        builder.setContentTitle(title);// 设置通知的标题
        builder.setContentText(msg);// 设置通知的内容
        builder.setWhen(System.currentTimeMillis());// 设置通知来到的时间
        builder.setAutoCancel(true); //自己维护通知的消失
        builder.setTicker(title);// 第一次提示消失的时候显示在通知栏上的
        builder.setOngoing(true);
        builder.setNumber(20);
        builder.setDefaults(Notification.FLAG_ONLY_ALERT_ONCE);

        Notification notification = builder.build();
        notification.flags = Notification.FLAG_AUTO_CANCEL;  //只有全部清除时，Notification才会清除
        notificationManager.notify(id, notification);
        presenter.getMsgList();
    }

    @Subscribe
    public void onEvent(ReceiveMessageEvent messageEvent) {
        String content = messageEvent.getMessage().getMetaInfo().getContent();
        if (content.contains("msgContent") && content.contains("msgType")) {
            PushMsg pushMsg = JsonUtils.parseObject(content, PushMsg.class);
            if (pushMsg.getMsgTerminate().contains("2")) {//1:APP 2:VEHICLE
                presenter.savaPush(pushMsg);
                showMesg(pushMsg.getMsgContent(), pushMsg.getMsgTitle());
            }
        }
    }

    @Override
    public void finish() {
        super.finish();
        presenter.detachView();
        EventBus.getDefault().unregister(this);
    }

    @Override
    public void getMsgList(List<PushMsg> pushMsgs) {
        mAdapter.getData().clear();
        mAdapter.getData().addAll(pushMsgs);
        mAdapter.notifyDataSetChanged();
    }
}
