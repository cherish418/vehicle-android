package aplug.di;

import javax.inject.Singleton;

import aplug.data.DataManager;
import aplug.data.DataManagerImpl;
import aplug.data.prefs.PreferencesHelper;
import aplug.data.prefs.PreferencesHelperImpl;
import dagger.Binds;
import dagger.Module;

/**
 * Created by yincs on 2017/8/30.
 */

@Module
public abstract class AppDataModules {

    @Singleton
    @Binds
    abstract PreferencesHelper providePreferencesHelper(PreferencesHelperImpl impl);

    //
//    @Singleton
//    @Binds
//    abstract SearchRecordHelper provideSearchRecordHelper(SearchRecordHelper.Impl impl);
//
    @Singleton
    @Binds
    abstract DataManager provideDataManager(DataManagerImpl impl);
}
