package aplug.di;

import android.content.Context;

import org.changs.s3.common.RemoteManager;

import javax.inject.Singleton;

import aplug.data.DataManager;
import aplug.data.DataManagerImpl;
import aplug.data.network.ApiHelper;
import aplug.data.network.AppApiHelper;
import aplug.data.prefs.PreferencesHelper;
import aplug.data.prefs.PreferencesHelperImpl;
import dagger.Binds;
import dagger.Module;
import dagger.Provides;

/**
 * Created by yincs on 2017/8/27.
 */

//存放全局对象
@Singleton
@Module
public abstract class AppModules {

    @Singleton
    @Binds
    abstract PreferencesHelper providePreferencesHelper(PreferencesHelperImpl impl);

    @Singleton
    @Binds
    abstract ApiHelper provideApiHelper(AppApiHelper impl);

    @Singleton
    @Binds
    abstract DataManager provideDataManager(DataManagerImpl impl);

    @Singleton
    @Provides
    static RemoteManager provideRemoteIPManager(Context context) {
        return new RemoteManager(context);
    }
}
