package aplug;

import android.content.Context;
import android.support.multidex.MultiDex;

import org.changs.aplug.AplugManager;

import aplug.di.DaggerAppComponent;
import dagger.android.AndroidInjector;
import dagger.android.support.DaggerApplication;
import timber.log.Timber;


/**
 * Created by yincs on 2017/7/25.
 */

public class MyApp extends DaggerApplication {

    @Override
    protected void attachBaseContext(Context base) {
        MultiDex.install(this);
        super.attachBaseContext(base);
    }

    @Override
    protected AndroidInjector<? extends DaggerApplication> applicationInjector() {
        AplugManager.get().onCreate(new AplugManager
                .Config(this)
                .debug());

        Timber.d("application onCreate");

        return DaggerAppComponent.builder().application(this).build();
    }

    @Override
    public void onTerminate() {
        super.onTerminate();
        AplugManager.get().onTerminate();
    }

}