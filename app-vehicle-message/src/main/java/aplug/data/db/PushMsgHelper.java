package aplug.data.db;

import org.changs.aplug.interf.IRxCurdDataHelper;
import org.changs.message.PushMsg;
import org.changs.message.PushMsgDao;

import java.util.List;

import javax.inject.Inject;

import io.reactivex.Observable;

/**
 * Created by Administrator on 2017/9/14.
 */

public class PushMsgHelper implements IRxCurdDataHelper<PushMsg> {
    private final PushMsgDao mDao;

    @Inject
    public PushMsgHelper(DbOpenHelper helper) {
        mDao = helper.getDaoSession().getPushMsgDao();
    }

    @Override
    public Observable<Boolean> insertOrUpdate(PushMsg data) {
        return Observable.fromCallable(() -> {
            try {
                mDao.insertOrReplace(data);
                return true;
            } catch (Exception e) {
                e.printStackTrace();
                return false;
            }
        });
    }

    @Override
    public Observable<Boolean> delete(PushMsg data) {
        return Observable.fromCallable(() -> {
            try {
                mDao.delete(data);
                return true;
            } catch (Exception e) {
                e.printStackTrace();
                return false;
            }
        });
    }

    @Override
    public Observable<Boolean> clear() {
        return Observable.fromCallable(() -> {
            try {
                mDao.deleteAll();
                return true;
            } catch (Exception e) {
                e.printStackTrace();
                return false;
            }
        });
    }

    @Override
    public Observable<List<PushMsg>> loadAll() {
        return Observable.fromCallable(() -> mDao.loadAll());
    }
}
