package aplug.data.db;

import android.content.Context;

import org.greenrobot.greendao.database.Database;

import javax.inject.Inject;
import javax.inject.Singleton;

import aplug.AppConfig;
import aplug.data.db.model.DaoMaster;
import aplug.data.db.model.DaoSession;
import timber.log.Timber;

/**
 * Created by yincs on 2017/7/22.
 */
@Singleton
public class DbOpenHelper extends DaoMaster.OpenHelper {


    @Inject
    public DbOpenHelper(Context context) {
        super(context, AppConfig.DATABASE_FILE_NAME);
    }

    @Override
    public void onUpgrade(Database db, int oldVersion, int newVersion) {
        super.onUpgrade(db, oldVersion, newVersion);
        Timber.d("DEBUG", "DB_OLD_VERSION : " + oldVersion + ", DB_NEW_VERSION : " + newVersion);
        switch (oldVersion) {
            case 1:
            case 2:
                //db.execSQL("ALTER TABLE " + UserDao.TABLENAME + " ADD COLUMN "
                // + UserDao.Properties.Name.columnName + " TEXT DEFAULT 'DEFAULT_VAL'");
        }
    }

    private DaoSession daoSession;

    public DaoSession getDaoSession() {
        if (daoSession == null) {
            daoSession = new DaoMaster(getWritableDb()).newSession();
        }
        return daoSession;
    }
}
