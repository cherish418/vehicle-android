package aplug.data;

import aplug.data.db.PushMsgHelper;
import aplug.data.network.ApiHelper;
import aplug.data.prefs.PreferencesHelper;

/**
 * Created by yincs on 2017/7/24.
 */

public interface DataManager {
    PushMsgHelper getPushMsgHelper();

    ApiHelper getApiHelper();

    PreferencesHelper getPreferencesHelper();

    enum LoggedInMode {

        LOGGED_IN_MODE_LOGGED_OUT(0),
        LOGGED_IN_MODE_GOOGLE(1),
        LOGGED_IN_MODE_FB(2),
        LOGGED_IN_MODE_SERVER(3);

        private final int mType;

        LoggedInMode(int type) {
            mType = type;
        }

        public int getType() {
            return mType;
        }
    }
}
