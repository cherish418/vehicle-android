package aplug.data;

import android.content.Context;

import javax.inject.Inject;
import javax.inject.Singleton;

import aplug.data.db.PushMsgHelper;
import aplug.data.network.ApiHelper;
import aplug.data.prefs.PreferencesHelper;

/**
 * Created by yincs on 2017/7/24.
 */

@Singleton
public class DataManagerImpl implements DataManager {


    private final Context context;
    private final PushMsgHelper pushMsgHelper;
    private final PreferencesHelper preferencesHelper;
    private final ApiHelper apiHelper;

    @Inject
    public DataManagerImpl(Context context,
                           PushMsgHelper pushMsgHelper,
                           PreferencesHelper preferencesHelper,
                           ApiHelper apiHelper) {
        this.context = context;
        this.pushMsgHelper = pushMsgHelper;
        this.preferencesHelper = preferencesHelper;
        this.apiHelper = apiHelper;
    }

    @Override
    public PushMsgHelper getPushMsgHelper() {
        return pushMsgHelper;
    }

    @Override
    public ApiHelper getApiHelper() {
        return apiHelper;
    }

    @Override
    public PreferencesHelper getPreferencesHelper() {
        return preferencesHelper;
    }
}
