package aplug;

import android.os.Environment;

import java.io.File;

/**
 * Created by yincs on 2017/8/24.
 */

public final class AppConfig {

    //aplug
    public static final String PREFERENCE_FILE_NAME = "preference";
    public static final String DATABASE_FILE_NAME = "data";


    public static File getRootFile() {
        return new File(Environment.getExternalStorageDirectory(), "app-s3-record");
    }

    public static File getPictureRootFile() {
        return new File(getRootFile(), "picture");
    }

    public static File getVideoRootFile() {
        return new File(getRootFile(), "video");
    }
}
